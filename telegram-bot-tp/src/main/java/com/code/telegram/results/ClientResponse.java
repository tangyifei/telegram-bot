package com.code.telegram.results;

import java.io.Serializable;

/**
 * 调用数字管理平台返回的响应数据格式
 *
 * @author xiaoyaowang
 */
public class ClientResponse implements Serializable {

    private static final long serialVersionUID = 2757586360411854807L;

    private int rtnCode;

    private String errCode;

    private String errMsg;

    private Object data = new Object();

    private Long total;

    public int getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(int rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
