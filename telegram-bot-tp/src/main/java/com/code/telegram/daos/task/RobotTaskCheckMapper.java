package com.code.telegram.daos.task;

import com.code.models.robot.RobotTaskCheck;
import com.code.telegram.daos.CrudMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 任务审核持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface RobotTaskCheckMapper extends CrudMapper<RobotTaskCheck> {

    /**
     * 判断用户的任务审核记录是否存在
     *
     * @param robotTaskCheck 机器人任务审核记录实体类
     * @return 审核记录
     */
    RobotTaskCheck getRobotTaskCheckByCondition(RobotTaskCheck robotTaskCheck);

    /**
     * 查询用户封禁审核记录
     *
     * @param chatId 聊天id
     * @return 用户封禁审核记录
     */
    Integer getBanTaskCheckByChatId(@Param("chatId") String chatId);

    /**
     * 获取审核成功的聊天用户数
     *
     * @param list 聊天唯一标识列表
     * @return 审核成功的聊天用户数
     */
    int getCheckSuccessChatNumsByChatIdList(List<String> list);

    /**
     * 通过chatId删除用户的审核记录
     *
     * @param chatId 聊天id
     */
    void deleteRobotTaskCheckByChatId(@Param("chatId") String chatId);

    /**
     * 获取已经领取的收益
     *
     * @param chatId 聊天唯一标识
     * @return 已经领取的收益
     */
    Integer getReceivedTpAmountByChatId(@Param("chatId") String chatId);

    /**
     * 根据聊天唯一标识获取审核主键
     *
     * @param chatId 聊天唯一标识
     * @return 审核主键
     */
    Integer getRobotTaskCheckIdByChatId(@Param("chatId") String chatId);

    /**
     * 根据聊天唯一标识获取代理人状态
     *
     * @param chatId 聊天唯一标识
     * @return 代理人状态
     */
    Integer getAgentStateByChatId(@Param("chatId") String chatId);

}
