package com.code.telegram.daos.task;

import com.code.telegram.daos.CrudMapper;
import com.code.telegram.models.VerifiedAddr;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 已校验地址持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface VerifiedAddrMapper extends CrudMapper<VerifiedAddr> {

    /**
     * 根据地址获取已校验成功记录
     *
     * @param coinAddr 钱包地址
     * @return 已校验成功记录
     */
    Integer getVerifySuccessCoinAddrByCoinAddr(@Param("coinAddr") String coinAddr);

    /**
     * 判断该地址是否已经用作空投
     *
     * @param coinAddr 钱包地址
     * @return 用作空投的地址数
     */
    Integer whetherAirdropped(@Param("coinAddr") String coinAddr);

    /**
     * 通过chatId获取用户已存在的地址数
     *
     * @param chatId 聊天id
     * @return 用户已存在的地址数
     */
    Integer getExistCoinAddrCountByChatId(@Param("chatId") String chatId);

    /**
     * 通过chatId获取校验的钱包地址
     *
     * @param chatId 聊天id
     * @return 钱包地址
     */
    String getCoinAddrByChatId(@Param("chatId") String chatId);

    /**
     * 通过校验的钱包地址获取chatId
     *
     * @param coinAddr 校验的钱包地址
     * @return chatId
     */
    String getChatIdByCoinAddr(@Param("coinAddr") String coinAddr);

}
