package com.code.telegram.daos.task;

import com.code.models.robot.CheckFailure;
import com.code.telegram.daos.CrudMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 审核失败持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface CheckFailureMapper extends CrudMapper<CheckFailure> {

    /**
     * 是否存在审核失败的记录
     *
     * @param chatId 聊天唯一标识
     * @return 是否存在审核失败的记录
     */
    Integer whetherCheckFailure(@Param("chatId") String chatId);

}
