package com.code.telegram.daos.task;

import com.code.models.robot.ChatMessageNums;
import com.code.telegram.daos.CrudMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * 用户发送消息的统计持久层
 *
 * @author xiaoyaowang
 */
public interface ChatMessageNumsMapper extends CrudMapper<ChatMessageNums> {

    /**
     * 通过chatId获取用户在群里发送的消息数
     *
     * @param chatId 聊天唯一标识
     * @return 封装用户在群里发送的消息数的对象
     */
    ChatMessageNums getChatMessageNumsByChatId(@Param("chatId") String chatId, @Param("currentDay") String currentDay);

    /**
     * 增加用户在群里发送的消息数
     *
     * @param id       主键
     * @param version  版本号
     * @param updateAt 更新时间
     * @return 影响的行数
     */
    int incrementChatMessageNums(@Param("id") Integer id, @Param("version") Integer version,
                                 @Param("updateAt") Date updateAt);

}
