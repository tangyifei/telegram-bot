package com.code.telegram.daos.task;

import com.code.models.robot.AgentReceiveIncome;
import com.code.telegram.daos.CrudMapper;

/**
 * 收益领取记录持久层
 *
 * @author xiaoyaowang
 */
public interface AgentReceiveIncomeMapper extends CrudMapper<AgentReceiveIncome> {

}
