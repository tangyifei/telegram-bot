package com.code.telegram.daos.group;

import com.code.telegram.daos.CrudMapper;
import com.code.telegram.models.ChatMemberGroup;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 新人加入的群组持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface ChatMemberGroupMapper extends CrudMapper<ChatMemberGroup> {

    /**
     * 获取用户加入的群组记录
     *
     * @param chatMemberGroup 新人加入的群组记录实体类
     * @return 用户加入的群组记录
     */
    ChatMemberGroup getChatMemberGroup(ChatMemberGroup chatMemberGroup);

    /**
     * 通过chatId获取telegram用户名
     *
     * @param chatId 聊天id
     * @return telegram用户名
     */
    String getUserNameByChatId(@Param("chatId") String chatId);

    /**
     * 通过chatId获取用户的telegram名称
     *
     * @param chatId 聊天id
     * @return 新人加入的群组记录实体类
     */
    ChatMemberGroup getChatMemberGroupByChatId(@Param("chatId") String chatId);

    /**
     * 删除加群记录
     *
     * @param chatId   聊天唯一标识
     * @param groupUrl 群组url
     */
    void deleteChatMemberGroup(@Param("chatId") String chatId, @Param("groupUrl") String groupUrl);

}
