package com.code.telegram.daos.task;

import com.code.models.robot.RobotRewardFlow;
import com.code.telegram.daos.CrudMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 机器人奖励流水持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface RobotRewardFlowMapper extends CrudMapper<RobotRewardFlow> {

    /**
     * 判断某一个钱包地址是否给予了奖励
     *
     * @param walletAddress 钱包地址
     * @param amount        奖励金额
     * @return 记录数
     */
    Integer existRobotRewardFlowByWalletAddressAndAmount(@Param("walletAddress") String walletAddress, @Param("amount") String amount);

    /**
     * 查询是否存在BZ奖励
     *
     * @param walletAddress  钱包地址
     * @param amount         奖金金额
     * @param serialNoPrefix 序列号前缀
     * @return 是否存在BZ奖励
     */
    Integer existBzRewardFlowByWalletAddressAndAmount(@Param("walletAddress") String walletAddress,
                                                      @Param("amount") String amount,
                                                      @Param("serialNoPrefix") String serialNoPrefix);

}
