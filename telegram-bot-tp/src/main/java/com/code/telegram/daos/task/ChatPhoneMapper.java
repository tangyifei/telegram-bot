package com.code.telegram.daos.task;

import com.code.telegram.daos.CrudMapper;
import com.code.telegram.models.ChatPhone;
import org.springframework.stereotype.Repository;

/**
 * 聊天手机号持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface ChatPhoneMapper extends CrudMapper<ChatPhone> {

}
