package com.code.telegram.daos.task;

import com.code.models.robot.FocusOnTwitterFailure;
import com.code.telegram.daos.CrudMapper;
import org.springframework.stereotype.Repository;

/**
 * 用户关注推特失败记录持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface FocusOnTwitterFailureMapper extends CrudMapper<FocusOnTwitterFailure> {

}
