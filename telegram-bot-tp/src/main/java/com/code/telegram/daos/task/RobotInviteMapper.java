package com.code.telegram.daos.task;

import com.code.models.robot.RobotInvite;
import com.code.telegram.daos.CrudMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 邀请持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface RobotInviteMapper extends CrudMapper<RobotInvite> {

    /**
     * 判断邀请记录是否存在
     *
     * @param chatId       被邀请人的chatId
     * @param inviteChatId 邀请人的chatId
     * @return 邀请记录存在数
     */
    Integer getRobotInviteByChatIdAndInviteChatId(@Param("chatId") String chatId, @Param("inviteChatId") String inviteChatId);

    /**
     * 根据邀请人的chatId查询邀请的人数
     *
     * @param chatId             邀请人的聊天id
     * @param inviterRewardReset 邀请人奖励重置 0-未重置 1-已重置
     * @return 邀请的人数
     */
    int getInvitePersonsByChatId(@Param("chatId") String chatId, @Param("inviterRewardReset") Integer inviterRewardReset);

    /**
     * 根据邀请人的聊天的id获取被邀请的聊天id列表
     *
     * @param chatId 聊天id
     * @return 被邀请的聊天id列表
     */
    List<String> getInvitedChatIdListByChatId(@Param("chatId") String chatId);

    /**
     * 通过聊天唯一标识获取用户邀请主键列表
     *
     * @param chatId 聊天唯一标识
     * @return 用户邀请记录主键列表
     */
    List<Integer> getRobotInviteIdListByInviteChatId(@Param("chatId") String chatId);

    /**
     * 批量重置邀请人奖励
     *
     * @param inviteIdList 邀请人主键列表
     * @return 影响的行数
     */
    int patchCancelRobotInviteRewardReset(List<Integer> inviteIdList);

}
