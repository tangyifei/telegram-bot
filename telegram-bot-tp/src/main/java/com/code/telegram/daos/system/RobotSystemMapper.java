package com.code.telegram.daos.system;

import com.code.models.robot.RobotSystem;
import com.code.telegram.daos.CrudMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 机器人系统设置持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface RobotSystemMapper extends CrudMapper<RobotSystem> {

    /**
     * 通过系统key获取系统值
     *
     * @param systemKey 系统key
     * @return 获取的系统值
     */
    String getSystemValueBySystemKey(@Param("systemKey") String systemKey);

}
