package com.code.telegram.daos.task;

import com.code.models.robot.RobotChatFinishTask;
import com.code.telegram.daos.CrudMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 用户已完成的任务持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface RobotChatFinishTaskMapper extends CrudMapper<RobotChatFinishTask> {

    /**
     * 根据聊天id获取完成的任务数
     *
     * @param chatId 聊天id
     * @return 完成的任务数
     */
    int getFinishTasksByChatId(@Param("chatId") String chatId);

}
