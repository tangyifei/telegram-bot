package com.code.telegram.enums;

/**
 * 奖励key的枚举
 *
 * @author xiaoyaowang
 */

public enum SystemKeyEnum {

    /**
     * 使用手机验证码还是图形验证码，为1是手机验证码，其他默认发送图形验证码
     */
    VERIFY_CODE_TYPE,

    /**
     * 提醒用户发送手机号的信息
     */
    SEND_PHONE_MESSAGE,

}
