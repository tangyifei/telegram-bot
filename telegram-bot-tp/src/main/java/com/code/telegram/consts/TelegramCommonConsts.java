package com.code.telegram.consts;

/**
 * 共享常量
 *
 * @author xiaoyaowang
 */
public interface TelegramCommonConsts {

    /**
     * 一个群最多分享多少个连接
     */
    int SHARE_GROUP_URL_MAX_TIMES = 3;

    /**
     * BZ群链接
     */
    String BZ_PLAN_GROUP = "https://t.me/Bzplan_XWC";

    /**
     * 重新加入BZ群的提示信息
     */
    String RE_JOIN_BZ_GROUP_MESSAGE = "Please Join the BzPlan Group first. Then Click It's done.";
}
