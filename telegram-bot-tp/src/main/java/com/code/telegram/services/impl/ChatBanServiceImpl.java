package com.code.telegram.services.impl;

import com.code.models.robot.ChatBan;
import com.code.telegram.daos.task.ChatBanMapper;
import com.code.telegram.services.ChatBanService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 封禁用户实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatBanServiceImpl implements ChatBanService {

    @Resource
    ChatBanMapper chatBanMapper;

    @Override
    public int insertChatBan(ChatBan chatBan) {
        chatBan.setDeleted(0);
        chatBan.setCreatedAt(new Date());
        chatBan.setUpdatedAt(new Date());
        return chatBanMapper.insert(chatBan);
    }

    @Override
    public boolean whetherExistChatBanByChatId(String chatId) {
        return null != chatBanMapper.existsChatBanByChatId(chatId);
    }

}
