package com.code.telegram.services.impl;

import com.code.consts.CommonConsts;
import com.code.models.robot.ChatTwitter;
import com.code.models.robot.FocusOnTwitterFailure;
import com.code.telegram.daos.task.ChatTwitterMapper;
import com.code.telegram.daos.task.FocusOnTwitterFailureMapper;
import com.code.telegram.services.ChatTwitterService;
import com.code.telegram.services.RobotTaskService;
import com.code.telegram.services.VerifiedAddrService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 推特账户服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatTwitterServiceImpl implements ChatTwitterService {

    @Resource
    ChatTwitterMapper chatTwitterMapper;

    @Resource
    FocusOnTwitterFailureMapper focusOnTwitterFailureMapper;

    @Resource
    VerifiedAddrService verifiedAddrService;

    @Resource
    RobotTaskService robotTaskService;

    @Override
    public int insertChatTwitter(String chatId, String twitterAccountName) {
        Integer existsChatTwitter = chatTwitterMapper.existsChatTwitterByChatIdAndTwitterAccountName(chatId, null);
        ChatTwitter chatTwitter = new ChatTwitter();
        chatTwitter.setChatId(chatId);
        chatTwitter.setTwitterAccountName(twitterAccountName);
        chatTwitter.setUpdatedAt(new Date());
        if (null == existsChatTwitter) {
            chatTwitter.setDeleted(0);
            chatTwitter.setCreatedAt(new Date());
            return chatTwitterMapper.insert(chatTwitter);
        } else {
            chatTwitter.setId(chatTwitterMapper.getChatTwitterIdByChatIdAndTwitterAccountName(chatId, null));
            return chatTwitterMapper.updateByPrimaryKeySelective(chatTwitter);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public void insertFocusOnTwitterFailure(FocusOnTwitterFailure focusOnTwitterFailure) {
        FocusOnTwitterFailure focusOnTwitterFailureQuery = new FocusOnTwitterFailure();
        String chatId = focusOnTwitterFailure.getChatId();
        focusOnTwitterFailureQuery.setChatId(chatId);
        focusOnTwitterFailureQuery.setDeleted(0);
        FocusOnTwitterFailure whetherExistFocusOnTwitterFailure = focusOnTwitterFailureMapper.selectOne(focusOnTwitterFailureQuery);
        String walletAddr = verifiedAddrService.getCoinAddrByChatId(chatId);
        if (null == whetherExistFocusOnTwitterFailure) {
            focusOnTwitterFailure.setDeleted(0);
            focusOnTwitterFailure.setCreatedAt(new Date());
            focusOnTwitterFailure.setUpdatedAt(new Date());
            focusOnTwitterFailure.setWalletAddr(walletAddr);
            focusOnTwitterFailureMapper.insert(focusOnTwitterFailure);
        } else {
            whetherExistFocusOnTwitterFailure.setUpdatedAt(new Date());
            whetherExistFocusOnTwitterFailure.setTwitterAccountName(focusOnTwitterFailure.getTwitterAccountName());
            focusOnTwitterFailureMapper.updateByPrimaryKey(whetherExistFocusOnTwitterFailure);
        }
        int finishTaskNums = robotTaskService.getTotalFinishTasks(chatId);
        if (finishTaskNums >= 3) {
            // 第一步：插入关注推特的完成任务和推特关注记录
//            RobotChatFinishTask robotChatFinishTask = new RobotChatFinishTask();
//            robotChatFinishTask.setChatId(chatId);
//            robotTaskService.insertRobotChatFinishTask(robotChatFinishTask);
            String twitterAccountName = focusOnTwitterFailure.getTwitterAccountName();
            if (!StringUtils.isBlank(twitterAccountName)) {
                Integer existsChatTwitter = chatTwitterMapper.existsChatTwitterByChatIdAndTwitterAccountName(chatId, null);
                ChatTwitter chatTwitter = new ChatTwitter();
                chatTwitter.setChatId(chatId);
                chatTwitter.setTwitterAccountName(twitterAccountName);
                chatTwitter.setUpdatedAt(new Date());
                if (null == existsChatTwitter) {
                    chatTwitter.setDeleted(0);
                    chatTwitter.setCreatedAt(new Date());
                    chatTwitterMapper.insert(chatTwitter);
                } else {
                    chatTwitter.setId(chatTwitterMapper.getChatTwitterIdByChatIdAndTwitterAccountName(chatId, null));
                    chatTwitterMapper.updateByPrimaryKeySelective(chatTwitter);
                }

//                RobotTaskCheck robotTaskCheck = new RobotTaskCheck();
//                robotTaskCheck.setChatId(chatId);
//                ChatMemberGroup chatMemberGroup = chatMemberGroupService.getChatMemberGroupByChatId(chatId);
//                if (null != chatMemberGroup) {
//                    robotTaskCheck.setUserName(chatMemberGroup.getUserName());
//                    robotTaskCheck.setFirstName(chatMemberGroup.getFirstName());
//                    robotTaskCheck.setLastName(chatMemberGroup.getLastName());
//                }
//                robotTaskCheck.setWalletAddr(verifiedAddrService.getCoinAddrByChatId(chatId));
//                robotTaskCheck.setState(0);
//                robotTaskCheckService.insertRobotTaskCheck(robotTaskCheck);
            }
        }

    }

    @Override
    public boolean whetherExistChatTwitterByChatId(String chatId) {
        return null != chatTwitterMapper.getChatTwitterByChatId(chatId);
    }

    @Override
    public boolean whetherExistChatTwitterAccountNameByTwitterAccountName(String twitterAccountName) {
        return null != chatTwitterMapper.getChatTwitterAccountNameCountByTwitterAccountName(twitterAccountName);
    }

    @Override
    public String getTwitterNameByChatId(String chatId) {
        return chatTwitterMapper.getTwitterAccountNameByChatId(chatId);
    }
}
