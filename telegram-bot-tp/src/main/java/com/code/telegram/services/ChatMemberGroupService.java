package com.code.telegram.services;

import com.code.telegram.models.ChatMemberGroup;

import java.util.List;

/**
 * 新人加入的群组任务业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatMemberGroupService {

    /**
     * 判断用户是否加入群组
     *
     * @param chatMemberGroup 任务序号
     * @return 用户是否加入群组
     */
    Boolean judgeUserWhetherJoinGroup(ChatMemberGroup chatMemberGroup);

    /**
     * 批量添加新人加入的群组记录
     *
     * @param chatMemberGroupList 添加新人加入的群组列表
     */
    int insertChatMemberGroupList(List<ChatMemberGroup> chatMemberGroupList);

    /**
     * 根据聊天id获取用户加入的群组信息
     *
     * @param chatId 聊天id
     * @return 用户加入的群组信息
     */
    ChatMemberGroup getChatMemberGroupByChatId(String chatId);

    /**
     * 通过chatId获取用户的telegram名称
     *
     * @param chatId 聊天id
     * @return 用户的telegram名称
     */
    String getUserNameByChatId(String chatId);

    /**
     * 删除加群记录
     *
     * @param chatId   聊天唯一标识
     * @param groupUrl 群组url
     */
    void deleteChatMemberGroup(String chatId, String groupUrl);

}
