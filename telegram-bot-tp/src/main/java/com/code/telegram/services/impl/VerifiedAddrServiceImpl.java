package com.code.telegram.services.impl;

import com.code.telegram.daos.task.ChatPhoneMapper;
import com.code.telegram.daos.task.VerifiedAddrMapper;
import com.code.telegram.models.ChatPhone;
import com.code.telegram.models.VerifiedAddr;
import com.code.telegram.services.VerifiedAddrService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 校验成功的地址任务服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class VerifiedAddrServiceImpl implements VerifiedAddrService {

    @Resource
    VerifiedAddrMapper verifiedAddrMapper;

    @Resource
    ChatPhoneMapper chatPhoneMapper;

    @Override
    public boolean judgeCoinAddrVerifySuccess(String coinAddr) {
        return null != verifiedAddrMapper.getVerifySuccessCoinAddrByCoinAddr(coinAddr);
    }

    @Override
    public String getCoinAddrByChatId(String chatId) {
        return verifiedAddrMapper.getCoinAddrByChatId(chatId);
    }

    @Override
    public String getChatIdByCoinAddr(String coinAddr) {
        return verifiedAddrMapper.getChatIdByCoinAddr(coinAddr);
    }

    @Override
    public int insertVerifiedAddr(String chatId, String coinAddr) {
        Integer existCount = verifiedAddrMapper.getVerifySuccessCoinAddrByCoinAddr(coinAddr);
        if (null == existCount) {
            Integer chatExistCoinAddrCount = verifiedAddrMapper.getExistCoinAddrCountByChatId(chatId);
            if (null == chatExistCoinAddrCount) {
                VerifiedAddr verifiedAddr = new VerifiedAddr();
                verifiedAddr.setChatId(chatId);
                verifiedAddr.setCoinAddr(coinAddr);
                verifiedAddr.setDeleted(0);
                verifiedAddr.setCreatedAt(new Date());
                verifiedAddr.setUpdatedAt(new Date());
                return verifiedAddrMapper.insert(verifiedAddr);
            }
        }
        return 0;
    }

    @Override
    public int insertChatPhone(String chatId, String phone) {
        ChatPhone chatPhone = new ChatPhone();
        chatPhone.setChatId(chatId);
        chatPhone.setPhone(phone);
        chatPhone.setDeleted(0);
        chatPhone.setCreatedAt(new Date());
        chatPhone.setUpdatedAt(new Date());
        return chatPhoneMapper.insert(chatPhone);
    }

}
