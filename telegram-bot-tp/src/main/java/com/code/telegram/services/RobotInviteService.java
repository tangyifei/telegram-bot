package com.code.telegram.services;

import java.util.List;

/**
 * 邀请业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotInviteService {

    /**
     * 插入邀请记录
     *
     * @param chatId       被邀请人的chatId
     * @param inviteChatId 邀请人的chatId
     * @return 影响的行数
     */
    int insertRobotInvite(String inviteChatId, String chatId);

    /**
     * 根据邀请人的chatId查询邀请的人数
     *
     * @param chatId             邀请人的聊天id
     * @param inviterRewardReset 邀请人奖励重置 0-未重置 1-已重置
     * @return 邀请的人数
     */
    int getInvitePersonsByChatId(String chatId, Integer inviterRewardReset);

    /**
     * 根据邀请人的chatId查询奖励
     *
     * @param chatId 邀请人的聊天id
     * @return 邀请别人获得的奖励
     */
    int getInvitePersonRewardsByChatId(String chatId);

    /**
     * 通过邀请人的聊天唯一标识获取用户邀请主键列表
     *
     * @param chatId 聊天唯一标识
     * @return 用户邀请主键列表
     */
    List<Integer> getRobotInviteIdListByInviteChatId(String chatId);

    /**
     * 批量重置邀请人奖励
     *
     * @param inviteIdList 邀请主键列表
     * @return 重置邀请人奖励
     */
    int patchCancelRobotInviteRewardReset(List<Integer> inviteIdList);

}
