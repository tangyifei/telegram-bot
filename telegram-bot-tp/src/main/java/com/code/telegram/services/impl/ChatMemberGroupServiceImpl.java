package com.code.telegram.services.impl;

import com.code.telegram.daos.group.ChatMemberGroupMapper;
import com.code.telegram.models.ChatMemberGroup;
import com.code.telegram.services.ChatMemberGroupService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 新人加入的群组服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatMemberGroupServiceImpl implements ChatMemberGroupService {

    @Resource
    ChatMemberGroupMapper chatMemberGroupMapper;

    @Override
    public Boolean judgeUserWhetherJoinGroup(ChatMemberGroup chatMemberGroup) {
        return null != chatMemberGroupMapper.getChatMemberGroup(chatMemberGroup);
    }

    @Override
    public int insertChatMemberGroupList(List<ChatMemberGroup> chatMemberGroupList) {
        return chatMemberGroupMapper.insertList(chatMemberGroupList);
    }

    @Override
    public ChatMemberGroup getChatMemberGroupByChatId(String chatId) {
        return chatMemberGroupMapper.getChatMemberGroupByChatId(chatId);
    }

    @Override
    public String getUserNameByChatId(String chatId) {
        return chatMemberGroupMapper.getUserNameByChatId(chatId);
    }

    @Override
    public void deleteChatMemberGroup(String chatId, String groupUrl) {
        ChatMemberGroup chatMemberGroup = new ChatMemberGroup();
        chatMemberGroup.setGroupUrl(groupUrl);
        chatMemberGroup.setChatId(chatId);
        if (null != chatMemberGroupMapper.getChatMemberGroup(chatMemberGroup)) {
            chatMemberGroupMapper.deleteChatMemberGroup(chatId, groupUrl);
        }
    }
}
