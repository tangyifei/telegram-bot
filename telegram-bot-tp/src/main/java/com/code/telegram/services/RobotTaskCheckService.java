package com.code.telegram.services;

import com.code.models.robot.RobotTaskCheck;

/**
 * 机器人任务审核业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotTaskCheckService {

    /**
     * 添加用户已完成的任务审核记录
     *
     * @param robotTaskCheck 用户已完成的任务审核列表
     * @return 影响的行数
     */
    int insertRobotTaskCheck(RobotTaskCheck robotTaskCheck);

    /**
     * 是否存在审核失败的记录
     *
     * @param chatId 聊天唯一标识
     * @return 是否存在审核失败的记录
     */
    boolean whetherCheckFailure(String chatId);

    /**
     * 用户是否完成发送截图的任务
     *
     * @param chatId 聊天id
     * @return 是否完成发送截图的任务
     */
    boolean whetherFinishSendCheckPhoto(String chatId);

    /**
     * 用户是否被封禁
     *
     * @param chatId 聊天id
     * @return 用户是否被封禁
     */
    boolean whetherBanedByChatId(String chatId);

    /**
     * 当前可用领取的TP数目
     *
     * @param chatId 聊天唯一标识
     * @return 当前可用领取的TP数目
     */
    int getUsableReceiveReward(String chatId);

    /**
     * 领取收益
     *
     * @param chatId 聊天唯一标识
     */
    void receiveIncome(String chatId);

}
