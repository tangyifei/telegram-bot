package com.code.telegram.services;

import java.io.UnsupportedEncodingException;

/**
 * 验证码业务接口
 *
 * @author xiaoyaowang
 */
public interface SmsService {

    /**
     * 发送短信
     *
     * @param phone   手机号
     * @param context 发送内容
     * @throws UnsupportedEncodingException 不支持的编码异常
     */
    void sendSms(String phone, String context) throws UnsupportedEncodingException;

}
