package com.code.telegram.services.impl;

import com.code.consts.CommonConsts;
import com.code.enums.RewardKeyEnum;
import com.code.models.robot.RobotInvite;
import com.code.telegram.daos.task.RobotInviteMapper;
import com.code.telegram.services.RobotInviteService;
import com.code.telegram.services.RobotSystemService;
import com.code.telegram.services.RobotTaskService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 邀请服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class RobotInviteServiceImpl implements RobotInviteService {

    @Resource
    RobotInviteMapper robotInviteMapper;

    @Resource
    RobotTaskService robotTaskService;

    @Resource
    RobotSystemService robotSystemService;

    @Override
    public int insertRobotInvite(String inviteChatId, String chatId) {
        Integer existCount = robotInviteMapper.getRobotInviteByChatIdAndInviteChatId(chatId, inviteChatId);
        if (null == existCount) {
            RobotInvite robotInvite = new RobotInvite();
            robotInvite.setChatId(chatId);
            robotInvite.setInviteChatId(inviteChatId);
            robotInvite.setInviterRewardReset(0);
            robotInvite.setDeleted(0);
            robotInvite.setCreatedAt(new Date());
            robotInvite.setUpdatedAt(new Date());
            return robotInviteMapper.insert(robotInvite);
        }
        return 0;
    }

    @Override
    public int getInvitePersonsByChatId(String chatId, Integer inviterRewardReset) {
        return robotInviteMapper.getInvitePersonsByChatId(chatId, inviterRewardReset);
    }

    @Override
    public int getInvitePersonRewardsByChatId(String chatId) {
        // 先获取被邀请的chatId列表
        List<String> invitedChatIdList = robotInviteMapper.getInvitedChatIdListByChatId(chatId);
        int rewards = 0;
        if (!CollectionUtils.isEmpty(invitedChatIdList)) {
            String systemValue = robotSystemService.getSystemValueBySystemKey(RewardKeyEnum.INVITE_FRIENDS_FINISH_TASK_REWARD.name());
            int inviteFriendsFinishTaskReward = 0;
            if (!StringUtils.isBlank(systemValue)) {
                inviteFriendsFinishTaskReward = Integer.parseInt(systemValue);
            }
            // 获取总的任务数，任务数的大小就是完成的最大任务号
            int totalTasks = robotTaskService.getTotalTasks();
            for (String invitedChatId : invitedChatIdList) {
                if (totalTasks == robotTaskService.getTotalFinishTasks(invitedChatId)) {
                    rewards += inviteFriendsFinishTaskReward;
                }
            }
        }
        String inviteFriendRewardUpLimitStr = robotSystemService.getSystemValueBySystemKey(RewardKeyEnum.INVITE_FRIEND_UP_LIMIT.name());
        if (StringUtils.isBlank(inviteFriendRewardUpLimitStr)) {
            inviteFriendRewardUpLimitStr = "0";
        }
        int inviteFriendRewardUpLimit = Integer.parseInt(inviteFriendRewardUpLimitStr);
        if (rewards > inviteFriendRewardUpLimit) {
            return inviteFriendRewardUpLimit;
        }
        return rewards;
    }

    @Override
    public List<Integer> getRobotInviteIdListByInviteChatId(String inviteChatId) {
        return robotInviteMapper.getRobotInviteIdListByInviteChatId(inviteChatId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public int patchCancelRobotInviteRewardReset(List<Integer> inviteIdList) {
        return robotInviteMapper.patchCancelRobotInviteRewardReset(inviteIdList);
    }

}
