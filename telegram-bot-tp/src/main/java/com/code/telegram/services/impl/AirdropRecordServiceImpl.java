package com.code.telegram.services.impl;

import com.code.telegram.daos.task.AirdropRecordMapper;
import com.code.telegram.models.AirdropRecord;
import com.code.telegram.services.AirdropRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 已空投实现类
 *
 * @author xiaoyaowang
 */
@Service
public class AirdropRecordServiceImpl implements AirdropRecordService {

    @Resource
    AirdropRecordMapper airdropRecordMapper;

    @Override
    public boolean existsAirdropRecordByCondition(AirdropRecord airdropRecord) {
        return null != airdropRecordMapper.existsAirdropRecordByCondition(airdropRecord);
    }

    @Override
    public AirdropRecord getAirdropRecordByCondition(AirdropRecord airdropRecord) {
        return airdropRecordMapper.getAirdropRecordByCondition(airdropRecord);
    }
}
