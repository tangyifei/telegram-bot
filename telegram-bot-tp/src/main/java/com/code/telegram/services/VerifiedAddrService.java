package com.code.telegram.services;

/**
 * 校验成功的地址任务业务接口
 *
 * @author xiaoyaowang
 */
public interface VerifiedAddrService {

    /**
     * 判断某一个地址是否检验成功
     *
     * @param coinAddr 钱包地址
     * @return 地址是否检验成功
     */
    boolean judgeCoinAddrVerifySuccess(String coinAddr);

    /**
     * 通过chatId获取校验的钱包地址
     *
     * @param chatId 聊天id
     * @return 钱包地址
     */
    String getCoinAddrByChatId(String chatId);

    /**
     * 通过校验的钱包地址获取chatId
     *
     * @param coinAddr 校验的钱包地址
     * @return chatId
     */
    String getChatIdByCoinAddr(String coinAddr);

    /**
     * 添加用户已完成的地址校验成功记录
     *
     * @param chatId   聊天id
     * @param coinAddr 币种地址
     * @return 影响的行数
     */
    int insertVerifiedAddr(String chatId, String coinAddr);

    /**
     * 保存手机号与聊天唯一标识的关系
     *
     * @param chatId 聊天id
     * @param phone  手机号
     * @return 影响的行数
     */
    int insertChatPhone(String chatId, String phone);

}
