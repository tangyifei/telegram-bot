package com.code.telegram.services.impl;

import com.code.telegram.daos.system.RobotSystemMapper;
import com.code.telegram.services.RobotSystemService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 系统设置服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class RobotSystemServiceImpl implements RobotSystemService {

    @Resource
    RobotSystemMapper robotSystemMapper;


    @Override
    public String getSystemValueBySystemKey(String systemKey) {
        return robotSystemMapper.getSystemValueBySystemKey(systemKey);
    }
}
