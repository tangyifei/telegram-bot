package com.code.telegram.services;

import com.code.models.robot.RobotRewardFlow;

/**
 * 机器人奖励流水业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotRewardFlowService {

    /**
     * 插入机器人奖励流水
     *
     * @param robotRewardFlow 机器人奖励流水实体类
     * @return 影响的行数
     */
    int insertRobotRewardFlow(RobotRewardFlow robotRewardFlow);

    /**
     * 判断某一个钱包地址是否给予了奖励
     *
     * @param walletAddress 钱包地址
     * @param amount        奖励金额
     * @return 存在与否
     */
    Boolean existRobotRewardFlowByWalletAddressAndAmount(String walletAddress, String amount);

    /**
     * 查询是否存在BZ奖励
     *
     * @param walletAddress 钱包地址
     * @param amount        奖金金额
     * @return 是否存在BZ奖励
     */
    Boolean existBzRewardFlowByWalletAddressAndAmount(String walletAddress, String amount);

}
