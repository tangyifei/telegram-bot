package com.code.telegram.services;

import com.code.models.robot.ChatBan;

/**
 * 用户封禁业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatBanService {

    /**
     * 添加封禁用户
     *
     * @param chatBan 封禁用户
     * @return 影响的行数
     */
    int insertChatBan(ChatBan chatBan);

    /**
     * 判断用户是否被封禁
     *
     * @param chatId 聊天唯一id
     * @return 判断用户是否被封禁
     */
    boolean whetherExistChatBanByChatId(String chatId);

}
