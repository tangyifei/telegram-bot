package com.code.telegram.services;

import com.code.telegram.models.RobotChat;

/**
 * 与机器人聊天的用户业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotChatService {

    /**
     * 添加与机器人聊天的用户
     *
     * @param robotChat 与相关机器人聊天的用户记录实体类
     * @return 添加成功的相关机器人聊天的用户记录
     */
    RobotChat insertRobotChat(RobotChat robotChat);

    /**
     * 增加用户在群里发送的消息数
     *
     * @param chatId 聊天唯一标识
     * @return 影响的行数
     */
    int incrementChatMessageNums(String chatId, String firstName,
                                 String lastName, String userName);

}
