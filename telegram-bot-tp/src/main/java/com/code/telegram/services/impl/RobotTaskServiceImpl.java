package com.code.telegram.services.impl;

import com.code.consts.CommonConsts;
import com.code.enums.RewardKeyEnum;
import com.code.models.robot.RobotChatFinishTask;
import com.code.models.robot.RobotTaskCheck;
import com.code.telegram.daos.task.RobotChatFinishTaskMapper;
import com.code.telegram.daos.task.RobotTaskMapper;
import com.code.telegram.models.AirdropRecord;
import com.code.telegram.models.ChatMemberGroup;
import com.code.telegram.models.RobotTask;
import com.code.telegram.services.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 机器人任务服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class RobotTaskServiceImpl implements RobotTaskService {

    @Resource
    RobotTaskMapper robotTaskMapper;

    @Resource
    RobotChatFinishTaskMapper robotChatFinishTaskMapper;

    @Resource
    VerifiedAddrService verifiedAddrService;

    @Resource
    ChatTwitterService chatTwitterService;

    @Resource
    RobotSystemService robotSystemService;

    @Resource
    RobotTaskCheckService robotTaskCheckService;

    @Resource
    ChatMemberGroupService chatMemberGroupService;

    @Resource
    AirdropRecordService airdropRecordService;

    @Override
    public int getFinishedTaskMaxSort(String chatId) {
        Integer finishedTaskMaxSort = robotTaskMapper.getFinishedTaskMaxSort(chatId);
        if (null != finishedTaskMaxSort) {
            return finishedTaskMaxSort;
        }
        return 0;
    }

    @Override
    public int getTotalTasks() {
        return robotTaskMapper.getTotalTasks();
    }

    @Override
    public int getTotalFinishTasks(String chatId) {
        return robotTaskMapper.getTotalFinishTasks(chatId);
    }

    @Override
    public boolean existsRobotChatFinishTaskByChatIdAndTaskId(String chatId, Integer taskId) {
        return null != robotTaskMapper.existsRobotChatFinishTaskByChatIdAndTaskId(taskId, chatId);
    }

    @Override
    public RobotTask getRobotTaskByTaskSort(Integer taskSort) {
        return robotTaskMapper.getRobotTaskByTaskSort(taskSort);
    }

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public RobotChatFinishTask insertRobotChatFinishTask(RobotChatFinishTask robotChatFinishTask) {
        String chatId = robotChatFinishTask.getChatId();
        Integer taskId = robotChatFinishTask.getTaskId();
        if (null != taskId) {
            Integer existCount = robotTaskMapper.existsRobotChatFinishTaskByChatIdAndTaskId(taskId, chatId);
            if (null == existCount) {
                robotChatFinishTask.setDeleted(0);
                robotChatFinishTask.setCreatedAt(new Date());
                robotChatFinishTask.setUpdatedAt(new Date());
                robotChatFinishTaskMapper.insert(robotChatFinishTask);
            }
        }
        String coinAddr = robotChatFinishTask.getCoinAddr();
        if (!StringUtils.isBlank(coinAddr)) {
            verifiedAddrService.insertVerifiedAddr(chatId, coinAddr);
        }
        String twitterAccountName = robotChatFinishTask.getTwitterAccountName();
        if (!StringUtils.isBlank(twitterAccountName)) {
            chatTwitterService.insertChatTwitter(chatId, twitterAccountName);
        }
        Integer sendPhoto = robotChatFinishTask.getSendPhoto();
        if (null != sendPhoto) {
            addRobotCheckTask(chatId);
        }
        return robotTaskMapper.getRobotChatFinishTaskByChatIdAndTaskId(taskId, chatId);
    }

    @Override
    public int insertRobotChatFinishTaskList(List<RobotChatFinishTask> robotChatFinishTaskList) {
        return robotChatFinishTaskMapper.insertList(robotChatFinishTaskList);
    }

    @Override
    public int getFinishTaskRewardsByChatId(String chatId) {
        int finishTasks = robotChatFinishTaskMapper.getFinishTasksByChatId(chatId);
        String everyTaskReward = robotSystemService.getSystemValueBySystemKey(RewardKeyEnum.EVERY_TASK_REWARD.name());
        if (StringUtils.isBlank(everyTaskReward)) {
            everyTaskReward = "0";
        }
        return finishTasks * Integer.parseInt(everyTaskReward);
    }

    @Override
    public boolean existsAirdropRecordByCondition(AirdropRecord airdropRecord) {
        return airdropRecordService.existsAirdropRecordByCondition(airdropRecord);
    }

    @Override
    public AirdropRecord getAirdropRecordByCondition(AirdropRecord airdropRecord) {
        return airdropRecordService.getAirdropRecordByCondition(airdropRecord);
    }

    private void addRobotCheckTask(String chatId) {
        RobotTaskCheck robotTaskCheck = new RobotTaskCheck();
        robotTaskCheck.setChatId(chatId);
        ChatMemberGroup chatMemberGroup = chatMemberGroupService.getChatMemberGroupByChatId(chatId);
        if (null != chatMemberGroup) {
            robotTaskCheck.setUserName(chatMemberGroup.getUserName());
            robotTaskCheck.setFirstName(chatMemberGroup.getFirstName());
            robotTaskCheck.setLastName(chatMemberGroup.getLastName());
        }
        robotTaskCheck.setWalletAddr(verifiedAddrService.getCoinAddrByChatId(chatId));
        robotTaskCheck.setState(0);
        robotTaskCheckService.insertRobotTaskCheck(robotTaskCheck);
    }
}
