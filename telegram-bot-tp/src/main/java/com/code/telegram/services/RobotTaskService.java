package com.code.telegram.services;

import com.code.models.robot.RobotChatFinishTask;
import com.code.telegram.models.AirdropRecord;
import com.code.telegram.models.RobotTask;

import java.util.List;

/**
 * 机器人任务业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotTaskService {

    /**
     * 获取用户完成的任务的最大序号
     *
     * @param chatId 聊天id
     * @return 用户完成的任务的最大序号
     */
    int getFinishedTaskMaxSort(String chatId);

    /**
     * 获取总的任务数
     *
     * @return 总的任务数
     */
    int getTotalTasks();

    /**
     * 获取完成的任务数
     *
     * @return 完成的任务数
     */
    int getTotalFinishTasks(String chatId);

    /**
     * 判断某一个任务是否存在
     *
     * @param chatId 聊天id
     * @param taskId 任务id
     * @return 某一个任务是否存在结果
     */
    boolean existsRobotChatFinishTaskByChatIdAndTaskId(String chatId, Integer taskId);

    /**
     * 根据序号获取任务
     *
     * @param taskSort 任务序号
     * @return 用户完成的任务的最大序号
     */
    RobotTask getRobotTaskByTaskSort(Integer taskSort);

    /**
     * 添加用户已完成的任务记录
     *
     * @param robotChatFinishTask 用户已完成的任务实体类
     * @return 添加成功的用户已完成的任务实体类
     */
    RobotChatFinishTask insertRobotChatFinishTask(RobotChatFinishTask robotChatFinishTask);

    /**
     * 批量添加用户已完成的任务记录
     *
     * @param robotChatFinishTaskList 用户已完成的任务列表
     * @return 影响的行数
     */
    int insertRobotChatFinishTaskList(List<RobotChatFinishTask> robotChatFinishTaskList);

    /**
     * 通过聊天id获取完成任务相关的奖励
     *
     * @param chatId 聊天id
     * @return 完成任务获取的奖励
     */
    int getFinishTaskRewardsByChatId(String chatId);

    /**
     * 判断用户是否参加过空投
     *
     * @param airdropRecord 已空投记录
     * @return 用户是否参加过空投
     */
    boolean existsAirdropRecordByCondition(AirdropRecord airdropRecord);

    /**
     * 根据相关条件获取空投信息
     *
     * @param airdropRecord 已空投记录
     * @return 空投信息
     */
    AirdropRecord getAirdropRecordByCondition(AirdropRecord airdropRecord);

}
