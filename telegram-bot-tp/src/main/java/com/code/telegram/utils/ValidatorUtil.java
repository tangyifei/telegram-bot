package com.code.telegram.utils;

import org.apache.commons.lang.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 验证工具类
 *
 * @author xiaoyaowang
 */
public class ValidatorUtil {

    private ValidatorUtil() {
    }

    /**
     * telegram的链接地址正则表达式
     */
    private static final String TELEGRAM_LINK_FORMAT = "https://t.me/[\\w-]+(/[\\w-./?%&=]*)?$";

    /**
     * 推特链接地址正则表达式
     */
    private static final String TWITTER_LINK_FORMAT = "https://twitter.com/";

    /**
     * telegram的链接地址匹配模式
     */
    private static final Pattern TELEGRAM_LINK_PATTERN = Pattern.compile(TELEGRAM_LINK_FORMAT);

    /**
     * 推特链接地址匹配模式
     */
//    private static final Pattern TWITTER_LINK_PATTERN = Pattern.compile(TWITTER_LINK_FORMAT);

    /**
     * 判断是否为telegram链接地址
     *
     * @param telegramLinkUrl telegram链接地址
     * @return 是否为telegram链接地址
     */
    public static boolean isTelegramLinkUrl(String telegramLinkUrl) {
        if (StringUtils.isBlank(telegramLinkUrl)) {
            return false;
        }
        Matcher m = TELEGRAM_LINK_PATTERN.matcher(telegramLinkUrl);
        return m.matches();
    }

    /**
     * 判断是否为推特链接地址
     *
     * @param twitterLinkUrl 推特链接地址
     * @return 是否为推特链接地址
     */
    public static boolean isTwitterLinkUrl(String twitterLinkUrl) {
        if (StringUtils.isBlank(twitterLinkUrl)) {
            return false;
        }
        return twitterLinkUrl.startsWith(TWITTER_LINK_FORMAT);
//        Matcher m = TWITTER_LINK_PATTERN.matcher(twitterLinkUrl);
//        return m.matches();
    }

//    public static void main(String[] args) {
//        System.out.println(isTelegramLinkUrl("https://t.me/tp_tes/166"));
//        System.out.println(isTwitterLinkUrl("https://twitter.com/Lumini081/status/1422965935787364352?s=20"));
//        String text = "https://twitter.com/hshj77431327/status/1422817004105334789";
//        System.out.println(text.substring(text.indexOf("twitter.com/") + 12, text.indexOf("/status")));
//}
}
