package com.code.telegram.bots;

import com.alibaba.fastjson.JSON;
import com.code.consts.CommonConsts;
import com.code.enums.CacheKeyEnum;
import com.code.enums.RewardKeyEnum;
import com.code.models.robot.ChatBan;
import com.code.models.robot.ChatLink;
import com.code.models.robot.RobotChatFinishTask;
import com.code.telegram.configs.redis.RedisHandler;
import com.code.telegram.consts.TelegramCommonConsts;
import com.code.telegram.enums.SystemKeyEnum;
import com.code.telegram.managers.HttpManager;
import com.code.telegram.models.AirdropRecord;
import com.code.telegram.models.ChatMemberGroup;
import com.code.telegram.models.RobotChat;
import com.code.telegram.models.RobotTask;
import com.code.telegram.services.*;
import com.code.telegram.utils.DynamicGenerateImageUtil;
import com.code.telegram.utils.ValidatorUtil;
import com.code.telegram.utils.VerifyCodeUtil;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * TokenSwap空投机器人
 * telegram bot 接受信息有2种实现方式
 * Long Polling Bot 长轮询的方式,每隔一段时间主动请求telegram，继承 TelegramLongPollingBot
 * webHook 方式,telegram 主动下发
 *
 * @author xiaoyaowang
 */
public class TokenSwapTaskRobot extends TelegramLongPollingBot {

    private final static String RECEIVE_INCOME = "receive income";

    private static Logger logger = LoggerFactory.getLogger(TokenSwapTaskRobot.class);

    private String menuFirstButtonText;

    private String menuSecondButtonText;

    private String menuThirdButtonText;

    private String menuFourthButtonText;

    private String earnTpFirstButtonText;

    private String earnTpSecondButtonText;

    private String earnTpThirdButtonText;

    private String firstTaskText;

    private String replyButtonText;

    private String replyDoneText;

    private String errorText;

    private String taskFinishMessage;

    private String listenJoinGroupUrlPrefix;

    private String listenJoinGroupName;

    private RobotChatService robotChatService;

    private RobotTaskService robotTaskService;

    private ChatMemberGroupService chatMemberGroupService;

    private RobotInviteService robotInviteService;

    private VerifiedAddrService verifiedAddrService;

    private RobotTaskCheckService robotTaskCheckService;

    private ChatLinkService chatLinkService;

    private RobotSystemService robotSystemService;

    private ChatBanService chatBanService;

    private String secondTaskImageUrl;

    private String checkImageUrl;

    private String secondTaskFinish;

    private String secondTaskAirdropLink;

    private HttpManager httpManager;

    private String robotName;

    private String robotToken;

    private String uploadUrl;

    private String uploadPath;

    private RedisHandler redisHandler;

    private ChatTwitterService chatTwitterService;

    private RobotRewardFlowService robotRewardFlowService;

    private SmsService smsService;

    public void setChatBanService(ChatBanService chatBanService) {
        this.chatBanService = chatBanService;
    }

    public void setVerifiedAddrService(VerifiedAddrService verifiedAddrService) {
        this.verifiedAddrService = verifiedAddrService;
    }

    public void setChatMemberGroupService(ChatMemberGroupService chatMemberGroupService) {
        this.chatMemberGroupService = chatMemberGroupService;
    }

    public void setRobotRewardFlowService(RobotRewardFlowService robotRewardFlowService) {
        this.robotRewardFlowService = robotRewardFlowService;
    }

    public void setMenuFirstButtonText(String menuFirstButtonText) {
        this.menuFirstButtonText = menuFirstButtonText;
    }

    public void setMenuSecondButtonText(String menuSecondButtonText) {
        this.menuSecondButtonText = menuSecondButtonText;
    }

    public void setRobotSystemService(RobotSystemService robotSystemService) {
        this.robotSystemService = robotSystemService;
    }

    public void setMenuThirdButtonText(String menuThirdButtonText) {
        this.menuThirdButtonText = menuThirdButtonText;
    }

    public void setMenuFourthButtonText(String menuFourthButtonText) {
        this.menuFourthButtonText = menuFourthButtonText;
    }

    public void setEarnTpFirstButtonText(String earnTpFirstButtonText) {
        this.earnTpFirstButtonText = earnTpFirstButtonText;
    }

    public void setEarnTpSecondButtonText(String earnTpSecondButtonText) {
        this.earnTpSecondButtonText = earnTpSecondButtonText;
    }

    public void setEarnTpThirdButtonText(String earnTpThirdButtonText) {
        this.earnTpThirdButtonText = earnTpThirdButtonText;
    }

    public void setSecondTaskFinish(String secondTaskFinish) {
        this.secondTaskFinish = secondTaskFinish;
    }

    public void setListenJoinGroupUrlPrefix(String listenJoinGroupUrlPrefix) {
        this.listenJoinGroupUrlPrefix = listenJoinGroupUrlPrefix;
    }

    public void setListenJoinGroupName(String listenJoinGroupName) {
        this.listenJoinGroupName = listenJoinGroupName;
    }

    public void setTaskFinishMessage(String taskFinishMessage) {
        this.taskFinishMessage = taskFinishMessage;
    }

    public void setReplyDoneText(String replyDoneText) {
        this.replyDoneText = replyDoneText;
    }

    public void setFirstTaskText(String firstTaskText) {
        this.firstTaskText = firstTaskText;
    }

    public void setReplyButtonText(String replyButtonText) {
        this.replyButtonText = replyButtonText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    public void setRobotName(String robotName) {
        this.robotName = robotName;
    }

    public void setRobotToken(String robotToken) {
        this.robotToken = robotToken;
    }

    public void setRobotChatService(RobotChatService robotChatService) {
        this.robotChatService = robotChatService;
    }

    public void setRobotTaskService(RobotTaskService robotTaskService) {
        this.robotTaskService = robotTaskService;
    }

    public void setHttpManager(HttpManager httpManager) {
        this.httpManager = httpManager;
    }

    public void setSecondTaskImageUrl(String secondTaskImageUrl) {
        this.secondTaskImageUrl = secondTaskImageUrl;
    }

    public void setSecondTaskAirdropLink(String secondTaskAirdropLink) {
        this.secondTaskAirdropLink = secondTaskAirdropLink;
    }

    public void setRobotInviteService(RobotInviteService robotInviteService) {
        this.robotInviteService = robotInviteService;
    }

    public void setRobotTaskCheckService(RobotTaskCheckService robotTaskCheckService) {
        this.robotTaskCheckService = robotTaskCheckService;
    }

    public void setChatTwitterService(ChatTwitterService chatTwitterService) {
        this.chatTwitterService = chatTwitterService;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public void setCheckImageUrl(String checkImageUrl) {
        this.checkImageUrl = checkImageUrl;
    }

    public void setRedisHandler(RedisHandler redisHandler) {
        this.redisHandler = redisHandler;
    }

    public void setSmsService(SmsService smsService) {
        this.smsService = smsService;
    }

    public void setChatLinkService(ChatLinkService chatLinkService) {
        this.chatLinkService = chatLinkService;
    }

    /**
     * 监听更新接收信息事件
     *
     * @param update 该对象表示传入的更新，比如接收到用户发来的新消息，就会获得新的更新
     */
    @Override
    public void onUpdateReceived(Update update) {
        logger.info("task bot start work,execute onUpdateReceived method，update:{}", JSON.toJSONString(update));
        try {
            if (update.hasMessage()) {
                Message messageText = update.getMessage();
                if (null != messageText) {
                    Long chatIdLong = messageText.getChatId();
                    if (null != chatIdLong) {
                        String chatId = chatIdLong.toString();
                        // 插入用户或者群组与机器人的绑定关系
                        RobotChat robotChat = new RobotChat();
                        robotChat.setChatId(chatId);
                        robotChat.setRobotName(robotName);
                        robotChatService.insertRobotChat(robotChat);
                        String stopRobot = robotSystemService.getSystemValueBySystemKey(RewardKeyEnum.STOP_ROBOT.name());
                        if (!redisHandler.sHasKey(CacheKeyEnum.TELEGRAM_RELEASE_ACCOUNT.formatKey(), chatId)) {
                            if ("1".equals(stopRobot)) {
                                SendMessage sendMessage = new SendMessage();
                                sendMessage.setChatId(chatId);
                                sendMessage.setText(robotSystemService.getSystemValueBySystemKey(RewardKeyEnum.STOP_ROBOT_MESSAGE.name()));
                                execute(sendMessage);
                                return;
                            }
                        }
                        if (messageText.hasText()) {
                            // 查看用户是否被封禁
                            if (robotTaskCheckService.whetherBanedByChatId(chatId)
                                    || chatBanService.whetherExistChatBanByChatId(chatId)) {
                                SendMessage sendMessage = new SendMessage();
                                sendMessage.setChatId(chatId);
                                sendMessage.setText(robotSystemService.getSystemValueBySystemKey(RewardKeyEnum.BAN.name()));
                                execute(sendMessage);
                                return;
                            }
                            String text = messageText.getText();
                            logger.info("chatId {} send message text:{}", chatId, text);
                            int taskMaxSort = robotTaskService.getFinishedTaskMaxSort(chatId);
                            // 返回到菜单页面
                            if (text.equals(replyButtonText)) {
                                backMenuPage(chatId);
                            } else {
                                // 点击菜单页面的Earn tp按钮进入Earn tp菜单页
                                if (text.equals(menuFirstButtonText)) {
                                    backEarnTpMenuPage(chatId);
                                }
                                // 点击菜单页面的TP Info 按钮
                                else if (text.equals(menuSecondButtonText)) {
                                    sendTpDetailMessage(chatId);
                                }
                                // 点击菜单页面的TP DownLoad 按钮
                                else if (text.equals(menuThirdButtonText)) {
                                    sendTpDownloadMessage(chatId);
                                }
                                // 点击菜单页面的My rewards 按钮
                                else if (text.equals(menuFourthButtonText)) {
                                    sendMyRewardsMessage(chatId);
                                }
                                // 执行任务
                                else {
                                    // 是否参加过空投任务
                                    Chat chat = messageText.getChat();
                                    if (null != chat) {
                                        String userName = chat.getUserName();
                                        Integer airdropFlag = whetherAirdrop(chatId, userName);
                                        if (airdropFlag == 1) {
                                            return;
                                        }
                                    }
                                    // 领取邀请收益
                                    if (RECEIVE_INCOME.equals(text)) {
                                        String agentReceiveIncomeOff = robotSystemService.getSystemValueBySystemKey(RewardKeyEnum.AGENT_RECEIVE_INCOME_OFF.name());
                                        if ("1".equals(agentReceiveIncomeOff)) {
                                            SendMessage sendMessage = new SendMessage();
                                            sendMessage.setChatId(chatId);
                                            sendMessage.setText("The agent's income collection function has been closed.");
                                            execute(sendMessage);
                                            return;
                                        }
                                        robotTaskCheckService.receiveIncome(chatId);
                                    }
                                    // 第一个任务
                                    if (taskMaxSort < 1) {
                                        // 处理第一个任务
                                        handleFirstTask(chatId, text);
                                        return;
                                    }

                                    // 继续第二个任务
                                    if (taskMaxSort == 1) {
                                        if (!robotTaskCheckService.whetherCheckFailure(chatId)) {
                                            // 校验验证码
                                            boolean verifyAuthCode = verifyAuthCode(chatId, text, robotSystemService.getSystemValueBySystemKey(SystemKeyEnum.VERIFY_CODE_TYPE.name()));
                                            if (!verifyAuthCode) {
                                                return;
                                            }
                                        }
                                        // 处理第二个任务
                                        handleSecondTask(chatId, text);
                                        return;
                                    }
                                    // 点击Share the link to get 1000 TP按钮判断第二步任务是否已完成
                                    if (text.equals(earnTpThirdButtonText)) {
                                        // 发送第2个任务已完成的消息
                                        sendTaskFinishMessage(chatId);
                                        return;

                                    }

                                    // 继续第三个任务
                                    if (taskMaxSort == 2) {
                                        redisHandler.del(chatId);
                                        redisHandler.del(CacheKeyEnum.TELEGRAM_AUTH_CODE_SUCCESS.formatKey(chatId));
                                        redisHandler.del(CacheKeyEnum.TELEGRAM_AUTH_CODE_FAILURE.formatKey(chatId));
                                        // 处理第3个任务
                                        handleThirdTask(chatId, messageText);
                                        return;
                                    }
                                    // 点击Join the official TG group按钮判断第3步任务是否已完成
                                    if (text.equals(earnTpFirstButtonText)) {
                                        // 发送第3个任务已完成的消息
                                        sendTaskFinishMessage(chatId);
                                        return;

                                    }

                                    // 继续第4个任务
                                    if (taskMaxSort == 3) {
                                        // 处理第4个任务
                                        handleFourthTask(chatId, text);
                                        return;
                                    }
                                    // 点击Follow TP Twitter按钮判断第四个任务是否完成
                                    if (text.equals(earnTpSecondButtonText)) {
                                        // 发送第4个任务已完成的消息
                                        sendTaskFinishMessage(chatId);
                                        return;
                                    }

                                    // 处理第五个任务
                                    if (taskMaxSort == 4) {
                                        // 处理第5个任务
                                        handleFifthTask(chatId, text);
                                        return;
                                    }

                                    // 处理第六个任务
                                    if (taskMaxSort == 5) {
                                        // 处理第6个任务
                                        handleSixthTask(chatId);
                                        return;
                                    }

                                    // 任务都已完成
                                    if (taskMaxSort == 6) {
                                        sendCongratulationMessage(chatId);
                                    }

                                }
                            }
                        }


                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("onUpdateReceived error:", e);
        }

    }

    /**
     * 获取电报机器人名称，在进入telegram账户向@BotFather发送/newbot指令的时候输入的机器人的名字，每一个机器人都是唯一的
     *
     * @return 电报机器人名称
     */
    @Override
    public String getBotUsername() {
        return robotName;
    }

    /**
     * 获取电报机器人的token，在进入telegram账户向@BotFather发送/newbot指令的时候BotFather返回给机器人的token，每一个机器人都是唯一的
     *
     * @return 电报机器人的token
     */
    @Override
    public String getBotToken() {
        return robotToken;
    }

    /**
     * 检验验证码
     *
     * @param chatId 聊天唯一标识号
     * @param text   发送的内容
     * @return 校验成功与否
     * @throws Exception 异常
     */
    private boolean verifyAuthCode(String chatId, String text, String verifyCodeType) throws Exception {
        // 输入错误超过3次，可以考虑直接转入被封禁的名单中
        if (redisHandler.hasKey(CacheKeyEnum.TELEGRAM_AUTH_CODE_FAILURE.formatKey(chatId))
                && (Integer) redisHandler.get(CacheKeyEnum.TELEGRAM_AUTH_CODE_FAILURE.formatKey(chatId)) > 2) {
            // 封禁用户
            ChatBan chatBan = new ChatBan();
            chatBan.setChatId(chatId);
            chatBanService.insertChatBan(chatBan);
            redisHandler.del(chatId);
            redisHandler.del(CacheKeyEnum.TELEGRAM_AUTH_CODE_FAILURE.formatKey(chatId));
            SendMessage sendMessage = new SendMessage();
            sendMessage.setChatId(chatId);
            sendMessage.setText(robotSystemService.getSystemValueBySystemKey(RewardKeyEnum.BAN.name()));
            execute(sendMessage);
            return false;
        }
        if (!redisHandler.hasKey(CacheKeyEnum.TELEGRAM_AUTH_CODE_SUCCESS.formatKey(chatId))) {
            if (redisHandler.hasKey(chatId)) {
                String authCode = (String) redisHandler.get(chatId);
                if (!text.equalsIgnoreCase(authCode)) {
                    handleGenerateAutoCodeTask(chatId, text, verifyCodeType);
                    redisHandler.incr(CacheKeyEnum.TELEGRAM_AUTH_CODE_FAILURE.formatKey(chatId));
                    return false;
                } else {
                    redisHandler.set(CacheKeyEnum.TELEGRAM_AUTH_CODE_SUCCESS.formatKey(chatId), 1);
                    return true;
                }
            } else {
                handleGenerateAutoCodeTask(chatId, text, verifyCodeType);
                return false;
            }
        }
        return true;
    }

    /**
     * 是否已参加空投
     *
     * @param chatId   聊天id
     * @param userName telegram账户名
     * @throws TelegramApiException 异常
     */
    private Integer whetherAirdrop(String chatId, String userName) throws TelegramApiException {
        AirdropRecord airdropRecord = new AirdropRecord();
        airdropRecord.setTelegramUserName(userName);
        boolean existsAirdropRecord = robotTaskService.existsAirdropRecordByCondition(airdropRecord);
        if (existsAirdropRecord) {
            sendAirdropMessage(chatId, userName);
            return 1;
        }
        return 0;
    }

    /**
     * 发送已空投的消息
     *
     * @param chatId           聊天id
     * @param telegramUserName telegram账户名
     * @throws TelegramApiException
     */
    private void sendAirdropMessage(String chatId, String telegramUserName) throws TelegramApiException {
        SendMessage message = new SendMessage();
        message.setChatId(chatId);
        StringBuilder stringBuilder = new StringBuilder(1 << 4);
        stringBuilder.append("You have already participated in the event");
        stringBuilder.append("\n");
        stringBuilder.append("Account information for rewards:");
        stringBuilder.append("\n");
        stringBuilder.append("Wallet address: ");
        AirdropRecord airdropRecord = new AirdropRecord();
        airdropRecord.setTelegramUserName(telegramUserName);
        AirdropRecord airdropRecordDb = robotTaskService.getAirdropRecordByCondition(airdropRecord);
        if (null != airdropRecordDb) {
            stringBuilder.append(airdropRecordDb.getWalletAddr());
            stringBuilder.append("\n");
            stringBuilder.append("Telegram username: ");
            stringBuilder.append(telegramUserName);
            stringBuilder.append("\n");
            stringBuilder.append("Twitter account: ");
            stringBuilder.append(airdropRecordDb.getTwitterAccount());
            stringBuilder.append("\n");
            stringBuilder.append("TP rewards: ");
            stringBuilder.append(airdropRecordDb.getTpReward());
            stringBuilder.append("\n");
            stringBuilder.append("\n");
            stringBuilder.append("Stay tuned, more events coming soon");
        }
        message.setText(stringBuilder.toString());
        execute(message);
    }

    /**
     * 发送已完成的消息
     *
     * @param chatId 聊天id
     * @throws TelegramApiException
     */
    private void sendTaskFinishMessage(String chatId) throws TelegramApiException {
        int finishTaskNums = robotTaskService.getTotalFinishTasks(chatId);
        int totalTaskNums = robotTaskService.getTotalTasks();
        SendMessage message = new SendMessage();
        message.setChatId(chatId);
        if (finishTaskNums < totalTaskNums) {
            message.setText(secondTaskFinish);
        } else {
            message.setText("All tasks have been completed.");
        }
        execute(message);
    }

    /**
     * 处理第一步任务
     *
     * @param chatId            聊天id
     * @param text              发送的文本
     * @param firstMessageAgain 是否再次发送 0-不需要 1-需要
     * @throws TelegramApiException 异常
     */
    private void handleFirstStepTask(String chatId, String text, int firstMessageAgain, int coinAddrRepeatFlag) throws TelegramApiException {
        if (firstMessageAgain == 0) {
            SendMessage firstTaskOneMessage = new SendMessage();
            firstTaskOneMessage.setChatId(chatId);
            firstTaskOneMessage.setText(text);
            execute(firstTaskOneMessage);

            SendMessage secondTaskOneMessage = new SendMessage();
            secondTaskOneMessage.setChatId(chatId);
            secondTaskOneMessage.setText(firstTaskText);

            // 设置带有回复选项的自定义键盘
            ReplyKeyboardMarkup replyKeyboardMarkup = getCommonReplyKeyboardMarkup(1);
            secondTaskOneMessage.setReplyMarkup(replyKeyboardMarkup);

            execute(secondTaskOneMessage);
        } else {
            // 发送错误文本
            SendMessage firstTaskErrorMessage = new SendMessage();
            firstTaskErrorMessage.setChatId(chatId);
            if (coinAddrRepeatFlag == 1) {
                firstTaskErrorMessage.setText("The wallet address has already been used, you need to change to another wallet address.");
            } else {
                firstTaskErrorMessage.setText(errorText);
            }
            execute(firstTaskErrorMessage);
            // 再次发送让用户提交地址的消息
            SendMessage firstTaskAgainMessage = new SendMessage();
            firstTaskAgainMessage.setChatId(chatId);
            firstTaskAgainMessage.setText(text);
            execute(firstTaskAgainMessage);
        }

    }

    /**
     * 生成验证码
     *
     * @param chatId 聊天id
     * @throws TelegramApiException 异常
     */
    private void handleGenerateAutoCodeTask(String chatId, String phone, String verifyCodeType) throws Exception {
        if ("1".equals(verifyCodeType)) {
            if (!redisHandler.hasKey(CacheKeyEnum.SMS_SEND_INTERVAL.formatKey(chatId))) {
                // 发送手机验证码
                String authCode = VerifyCodeUtil.generateVerifyNum(6);
                logger.info("手机号：{} chatId:{} 生成的手机验证码:{}", phone, chatId, authCode);
                redisHandler.set(CacheKeyEnum.TELEGRAM_AUTH_CODE.formatKey(chatId), authCode, CacheKeyEnum.TELEGRAM_AUTH_CODE.sec());
                smsService.sendSms(phone, authCode);
                verifiedAddrService.insertChatPhone(chatId, phone);
                redisHandler.set(CacheKeyEnum.SMS_SEND_INTERVAL.formatKey(chatId), phone, CacheKeyEnum.SMS_SEND_INTERVAL.sec());
            }
            SendMessage generateAutoCodeMessage = new SendMessage();
            generateAutoCodeMessage.setChatId(chatId);
            generateAutoCodeMessage.setParseMode(ParseMode.HTML);
            // 针对html内容在a标签中插入图片，a标签最好加入文本内容，比如下面的Please see the second task below:
            generateAutoCodeMessage.setText("Enter SMS verification code. Start tasks after confirmation.");
            execute(generateAutoCodeMessage);
        } else {
            // 发送图形验证码
            DynamicGenerateImageUtil imageUtil = new DynamicGenerateImageUtil.Builder().textSize(4).build();
            String authCodeFileName = chatId + System.currentTimeMillis() + ".jpg";
            String authCodeUrl = uploadPath + authCodeFileName;
            boolean writeResult = imageUtil.output(new FileOutputStream(new File(authCodeUrl)));
            if (writeResult) {
                String authCode = imageUtil.getText();
                logger.info("生成的图形验证码:{}", authCode);
                redisHandler.set(CacheKeyEnum.TELEGRAM_AUTH_CODE.formatKey(chatId), authCode, CacheKeyEnum.TELEGRAM_AUTH_CODE.sec());
                SendMessage generateAutoCodeMessage = new SendMessage();
                generateAutoCodeMessage.setChatId(chatId);
                generateAutoCodeMessage.setParseMode(ParseMode.HTML);
                // 针对html内容在a标签中插入图片，a标签最好加入文本内容，比如下面的Please see the second task below:
                String authCodeImageUrl = uploadUrl + "/" + authCodeFileName;
                String imageUrl = "<a href=" + "\"" + authCodeImageUrl + "\"" + "> </a>";
                String sendText = imageUrl + "\r\n" + "Please enter verification to prove you are Human Being.";
                logger.info("seng authCode content:{}", sendText);
                generateAutoCodeMessage.setDisableWebPagePreview(false);
                generateAutoCodeMessage.setText(sendText);
                execute(generateAutoCodeMessage);
            }
        }

    }

    /**
     * 处理第2步任务
     *
     * @param chatId 聊天id
     * @param text   发送的文本
     * @throws TelegramApiException 异常
     */
    private void handleSecondStepTask(String chatId, String text) throws TelegramApiException {
        SendMessage secondTaskMessage = new SendMessage();
        secondTaskMessage.setChatId(chatId);
        secondTaskMessage.setParseMode(ParseMode.HTML);
        // 针对html内容在a标签中插入图片，a标签最好加入文本内容，比如下面的Please see the second task below:
        String imageUrl = "<a href=" + "\"" + secondTaskImageUrl + "\"" + "> </a>";
        text = imageUrl + "\r\n" + text;
        secondTaskMessage.setText(text);
        secondTaskMessage.setDisableWebPagePreview(false);

        // 设置带有回复选项的自定义键盘
        ReplyKeyboardMarkup replyKeyboardMarkup = getCommonReplyKeyboardMarkup(1);
        secondTaskMessage.setReplyMarkup(replyKeyboardMarkup);

        execute(secondTaskMessage);

    }

    /**
     * 处理第3步任务
     *
     * @param chatId 聊天id
     * @param text   发送的文本
     * @throws TelegramApiException 异常
     */
    private void handleThirdStepTask(String chatId, String text) throws TelegramApiException {
        SendMessage thirdTaskMessage = new SendMessage();
        thirdTaskMessage.setChatId(chatId);
        thirdTaskMessage.setText(text);
        // 增加内联按钮
        List<List<InlineKeyboardButton>> keyboardList = new ArrayList<>();
        List<InlineKeyboardButton> keyboardButtons = new ArrayList<>();
        // 此对象表示嵌入式键盘的一个按钮，您必须完全使用可选字段之一
        InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
        // 可选的。按下按钮时将打开HTTP或tg:// URL
        inlineKeyboardButton.setUrl(listenJoinGroupUrlPrefix + listenJoinGroupName);
        // 在按钮上标记文本
        inlineKeyboardButton.setText("Join");
        keyboardButtons.add(inlineKeyboardButton);
        keyboardList.add(keyboardButtons);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        // 按钮行数组，每个行由一个InlineKeyboardButton对象数组表示，这仅适用于2016年4月9日之后发布的电报版本。较旧的客户端将显示不受支持的消息。
        inlineKeyboardMarkup.setKeyboard(keyboardList);
        // 可选的。 消息附带的嵌入式键盘。 login_url按钮表示为普通url按钮。
        thirdTaskMessage.setReplyMarkup(inlineKeyboardMarkup);
        execute(thirdTaskMessage);

    }

    /**
     * 设置带有回复选项的自定义键盘
     *
     * @param showDoneButton 是否显示done按钮 0-不显示 1-显示
     * @return 带有回复选项的自定义键盘
     */
    private ReplyKeyboardMarkup getCommonReplyKeyboardMarkup(Integer showDoneButton) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        //按钮行数组，每个行由一个KeyboardButton对象数组表示
        List<KeyboardRow> keyboard = new ArrayList<>();
        // 第一行
        KeyboardRow keyboardRow1 = new KeyboardRow();
        // 第一行的按钮
        if (showDoneButton == 1) {
            // 显示It's done,check it!相关的按钮
            keyboardRow1.add(replyDoneText);
        }
        keyboardRow1.add(replyButtonText);
        keyboard.add(keyboardRow1);
        replyKeyboardMarkup.setKeyboard(keyboard);
        // 可选的。请求客户垂直调整键盘大小以达到最佳适合度（例如，如果只有两行按钮，则使键盘变小）。默认为false，在这种情况下，自定义键盘的高度始终与应用程序的标准键盘相同。
        replyKeyboardMarkup.setResizeKeyboard(true);
        // 可选的。如果只想向特定用户显示键盘，请使用此参数。目标：1）在Message对象的文本中@提及的用户； 2）如果机器人的消息是回复（具有reply_to_message_id），则为原始消息的发送者。
        replyKeyboardMarkup.setSelective(true);
        // 如果需要点击一次就隐藏的键盘（但是依旧可以点击按钮显示），那么可以给 markup 构造时加入one_time_keyboard=True的参数
        //replyKeyboardMarkup.setOneTimeKeyboard(true);
        return replyKeyboardMarkup;
    }

    /**
     * 处理第一个任务
     *
     * @param chatId 聊天id
     * @param text   发送的文本
     * @throws Exception 异常
     */
    private void handleFirstTask(String chatId, String text) throws Exception {
        RobotTask robotTask = robotTaskService.getRobotTaskByTaskSort(1);
        if (null != robotTask) {
            String taskDescription = robotTask.getTaskDescription();
            if (!StringUtils.isBlank(text)) {
                if (text.contains("/start")) {
                    String[] str = text.split("/start");
                    if (ArrayUtils.isNotEmpty(str) && str.length > 1) {
                        String inviteCoinAddr = str[1].trim();
                        // 添加邀请人与被邀请人记录
                        String inviterChatId = verifiedAddrService.getChatIdByCoinAddr(inviteCoinAddr);
                        if (!StringUtils.isBlank(inviterChatId) && !Objects.equals(inviterChatId, chatId)) {
                            robotInviteService.insertRobotInvite(inviterChatId, chatId);
                        }
                    }
                    handleFirstStepTask(chatId, taskDescription, 0, 0);
                } else {
                    // 首先判断地址是否校验了
                    boolean verifyAddrSuccess = verifiedAddrService.judgeCoinAddrVerifySuccess(text);
                    if (verifyAddrSuccess) {
                        handleFirstStepTask(chatId, taskDescription, 1, 1);
                    } else {
                        // 校验XWC的地址
                        boolean verifyXwcAddr = httpManager.verifyCoinAddress(text);
                        if (verifyXwcAddr) {
                            RobotChatFinishTask robotChatFinishTask = new RobotChatFinishTask();
                            robotChatFinishTask.setChatId(chatId);
                            robotChatFinishTask.setTaskId(robotTask.getId());
                            robotChatFinishTask.setCoinAddr(text);
                            robotTaskService.insertRobotChatFinishTask(robotChatFinishTask);
                            if ("1".equals(robotSystemService.getSystemValueBySystemKey(SystemKeyEnum.VERIFY_CODE_TYPE.name()))) {
                                sendPhone(chatId);
                            } else {
                                verifyAuthCode(chatId, text, null);
                            }
                        } else {
                            handleFirstStepTask(chatId, taskDescription, 1, 0);
                        }
                    }

                }

            }
        }
    }

    /**
     * 处理第2个任务
     *
     * @param chatId 聊天id
     * @throws TelegramApiException 异常
     */
    private void handleSecondTask(String chatId, String text) throws TelegramApiException {
        RobotTask robotTask = robotTaskService.getRobotTaskByTaskSort(2);
        if (null != robotTask) {
            if (replyDoneText.equals(text)) {
                // TODO 分享5个币圈群
                RobotChatFinishTask robotChatFinishTask = new RobotChatFinishTask();
                robotChatFinishTask.setChatId(chatId);
                robotChatFinishTask.setTaskId(robotTask.getId());
                robotTaskService.insertRobotChatFinishTask(robotChatFinishTask);
                // 弹出第三个任务
                RobotTask robotTask3 = robotTaskService.getRobotTaskByTaskSort(3);
                if (null != robotTask3) {
                    handleThirdStepTask(chatId, robotTask3.getTaskDescription());
                }
            } else {
                handleSecondStepTask(chatId, robotTask.getTaskDescription());
            }
        }
    }

    /**
     * 判断是否分享链接到其他的群组
     *
     * @param chatId       聊天id
     * @param shareLink    分享链接地址
     * @param linkCategory 1-发送分享链接地址信息 2-发送推特链接地址相关的信息
     * @return 是否分享链接到其他的群组
     */
    private boolean whetherShareLinkToOtherGroup(String chatId, String shareLink, Integer linkCategory) {
        boolean whetherExistChatLink = chatLinkService.whetherExistChatLinkByChatIdAndShareLink(null, shareLink);
        if (whetherExistChatLink) {
            return false;
        } else {
            try {
                ChatLink chatLink = new ChatLink();
                chatLink.setLinkCategory(linkCategory);
                chatLink.setChatId(chatId);
                chatLink.setShareLink(shareLink);
                chatLinkService.insertChatLink(chatLink);
            } catch (Exception e) {
                logger.error("插入{}的链接发生异常:", chatId, e);
                return false;
            }
            return true;
        }
    }

    /**
     * 处理第3个任务
     *
     * @param chatId      聊天id
     * @param messageText 聊天text
     * @throws TelegramApiException 异常
     */
    private void handleThirdTask(String chatId, Message messageText) throws TelegramApiException {
        RobotTask robotTask = robotTaskService.getRobotTaskByTaskSort(3);
        if (null != robotTask) {
            String taskDescription = robotTask.getTaskDescription();
            Chat chat = messageText.getChat();
            logger.info("new chat member:{}", JSON.toJSONString(chat));
            if (null != chat) {
                ChatMemberGroup chatMemberGroup = new ChatMemberGroup();
                chatMemberGroup.setGroupUrl(listenJoinGroupUrlPrefix + listenJoinGroupName);
                chatMemberGroup.setChatId(chatId);
                // 判断用户是否加入群组
                if (!chatMemberGroupService.judgeUserWhetherJoinGroup(chatMemberGroup)) {
                    // 发送第3步任务的内容
                    handleThirdStepTask(chatId, taskDescription);
                } else {
                    RobotChatFinishTask robotChatFinishTask = new RobotChatFinishTask();
                    robotChatFinishTask.setChatId(chatId);
                    robotChatFinishTask.setTaskId(robotTask.getId());
                    robotTaskService.insertRobotChatFinishTask(robotChatFinishTask);
                    handleFourthStepTask(chatId, null);
                }

            }

        }
    }

    /**
     * 处理第4个任务
     *
     * @param chatId 聊天id
     * @throws TelegramApiException 异常
     */
    private void handleFourthTask(String chatId, String text) throws TelegramApiException {
        RobotTask robotTask = robotTaskService.getRobotTaskByTaskSort(4);
        if (null != robotTask) {
            Integer taskId = robotTask.getId();
            String taskDescription = robotTask.getTaskDescription();
            if (replyDoneText.equalsIgnoreCase(text)) {
                int twitterLinkCount = chatLinkService.getChatLinkCountByChatIdAndCategory(chatId, 2);
                boolean whetherExistChatTwitter = chatTwitterService.whetherExistChatTwitterByChatId(chatId);
                if (whetherExistChatTwitter && twitterLinkCount > 0) {
                    RobotChatFinishTask robotChatFinishTask = new RobotChatFinishTask();
                    robotChatFinishTask.setChatId(chatId);
                    robotChatFinishTask.setTaskId(taskId);
                    robotTaskService.insertRobotChatFinishTask(robotChatFinishTask);
                    // 处理第五步任务
                    handleFifthTask(chatId, null);
                } else {
                    handleFourthStepTask(chatId, taskDescription);
                }
            } else {
                if (ValidatorUtil.isTwitterLinkUrl(text)) {
                    boolean whetherShareLink = whetherShareLinkToOtherGroup(chatId, text, 2);
                    if (!whetherShareLink) {
                        handleFourthStepTask(chatId, taskDescription);
                    } else {
                        String twitterAccountName = text.substring(text.indexOf("twitter.com/") + 12, text.indexOf("/status"));
                        boolean whetherExistChatTwitterAccountName = chatTwitterService.whetherExistChatTwitterAccountNameByTwitterAccountName(twitterAccountName);
                        if (whetherExistChatTwitterAccountName) {
                            // 判断推特账号是不是自己的
                            String twitterAccountNameDb = chatTwitterService.getTwitterNameByChatId(chatId);
                            if (!Objects.equals(twitterAccountName, twitterAccountNameDb)) {
                                handleFourthStepTask(chatId, taskDescription);
                            } else {
                                RobotChatFinishTask robotChatFinishTask = new RobotChatFinishTask();
                                robotChatFinishTask.setChatId(chatId);
                                robotChatFinishTask.setTaskId(taskId);
                                robotChatFinishTask.setTwitterAccountName(twitterAccountName);
                                robotTaskService.insertRobotChatFinishTask(robotChatFinishTask);
                                handleFifthTask(chatId, null);
                            }
                        } else {
                            boolean attentionTwitterAccount = httpManager.whetherAttentionTokenSwapDex(chatId, twitterAccountName);
                            RobotChatFinishTask robotChatFinishTask = new RobotChatFinishTask();
                            robotChatFinishTask.setChatId(chatId);
                            robotChatFinishTask.setTaskId(taskId);
                            if (attentionTwitterAccount) {
                                robotChatFinishTask.setTwitterAccountName(twitterAccountName);
                            }
                            robotTaskService.insertRobotChatFinishTask(robotChatFinishTask);
                            handleFifthTask(chatId, null);
                        }
                    }
                } else {
                    if (text.startsWith("@")) {
                        text = text.substring(text.indexOf("@") + 1);
                    }
                    boolean whetherExistChatTwitterAccountName = chatTwitterService.whetherExistChatTwitterAccountNameByTwitterAccountName(text);
                    if (whetherExistChatTwitterAccountName) {
                        // 判断推特账号是不是自己的
                        String twitterAccountNameDb = chatTwitterService.getTwitterNameByChatId(chatId);
                        if (!Objects.equals(text, twitterAccountNameDb)) {
                            handleFourthStepTask(chatId, taskDescription);
                        } else {
                            RobotChatFinishTask robotChatFinishTask = new RobotChatFinishTask();
                            robotChatFinishTask.setChatId(chatId);
                            robotChatFinishTask.setTwitterAccountName(text);
                            robotTaskService.insertRobotChatFinishTask(robotChatFinishTask);
                        }
                    } else {
                        boolean attentionTwitterAccount = httpManager.whetherAttentionTokenSwapDex(chatId, text);
                        if (attentionTwitterAccount) {
                            RobotChatFinishTask robotChatFinishTask = new RobotChatFinishTask();
                            robotChatFinishTask.setChatId(chatId);
                            robotChatFinishTask.setTwitterAccountName(text);
                            robotTaskService.insertRobotChatFinishTask(robotChatFinishTask);
                        }
                    }
                }

            }
        }
    }

    /**
     * 处理第5个任务
     *
     * @param chatId 聊天id
     * @param text   发送内容
     * @throws TelegramApiException 异常
     */
    private void handleFifthTask(String chatId, String text) throws TelegramApiException {
        RobotTask robotTask = robotTaskService.getRobotTaskByTaskSort(5);
        if (null != robotTask) {
            String taskDescription = robotTask.getTaskDescription();
            if (ValidatorUtil.isTelegramLinkUrl(text)) {
                // 判断某一个分享的群是否在3天内被提交分享地址并且通过这些提交的分享地址审核成功15次或15次以上
                String[] shareGroupUrlArray = text.split("/");
                if (ArrayUtils.isNotEmpty(shareGroupUrlArray) && shareGroupUrlArray.length >= 4) {
                    String shareGroupUrlPrefix = shareGroupUrlArray[0] + "//" + shareGroupUrlArray[2] + "/" + shareGroupUrlArray[3];
                    boolean shareGroupUrlWhetherBan = chatLinkService.shareGroupUrlWhetherBan(shareGroupUrlPrefix);
                    if (shareGroupUrlWhetherBan) {
                        SendMessage message = new SendMessage();
                        message.setChatId(chatId);
                        message.setText("The shared group address " + shareGroupUrlPrefix + " has been disabled, please change to another group to share ");
                        execute(message);
                        return;
                    }
                    boolean shareGroupUrlWhetherOverTimesInGroup = chatLinkService.shareGroupUrlWhetherOverTimesInGroup(shareGroupUrlPrefix);
                    if (shareGroupUrlWhetherOverTimesInGroup) {
                        SendMessage message = new SendMessage();
                        message.setChatId(chatId);
                        message.setText("Link submission has been detected over " + TelegramCommonConsts.SHARE_GROUP_URL_MAX_TIMES + " times within 3 days by TP back end." +
                                "\n" +
                                "Please resubmit other blockchain group links");
                        execute(message);
                        return;
                    }
                }
                boolean whetherShareLink = whetherShareLinkToOtherGroup(chatId, text, 1);
                int shareGroupLinkCount = chatLinkService.getChatLinkCountByChatIdAndCategory(chatId, 1);
                if (shareGroupLinkCount < 3 || !whetherShareLink) {
                    handleFifthStepTask(chatId, taskDescription, shareGroupLinkCount);
                } else {
                    RobotChatFinishTask robotChatFinishTask = new RobotChatFinishTask();
                    robotChatFinishTask.setChatId(chatId);
                    robotChatFinishTask.setTaskId(robotTask.getId());
                    robotChatFinishTask.setSendPhoto(1);
                    robotTaskService.insertRobotChatFinishTask(robotChatFinishTask);
                    handleSixthTask(chatId);
                }
            } else {
                handleFifthStepTask(chatId, taskDescription, chatLinkService.getChatLinkCountByChatIdAndCategory(chatId, 1));
            }
        }
    }

    /**
     * 处理第6个任务
     *
     * @param chatId 聊天id
     * @throws TelegramApiException 异常
     */
    private void handleSixthTask(String chatId) throws TelegramApiException {
        RobotTask robotTask = robotTaskService.getRobotTaskByTaskSort(6);
        if (null != robotTask) {
            // 判断用户是否发送了最低四张截图图片
            boolean whetherFinishSendCheckPhoto = robotTaskCheckService.whetherFinishSendCheckPhoto(chatId);
            if (whetherFinishSendCheckPhoto) {
                RobotChatFinishTask robotChatFinishTask = new RobotChatFinishTask();
                robotChatFinishTask.setChatId(chatId);
                robotChatFinishTask.setTaskId(robotTask.getId());
                robotTaskService.insertRobotChatFinishTask(robotChatFinishTask);
                sendLastMessage(chatId);
            } else {
                handleSixthStepTask(chatId, robotTask.getTaskDescription());
            }
        }
    }

    /**
     * 发送最后一个提醒加群截图的消息
     *
     * @param chatId 聊天id
     * @throws TelegramApiException telegram相关异常
     */
    private void sendLastMessage(String chatId) throws TelegramApiException {
        sendCongratulationMessage(chatId);
    }

    /**
     * 提醒用户发送手机号
     *
     * @param chatId 聊天id
     * @throws TelegramApiException telegram相关异常
     */
    private void sendPhone(String chatId) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(robotSystemService.getSystemValueBySystemKey(SystemKeyEnum.SEND_PHONE_MESSAGE.name()));
        sendMessage.setParseMode(ParseMode.HTML);
        execute(sendMessage);
    }

    /**
     * 处理第4步任务
     *
     * @param chatId          聊天id
     * @param taskDescription 任务描述
     * @throws TelegramApiException 异常
     */
    private void handleFourthStepTask(String chatId, String taskDescription) throws TelegramApiException {
        if (StringUtils.isBlank(taskDescription)) {
            RobotTask robotTask = robotTaskService.getRobotTaskByTaskSort(4);
            if (null != robotTask) {
                taskDescription = robotTask.getTaskDescription();
            }
        }
        SendMessage fourthTaskMessage = new SendMessage();
        fourthTaskMessage.setParseMode(ParseMode.HTML);
        fourthTaskMessage.setChatId(chatId);
        fourthTaskMessage.setText(taskDescription);
        // 增加内联按钮
        List<List<InlineKeyboardButton>> keyboardList = new ArrayList<>();
        List<InlineKeyboardButton> keyboardButtons = new ArrayList<>();
        // 此对象表示嵌入式键盘的一个按钮，您必须完全使用可选字段之一
        InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
        // 可选的。按下按钮时将打开HTTP或tg:// URL
        inlineKeyboardButton.setUrl("https://twitter.com/Tokenswap_DEX/status/1400423838886682624");
        // 在按钮上标记文本
        inlineKeyboardButton.setText("Open Twitter");
        keyboardButtons.add(inlineKeyboardButton);
        keyboardList.add(keyboardButtons);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        // 按钮行数组，每个行由一个InlineKeyboardButton对象数组表示，这仅适用于2016年4月9日之后发布的电报版本。较旧的客户端将显示不受支持的消息。
        inlineKeyboardMarkup.setKeyboard(keyboardList);
        // 可选的。 消息附带的嵌入式键盘。 login_url按钮表示为普通url按钮。
        fourthTaskMessage.setReplyMarkup(inlineKeyboardMarkup);

        execute(fourthTaskMessage);

    }

    /**
     * 处理第5步任务
     *
     * @param chatId              聊天id
     * @param taskDescription     任务描述
     * @param shareGroupLinkCount 分享群的次数
     * @throws TelegramApiException 异常
     */
    private void handleFifthStepTask(String chatId, String taskDescription, Integer shareGroupLinkCount) throws TelegramApiException {
        SendMessage fifthTaskMessage = new SendMessage();
        fifthTaskMessage.setChatId(chatId);
        if (null == shareGroupLinkCount) {
            shareGroupLinkCount = 0;
        }
        shareGroupLinkCount += 1;
        fifthTaskMessage.setText(taskDescription + "(" + shareGroupLinkCount + "/3" + ")");
        execute(fifthTaskMessage);
    }

    /**
     * 处理第6步任务
     *
     * @param chatId          聊天id
     * @param taskDescription 任务描述
     * @throws TelegramApiException 异常
     */
    private void handleSixthStepTask(String chatId, String taskDescription) throws TelegramApiException {
        SendMessage sixthTaskMessage = new SendMessage();
        sixthTaskMessage.setParseMode(ParseMode.HTML);
        sixthTaskMessage.setChatId(chatId);
        StringBuilder sb = new StringBuilder(1 << 7);
        sb.append(taskDescription);
        sb.append("\n");
        sb.append("\n");
        sb.append("Do not send compressed images,otherwise it will be invalid.");
        sb.append("\n");
        sb.append("Please cancel compress and resend.");
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setParseMode(ParseMode.HTML);
        String imageUrl = "<a href=" + "\"" + checkImageUrl + "\"" + "> </a>";
        String text = imageUrl + "\r\n" + sb.toString();
        sixthTaskMessage.setText(text);
        // 增加内联按钮
        List<List<InlineKeyboardButton>> keyboardList = new ArrayList<>();
        List<InlineKeyboardButton> keyboardButtons = new ArrayList<>();
        // 此对象表示嵌入式键盘的一个按钮，您必须完全使用可选字段之一
        InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
        // 可选的。按下按钮时将打开HTTP或tg:// URL
        inlineKeyboardButton.setUrl(listenJoinGroupUrlPrefix + listenJoinGroupName);
        // 在按钮上标记文本
        inlineKeyboardButton.setText("Join");
        keyboardButtons.add(inlineKeyboardButton);
        keyboardList.add(keyboardButtons);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        // 按钮行数组，每个行由一个InlineKeyboardButton对象数组表示，这仅适用于2016年4月9日之后发布的电报版本。较旧的客户端将显示不受支持的消息。
        inlineKeyboardMarkup.setKeyboard(keyboardList);
        // 可选的。 消息附带的嵌入式键盘。 login_url按钮表示为普通url按钮。
        sixthTaskMessage.setReplyMarkup(inlineKeyboardMarkup);
        execute(sixthTaskMessage);
    }

    /**
     * 返回到菜单页面
     *
     * @param chatId 聊天id
     */
    private void backMenuPage(String chatId) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText("Please check the menu below, click the relevant button to complete the operation");
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        //按钮行数组，每个行由一个KeyboardButton对象数组表示
        List<KeyboardRow> keyboard = new ArrayList<>();
        // 第一行
        KeyboardRow keyboardRow1 = new KeyboardRow();
        // 第一行的两个按钮
        keyboardRow1.add(menuFirstButtonText);
        keyboardRow1.add(menuSecondButtonText);
        keyboard.add(keyboardRow1);
        // 第二行
        KeyboardRow keyboardRow2 = new KeyboardRow();
        // 第一行的两个按钮
        keyboardRow2.add(menuThirdButtonText);
        keyboardRow2.add(menuFourthButtonText);
        keyboard.add(keyboardRow2);
        replyKeyboardMarkup.setKeyboard(keyboard);
        // 可选的。请求客户垂直调整键盘大小以达到最佳适合度（例如，如果只有两行按钮，则使键盘变小）。默认为false，在这种情况下，自定义键盘的高度始终与应用程序的标准键盘相同。
        replyKeyboardMarkup.setResizeKeyboard(true);
        // 可选的。如果只想向特定用户显示键盘，请使用此参数。目标：1）在Message对象的文本中@提及的用户； 2）如果机器人的消息是回复（具有reply_to_message_id），则为原始消息的发送者。
        replyKeyboardMarkup.setSelective(true);
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        execute(sendMessage);
    }

    /**
     * 返回到Earn tp页面
     *
     * @param chatId 聊天id
     */
    private void backEarnTpMenuPage(String chatId) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText("Please check the menu below, click the relevant button to complete the operation");
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        //按钮行数组，每个行由一个KeyboardButton对象数组表示
        List<KeyboardRow> keyboard = new ArrayList<>();
        // 第一行
        KeyboardRow keyboardRow1 = new KeyboardRow();
        // 第一行的两个按钮
        keyboardRow1.add(earnTpFirstButtonText);
        keyboardRow1.add(earnTpSecondButtonText);
        keyboard.add(keyboardRow1);
        // 第二行
        KeyboardRow keyboardRow2 = new KeyboardRow();
        // 第一行的两个按钮
        keyboardRow2.add(earnTpThirdButtonText);
        keyboardRow2.add(replyButtonText);
        keyboard.add(keyboardRow2);
        replyKeyboardMarkup.setKeyboard(keyboard);
        // 可选的。请求客户垂直调整键盘大小以达到最佳适合度（例如，如果只有两行按钮，则使键盘变小）。默认为false，在这种情况下，自定义键盘的高度始终与应用程序的标准键盘相同。
        replyKeyboardMarkup.setResizeKeyboard(true);
        // 可选的。如果只想向特定用户显示键盘，请使用此参数。目标：1）在Message对象的文本中@提及的用户； 2）如果机器人的消息是回复（具有reply_to_message_id），则为原始消息的发送者。
        replyKeyboardMarkup.setSelective(true);
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        execute(sendMessage);
    }

    /**
     * 发送校验奖励的信息
     *
     * @param chatId 聊天id
     */
    private void sendCongratulationMessage(String chatId) throws TelegramApiException {
        StringBuilder sb = new StringBuilder(1 << 7);
        sb.append(taskFinishMessage);
        sb.append("\n");
        sb.append("\n");
        sb.append("TPs distribution will be within 2days. Please do not repeat relevant question.");
        sb.append("\n");
        sb.append(" We won’t respond it any more.");
        sb.append("\n");
        sb.append("\n");
        sb.append("More referrals, more earnings. Remunerations are distributed in line with tasks-based referral links. Specifically, ");
        sb.append("\n");
        sb.append("1.If one has recommended ≤100 referees to successfully complete TP tasks, then he can get excess bonuses, the calculation 100TPs/head.");
        sb.append("\n");
        sb.append("2.If one has recommended ≥200 referees to successfully complete TP tasks, then he can get excess bonuses, the calculation 200TPs/head.");
        sb.append("\n");
        sb.append("\n");
        sb.append("\uD83D\uDD17 Referral link: ");
        String coinAddr = verifiedAddrService.getCoinAddrByChatId(chatId);
        String airdropLink = secondTaskAirdropLink + coinAddr;
        sb.append(airdropLink);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(sb.toString());
        sendMessage.setParseMode(ParseMode.HTML);
        execute(sendMessage);
    }

    /**
     * 发送我的奖励的信息
     *
     * @param chatId 聊天id
     */
    private void sendMyRewardsMessage(String chatId) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        StringBuilder sb = new StringBuilder(1 << 5);
        sb.append("\uD83D\uDC64 User: ");
        String userName = chatMemberGroupService.getUserNameByChatId(chatId);
        if (!StringUtils.isBlank(userName)) {
            sb.append(userName);
        }
        sb.append("\n");
        // 完成所有的任务获取的TP
        sb.append("\uD83D\uDCB0 Airdrop Balance: ");
        String finishTaskReward = robotSystemService.getSystemValueBySystemKey(RewardKeyEnum.FINISH_TASK_REWARD.name());
        if (StringUtils.isBlank(finishTaskReward)) {
            finishTaskReward = "0";
        }
        sb.append(finishTaskReward);
        sb.append(" TP");
        sb.append("\n");
        sb.append("\uD83D\uDCB0 BZPlan Balance: ");
        boolean existBzPlanRobotRewardFlow = robotRewardFlowService.existBzRewardFlowByWalletAddressAndAmount(verifiedAddrService.getCoinAddrByChatId(chatId), CommonConsts.BZ_REWARD);
        String bzPlanReward;
        if (!existBzPlanRobotRewardFlow) {
            bzPlanReward = "0";
        } else {
            bzPlanReward = CommonConsts.BZ_REWARD;
        }
        sb.append(bzPlanReward);
        sb.append(" TP");
        sb.append("\n");
        // 邀请好友获取的TP（上限100TP，下限为0TP）
        sb.append("\uD83D\uDD17 Referral Balance: ");
        int invitedFriendsRewards = robotInviteService.getInvitePersonRewardsByChatId(chatId);
        sb.append(invitedFriendsRewards);
        sb.append(" TP");
        sb.append("\n");
        sb.append("Total invitations: ");
        sb.append(robotInviteService.getInvitePersonsByChatId(chatId, 0));
        sb.append("\n");
        sb.append("Received rewards: ");
        sb.append(robotTaskCheckService.getUsableReceiveReward(chatId));
        sb.append(" TP");
        sb.append("\n");
        sb.append("\n");
        sb.append("\uD83D\uDCD4 Your Submitted Data:");
        sb.append("\n");
        sb.append("Twitter: ");
        // 获取推特账号
        String twitterName = chatTwitterService.getTwitterNameByChatId(chatId);
        if (!StringUtils.isBlank(twitterName)) {
            sb.append(twitterName);
        }
        sb.append("\n");
        sb.append("Wallet: ");
        String walletAddress = verifiedAddrService.getCoinAddrByChatId(chatId);
        if (!StringUtils.isBlank(twitterName)) {
            sb.append(walletAddress);
        }
        sb.append("\n");
        sb.append("Message link: ");
        List<String> groupShareUrlList = chatLinkService.getChatLinkListByChatIdAndLinkCategory(chatId, 1);
        if (!CollectionUtils.isEmpty(groupShareUrlList)) {
            sb.append(groupShareUrlList.get(0));
        }
        sb.append("\n");
        sb.append("Retweet link: ");
        List<String> retweetUrlList = chatLinkService.getChatLinkListByChatIdAndLinkCategory(chatId, 2);
        if (!CollectionUtils.isEmpty(retweetUrlList)) {
            sb.append(retweetUrlList.get(0));
        }
        sb.append("\n");
        sb.append("\n");
        sb.append("Note: If your submitted data wrong then you can go back and resubmit the data again");
        sb.append("\n");
        sb.append("before airdrop ends.");
        String tpRewards = sb.toString();
        sendMessage.setText(tpRewards);
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        //按钮行数组，每个行由一个KeyboardButton对象数组表示
        List<KeyboardRow> keyboard = new ArrayList<>();
        // 第一行
        KeyboardRow keyboardRow1 = new KeyboardRow();
        // 第一行的2个按钮
        keyboardRow1.add(RECEIVE_INCOME);
        keyboardRow1.add(replyButtonText);
        keyboard.add(keyboardRow1);
        replyKeyboardMarkup.setKeyboard(keyboard);
        // 可选的。请求客户垂直调整键盘大小以达到最佳适合度（例如，如果只有两行按钮，则使键盘变小）。默认为false，在这种情况下，自定义键盘的高度始终与应用程序的标准键盘相同。
        replyKeyboardMarkup.setResizeKeyboard(true);
        // 可选的。如果只想向特定用户显示键盘，请使用此参数。目标：1）在Message对象的文本中@提及的用户； 2）如果机器人的消息是回复（具有reply_to_message_id），则为原始消息的发送者。
        replyKeyboardMarkup.setSelective(true);
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        execute(sendMessage);
    }

    /**
     * 发送Tp的详细信息
     *
     * @param chatId 聊天id
     * @throws TelegramApiException
     */
    private void sendTpDetailMessage(String chatId) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        StringBuilder sb = new StringBuilder(1 << 4);
        sb.append("Tokenswap, a dex based on Whitecoin crosschain");
        sb.append("\n");
        sb.append("ecology, can realize the full asset exchange of");
        sb.append("\n");
        sb.append("Whitecoin (XWC) ecological assets and other");
        sb.append("\n");
        sb.append("cross-chain assets like BTC, ETH, TRX, EOS. TP");
        sb.append("\n");
        sb.append("holders can enjoy the rights and benefits from");
        sb.append("\n");
        sb.append("governing the whole ecology.");
        String tpDetail = sb.toString();
        sendMessage.setText(tpDetail);
        execute(sendMessage);
    }

    /**
     * 发送Tp下载的详细信息
     *
     * @param chatId 聊天id
     * @throws TelegramApiException
     */
    private void sendTpDownloadMessage(String chatId) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        StringBuilder sb = new StringBuilder(1 << 3);
        sb.append("All the rewards will be sent to your");
        sb.append("\n");
        sb.append("TokenSwap directly.");
        sb.append("\n");
        sb.append("Pls install Tokenswap mobile app and login");
        String tpDownload = sb.toString();
        sendMessage.setText(tpDownload);
        // 增加内联按钮
        List<List<InlineKeyboardButton>> keyboardList = new ArrayList<>();
        List<InlineKeyboardButton> keyboardButtons = new ArrayList<>();
        // 此对象表示嵌入式键盘的一个按钮，您必须完全使用可选字段之一
        InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
        // 可选的。按下按钮时将打开HTTP或tg:// URL
        inlineKeyboardButton.setUrl("https://download.tokenswap.info/?t=download");
        // 在按钮上标记文本
        inlineKeyboardButton.setText("Install the app");
        keyboardButtons.add(inlineKeyboardButton);
        keyboardList.add(keyboardButtons);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        // 按钮行数组，每个行由一个InlineKeyboardButton对象数组表示，这仅适用于2016年4月9日之后发布的电报版本。较旧的客户端将显示不受支持的消息。
        inlineKeyboardMarkup.setKeyboard(keyboardList);
        // 可选的。 消息附带的嵌入式键盘。 login_url按钮表示为普通url按钮。
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        execute(sendMessage);
    }

}
