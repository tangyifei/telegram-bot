package com.code.telegram.runners;

import com.code.telegram.bots.JoinGroupCollectPictureBot;
import com.code.telegram.bots.TokenSwapTaskRobot;
import com.code.telegram.configs.redis.RedisHandler;
import com.code.telegram.managers.HttpManager;
import com.code.telegram.services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import javax.annotation.Resource;

/**
 * 注册机器人
 *
 * @author xiaoyaowang
 */
@Component
public class RobotRunner implements ApplicationRunner {

    @Value("${first.task.text}")
    private String firstTaskText;

    @Value("${reply.button.text}")
    private String replyButtonText;

    @Value("${reply.done.text}")
    private String replyDoneText;

    @Value("${error.text}")
    private String errorText;

    @Value("${menu.first-button-text}")
    private String menuFirstButtonText;

    @Value("${menu.second-button-text}")
    private String menuSecondButtonText;

    @Value("${menu.third-button-text}")
    private String menuThirdButtonText;

    @Value("${menu.fourth-button-text}")
    private String menuFourthButtonText;

    @Value("${earn.tp.first-button-text}")
    private String earnTpFirstButtonText;

    @Value("${earn.tp.second-button-text}")
    private String earnTpSecondButtonText;

    @Value("${earn.tp.third-button-text}")
    private String earnTpThirdButtonText;

    @Value("${task.finish.message}")
    private String taskFinishMessage;

    @Value("${second.task.image-url}")
    private String secondTaskImageUrl;

    @Value("${check.image-url}")
    private String checkImageUrl;

    @Value("${second.task.finish}")
    private String secondTaskFinish;

    @Value("${second.task.airdrop.link}")
    private String secondTaskAirdropLink;

    @Value("${upload.url}")
    private String uploadUrl;

    @Value("${upload.path}")
    private String uploadPath;

    @Resource
    private RobotChatService robotChatService;

    @Resource
    private RobotTaskCheckService robotTaskCheckService;

    @Resource
    private RobotTaskService robotTaskService;

    @Resource
    private ChatMemberGroupService chatMemberGroupService;

    @Resource
    private VerifiedAddrService verifiedAddrService;

    @Resource
    private RobotInviteService robotInviteService;

    @Resource
    private ChatLinkService chatLinkService;

    @Resource
    private ChatBanService chatBanService;

    @Resource
    private RobotSystemService robotSystemService;

    @Resource
    RobotRewardFlowService robotRewardFlowService;

    @Resource
    SmsService smsService;

    @Resource
    private HttpManager httpManager;

    @Resource
    private RedisHandler redisHandler;

    @Resource
    private ChatTwitterService chatTwitterService;

    @Value("${listen.join.group.bot}")
    private String listenJoinGroupBot;

    @Value("${listen.join.group.token}")
    private String listenJoinGroupToken;

    @Value("${execute.task.bot.name}")
    private String taskBotName;

    @Value("${execute.task.bot.token}")
    private String taskBotToken;

    @Value("${listen.join.group.url-prefix}")
    private String listenJoinGroupUrlPrefix;

    @Value("${listen.join.group.name}")
    private String listenJoinGroupName;

    @Value("${listen.chat.messages.group.name}")
    private String listenChatMessagesGroupName;

    private static Logger logger = LoggerFactory.getLogger(RobotRunner.class);

    @Override
    public void run(ApplicationArguments args) {
        // 电报机器人配置
        try {
            TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);

            TokenSwapTaskRobot tokenSwapTaskRobot = new TokenSwapTaskRobot();
            tokenSwapTaskRobot.setRobotName(taskBotName);
            tokenSwapTaskRobot.setRobotToken(taskBotToken);
            tokenSwapTaskRobot.setListenJoinGroupUrlPrefix(listenJoinGroupUrlPrefix);
            tokenSwapTaskRobot.setListenJoinGroupName(listenJoinGroupName);
            tokenSwapTaskRobot.setChatMemberGroupService(chatMemberGroupService);
            tokenSwapTaskRobot.setRobotChatService(robotChatService);
            tokenSwapTaskRobot.setHttpManager(httpManager);
            tokenSwapTaskRobot.setVerifiedAddrService(verifiedAddrService);
            tokenSwapTaskRobot.setChatTwitterService(chatTwitterService);
            tokenSwapTaskRobot.setUploadUrl(uploadUrl);
            tokenSwapTaskRobot.setUploadPath(uploadPath);
            tokenSwapTaskRobot.setCheckImageUrl(checkImageUrl);
            tokenSwapTaskRobot.setRobotTaskService(robotTaskService);
            tokenSwapTaskRobot.setRobotTaskCheckService(robotTaskCheckService);
            tokenSwapTaskRobot.setRobotSystemService(robotSystemService);
            tokenSwapTaskRobot.setChatBanService(chatBanService);
            tokenSwapTaskRobot.setRobotRewardFlowService(robotRewardFlowService);
            tokenSwapTaskRobot.setSecondTaskAirdropLink(secondTaskAirdropLink);
            tokenSwapTaskRobot.setRobotInviteService(robotInviteService);
            tokenSwapTaskRobot.setFirstTaskText(firstTaskText);
            tokenSwapTaskRobot.setReplyButtonText(replyButtonText);
            tokenSwapTaskRobot.setErrorText(errorText);
            tokenSwapTaskRobot.setMenuFirstButtonText(menuFirstButtonText);
            tokenSwapTaskRobot.setMenuSecondButtonText(menuSecondButtonText);
            tokenSwapTaskRobot.setMenuThirdButtonText(menuThirdButtonText);
            tokenSwapTaskRobot.setMenuFourthButtonText(menuFourthButtonText);
            tokenSwapTaskRobot.setEarnTpFirstButtonText(earnTpFirstButtonText);
            tokenSwapTaskRobot.setEarnTpSecondButtonText(earnTpSecondButtonText);
            tokenSwapTaskRobot.setEarnTpThirdButtonText(earnTpThirdButtonText);
            tokenSwapTaskRobot.setTaskFinishMessage(taskFinishMessage);
            tokenSwapTaskRobot.setReplyDoneText(replyDoneText);
            tokenSwapTaskRobot.setSecondTaskImageUrl(secondTaskImageUrl);
            tokenSwapTaskRobot.setSecondTaskFinish(secondTaskFinish);
            tokenSwapTaskRobot.setChatLinkService(chatLinkService);
            tokenSwapTaskRobot.setRedisHandler(redisHandler);
            tokenSwapTaskRobot.setSmsService(smsService);
            botsApi.registerBot(tokenSwapTaskRobot);
            logger.info("任务机器人已注册，名称:{} token:{}", taskBotName, taskBotToken);

            JoinGroupCollectPictureBot joinGroupCollectPictureBot = new JoinGroupCollectPictureBot();
            joinGroupCollectPictureBot.setRobotName(listenJoinGroupBot);
            joinGroupCollectPictureBot.setRobotToken(listenJoinGroupToken);
            joinGroupCollectPictureBot.setListenJoinGroupUrlPrefix(listenJoinGroupUrlPrefix);
            joinGroupCollectPictureBot.setListenJoinGroupName(listenJoinGroupName);
            joinGroupCollectPictureBot.setListenChatMessagesGroupPrefix(listenJoinGroupUrlPrefix);
            joinGroupCollectPictureBot.setListenChatMessagesGroupName(listenChatMessagesGroupName);
            joinGroupCollectPictureBot.setChatMemberGroupService(chatMemberGroupService);
            joinGroupCollectPictureBot.setRobotChatService(robotChatService);
            joinGroupCollectPictureBot.setHttpManager(httpManager);
            joinGroupCollectPictureBot.setVerifiedAddrService(verifiedAddrService);
            joinGroupCollectPictureBot.setUploadUrl(uploadUrl);
            joinGroupCollectPictureBot.setUploadPath(uploadPath);
            joinGroupCollectPictureBot.setRobotTaskService(robotTaskService);
            joinGroupCollectPictureBot.setRobotTaskCheckService(robotTaskCheckService);
            joinGroupCollectPictureBot.setRobotSystemService(robotSystemService);
            joinGroupCollectPictureBot.setChatBanService(chatBanService);
            joinGroupCollectPictureBot.setRobotRewardFlowService(robotRewardFlowService);
            botsApi.registerBot(joinGroupCollectPictureBot);
            logger.info("监听加群机器人已注册，名称:{} token:{}", listenJoinGroupBot, listenJoinGroupToken);

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

}
