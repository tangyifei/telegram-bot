package com.code.telegram.models;

import com.code.models.robot.BaseRobot;

/**
 * 聊天手机号记录实体类
 *
 * @author xiaoyaowang
 */
public class ChatPhone extends BaseRobot {

    private static final long serialVersionUID = -3621020940477746864L;

    private String chatId;

    private String phone;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "ChatPhone{" +
                "chatId='" + chatId + '\'' +
                ", phone='" + phone + '\'' +
                "} " + super.toString();
    }
}
