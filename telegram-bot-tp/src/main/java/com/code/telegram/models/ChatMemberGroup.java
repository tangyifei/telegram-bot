package com.code.telegram.models;

import com.code.models.robot.BaseRobot;

/**
 * 新人加入的群组记录实体类
 *
 * @author xiaoyaowang
 */
public class ChatMemberGroup extends BaseRobot {

    private static final long serialVersionUID = 2867606225292927033L;

    private String chatId;

    private String groupUrl;

    private String groupName;

    private String firstName;

    private String lastName;

    private Integer isBot;

    private String userName;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getGroupUrl() {
        return groupUrl;
    }

    public void setGroupUrl(String groupUrl) {
        this.groupUrl = groupUrl;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getIsBot() {
        return isBot;
    }

    public void setIsBot(Integer isBot) {
        this.isBot = isBot;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "ChatMemberGroup{" +
                "chatId='" + chatId + '\'' +
                ", groupUrl='" + groupUrl + '\'' +
                ", groupName='" + groupName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", isBot=" + isBot +
                ", userName='" + userName + '\'' +
                "} " + super.toString();
    }
}
