package com.code.telegram.models;

import com.code.models.robot.BaseRobot;

/**
 * 已空投记录
 *
 * @author xiaoyaowang
 */
public class AirdropRecord extends BaseRobot {

    private static final long serialVersionUID = 776516229102293930L;

    private String walletAddr;

    private String telegramUserName;

    private String twitterAccount;

    private String tpReward;

    public String getWalletAddr() {
        return walletAddr;
    }

    public void setWalletAddr(String walletAddr) {
        this.walletAddr = walletAddr;
    }

    public String getTelegramUserName() {
        return telegramUserName;
    }

    public void setTelegramUserName(String telegramUserName) {
        this.telegramUserName = telegramUserName;
    }

    public String getTwitterAccount() {
        return twitterAccount;
    }

    public void setTwitterAccount(String twitterAccount) {
        this.twitterAccount = twitterAccount;
    }

    public String getTpReward() {
        return tpReward;
    }

    public void setTpReward(String tpReward) {
        this.tpReward = tpReward;
    }

    @Override
    public String toString() {
        return "AirdropRecord{" +
                "walletAddr='" + walletAddr + '\'' +
                ", telegramUserName='" + telegramUserName + '\'' +
                ", twitterAccount='" + twitterAccount + '\'' +
                ", tpReward='" + tpReward + '\'' +
                "} " + super.toString();
    }
}
