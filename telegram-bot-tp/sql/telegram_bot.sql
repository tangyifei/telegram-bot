/*
 Navicat Premium Data Transfer

 Source Server         : 空投机器人线上环境
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : rm-j6ca3jeeni2t070ln.mysql.rds.aliyuncs.com:3306
 Source Schema         : telegram_bot

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 08/09/2021 10:59:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for airdrop_record
-- ----------------------------
DROP TABLE IF EXISTS `airdrop_record`;
CREATE TABLE `airdrop_record`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `wallet_addr` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '钱包地址',
  `telegram_user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'telegram账号',
  `twitter_account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '推特账号',
  `tp_reward` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'tp奖励',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL,
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_wallet_addr`(`wallet_addr`) USING BTREE COMMENT '钱包地址唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for chat_ban
-- ----------------------------
DROP TABLE IF EXISTS `chat_ban`;
CREATE TABLE `chat_ban`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天唯一id',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_delete`(`chat_id`, `deleted`) USING BTREE COMMENT '聊天唯一标识和删除状态的唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for chat_link
-- ----------------------------
DROP TABLE IF EXISTS `chat_link`;
CREATE TABLE `chat_link`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天唯一id',
  `share_link` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分享连接',
  `link_category` tinyint(1) NOT NULL COMMENT '1-群分享链接 2-推特链接',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_share_link`(`chat_id`, `share_link`) USING BTREE COMMENT '聊天id和链接联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 15552 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for chat_member_group
-- ----------------------------
DROP TABLE IF EXISTS `chat_member_group`;
CREATE TABLE `chat_member_group`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天唯一标识',
  `group_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '相关的群的url',
  `group_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '群组的名字',
  `first_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '新人的first_name',
  `last_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '新人的last_name',
  `is_bot` tinyint(1) NOT NULL COMMENT '是否是bot（0-不是 1-是）',
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_deleted`(`chat_id`, `deleted`) USING BTREE COMMENT '聊天id和删除状态联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 8900 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for chat_twitter
-- ----------------------------
DROP TABLE IF EXISTS `chat_twitter`;
CREATE TABLE `chat_twitter`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天id',
  `twitter_account_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '推特账号',
  `deleted` tinyint(1) NOT NULL COMMENT '删除状态 0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_account_name`(`chat_id`, `twitter_account_name`, `deleted`) USING BTREE COMMENT '聊天id和推特账户名联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 9795 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for focus_on_twitter_failure
-- ----------------------------
DROP TABLE IF EXISTS `focus_on_twitter_failure`;
CREATE TABLE `focus_on_twitter_failure`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天唯一标识',
  `twitter_account_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '推特账号',
  `wallet_addr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '钱包地址',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_deleted`(`chat_id`, `deleted`) USING BTREE COMMENT '聊天标识和删除状态唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 8934 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for login_credential
-- ----------------------------
DROP TABLE IF EXISTS `login_credential`;
CREATE TABLE `login_credential`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
  `account` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号',
  `pwd` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录密码',
  `random_salt` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '随机盐',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '登录凭证' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_chat
-- ----------------------------
DROP TABLE IF EXISTS `robot_chat`;
CREATE TABLE `robot_chat`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天唯一标识',
  `robot_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '机器人用户名',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9233 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_chat_finish_task
-- ----------------------------
DROP TABLE IF EXISTS `robot_chat_finish_task`;
CREATE TABLE `robot_chat_finish_task`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天id',
  `task_id` int(11) NOT NULL COMMENT '任务主键',
  `deleted` tinyint(1) NOT NULL COMMENT '删除状态 0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_task_id`(`chat_id`, `task_id`, `deleted`) USING BTREE COMMENT '聊天id和任务id删除状态的联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 49434 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_invite
-- ----------------------------
DROP TABLE IF EXISTS `robot_invite`;
CREATE TABLE `robot_invite`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `invite_chat_id` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邀请人聊天id',
  `chat_id` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '被邀请人的聊天id',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7339 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_reward_flow
-- ----------------------------
DROP TABLE IF EXISTS `robot_reward_flow`;
CREATE TABLE `robot_reward_flow`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `serial_no` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '流水号',
  `wallet_address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '钱包地址',
  `amount` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '奖励金额',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_wallet_addr_reward`(`wallet_address`, `amount`, `deleted`) USING BTREE COMMENT '钱包地址和奖励的联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 12704 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_system
-- ----------------------------
DROP TABLE IF EXISTS `robot_system`;
CREATE TABLE `robot_system`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `system_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统key',
  `system_value` varchar(1500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统值',
  `remark` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

INSERT INTO `robot_system`(`id`, `system_key`, `system_value`, `remark`, `deleted`, `created_at`, `updated_at`) VALUES (1, 'INVITE_FRIENDS_FINISH_TASK_REWARD', '100', '邀请朋友获得的奖励', 0, '2021-07-13 10:38:20', '2021-07-12 15:34:10');
INSERT INTO `robot_system`(`id`, `system_key`, `system_value`, `remark`, `deleted`, `created_at`, `updated_at`) VALUES (2, 'JOIN_TG_GROUP_REWARD', '200', '加入TG群组获取的奖励', 0, '2021-07-12 15:34:40', '2021-07-12 15:34:43');
INSERT INTO `robot_system`(`id`, `system_key`, `system_value`, `remark`, `deleted`, `created_at`, `updated_at`) VALUES (3, 'ATTENTION_TOKEN_SWAP_TWITTER_REWARD', '100', '关注推特获得的奖励', 0, '2021-07-12 15:35:03', '2021-07-12 15:35:05');
INSERT INTO `robot_system`(`id`, `system_key`, `system_value`, `remark`, `deleted`, `created_at`, `updated_at`) VALUES (4, 'FINISH_TASK_REWARD', '400', '完成任务获得的奖励', 0, '2021-07-13 10:38:21', '2021-07-13 10:38:23');
INSERT INTO `robot_system`(`id`, `system_key`, `system_value`, `remark`, `deleted`, `created_at`, `updated_at`) VALUES (5, 'WITHDRAW_OFF', '0', '关闭提币', 0, '2021-07-23 20:03:27', '2021-07-13 17:51:00');
INSERT INTO `robot_system`(`id`, `system_key`, `system_value`, `remark`, `deleted`, `created_at`, `updated_at`) VALUES (6, 'EVERY_TASK_REWARD', '100', '每个任务的奖励', 0, '2021-07-15 18:02:22', '2021-07-15 18:02:25');
INSERT INTO `robot_system`(`id`, `system_key`, `system_value`, `remark`, `deleted`, `created_at`, `updated_at`) VALUES (7, 'INVITE_FRIEND_UP_LIMIT', '100', '邀请好友的获得的奖励上限', 0, '2021-07-15 18:18:21', '2021-07-15 18:18:13');
INSERT INTO `robot_system`(`id`, `system_key`, `system_value`, `remark`, `deleted`, `created_at`, `updated_at`) VALUES (8, 'MANUAL_FINISH_FOURTH_TASK', '1', '手动完成第四步任务与否', 0, '2021-07-30 09:40:08', '2021-07-29 08:02:17');
INSERT INTO `robot_system`(`id`, `system_key`, `system_value`, `remark`, `deleted`, `created_at`, `updated_at`) VALUES (9, 'PHOTO_DOWN_LIMIT', '4', '发送的截图最低限制', 0, '2021-07-29 12:00:30', '2021-07-29 12:00:32');
INSERT INTO `robot_system`(`id`, `system_key`, `system_value`, `remark`, `deleted`, `created_at`, `updated_at`) VALUES (10, 'SEND_PHOTO_TASK', 'Task is not qualified. Please check your tasks and submit them again.\r\n\r\nUnqualified reasons:\r\nInformation on twitter account, screenshot and telegram ID is inconsistent \r\nForwarded groups are poor and valueless, or forwarding contents are substandard\r\nParticipants have unfollowed and deleted the retweet. \r\nTracks that share to the groups fail to be detected.\r\nYour account is suspicious of falsification.', '审核不合格主动向用户发送的信息', 1, '2021-07-29 15:32:29', '2021-07-29 15:32:31');
INSERT INTO `robot_system`(`id`, `system_key`, `system_value`, `remark`, `deleted`, `created_at`, `updated_at`) VALUES (11, 'BAN', 'Account is banned for cheating\r\nIf you have any questions, please contact the admini for ask.\r\n', '用户被封禁信息', 0, '2021-08-11 16:17:29', '2021-08-11 16:17:31');
INSERT INTO `robot_system`(`id`, `system_key`, `system_value`, `remark`, `deleted`, `created_at`, `updated_at`) VALUES (12, 'STOP_ROBOT', '0', '暂停机器人执行任务', 0, '2021-08-16 17:31:24', '2021-09-07 11:43:10');
INSERT INTO `robot_system`(`id`, `system_key`, `system_value`, `remark`, `deleted`, `created_at`, `updated_at`) VALUES (13, 'STOP_ROBOT_MESSAGE', '🛠🛠🛠   Airdrop Bot Maintenance/Update  🛠🛠🛠\r\n\r\n▪️Airdrop campaigns may be tentatively suspended due to abrupt bot bugs or periodical iteration\r\n\r\n▪️Airdrop will be reopen later following completing maintenance and update\r\n\r\n▪️Airdrop is a long-term event while it will bring inescapably pending.\r\n\r\n▪️Airdrop tasks and procedures may be occasionally changed due to certain reasons\r\n\r\n▪️Any change will receive an instant announcement in TokenSwap EN Group.\r\n\r\n*Feel free to contact our group t.me/TP_EN1 when there is any question', '暂停机器人执行任务时发送的消息', 0, '2021-08-16 17:31:41', '2021-08-16 17:31:43');

-- ----------------------------
-- Table structure for robot_task
-- ----------------------------
DROP TABLE IF EXISTS `robot_task`;
CREATE TABLE `robot_task`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `task_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务标题',
  `task_description` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务描述',
  `task_sort` tinyint(2) NOT NULL COMMENT '任务序号',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

INSERT INTO `robot_task`(`id`, `task_title`, `task_description`, `task_sort`, `deleted`, `created_at`, `updated_at`) VALUES (1, 'verify_wallet_address', '🔹 Submit your TokenSwap Wallet Address', 1, 0, '2021-07-14 18:20:53', '2021-06-24 09:07:26');
INSERT INTO `robot_task`(`id`, `task_title`, `task_description`, `task_sort`, `deleted`, `created_at`, `updated_at`) VALUES (2, 'Share 5 coin circle groups', 'How to get TPs? \r\nTask 1: Join <a href=\"https://t.me/TP_AD\">Tokenswap Telegram Airdrop Group</a>\r\nTask 2: Follow <a href=\"https://twitter.com/Tokenswap_DEX/status/1400423838886682624\">Tokenswap Twitter</a> & Retweet Pinned post with tagging 3 friends & Submit the link1\r\nTask 3: Send the poster content in the form of text to at least 3 TG blockchain groups & 3 Screenshots & Submit group message link2\r\n\r\nWhat’s TP airdrop rewarding?\r\n1. Every participant can get 400 TPs after succeeding in task-completing above.\r\n2. Referrer will gain another 100 TPs conditioned the referee succeeds in the tasks completion.  ', 2, 0, '2021-07-22 18:24:41', '2021-06-24 09:19:09');
INSERT INTO `robot_task`(`id`, `task_title`, `task_description`, `task_sort`, `deleted`, `created_at`, `updated_at`) VALUES (3, 'Join our Telegram Group', 'Join Tokensawp Telegram Group.', 3, 0, '2021-07-15 17:01:05', '2021-06-25 17:04:01');
INSERT INTO `robot_task`(`id`, `task_title`, `task_description`, `task_sort`, `deleted`, `created_at`, `updated_at`) VALUES (4, 'Follow Tokenswap Twitter tag 3 friends and retweet the pinned tweet', 'Follow <a href=\"https://twitter.com/Tokenswap_DEX/status/1400423838886682624\">Tokenswap Twitter</a> & Retweet Pinned post with tagging 3 friends\r\nSubmit your twitter username (@username) & Submit your retweet link1 \r\n(submissions in respective dialog box)', 4, 0, '2021-07-14 14:02:51', '2021-06-28 10:08:27');
INSERT INTO `robot_task`(`id`, `task_title`, `task_description`, `task_sort`, `deleted`, `created_at`, `updated_at`) VALUES (5, 'Share Group Url', 'Submit one blockchain-group link2 featuring retweet texts.', 5, 0, '2021-08-27 10:54:42', '2021-08-27 10:54:44');
INSERT INTO `robot_task`(`id`, `task_title`, `task_description`, `task_sort`, `deleted`, `created_at`, `updated_at`) VALUES (6, 'Sent task screenshot to TP airdrop group', 'Send all 4 screenshots to <a href=\"https://t.me/TP_A\">TP Airdrop Group</a>. Do not compress screenshots.\r\nTips:\r\n/ 3 screenshots of 3 blockchain groups sharing\r\n/ 1 retweet screenshot \r\n/ Please text the poster content to different groups when sharing.', 6, 0, '2021-07-30 09:09:01', '2021-07-30 09:09:04');

-- ----------------------------
-- Table structure for robot_task_check
-- ----------------------------
DROP TABLE IF EXISTS `robot_task_check`;
CREATE TABLE `robot_task_check`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天id',
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `first_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名',
  `last_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓',
  `wallet_addr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '钱包地址',
  `state` tinyint(1) NOT NULL COMMENT '审核状态 0-未审核（待审核） 1-审核失败 2-审核成功 3-已封禁',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_wallet_addr`(`chat_id`, `wallet_addr`, `deleted`) USING BTREE COMMENT '聊天id和钱包地址删除状态联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 9628 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_task_check_content
-- ----------------------------
DROP TABLE IF EXISTS `robot_task_check_content`;
CREATE TABLE `robot_task_check_content`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `task_check_id` int(11) NOT NULL COMMENT '任务审核主键',
  `pic_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '提交的审核图片地址',
  `send_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '审核的内容',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37468 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
  `user_status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态ENABLED 启用 DISABLED 禁用',
  `is_admin` tinyint(1) UNSIGNED NOT NULL COMMENT '是否为超级管理员 0不是超级管理员  1超级管理员',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_user_id`(`id`) USING BTREE,
  UNIQUE INDEX `udx_phone`(`phone`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for verified_addr
-- ----------------------------
DROP TABLE IF EXISTS `verified_addr`;
CREATE TABLE `verified_addr`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天id',
  `coin_addr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '已经校验的钱包地址',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_coin_addr`(`chat_id`, `coin_addr`, `deleted`) USING BTREE COMMENT '聊天id和钱包地址联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 10363 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for verified_addr_0803
-- ----------------------------
DROP TABLE IF EXISTS `verified_addr_0803`;
CREATE TABLE `verified_addr_0803`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天id',
  `coin_addr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '已经校验的钱包地址',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_coin_addr`(`chat_id`, `coin_addr`, `deleted`) USING BTREE COMMENT '聊天id和钱包地址联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 1903 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
