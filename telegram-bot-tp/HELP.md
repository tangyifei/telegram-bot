# 设置 TokenSwap 空投机器人
[telegram-bot中文相关的文档](https://www.cnblogs.com/kainhuck/p/13576012.html)

[推特开发API文档](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/get-users-show)

[相关的按钮的简单实现python代码](https://dmesg.app/tgbot1.html)

## 1、设置机器人描述
首先搜索BotFather，然后输入指令/mybots，查看我注册的机器人，然后选择相关的机器人，点击Edit Description按钮，输入如下内容：

What can this bot do?
 
Earn up to $10,000 of free in TP Airdrops  
inviting friends and completing easy tasks.
 
Click start now!

这样就完成了机器人的相关描述。

## 2、判断用户是否加入相关的群组
这里首先向BotFather注册一个机器人，我这里是因为要加入TOKENSWAP GROUP群组，所以取名为TpAirdropGroupBot，这个机器人主要是针对TOKENSWAP GROUP群组中的新人进行监控，方便记录新人与群的绑定关系，
然后在做第三个任务的时候就可以判断用户是否加入某一个群组，注意相关的群组管理员一定要把TpAirdropGroupBot机器人加入群组中，并编辑
该机器人权限，这样任务机器人就可以根据TpAirdropGroupBot机器人的监控情况进行新人加群的判断。

如果发送信息的时候报如下错误：

```
org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException: Error sending message
```
则可能是发送的相关信息不符合telegram-bot的语法，自己参考[telegram-bot中文相关的文档](https://www.cnblogs.com/kainhuck/p/13576012.html)下面的Markdown和HTML格式说明。

如果修改本项目关联的数据库表中的相关字段，那么[机器人相关任务的后台](http://154.222.22.70:20080/whitecoin/telegram-bot.git)中的mapper文件中的表字段要同步更改。

## 3、部署
首先创建数据库telegram_bot，然后把telegram-bot-tp项目中的sql目录下面的脚本文件部署到该数据库中，然后使用maven命令分别对telegram-bot-tp和telegram-bot-manage打jar包，打完jar包后分别上传到服务器的
/data/server/telegram_bot/tp目录和/data/server/telegram_bot/manage目录，把telegram-bot-tp项目中的script目录中的application.yml文件和启动脚本server.sh文件上传到/data/server/telegram_bot/tp目录,
然后修改server.sh的java环境指向，使用chmod +x server.sh 设置server.sh的权限为可执行，最后执行./server.sh start启动telegram-bot-tp并进入logs目录查看日志看启动成功与否，复制server.sh到
/data/server/telegram_bot/manage目录，修改里面的jar包名称并替换nohup的内容为 nohup $JRE_HOME/bin/java -jar -Xms1024m -Xmx1024m $JAR_NAME  --spring.profiles.active=prod >/dev/null 2>&1 &即可，
然后使用chmod +x server.sh 设置server.sh的权限为可执行，最后执行./server.sh start启动telegram-bot-manage并进入logs目录查看日志看启动成功与否。




