/*
 Navicat Premium Data Transfer

 Source Server         : BZ新的数据库
 Source Server Type    : MySQL
 Source Server Version : 50735
 Source Host           : localhost:5009
 Source Schema         : telegram_bot_bz

 Target Server Type    : MySQL
 Target Server Version : 50735
 File Encoding         : 65001

 Date: 27/09/2021 09:27:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for airdrop_record
-- ----------------------------
DROP TABLE IF EXISTS `airdrop_record`;
CREATE TABLE `airdrop_record`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `wallet_addr` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '钱包地址',
  `telegram_user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'telegram账号',
  `twitter_account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '推特账号',
  `tp_reward` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'tp奖励',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11',
  `updated_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_wallet_addr`(`wallet_addr`) USING BTREE COMMENT '钱包地址唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '已空投记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bz_account
-- ----------------------------
DROP TABLE IF EXISTS `bz_account`;
CREATE TABLE `bz_account`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天id',
  `bz_account` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'bz账号',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_bz_account`(`chat_id`, `bz_account`, `deleted`) USING BTREE COMMENT '聊天id和账户联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 950 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '地址校验' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for chat_ban
-- ----------------------------
DROP TABLE IF EXISTS `chat_ban`;
CREATE TABLE `chat_ban`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天唯一id',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_delete`(`chat_id`, `deleted`) USING BTREE COMMENT '聊天唯一标识和删除状态的唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '聊天封禁' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for chat_link
-- ----------------------------
DROP TABLE IF EXISTS `chat_link`;
CREATE TABLE `chat_link`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天唯一id',
  `share_link` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分享连接',
  `link_category` tinyint(1) NOT NULL COMMENT '1-群分享链接 2-推特链接',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_share_link`(`chat_id`, `share_link`) USING BTREE COMMENT '聊天id和链接联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 163 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '聊天链接' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for chat_member_group
-- ----------------------------
DROP TABLE IF EXISTS `chat_member_group`;
CREATE TABLE `chat_member_group`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天唯一标识',
  `group_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '相关的群的url',
  `group_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '群组的名字',
  `first_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '新人的first_name',
  `last_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '新人的last_name',
  `is_bot` tinyint(1) NOT NULL COMMENT '是否是bot（0-不是 1-是）',
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_group_deleted`(`chat_id`, `group_url`, `deleted`) USING BTREE COMMENT '聊天id、群组链接和删除状态联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 321 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '加群记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for chat_twitter
-- ----------------------------
DROP TABLE IF EXISTS `chat_twitter`;
CREATE TABLE `chat_twitter`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天id',
  `twitter_account_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '推特账号',
  `deleted` tinyint(1) NOT NULL COMMENT '删除状态 0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_account_name`(`chat_id`, `twitter_account_name`, `deleted`) USING BTREE COMMENT '聊天id和推特账户名联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 755 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '关注推特记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for focus_on_twitter_failure
-- ----------------------------
DROP TABLE IF EXISTS `focus_on_twitter_failure`;
CREATE TABLE `focus_on_twitter_failure`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天唯一标识',
  `twitter_account_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '推特账号',
  `bz_account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'bz账号',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_deleted`(`chat_id`, `deleted`) USING BTREE COMMENT '聊天标识和删除状态唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '关注推特失败记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_chat
-- ----------------------------
DROP TABLE IF EXISTS `robot_chat`;
CREATE TABLE `robot_chat`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天唯一标识',
  `robot_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '机器人用户名',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 256 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '机器人聊天' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_chat_finish_task
-- ----------------------------
DROP TABLE IF EXISTS `robot_chat_finish_task`;
CREATE TABLE `robot_chat_finish_task`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天id',
  `task_id` int(11) NOT NULL COMMENT '任务主键',
  `deleted` tinyint(1) NOT NULL COMMENT '删除状态 0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id_task_id`(`chat_id`, `task_id`, `deleted`) USING BTREE COMMENT '聊天id和任务id删除状态的联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 1060 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '完成任务记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_system
-- ----------------------------
DROP TABLE IF EXISTS `robot_system`;
CREATE TABLE `robot_system`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `system_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统key',
  `system_value` varchar(1500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统值',
  `remark` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统字典' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_task
-- ----------------------------
DROP TABLE IF EXISTS `robot_task`;
CREATE TABLE `robot_task`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `task_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务标题',
  `task_description` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务描述',
  `task_sort` tinyint(2) NOT NULL COMMENT '任务序号',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '机器人具体任务' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_task_check
-- ----------------------------
DROP TABLE IF EXISTS `robot_task_check`;
CREATE TABLE `robot_task_check`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天id',
  `state` tinyint(1) NOT NULL COMMENT '审核状态 0-未审核（待审核） 1-审核失败 2-审核成功 3-已封禁',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `udx_chat_id`(`chat_id`, `deleted`) USING BTREE COMMENT '聊天id和删除状态联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 233 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '审核记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_task_check_content
-- ----------------------------
DROP TABLE IF EXISTS `robot_task_check_content`;
CREATE TABLE `robot_task_check_content`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `task_check_id` int(11) NOT NULL COMMENT '任务审核主键',
  `pic_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '提交的审核图片地址',
  `send_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '审核的内容',
  `deleted` tinyint(1) NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '2021-09-24 14:41:11' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '审核关联图片记录' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
