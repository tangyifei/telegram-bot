package com.telegram.bz.managers;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.code.models.robot.FocusOnTwitterFailure;
import com.telegram.bz.services.ChatTwitterService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.User;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * http调用的相关管理类
 *
 * @author xiaoyaowang
 */
@Component
public class HttpManager {

    private static Logger log = LoggerFactory.getLogger(HttpManager.class);

    private static final String TWITTER_URL = "https://api.twitter.com/1.1";

    @Value("${telegram.url}")
    private String telegramUrl;

    @Value("${twitter.bearerToken}")
    private String bearerToken;

    @Value("${twitter.account}")
    private String twitterAccount;

    @Value("${listen.join.group.token}")
    private String collectToken;

    @Value("${check.image-url}")
    private String checkImageUrl;

    @Value("${bz.url}")
    private String bzUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Resource
    private ChatTwitterService chatTwitterService;

    /**
     * 判断用户账号是否存在和是否购买过理财产品
     *
     * @param bzAccount bz账号
     * @return 用户账号是否存在和是否购买过理财产品
     */
    public Boolean judgeExistBzAccount(String bzAccount) {
        if (StringUtils.isBlank(bzAccount)) {
            return false;
        }
        String uri = bzUrl + "users/judge-account-exist?userAccount=" + bzAccount;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        String strBody = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class).getBody();
        log.info("判断用户账号是否存在和是否购买过理财产品的消息的响应:{}", strBody);
        if (StringUtils.isNotBlank(strBody)) {
            JSONObject dataJsonObject = JSON.parseObject(strBody);
            if (null != dataJsonObject) {
                int code = dataJsonObject.getInteger("code");
                if (code == 1) {
                    return dataJsonObject.getBoolean("data");
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * 根据文件id获取文件的路径
     *
     * @param token  机器人的token
     * @param fileId 文件的id
     * @return 文件路径
     */
    public String getFilePath(String token, String fileId) {
        String uri = telegramUrl + "/bot" + token + "/getfile?file_id=" + fileId;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        String strBody = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class).getBody();
        JSONObject resultJson = JSON.parseObject(strBody);
        if (null != resultJson && resultJson.containsKey("ok")) {
            boolean ok = resultJson.getBoolean("ok");
            if (ok) {
                if (resultJson.containsKey("result")) {
                    JSONObject result = resultJson.getJSONObject("result");
                    if (null != result) {
                        String filePath = result.getString("file_path");
                        filePath = telegramUrl + "/file/bot" + token + "/" + filePath;
                        return filePath;
                    }

                }
            }
        }
        return null;
    }

    /**
     * 判断用户是否关注Tokenswap_DEX推特账号
     *
     * @param chatId             聊天唯一标识
     * @param twitterAccountName 推特账号名称
     * @return 是否关注Tokenswap_DEX推特账号
     */
    public Boolean whetherAttentionTokenSwapDex(String chatId, String twitterAccountName) {
        String uri = TWITTER_URL + "/friends/list.json?cursor=-1&screen_name=" + twitterAccountName + "&skip_status=true&include_user_entities=false";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.add("Authorization", bearerToken);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        try {
            String strBody = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class).getBody();
            log.info("判断用户是否关注某个推特账号的结果：{}", strBody);
            JSONObject twitterFriendResponse = JSON.parseObject(strBody);
            if (null != twitterFriendResponse && twitterFriendResponse.containsKey("users")) {
                JSONArray userJsonArray = twitterFriendResponse.getJSONArray("users");
                if (!CollectionUtils.isEmpty(userJsonArray)) {
                    JSONObject userJson;
                    for (Object userObject : userJsonArray) {
                        userJson = JSON.parseObject(JSON.toJSONString(userObject));
                        if (null != userJson && userJson.containsKey("screen_name")) {
                            if (twitterAccount.equalsIgnoreCase(userJson.getString("screen_name"))) {
                                return true;
                            }
                        }

                    }
                }
            }
        } catch (RestClientException e) {
            log.error("判断用户是否关注Tokenswap_DEX推特账号发生异常：", e);
            FocusOnTwitterFailure focusOnTwitterFailure = new FocusOnTwitterFailure();
            focusOnTwitterFailure.setTwitterAccountName(twitterAccountName);
            focusOnTwitterFailure.setChatId(chatId);
            chatTwitterService.insertFocusOnTwitterFailure(focusOnTwitterFailure);
            return false;
        }
        return false;
    }

    /**
     * 发送重新发送截图的消息
     *
     * @param chatId      群组唯一标识
     * @param messageText 发送的内容
     */
    @Async
    public void sendReSendPhoto(Long chatId, Message messageText) throws InterruptedException {
        // 睡个10秒，避免刷屏
        TimeUnit.SECONDS.sleep(10);
        StringBuilder sb = new StringBuilder(1 << 4);
        sb.append("@");
        User fromUser = messageText.getFrom();
        if (null != fromUser) {
            String userName = fromUser.getUserName();
            if (StringUtils.isBlank(userName)) {
                userName = fromUser.getFirstName() + fromUser.getLastName();
            }
            sb.append(userName);
        }
        sb.append(" Please do not send compressed images,");
        sb.append("Refer to the following methods to send the task screenshot, otherwise it will be invalid.");
        String imageUrl = "<a href=" + "\"" + checkImageUrl + "\"" + ">.</a>";
        String text = imageUrl + "\r\n" + sb.toString();
        log.info("发送的内容：{}", text);
        String uri = telegramUrl + "/bot" + collectToken + "/sendMessage?chat_id=" + chatId + "&text=" + text + "&parse_mode=HTML";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        String strBody = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class).getBody();
        log.info("发送重新发送截图的消息的响应:{}", strBody);
    }

}
