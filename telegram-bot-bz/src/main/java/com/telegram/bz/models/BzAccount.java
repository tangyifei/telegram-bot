package com.telegram.bz.models;

import com.code.models.robot.BaseRobot;

/**
 * 已参加活动的BZ账户
 *
 * @author xiaoyaowang
 */
public class BzAccount extends BaseRobot {

    private static final long serialVersionUID = 776516229102293930L;

    /**
     * 聊天唯一标识
     */
    private String chatId;

    /**
     * BZ 账号
     */
    private String bzAccount;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getBzAccount() {
        return bzAccount;
    }

    public void setBzAccount(String bzAccount) {
        this.bzAccount = bzAccount;
    }

    @Override
    public String toString() {
        return "BzAccount{" +
                "chatId='" + chatId + '\'' +
                ", bzAccount='" + bzAccount + '\'' +
                "} " + super.toString();
    }
}
