package com.telegram.bz.consts;

/**
 * 共享常量
 *
 * @author xiaoyaowang
 */
public interface TelegramCommonConsts {

    /**
     * 一个群最多分享多少个连接
     */
    int SHARE_GROUP_URL_MAX_TIMES = 6;

}
