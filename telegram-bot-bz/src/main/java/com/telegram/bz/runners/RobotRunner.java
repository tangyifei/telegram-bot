package com.telegram.bz.runners;

import com.telegram.bz.bots.BzAirdropTaskRobot;
import com.telegram.bz.bots.JoinGroupCollectPictureBot;
import com.telegram.bz.configs.redis.RedisHandler;
import com.telegram.bz.managers.HttpManager;
import com.telegram.bz.services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import javax.annotation.Resource;

/**
 * 注册机器人
 *
 * @author xiaoyaowang
 */
@Component
public class RobotRunner implements ApplicationRunner {

    @Value("${first.task.text}")
    private String firstTaskText;

    @Value("${reply.button.text}")
    private String replyButtonText;

    @Value("${reply.done.text}")
    private String replyDoneText;

    @Value("${error.text}")
    private String errorText;

    @Value("${menu.first-button-text}")
    private String menuFirstButtonText;

    @Value("${menu.second-button-text}")
    private String menuSecondButtonText;

    @Value("${menu.third-button-text}")
    private String menuThirdButtonText;

    @Value("${menu.fourth-button-text}")
    private String menuFourthButtonText;

    @Value("${earn.tp.first-button-text}")
    private String earnTpFirstButtonText;

    @Value("${earn.tp.second-button-text}")
    private String earnTpSecondButtonText;

    @Value("${task.finish.message}")
    private String taskFinishMessage;

    @Value("${second.task.image-url}")
    private String secondTaskImageUrl;

    @Value("${check.image-url}")
    private String checkImageUrl;

    @Value("${second.task.finish}")
    private String secondTaskFinish;

    @Value("${upload.url}")
    private String uploadUrl;

    @Value("${upload.path}")
    private String uploadPath;

    @Resource
    private RobotChatService robotChatService;

    @Resource
    private RobotTaskCheckService robotTaskCheckService;

    @Resource
    private RobotTaskService robotTaskService;

    @Resource
    private ChatMemberGroupService chatMemberGroupService;

    @Resource
    private BzAccountService bzAccountService;

    @Resource
    private ChatLinkService chatLinkService;

    @Resource
    private ChatBanService chatBanService;

    @Resource
    private RobotSystemService robotSystemService;

    @Resource
    private HttpManager httpManager;

    @Resource
    private RedisHandler redisHandler;

    @Resource
    private ChatTwitterService chatTwitterService;

    @Value("${listen.join.group.bot}")
    private String listenJoinGroupBot;

    @Value("${listen.join.group.token}")
    private String listenJoinGroupToken;

    @Value("${execute.task.bot.name}")
    private String taskBotName;

    @Value("${execute.task.bot.token}")
    private String taskBotToken;

    @Value("${listen.join.group.url-prefix}")
    private String listenJoinGroupUrlPrefix;

    @Value("${listen.join.group.name}")
    private String listenJoinGroupName;

    private static Logger logger = LoggerFactory.getLogger(RobotRunner.class);

    @Override
    public void run(ApplicationArguments args) {
        // 电报机器人配置
        try {
            TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
            BzAirdropTaskRobot bzAirdropTaskRobot = new BzAirdropTaskRobot();
            bzAirdropTaskRobot.setRobotName(taskBotName);
            bzAirdropTaskRobot.setRobotToken(taskBotToken);
            bzAirdropTaskRobot.setListenJoinGroupUrlPrefix(listenJoinGroupUrlPrefix);
            bzAirdropTaskRobot.setListenJoinGroupName(listenJoinGroupName);
            bzAirdropTaskRobot.setChatMemberGroupService(chatMemberGroupService);
            bzAirdropTaskRobot.setRobotChatService(robotChatService);
            bzAirdropTaskRobot.setHttpManager(httpManager);
            bzAirdropTaskRobot.setBzAccountService(bzAccountService);
            bzAirdropTaskRobot.setChatTwitterService(chatTwitterService);
            bzAirdropTaskRobot.setUploadUrl(uploadUrl);
            bzAirdropTaskRobot.setUploadPath(uploadPath);
            bzAirdropTaskRobot.setCheckImageUrl(checkImageUrl);
            bzAirdropTaskRobot.setRobotTaskService(robotTaskService);
            bzAirdropTaskRobot.setRobotTaskCheckService(robotTaskCheckService);
            bzAirdropTaskRobot.setRobotSystemService(robotSystemService);
            bzAirdropTaskRobot.setChatBanService(chatBanService);
            bzAirdropTaskRobot.setFirstTaskText(firstTaskText);
            bzAirdropTaskRobot.setReplyButtonText(replyButtonText);
            bzAirdropTaskRobot.setErrorText(errorText);
            bzAirdropTaskRobot.setMenuFirstButtonText(menuFirstButtonText);
            bzAirdropTaskRobot.setMenuSecondButtonText(menuSecondButtonText);
            bzAirdropTaskRobot.setMenuThirdButtonText(menuThirdButtonText);
            bzAirdropTaskRobot.setMenuFourthButtonText(menuFourthButtonText);
            bzAirdropTaskRobot.setEarnTpFirstButtonText(earnTpFirstButtonText);
            bzAirdropTaskRobot.setEarnTpSecondButtonText(earnTpSecondButtonText);
            bzAirdropTaskRobot.setTaskFinishMessage(taskFinishMessage);
            bzAirdropTaskRobot.setReplyDoneText(replyDoneText);
            bzAirdropTaskRobot.setSecondTaskImageUrl(secondTaskImageUrl);
            bzAirdropTaskRobot.setSecondTaskFinish(secondTaskFinish);
            bzAirdropTaskRobot.setChatLinkService(chatLinkService);
            bzAirdropTaskRobot.setRedisHandler(redisHandler);
            botsApi.registerBot(bzAirdropTaskRobot);
            logger.info("任务机器人注册成功，名称:{} token:{}", taskBotName, taskBotToken);

            JoinGroupCollectPictureBot joinGroupCollectPictureBot = new JoinGroupCollectPictureBot();
            joinGroupCollectPictureBot.setRobotName(listenJoinGroupBot);
            joinGroupCollectPictureBot.setRobotToken(listenJoinGroupToken);
            joinGroupCollectPictureBot.setListenJoinGroupUrlPrefix(listenJoinGroupUrlPrefix);
            joinGroupCollectPictureBot.setListenJoinGroupName(listenJoinGroupName);
            joinGroupCollectPictureBot.setChatMemberGroupService(chatMemberGroupService);
            joinGroupCollectPictureBot.setRobotChatService(robotChatService);
            joinGroupCollectPictureBot.setHttpManager(httpManager);
            joinGroupCollectPictureBot.setUploadUrl(uploadUrl);
            joinGroupCollectPictureBot.setUploadPath(uploadPath);
            joinGroupCollectPictureBot.setRobotTaskService(robotTaskService);
            joinGroupCollectPictureBot.setRobotTaskCheckService(robotTaskCheckService);
            joinGroupCollectPictureBot.setRobotSystemService(robotSystemService);
            joinGroupCollectPictureBot.setChatBanService(chatBanService);
            botsApi.registerBot(joinGroupCollectPictureBot);
            logger.info("监听加群机器人注册成功，名称:{} token:{}", listenJoinGroupBot, listenJoinGroupToken);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

}
