package com.telegram.bz.services.impl;

import com.code.consts.CommonConsts;
import com.code.models.robot.ChatTwitter;
import com.code.models.robot.FocusOnTwitterFailure;
import com.telegram.bz.daos.task.ChatTwitterMapper;
import com.telegram.bz.daos.task.FocusOnTwitterFailureMapper;
import com.telegram.bz.services.BzAccountService;
import com.telegram.bz.services.ChatTwitterService;
import com.telegram.bz.services.RobotTaskService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 推特账户服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatTwitterServiceImpl implements ChatTwitterService {

    @Resource
    ChatTwitterMapper chatTwitterMapper;

    @Resource
    FocusOnTwitterFailureMapper focusOnTwitterFailureMapper;

    @Resource
    RobotTaskService robotTaskService;

    @Resource
    BzAccountService bzAccountService;

    @Override
    public int insertChatTwitter(String chatId, String twitterAccountName) {
        Integer existsChatTwitter = chatTwitterMapper.existsChatTwitterByChatIdAndTwitterAccountName(chatId, null);
        ChatTwitter chatTwitter = new ChatTwitter();
        chatTwitter.setChatId(chatId);
        chatTwitter.setTwitterAccountName(twitterAccountName);
        chatTwitter.setUpdatedAt(new Date());
        if (null == existsChatTwitter) {
            chatTwitter.setDeleted(0);
            chatTwitter.setCreatedAt(new Date());
            return chatTwitterMapper.insert(chatTwitter);
        } else {
            chatTwitter.setId(chatTwitterMapper.getChatTwitterIdByChatIdAndTwitterAccountName(chatId, null));
            return chatTwitterMapper.updateByPrimaryKeySelective(chatTwitter);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public void insertFocusOnTwitterFailure(FocusOnTwitterFailure focusOnTwitterFailure) {
        String chatId = focusOnTwitterFailure.getChatId();
        FocusOnTwitterFailure whetherExistFocusOnTwitterFailure = focusOnTwitterFailureMapper.getFocusOnTwitterFailureByChatId(chatId);
        if (null == whetherExistFocusOnTwitterFailure) {
            focusOnTwitterFailure.setDeleted(0);
            focusOnTwitterFailure.setCreatedAt(new Date());
            focusOnTwitterFailure.setUpdatedAt(new Date());
            focusOnTwitterFailure.setBzAccount(bzAccountService.getBzAccountByChatId(chatId));
            focusOnTwitterFailureMapper.insertFocusOnTwitterFailure(focusOnTwitterFailure);
        } else {
            whetherExistFocusOnTwitterFailure.setUpdatedAt(new Date());
            whetherExistFocusOnTwitterFailure.setTwitterAccountName(focusOnTwitterFailure.getTwitterAccountName());
            focusOnTwitterFailureMapper.updateTwitterAccountName(whetherExistFocusOnTwitterFailure);
        }
        int finishTaskNums = robotTaskService.getTotalFinishTasks(chatId);
        if (finishTaskNums >= 3) {
            // 第一步：插入关注推特的完成任务和推特关注记录
            String twitterAccountName = focusOnTwitterFailure.getTwitterAccountName();
            if (!StringUtils.isBlank(twitterAccountName)) {
                Integer existsChatTwitter = chatTwitterMapper.existsChatTwitterByChatIdAndTwitterAccountName(chatId, null);
                ChatTwitter chatTwitter = new ChatTwitter();
                chatTwitter.setChatId(chatId);
                chatTwitter.setTwitterAccountName(twitterAccountName);
                chatTwitter.setUpdatedAt(new Date());
                if (null == existsChatTwitter) {
                    chatTwitter.setDeleted(0);
                    chatTwitter.setCreatedAt(new Date());
                    chatTwitterMapper.insert(chatTwitter);
                } else {
                    chatTwitter.setId(chatTwitterMapper.getChatTwitterIdByChatIdAndTwitterAccountName(chatId, null));
                    chatTwitterMapper.updateByPrimaryKeySelective(chatTwitter);
                }

            }
        }

    }

    @Override
    public boolean whetherExistChatTwitterByChatId(String chatId) {
        return null != chatTwitterMapper.getChatTwitterByChatId(chatId);
    }

    @Override
    public boolean whetherExistChatTwitterAccountNameByTwitterAccountName(String twitterAccountName) {
        return null != chatTwitterMapper.getChatTwitterAccountNameCountByTwitterAccountName(twitterAccountName);
    }

    @Override
    public String getTwitterNameByChatId(String chatId) {
        return chatTwitterMapper.getTwitterAccountNameByChatId(chatId);
    }
}
