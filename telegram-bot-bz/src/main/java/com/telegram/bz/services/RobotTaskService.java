package com.telegram.bz.services;

import com.code.models.robot.RobotChatFinishTask;
import com.telegram.bz.models.AirdropRecord;
import com.telegram.bz.models.RobotTask;

/**
 * 机器人任务业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotTaskService {

    /**
     * 获取用户完成的任务的最大序号
     *
     * @param chatId 聊天id
     * @return 用户完成的任务的最大序号
     */
    int getFinishedTaskMaxSort(String chatId);

    /**
     * 获取总的任务数
     *
     * @return 总的任务数
     */
    int getTotalTasks();

    /**
     * 获取完成的任务数
     *
     * @return 完成的任务数
     */
    int getTotalFinishTasks(String chatId);

    /**
     * 根据序号获取任务
     *
     * @param taskSort 任务序号
     * @return 用户完成的任务的最大序号
     */
    RobotTask getRobotTaskByTaskSort(Integer taskSort);

    /**
     * 添加用户已完成的任务记录
     *
     * @param robotChatFinishTask 用户已完成的任务实体类
     * @return 添加成功的用户已完成的任务实体类
     */
    RobotChatFinishTask insertRobotChatFinishTask(RobotChatFinishTask robotChatFinishTask);

    /**
     * 判断用户是否参加过空投
     *
     * @param airdropRecord 已空投记录
     * @return 用户是否参加过空投
     */
    boolean existsAirdropRecordByCondition(AirdropRecord airdropRecord);

    /**
     * 根据相关条件获取空投信息
     *
     * @param airdropRecord 已空投记录
     * @return 空投信息
     */
    AirdropRecord getAirdropRecordByCondition(AirdropRecord airdropRecord);

}
