package com.telegram.bz.services;

import com.telegram.bz.models.RobotChat;

/**
 * 与机器人聊天的用户业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotChatService {

    /**
     * 添加与机器人聊天的用户
     *
     * @param robotChat 与相关机器人聊天的用户记录实体类
     * @return 添加成功的相关机器人聊天的用户记录
     */
    RobotChat insertRobotChat(RobotChat robotChat);

}
