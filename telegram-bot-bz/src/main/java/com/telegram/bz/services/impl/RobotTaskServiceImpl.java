package com.telegram.bz.services.impl;

import com.code.consts.CommonConsts;
import com.code.models.robot.RobotChatFinishTask;
import com.code.models.robot.RobotTaskCheck;
import com.telegram.bz.daos.task.RobotChatFinishTaskMapper;
import com.telegram.bz.daos.task.RobotTaskMapper;
import com.telegram.bz.models.AirdropRecord;
import com.telegram.bz.models.ChatMemberGroup;
import com.telegram.bz.models.RobotTask;
import com.telegram.bz.services.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 机器人任务服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class RobotTaskServiceImpl implements RobotTaskService {

    @Resource
    RobotTaskMapper robotTaskMapper;

    @Resource
    RobotChatFinishTaskMapper robotChatFinishTaskMapper;

    @Resource
    BzAccountService bzAccountService;

    @Resource
    ChatTwitterService chatTwitterService;

    @Resource
    RobotTaskCheckService robotTaskCheckService;

    @Resource
    ChatMemberGroupService chatMemberGroupService;

    @Resource
    AirdropRecordService airdropRecordService;

    @Override
    public int getFinishedTaskMaxSort(String chatId) {
        Integer finishedTaskMaxSort = robotTaskMapper.getFinishedTaskMaxSort(chatId);
        if (null != finishedTaskMaxSort) {
            return finishedTaskMaxSort;
        }
        return 0;
    }

    @Override
    public int getTotalTasks() {
        return robotTaskMapper.getTotalTasks();
    }

    @Override
    public int getTotalFinishTasks(String chatId) {
        return robotTaskMapper.getTotalFinishTasks(chatId);
    }

    @Override
    public RobotTask getRobotTaskByTaskSort(Integer taskSort) {
        return robotTaskMapper.getRobotTaskByTaskSort(taskSort);
    }

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public RobotChatFinishTask insertRobotChatFinishTask(RobotChatFinishTask robotChatFinishTask) {
        String chatId = robotChatFinishTask.getChatId();
        Integer taskId = robotChatFinishTask.getTaskId();
        if (null != taskId) {
            Integer existCount = robotTaskMapper.existsRobotChatFinishTaskByChatIdAndTaskId(taskId, chatId);
            if (null == existCount) {
                robotChatFinishTask.setDeleted(0);
                robotChatFinishTask.setCreatedAt(new Date());
                robotChatFinishTask.setUpdatedAt(new Date());
                robotChatFinishTaskMapper.insert(robotChatFinishTask);
            }
        }
        String bzAccount = robotChatFinishTask.getBzAccount();
        if (StringUtils.isNotBlank(bzAccount)) {
            bzAccountService.insertBzAccount(chatId, bzAccount);
        }
        String twitterAccountName = robotChatFinishTask.getTwitterAccountName();
        if (StringUtils.isNotBlank(twitterAccountName)) {
            chatTwitterService.insertChatTwitter(chatId, twitterAccountName);
        }
        Integer sendPhoto = robotChatFinishTask.getSendPhoto();
        if (null != sendPhoto) {
            addRobotCheckTask(chatId);
        }
        return robotTaskMapper.getRobotChatFinishTaskByChatIdAndTaskId(taskId, chatId);
    }

    @Override
    public boolean existsAirdropRecordByCondition(AirdropRecord airdropRecord) {
        return airdropRecordService.existsAirdropRecordByCondition(airdropRecord);
    }

    @Override
    public AirdropRecord getAirdropRecordByCondition(AirdropRecord airdropRecord) {
        return airdropRecordService.getAirdropRecordByCondition(airdropRecord);
    }

    private void addRobotCheckTask(String chatId) {
        RobotTaskCheck robotTaskCheck = new RobotTaskCheck();
        robotTaskCheck.setChatId(chatId);
        ChatMemberGroup chatMemberGroup = chatMemberGroupService.getChatMemberGroupByChatId(chatId);
        if (null != chatMemberGroup) {
            robotTaskCheck.setUserName(chatMemberGroup.getUserName());
            robotTaskCheck.setFirstName(chatMemberGroup.getFirstName());
            robotTaskCheck.setLastName(chatMemberGroup.getLastName());
        }
        robotTaskCheck.setState(0);
        robotTaskCheckService.insertRobotTaskCheck(robotTaskCheck);
    }
}
