package com.telegram.bz.services.impl;

import com.code.models.robot.ChatLink;
import com.telegram.bz.consts.TelegramCommonConsts;
import com.telegram.bz.daos.task.ChatLinkMapper;
import com.telegram.bz.services.ChatLinkService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 分享链接实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatLinkServiceImpl implements ChatLinkService {

    @Resource
    ChatLinkMapper chatLinkMapper;

    @Override
    public List<String> getChatLinkListByChatIdAndLinkCategory(String chatId, Integer linkCategory) {
        return chatLinkMapper.getChatLinkListByChatIdAndLinkCategory(chatId, linkCategory);
    }

    @Override
    public int insertChatLink(ChatLink chatLink) {
        ChatLink chatLinkInDbResult = chatLinkMapper.selectOne(chatLink);
        if (null == chatLinkInDbResult) {
            chatLink.setDeleted(0);
            chatLink.setCreatedAt(new Date());
            chatLink.setUpdatedAt(new Date());
            return chatLinkMapper.insert(chatLink);
        }
        return 0;
    }

    @Override
    public boolean whetherExistChatLinkByChatIdAndShareLink(String chatId, String shareLink) {
        return null != chatLinkMapper.existsChatLinkByChatIdAndShareLink(chatId, shareLink);
    }

    @Override
    public int getChatLinkCountByChatIdAndCategory(String chatId, Integer category) {
        return chatLinkMapper.getChatLinkCountByChatIdAndCategory(chatId, category);
    }

    @Override
    public boolean shareGroupUrlWhetherOverTimesInGroup(String groupUrl) {
        return TelegramCommonConsts.SHARE_GROUP_URL_MAX_TIMES <= chatLinkMapper.getChatIdNumsInThreeDays(groupUrl);
    }

}
