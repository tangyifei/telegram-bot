package com.telegram.bz.services.impl;

import com.code.consts.CommonConsts;
import com.code.enums.RewardKeyEnum;
import com.code.models.robot.RobotTaskCheck;
import com.code.models.robot.RobotTaskCheckContent;
import com.telegram.bz.daos.task.RobotTaskCheckContentMapper;
import com.telegram.bz.daos.task.RobotTaskCheckMapper;
import com.telegram.bz.services.RobotSystemService;
import com.telegram.bz.services.RobotTaskCheckService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 机器人任务审核服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class RobotTaskCheckServiceImpl implements RobotTaskCheckService {

    @Resource
    RobotTaskCheckMapper robotTaskCheckMapper;

    @Resource
    RobotTaskCheckContentMapper robotTaskCheckContentMapper;

    @Resource
    RobotSystemService robotSystemService;

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public int insertRobotTaskCheck(RobotTaskCheck robotTaskCheck) {
        RobotTaskCheck robotTaskCheckForDb = robotTaskCheckMapper.getRobotTaskCheckByCondition(robotTaskCheck);
        Integer taskCheckId;
        int count = 0;
        if (null == robotTaskCheckForDb) {
            robotTaskCheck.setDeleted(0);
            robotTaskCheck.setCreatedAt(new Date());
            robotTaskCheck.setUpdatedAt(new Date());
            Integer state = robotTaskCheck.getState();
            if (state != 3) {
                robotTaskCheck.setState(0);
            }
            count = robotTaskCheckMapper.insertRobotTaskCheck(robotTaskCheck);
            robotTaskCheckForDb = robotTaskCheckMapper.getRobotTaskCheckByCondition(robotTaskCheck);
        }
        String picUrl = robotTaskCheck.getPicUrl();
        if (!StringUtils.isBlank(picUrl)) {
            taskCheckId = robotTaskCheckForDb.getId();
            if (null == robotTaskCheckContentMapper.getRobotTaskCheckContentCount(taskCheckId, picUrl)) {
                RobotTaskCheckContent robotTaskCheckContent = new RobotTaskCheckContent();
                robotTaskCheckContent.setPicUrl(picUrl);
                robotTaskCheckContent.setTaskCheckId(taskCheckId);
                robotTaskCheckContent.setSendText(robotTaskCheck.getSendText());
                robotTaskCheckContent.setDeleted(0);
                robotTaskCheckContent.setCreatedAt(new Date());
                robotTaskCheckContent.setUpdatedAt(new Date());
                count += robotTaskCheckContentMapper.insert(robotTaskCheckContent);
            }
        }
        return count;
    }

    @Override
    public boolean whetherFinishSendCheckPhoto(String chatId) {
        String value = robotSystemService.getSystemValueBySystemKey(RewardKeyEnum.PHOTO_DOWN_LIMIT.name());
        if (StringUtils.isBlank(value)) {
            value = "4";
        }
        return robotTaskCheckContentMapper.getRobotTaskCheckContentCountByChatId(chatId) >= Integer.parseInt(value);
    }

    @Override
    public boolean whetherBanedByChatId(String chatId) {
        return null != robotTaskCheckMapper.getBanTaskCheckByChatId(chatId);
    }

}
