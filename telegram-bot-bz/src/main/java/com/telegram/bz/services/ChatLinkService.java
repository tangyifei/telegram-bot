package com.telegram.bz.services;

import com.code.models.robot.ChatLink;

import java.util.List;

/**
 * 用户分享链接业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatLinkService {

    /**
     * 根据用户链接地址和链接种类获取用户的分享链接
     *
     * @param chatId       聊天唯一id
     * @param linkCategory 分享链接种类
     * @return 用户的分享链接
     */
    List<String> getChatLinkListByChatIdAndLinkCategory(String chatId, Integer linkCategory);

    /**
     * 添加与机器人聊天的分享链接
     *
     * @param chatLink 聊天id
     * @return 影响的行数
     */
    int insertChatLink(ChatLink chatLink);

    /**
     * 根据用户链接地址判断是否存在用户的分享链接
     *
     * @param chatId    聊天唯一id
     * @param shareLink 分享链接
     * @return 是否存在用户的分享链接
     */
    boolean whetherExistChatLinkByChatIdAndShareLink(String chatId, String shareLink);

    /**
     * 通过聊天唯一id和种类获取用户的链接数
     *
     * @param chatId   聊天唯一id
     * @param category 用户的链接种类
     * @return 链接数
     */
    int getChatLinkCountByChatIdAndCategory(String chatId, Integer category);


    /**
     * 判断某一个分享的群是否在3天内被提交分享地址并且通过这些提交的分享地址审核成功15次或15次以上
     *
     * @param groupUrl 分享群组地址
     * @return 结果
     */
    boolean shareGroupUrlWhetherOverTimesInGroup(String groupUrl);

}
