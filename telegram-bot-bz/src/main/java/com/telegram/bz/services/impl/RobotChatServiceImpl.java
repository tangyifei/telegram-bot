package com.telegram.bz.services.impl;

import com.telegram.bz.daos.task.RobotChatMapper;
import com.telegram.bz.models.RobotChat;
import com.telegram.bz.services.RobotChatService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 与机器人聊天的用户服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class RobotChatServiceImpl implements RobotChatService {

    @Resource
    RobotChatMapper robotChatMapper;

    @Override
    public RobotChat insertRobotChat(RobotChat robotChat) {
        String chatId = robotChat.getChatId();
        Integer existCount = robotChatMapper.existsRobotChatByChatId(chatId);
        if (null == existCount) {
            robotChat.setDeleted(0);
            robotChat.setCreatedAt(new Date());
            robotChat.setUpdatedAt(new Date());
            robotChatMapper.insert(robotChat);
        }
        return robotChatMapper.getRobotChatByChatId(chatId);
    }
}
