package com.telegram.bz.services.impl;

import com.telegram.bz.daos.group.ChatMemberGroupMapper;
import com.telegram.bz.models.ChatMemberGroup;
import com.telegram.bz.services.ChatMemberGroupService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 新人加入的群组服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatMemberGroupServiceImpl implements ChatMemberGroupService {

    @Resource
    ChatMemberGroupMapper chatMemberGroupMapper;

    @Override
    public Boolean judgeUserWhetherJoinGroup(ChatMemberGroup chatMemberGroup) {
        return null != chatMemberGroupMapper.getChatMemberGroup(chatMemberGroup);
    }

    @Override
    public int insertChatMemberGroupList(List<ChatMemberGroup> chatMemberGroupList) {
        return chatMemberGroupMapper.insertList(chatMemberGroupList);
    }

    @Override
    public ChatMemberGroup getChatMemberGroupByChatId(String chatId) {
        return chatMemberGroupMapper.getChatMemberGroupByChatId(chatId);
    }

}
