package com.telegram.bz.services;

/**
 * 已参加空投任务的BZ账户业务接口
 *
 * @author xiaoyaowang
 */
public interface BzAccountService {

    /**
     * 判断用户是否已经参加过空投
     *
     * @param chatId    聊天唯一id
     * @param bzAccount bz账号
     * @return 是否已经参加过空投结果
     */
    boolean whetherHaveAlreadyJoinBzAirdrop(String chatId, String bzAccount);

    /**
     * 插入用户账户与BZ空投任务的关联记录
     *
     * @param chatId    聊天唯一id
     * @param bzAccount bz账号
     * @return 影响的行数
     */
    int insertBzAccount(String chatId, String bzAccount);

    /**
     * 根据聊天唯一标识获取BZ账号
     *
     * @param chatId 聊天唯一标识
     * @return BZ账号
     */
    String getBzAccountByChatId(String chatId);

}
