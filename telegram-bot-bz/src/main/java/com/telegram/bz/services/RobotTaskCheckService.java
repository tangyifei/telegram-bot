package com.telegram.bz.services;

import com.code.models.robot.RobotTaskCheck;

import java.util.List;

/**
 * 机器人任务审核业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotTaskCheckService {

    /**
     * 添加用户已完成的任务审核记录
     *
     * @param robotTaskCheck 用户已完成的任务审核列表
     * @return 影响的行数
     */
    int insertRobotTaskCheck(RobotTaskCheck robotTaskCheck);

    /**
     * 用户是否完成发送截图的任务
     *
     * @param chatId 聊天id
     * @return 是否完成发送截图的任务
     */
    boolean whetherFinishSendCheckPhoto(String chatId);

    /**
     * 用户是否被封禁
     *
     * @param chatId 聊天id
     * @return 用户是否被封禁
     */
    boolean whetherBanedByChatId(String chatId);

}
