package com.telegram.bz.services;

/**
 * 机器人系统设置业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotSystemService {

    /**
     * 根据系统key获取系统value
     *
     * @param systemKey 系统key
     * @return 系统value
     */
    String getSystemValueBySystemKey(String systemKey);

}
