package com.telegram.bz.services.impl;

import com.telegram.bz.daos.task.BzAccountMapper;
import com.telegram.bz.models.BzAccount;
import com.telegram.bz.services.BzAccountService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 已参加空投任务的BZ账户服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class BzAccountServiceImpl implements BzAccountService {

    @Resource
    BzAccountMapper bzAccountMapper;

    @Override
    public boolean whetherHaveAlreadyJoinBzAirdrop(String chatId, String bzAccount) {
        return null != bzAccountMapper.whetherHaveAlreadyJoinBzAirdrop(chatId, bzAccount);
    }

    @Override
    public int insertBzAccount(String chatId, String bzAccount) {
        if (null == bzAccountMapper.whetherHaveAlreadyJoinBzAirdrop(chatId, bzAccount)) {
            BzAccount account = new BzAccount();
            account.setChatId(chatId);
            account.setBzAccount(bzAccount);
            account.setDeleted(0);
            account.setCreatedAt(new Date());
            account.setUpdatedAt(new Date());
            return bzAccountMapper.insert(account);
        }
        return 0;
    }

    @Override
    public String getBzAccountByChatId(String chatId) {
        return bzAccountMapper.getBzAccountByChatId(chatId);
    }
}
