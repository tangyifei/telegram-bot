package com.telegram.bz.services;

import com.code.models.robot.FocusOnTwitterFailure;

/**
 * 推特账户业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatTwitterService {

    /**
     * 添加与机器人聊天的推特账户
     *
     * @param chatId             聊天id
     * @param twitterAccountName 推特账户名
     * @return 影响的行数
     */
    int insertChatTwitter(String chatId, String twitterAccountName);

    /**
     * 添加用户关注推特失败的记录
     *
     * @param focusOnTwitterFailure 用户关注推特失败实体类
     */
    void insertFocusOnTwitterFailure(FocusOnTwitterFailure focusOnTwitterFailure);

    /**
     * 通过聊天id判断用户是否有推特账号
     *
     * @param chatId 聊天id
     * @return 用户是否有推特账号
     */
    boolean whetherExistChatTwitterByChatId(String chatId);

    /**
     * 判断该推特账户是否被使用
     *
     * @param twitterAccountName 推特账户
     * @return 推特账户是否被使用
     */
    boolean whetherExistChatTwitterAccountNameByTwitterAccountName(String twitterAccountName);

    /**
     * 通过聊天id获取推特账号
     *
     * @param chatId 聊天id
     * @return 推特账号
     */
    String getTwitterNameByChatId(String chatId);

}
