package com.telegram.bz.daos.task;

import com.telegram.bz.daos.CrudMapper;
import com.telegram.bz.models.BzAccount;
import org.apache.ibatis.annotations.Param;

/**
 * 已参加空投任务的BZ账户持久层
 *
 * @author xiaoyaowang
 */
public interface BzAccountMapper extends CrudMapper<BzAccount> {

    /**
     * 判断用户是否已经参加过空投
     *
     * @param chatId    聊天唯一id
     * @param bzAccount bz账号
     * @return 是否已经参加过空投结果
     */
    Integer whetherHaveAlreadyJoinBzAirdrop(@Param("chatId") String chatId, @Param("bzAccount") String bzAccount);

    /**
     * 根据聊天唯一标识获取BZ账号
     *
     * @param chatId 聊天唯一标识
     * @return BZ账号
     */
    String getBzAccountByChatId(@Param("chatId") String chatId);

}
