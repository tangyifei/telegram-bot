package com.telegram.bz.daos.task;

import com.code.models.robot.FocusOnTwitterFailure;
import com.telegram.bz.daos.CrudMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 用户关注推特失败记录持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface FocusOnTwitterFailureMapper extends CrudMapper<FocusOnTwitterFailure> {

    /**
     * 插入推特关注失败的记录
     *
     * @param focusOnTwitterFailure 用户关注推特失败实体类
     * @return 影响的行数
     */
    int insertFocusOnTwitterFailure(FocusOnTwitterFailure focusOnTwitterFailure);

    /**
     * 修改推特账户名
     *
     * @param focusOnTwitterFailure 用户关注推特失败实体类
     * @return 影响的行数
     */
    int updateTwitterAccountName(FocusOnTwitterFailure focusOnTwitterFailure);

    /**
     * 通过chatId查询关注推特失败的记录
     *
     * @param chatId 聊天唯一标识
     * @return 关注推特失败的记录
     */
    FocusOnTwitterFailure getFocusOnTwitterFailureByChatId(@Param("chatId") String chatId);

}
