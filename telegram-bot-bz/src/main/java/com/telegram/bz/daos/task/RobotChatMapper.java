package com.telegram.bz.daos.task;

import com.telegram.bz.daos.CrudMapper;
import com.telegram.bz.models.RobotChat;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 与机器人聊天的用户持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface RobotChatMapper extends CrudMapper<RobotChat> {

    /**
     * 判断与机器人聊天的用户是否存在
     *
     * @param chatId 聊天id
     * @return 存在记录数
     */
    Integer existsRobotChatByChatId(@Param("chatId") String chatId);

    /**
     * 通过聊天id获取单个RobotChat
     *
     * @param chatId 聊天id
     * @return 单个RobotChat
     */
    RobotChat getRobotChatByChatId(@Param("chatId") String chatId);

}
