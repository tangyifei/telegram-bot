package com.telegram.bz.daos.task;

import com.code.models.robot.RobotTaskCheckContent;
import com.telegram.bz.daos.CrudMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 任务审核持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface RobotTaskCheckContentMapper extends CrudMapper<RobotTaskCheckContent> {

    /**
     * 判断用户的任务审核记录是否存在
     *
     * @param taskCheckId 任务审核主键
     * @param picUrl      图片地址
     * @return 存在的记录数
     */
    Integer getRobotTaskCheckContentCount(@Param("taskCheckId") Integer taskCheckId, @Param("picUrl") String picUrl);

    /**
     * 通过用户的聊天id查看用户发的截图数
     *
     * @param chatId 聊天唯一标识
     * @return 用户发的截图数
     */
    Integer getRobotTaskCheckContentCountByChatId(@Param("chatId") String chatId);

}
