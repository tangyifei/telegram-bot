package com.telegram.bz.daos.task;

import com.code.models.robot.ChatBan;
import com.telegram.bz.daos.CrudMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 封禁用户持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface ChatBanMapper extends CrudMapper<ChatBan> {

    /**
     * 判断用户是否被封禁
     *
     * @param chatId 聊天唯一id
     * @return 判断用户是否被封禁
     */
    Integer existsChatBanByChatId(@Param("chatId") String chatId);

}
