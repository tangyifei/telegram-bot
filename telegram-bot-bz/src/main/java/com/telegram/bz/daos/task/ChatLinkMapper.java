package com.telegram.bz.daos.task;

import com.code.models.robot.ChatLink;
import com.telegram.bz.daos.CrudMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户分享链接持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface ChatLinkMapper extends CrudMapper<ChatLink> {

    /**
     * 根据用户链接地址判断是否存在用户的分享链接
     *
     * @param chatId    聊天唯一id
     * @param shareLink 分享链接
     * @return 是否存在用户的分享链接
     */
    Integer existsChatLinkByChatIdAndShareLink(@Param("chatId") String chatId, @Param("shareLink") String shareLink);

    /**
     * 通过聊天唯一id和种类获取用户的链接数
     *
     * @param chatId   聊天唯一id
     * @param category 用户的链接种类
     * @return 链接数
     */
    int getChatLinkCountByChatIdAndCategory(@Param("chatId") String chatId, @Param("category") Integer category);

    /**
     * 根据用户链接地址和链接种类获取用户的分享链接
     *
     * @param chatId       聊天唯一id
     * @param linkCategory 分享链接种类
     * @return 用户的分享链接
     */
    List<String> getChatLinkListByChatIdAndLinkCategory(@Param("chatId") String chatId, @Param("linkCategory") Integer linkCategory);


    /**
     * 找出分享到当前群3天以内的chatId列表
     *
     * @param shareGroupUrl 分享群组的url
     * @return 分享到当前群3天以内的chatId列表
     */
    int getChatIdNumsInThreeDays(@Param("shareGroupUrl") String shareGroupUrl);

}
