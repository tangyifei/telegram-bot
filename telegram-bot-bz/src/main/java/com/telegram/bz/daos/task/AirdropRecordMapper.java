package com.telegram.bz.daos.task;

import com.telegram.bz.daos.CrudMapper;
import com.telegram.bz.models.AirdropRecord;
import org.springframework.stereotype.Repository;

/**
 * 已空投记录持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface AirdropRecordMapper extends CrudMapper<AirdropRecord> {

    /**
     * 根据相关条件判断用户是否已参加过空投
     *
     * @param airdropRecord 已空投记录
     * @return 存在记录数
     */
    Integer existsAirdropRecordByCondition(AirdropRecord airdropRecord);

    /**
     * 根据相关条件获取空投信息
     *
     * @param airdropRecord 已空投记录
     * @return 空投信息
     */
    AirdropRecord getAirdropRecordByCondition(AirdropRecord airdropRecord);

}
