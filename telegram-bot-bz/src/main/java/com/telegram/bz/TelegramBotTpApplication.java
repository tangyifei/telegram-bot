package com.telegram.bz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 机器人启动类
 *
 * @author xiaoyaowang
 */
@SpringBootApplication
@EnableAsync
public class TelegramBotTpApplication {

    public static void main(String[] args) {
        SpringApplication.run(TelegramBotTpApplication.class, args);
    }

}
