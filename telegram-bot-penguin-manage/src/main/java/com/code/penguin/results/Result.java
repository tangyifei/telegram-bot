package com.code.penguin.results;

import java.io.Serializable;

/**
 * 响应格式父接口
 *
 * @author xiaoyaowang
 */
public interface Result extends Serializable {
}
