package com.code.penguin.daos.robot;

import com.code.penguin.daos.base.CrudMapper;
import com.code.penguin.models.ChatLink;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户分享链接持久层
 *
 * @author xiaoyaowang
 */
public interface ChatLinkMapper extends CrudMapper<ChatLink> {

    /**
     * 根据用户链接地址和链接种类获取用户的分享链接
     *
     * @param chatId       聊天唯一id
     * @param linkCategory 分享链接种类
     * @return 用户的分享链接
     */
    String getChatLinkListByChatIdAndLinkCategory(@Param("chatId") String chatId, @Param("linkCategory") Integer linkCategory);

    /**
     * 通过聊天唯一标识删除用户关注的链接记录
     *
     * @param chatId 聊天唯一标识
     */
    void deleteChatLinkByChatId(String chatId);

    /**
     * 用户是否存在分享链接
     *
     * @param chatId 聊天唯一标识
     * @return 是否存在分享链接
     */
    Integer existsChatLinkByChatId(@Param("chatId") String chatId);

}
