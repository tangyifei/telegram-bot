package com.code.penguin.daos.robot;

import com.code.penguin.daos.base.CrudMapper;
import com.code.penguin.models.po.OperateLog;
import com.code.penguin.models.qo.OperateLogQO;

import java.util.List;

/**
 * 操作日志久层
 *
 * @author xiaoyaowang
 */
public interface OperateLogMapper extends CrudMapper<OperateLog> {

    /**
     * 获取操作日志列表
     *
     * @param operateLogQO 操作日志查询实体类
     * @return 邮箱地址列表
     */
    List<OperateLog> getOperateLogListByCondition(OperateLogQO operateLogQO);

}
