package com.code.penguin.daos.robot;

import com.code.penguin.daos.base.CrudMapper;
import com.code.penguin.models.ChatTwitter;
import org.apache.ibatis.annotations.Param;

/**
 * 关注推特记录持久层
 *
 * @author xiaoyaowang
 */
public interface ChatTwitterMapper extends CrudMapper<ChatTwitter> {

    Integer existsChatTwitterByChatId(@Param("chatId") String chatId);

    void deleteChatTwitterByChatId(@Param("chatId") String chatId);

}
