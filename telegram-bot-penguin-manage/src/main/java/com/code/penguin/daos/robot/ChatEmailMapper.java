package com.code.penguin.daos.robot;

import com.code.penguin.daos.base.CrudMapper;
import com.code.penguin.models.ChatEmail;
import com.code.penguin.models.qo.ChatEmailQO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户邮箱持久层
 *
 * @author xiaoyaowang
 */
public interface ChatEmailMapper extends CrudMapper<ChatEmail> {

    /**
     * 获取邮箱地址列表
     *
     * @param chatEmailQO 邮箱查询实体类
     * @return 邮箱地址列表
     */
    List<ChatEmail> getEmailListByCondition(ChatEmailQO chatEmailQO);

    /**
     * 判断是否存在邮箱地址
     *
     * @param email 邮箱地址
     * @return 是否存在邮箱地址
     */
    Integer whetherExistEmailByEmail(@Param("email") String email);

}
