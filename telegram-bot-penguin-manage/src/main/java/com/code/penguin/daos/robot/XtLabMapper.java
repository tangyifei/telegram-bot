package com.code.penguin.daos.robot;

import com.code.penguin.daos.base.CrudMapper;
import com.code.penguin.models.XtLab;
import com.code.penguin.models.qo.XtLabsQO;

import java.util.List;

/**
 * xtLabs持久层
 *
 * @author xiaoyaowang
 */
public interface XtLabMapper extends CrudMapper<XtLab> {

    int getXtLabCount();

    List<XtLab> getXtLabListByPage(XtLabsQO xtLabsQO);

}
