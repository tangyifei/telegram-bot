package com.code.penguin.daos.robot;

import com.code.penguin.daos.base.CrudMapper;
import com.code.penguin.models.RobotChatFinishTask;
import com.code.penguin.models.RobotTaskCheck;
import com.code.penguin.models.qo.RobotTaskCheckQO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 任务审核持久层
 *
 * @author xiaoyaowang
 */
public interface RobotTaskCheckMapper extends CrudMapper<RobotTaskCheck> {

    /**
     * 根据相关的条件获取机器人任务审核记录列表
     *
     * @param robotTaskCheckQO 机器人任务审核记录查询实体类QO
     * @return 机器人任务审核记录列表
     */
    List<RobotTaskCheck> getRobotTaskCheckListByPage(RobotTaskCheckQO robotTaskCheckQO);

    /**
     * 根据主键查询是否存在某一条记录
     *
     * @param id 主键
     * @return 是否存在某一条记录
     */
    Integer whetherExistRobotTaskCheckById(@Param("id") Integer id);

    /**
     * 根据收集主键查询是否存在收集内容记录
     *
     * @param id 收集主键
     * @return 是否存在收集内容记录
     */
    Integer whetherCollectContentByCollectId(@Param("id") Integer id);

    /**
     * 通过主键获取聊天唯一标识
     *
     * @param id 主键
     * @return 聊天唯一标识
     */
    String getChatIdById(@Param("id") Integer id);

    /**
     * 通过聊天唯一标识获取收集主键
     *
     * @param chatId 聊天唯一标识
     * @return 收集主键
     */
    Integer getChatCollectIdByChatId(@Param("chatId") String chatId);

    /**
     * 通过聊天唯一标识获取收集内容
     *
     * @param chatId 聊天唯一标识
     * @return 收集内容
     */
    String getChatCollectContentByChatId(@Param("chatId") String chatId);

    /**
     * 根据主键获取任务审核详情记录
     *
     * @param id 主键
     * @return 任务审核详情记录
     */
    RobotTaskCheck getRobotTaskCheckById(@Param("id") int id);

    /**
     * 更新任务的审核状态
     *
     * @param id        主键
     * @param state     审核状态 0-未审核 1-审核失败 2-审核成功
     * @param updatedAt 更新时间
     * @return 影响的行数
     */
    int updateRobotTaskState(@Param("id") int id, @Param("state") int state, @Param("updatedAt") Date updatedAt);

    /**
     * 根据排序获取任务主键
     *
     * @param sort 排序
     * @return 任务主键
     */
    Integer getTaskIdBySort(@Param("sort") Integer sort);

    /**
     * 通过taskId和chatId获取完成的任务记录
     *
     * @param chatId 聊天唯一标识
     * @param taskId 任务主键
     * @return 完成的任务记录
     */
    RobotChatFinishTask getRobotChatFinishTaskByChatIdAndTaskId(@Param("chatId") String chatId, @Param("taskId") Integer taskId);

    /**
     * 删除用户完成任务记录
     *
     * @param robotChatFinishTaskDb 用户已完成的任务实体类
     */
    void deleteRobotChatFinishTask(RobotChatFinishTask robotChatFinishTaskDb);

    /**
     * 判断用户是否被封禁
     *
     * @param chatId 聊天唯一id
     * @return 判断用户是否被封禁
     */
    Integer existsChatBanByChatId(@Param("chatId") String chatId);

    /**
     * 根据聊天唯一标识判断用户钱包地址是否存在
     *
     * @param chatId 聊天唯一标识
     * @return 用户钱包地址是否存在
     */
    Integer whetherExistWalletAddressByChatId(@Param("chatId") String chatId);

    /**
     * 根据聊天唯一标识判断discord是否存在
     *
     * @param chatId 聊天唯一标识
     * @return 用户discord是否存在
     */
    Integer whetherExistDiscordByChatId(@Param("chatId") String chatId);

    /**
     * 根据聊天唯一标识判断email是否存在
     *
     * @param chatId 聊天唯一标识
     * @return 用户email是否存在
     */
    Integer whetherExistEmailByChatId(@Param("chatId") String chatId);

    /**
     * 删除封禁的用户
     *
     * @param chatId 聊天唯一id
     */
    void deleteChatBanByChatId(@Param("chatId") String chatId);

    /**
     * 根据主键获取任务审核详情记录
     *
     * @param id 主键
     * @return 任务审核详情记录
     */
    RobotTaskCheck selectRobotTaskCheckById(@Param("id") int id);

    /**
     * 删除审核记录
     *
     * @param id 主键
     */
    void deleteRobotTaskCheckById(@Param("id") int id);

    /**
     * 根据收集主键删除收集内容
     *
     * @param id 收集主键
     */
    void deleteCollectContentByCollectId(@Param("id") int id);

    /**
     * 根据收集主键删除收集记录
     *
     * @param id 收集主键
     */
    void deleteCollectByCollectId(@Param("id") int id);

    /**
     * 通过聊天唯一标识删除用户钱包地址
     *
     * @param chatId 聊天唯一标识
     */
    void deleteWalletAddressByChatId(@Param("chatId") String chatId);

    /**
     * 通过聊天唯一标识删除用户discord
     *
     * @param chatId 聊天唯一标识
     */
    void deleteDiscordByChatId(@Param("chatId") String chatId);

    /**
     * 通过聊天唯一标识删除用户邮箱
     *
     * @param chatId 聊天唯一标识
     */
    void deleteEmailByChatId(@Param("chatId") String chatId);

}
