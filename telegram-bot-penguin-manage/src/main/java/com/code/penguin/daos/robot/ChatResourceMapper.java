package com.code.penguin.daos.robot;

import com.code.penguin.daos.base.CrudMapper;
import com.code.penguin.models.po.ChatResource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 资源持久层
 *
 * @author xiaoyaowang
 */
public interface ChatResourceMapper extends CrudMapper<ChatResource> {

    List<ChatResource> getChatResourceListByOrgId(@Param("orgId") Integer orgId);

}
