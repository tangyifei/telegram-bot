package com.code.penguin.daos.penguinwebsite;

import com.code.penguin.daos.base.CrudMapper;
import com.code.penguin.models.po.PenguinWebsiteNews;
import com.code.penguin.models.qo.PenguinWebsiteNewsQO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 新闻持久层
 *
 * @author xiaoyaowang
 */
public interface PenguinWebsiteNewsMapper extends CrudMapper<PenguinWebsiteNews> {

    /**
     * 获取启用的新闻列表
     *
     * @param penguinWebsiteNewsQO 新闻查询实体类
     * @return 新闻列表
     */
    List<PenguinWebsiteNews> getEnableNewsListByCondition(PenguinWebsiteNewsQO penguinWebsiteNewsQO);

    /**
     * 获取所有新闻列表
     *
     * @param penguinWebsiteNewsQO 新闻查询实体类
     * @return 新闻列表
     */
    List<PenguinWebsiteNews> getAllNewsListByCondition(PenguinWebsiteNewsQO penguinWebsiteNewsQO);

    /**
     * 插入新闻正文内容
     *
     * @param penguinWebsiteNews 新闻实体类
     * @return 影响的行数
     */
    int insertPenguinWebsiteNewsContent(PenguinWebsiteNews penguinWebsiteNews);

    /**
     * 更新新闻正文内容
     *
     * @param penguinWebsiteNews 新闻实体类
     * @return 影响的行数
     */
    int updatePenguinWebsiteNewsContent(PenguinWebsiteNews penguinWebsiteNews);

    /**
     * 是否存在新闻正文
     *
     * @param newsId 新闻主键
     * @return 是否存在新闻正文
     */
    Integer whetherExistNewsContentByNewsId(@Param("newsId") Integer newsId);

}
