package com.code.penguin.enums;

/**
 * API统一返回状态码枚举
 *
 * @author xiaoyaowang
 */
public enum ResultCode {

    /* 成功状态码 */
    SUCCESS(1, "成功"),
    REQUEST_IS_TOO_HIGH(2, "请求太频繁"),
    PROD_METHOD_NO_AUTH_CALL(3, "没有权限调用线上的相关方法"),

    /* 参数错误：10001-19999 */
    PARAM_IS_INVALID(10001, "参数无效"),
    PARAM_IS_BLANK(10002, "参数为空"),
    PARAM_TYPE_BIND_ERROR(10003, "参数类型错误"),
    PARAM_NOT_COMPLETE(10004, "参数缺失"),
    API_VERSION_TOO_LOW(10005, "api版本号太低"),
    APP_VERSION_TOO_LOW(10006, "app版本号太低"),

    /* 用户错误：20001-29999*/
    USER_NOT_LOGGED_IN(20001, "用户未登录"),
    USER_LOGIN_ERROR(20002, "账号不存在或密码错误"),
    USER_ACCOUNT_FORBIDDEN(20003, "账号已被禁用"),
    USER_NOT_EXIST(20004, "用户不存在"),
    AUTH_CODE_ERROR(20005, "验证码不正确"),
    USER_PWD_ERROR(20006, "密码不正确"),
    TOKEN_IS_ERROR(20007, "token.is.error"),
    FORCED_OFFLINE(20008, "forced.offline"),
    TRAN_PWD_ERROR(20009, "交易密码错误"),
    ACCOUNT_NAME_ERROR(20010, "账户名不能为空"),

    MANUAL_FINISH_FOURTH_TASK_OFF_ERROR(30000, "手动关注推特功能已关闭"),

    /* 系统错误：40001-49999 */
    SYSTEM_INNER_ERROR(40001, "系统繁忙，请稍后重试"),
    MALICE_OPERATE_ERROR(40002, "恶意操作!"),

    /* 数据错误：50001-599999 */
    RESULE_DATA_NONE(50001, "数据未找到"),
    DATA_IS_WRONG(50002, "数据有误"),
    DATA_ALREADY_EXISTED(50003, "数据已存在"),

    /* 接口错误：60001-69999 */
    INTERFACE_INNER_INVOKE_ERROR(60001, "内部系统接口调用异常"),
    INTERFACE_OUTTER_INVOKE_ERROR(60002, "外部系统接口调用异常"),
    INTERFACE_FORBID_VISIT(60003, "该接口禁止访问"),
    INTERFACE_ADDRESS_INVALID(60004, "接口地址无效"),
    INTERFACE_REQUEST_TIMEOUT(60005, "接口请求超时"),
    INTERFACE_EXCEED_LOAD(60006, "接口负载过高"),

    /* 权限错误：70001-79999 */
    PERMISSION_NO_ACCESS(70001, "无访问权限"),
    RESOURCE_EXISTED(70002, "资源已存在"),
    RESOURCE_NOT_EXISTED(70003, "资源不存在"),

    /* 系统错误：90001-99999 */
    SYSTEM_PROTECT(90001, "系统维护中"),
    SUPER_ADMIN_ERROR(90002, "不是超级管理员，不能执行该操作"),
    ILLEGAL_LOGIN_IP_ERROR(90003, "恶意登录ip"),
    VALIDATE_SIGN_ERROR(90004, "验签失败"),

    /* 接口调用错误：100001-109999 */
    ERROR_CODE_100001(100001, "系统异常，请稍后重试"),
    ;

    private final Integer code;

    private final String message;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer code() {
        return this.code;
    }

    public String message() {
        return this.message;
    }

    /**
     * 获取枚举中的状态信息
     *
     * @param name 枚举名称
     * @return 相关枚举项的枚举状态信息
     */
    public static String getMessage(String name) {
        for (ResultCode item : ResultCode.values()) {
            if (item.name().equals(name)) {
                return item.message;
            }
        }
        return name;
    }

    /**
     * 获取枚举中的返回状态码
     *
     * @param name 枚举名称
     * @return 相关枚举项的枚举返回状态码
     */
    public static Integer getCode(String name) {
        for (ResultCode item : ResultCode.values()) {
            if (item.name().equals(name)) {
                return item.code;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return this.name();
    }

}
