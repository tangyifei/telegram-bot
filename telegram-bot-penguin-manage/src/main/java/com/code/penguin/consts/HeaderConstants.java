package com.code.penguin.consts;

/**
 * Header的key罗列
 *
 * @author xiaoyaowang
 */
public interface HeaderConstants {

    /**
     * 用户的登录token
     */
    String X_TOKEN = "token";

    /**
     * penguin官网调用凭证
     */
    String PENGUIN_WEBSITE_ACCESS_TOKEN = "Penguin-Website-Access-Token";

    /**
     * 账号名
     */
    String ACCOUNT_NAME = "AccountName";

    /**
     * 语言
     */
    String LANGUAGE = "App-Language";

    /**
     * 签名
     */
    String SIGN = "sign";

    /**
     * nonce
     */
    String NONCE = "nonce";

    /**
     * 时间戳
     */
    String TIMESTAMP = "timestamp";

    /**
     * 调用来源
     */
    String CALL_SOURCE = "Call-Source";

}
