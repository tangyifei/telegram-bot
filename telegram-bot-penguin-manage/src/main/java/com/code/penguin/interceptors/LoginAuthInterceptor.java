package com.code.penguin.interceptors;

import com.code.penguin.annotations.LoginAuth;
import com.code.penguin.annotations.PenguinWebSiteAccessAuth;
import com.code.penguin.consts.HeaderConstants;
import com.code.penguin.enums.ResultCode;
import com.code.penguin.exceptions.BusinessException;
import com.code.penguin.managers.HttpManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * 已登录权限验证拦截器 备注：通过{@link LoginAuth}配合使用
 *
 * @author xiaoyaowang
 */
@Slf4j
public class LoginAuthInterceptor implements HandlerInterceptor {

    @Autowired
    private HttpManager httpManager;

    @Value("${penguinWebsiteAccessToken}")
    private String penguinWebsiteAccessToken;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (handler instanceof HandlerMethod) {
            final HandlerMethod handlerMethod = (HandlerMethod) handler;
            final Class<?> clazz = handlerMethod.getBeanType();
            final Method method = handlerMethod.getMethod();

            if (clazz.isAnnotationPresent(LoginAuth.class) || method.isAnnotationPresent(LoginAuth.class)) {
                String token = request.getHeader(HeaderConstants.X_TOKEN);
                if (StringUtils.isBlank(token)) {
                    throw new BusinessException(ResultCode.USER_NOT_LOGGED_IN);
                }
                Boolean verifyToken = httpManager.verifyToken(token);
                if (!verifyToken) {
                    throw new BusinessException(ResultCode.USER_NOT_LOGGED_IN);
                }
            }
            if (clazz.isAnnotationPresent(PenguinWebSiteAccessAuth.class)
                    || method.isAnnotationPresent(PenguinWebSiteAccessAuth.class)) {
                String penguinWebsiteAccessTokenClient = request.getHeader(HeaderConstants.PENGUIN_WEBSITE_ACCESS_TOKEN);
                if (StringUtils.isBlank(penguinWebsiteAccessTokenClient)) {
                    throw new BusinessException(ResultCode.INTERFACE_FORBID_VISIT);
                }
                if (!penguinWebsiteAccessToken.equals(penguinWebsiteAccessTokenClient)) {
                    throw new BusinessException(ResultCode.INTERFACE_FORBID_VISIT);
                }
            }

        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        // nothing to do
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        // nothing to do
    }

}
