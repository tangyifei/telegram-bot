package com.code.penguin.controllers;

import com.code.penguin.annotations.LoginAuth;
import com.code.penguin.annotations.OperateLogAnno;
import com.code.penguin.annotations.PenguinWebSiteAccessAuth;
import com.code.penguin.annotations.ResponseResult;
import com.code.penguin.models.ChatEmail;
import com.code.penguin.models.Update;
import com.code.penguin.models.po.PenguinWebsiteNews;
import com.code.penguin.models.qo.ChatEmailQO;
import com.code.penguin.models.qo.PenguinWebsiteNewsQO;
import com.code.penguin.services.ChatEmailService;
import com.code.penguin.services.PenguinWebsiteNewsService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * penguin官网调用相关的控制器
 *
 * @author xiaoyaowang
 */
@ResponseResult
@RestController("penguinWebsiteController")
@RequestMapping("/penguin-websites")
public class PenguinWebsiteController {

    private final ChatEmailService chatEmailService;

    private final PenguinWebsiteNewsService penguinWebsiteNewsService;

    public PenguinWebsiteController(ChatEmailService chatEmailService, PenguinWebsiteNewsService penguinWebsiteNewsService) {
        this.chatEmailService = chatEmailService;
        this.penguinWebsiteNewsService = penguinWebsiteNewsService;
    }

    @ApiOperation(value = "分页获取邮箱地址列表", response = ChatEmail.class, notes = "分页获取邮箱地址列表", httpMethod = "POST")
    @PostMapping("/get-email-list")
    @LoginAuth
    public PageInfo<ChatEmail> getEmailListByCondition(@Validated @RequestBody ChatEmailQO chatEmailQO) {
        return chatEmailService.getEmailListByCondition(chatEmailQO);
    }

    @ApiOperation(value = "保存邮箱", notes = "保存邮箱", httpMethod = "POST")
    @PostMapping("")
    @PenguinWebSiteAccessAuth
    public int insertChatEmail(@Validated @RequestBody ChatEmail chatEmail) {
        return chatEmailService.insertChatEmail(chatEmail);
    }

    @ApiOperation(value = "发布新闻", notes = "发布新闻", httpMethod = "POST")
    @PostMapping("/news")
    @LoginAuth
    @OperateLogAnno(operateItem = "新闻", pageName = "PEN新闻管理")
    public int insertNews(@Validated @RequestBody PenguinWebsiteNews penguinWebsiteNews) {
        return penguinWebsiteNewsService.insertNews(penguinWebsiteNews);
    }

    @ApiOperation(value = "更新新闻", notes = "更新新闻", httpMethod = "PATCH")
    @PatchMapping("/news")
    @LoginAuth
    @OperateLogAnno(operateItem = "新闻", pageName = "PEN新闻管理")
    public int updateNews(@Validated(Update.class) @RequestBody PenguinWebsiteNews penguinWebsiteNews) {
        return penguinWebsiteNewsService.updateNews(penguinWebsiteNews);
    }

    @ApiOperation(value = "分页获取新闻列表", response = PenguinWebsiteNews.class, notes = "分页获取新闻列表", httpMethod = "POST")
    @PostMapping("/get-news-list")
    public PageInfo<PenguinWebsiteNews> getNewsListByCondition(@Validated @RequestBody PenguinWebsiteNewsQO penguinWebsiteNewsQO) {
        return penguinWebsiteNewsService.getNewsListByCondition(penguinWebsiteNewsQO);
    }

}
