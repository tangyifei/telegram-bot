package com.code.penguin.controllers;

import com.code.penguin.annotations.LoginAuth;
import com.code.penguin.annotations.OperateLogAnno;
import com.code.penguin.annotations.ResponseResult;
import com.code.penguin.models.RobotTaskCheck;
import com.code.penguin.models.Update;
import com.code.penguin.models.bo.TreeNode;
import com.code.penguin.models.po.ChatResource;
import com.code.penguin.models.po.OperateLog;
import com.code.penguin.models.qo.OperateLogQO;
import com.code.penguin.models.qo.RobotTaskCheckQO;
import com.code.penguin.services.ChatResourceService;
import com.code.penguin.services.OperateLogService;
import com.code.penguin.services.RobotTaskCheckService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 电报机器人相关的控制器
 *
 * @author xiaoyaowang
 */
@ResponseResult
@RestController("telegramPenguinController")
@RequestMapping("/task-checks")
public class TelegramPenguinManageController {

    private final RobotTaskCheckService robotTaskCheckService;

    private final OperateLogService operateLogService;

    private final ChatResourceService chatResourceService;

    public TelegramPenguinManageController(RobotTaskCheckService robotTaskCheckService, OperateLogService operateLogService, ChatResourceService chatResourceService) {
        this.robotTaskCheckService = robotTaskCheckService;
        this.operateLogService = operateLogService;
        this.chatResourceService = chatResourceService;
    }

    @ApiOperation(value = "更新机器人任务的审核状态", notes = "更新机器人任务的审核状态", httpMethod = "PATCH")
    @PatchMapping("")
    @LoginAuth
    @OperateLogAnno(operateItem = "审核", pageName = "PEN活动审核")
    public int updateRobotTaskState(@RequestBody @Validated(Update.class) RobotTaskCheck robotTaskCheck) {
        return robotTaskCheckService.updateRobotTaskState(robotTaskCheck);
    }

    @ApiOperation(value = "分页获取机器人任务审核列表", response = RobotTaskCheck.class, notes = "分页获取机器人任务审核列表", httpMethod = "POST")
    @PostMapping("")
    @LoginAuth
    public PageInfo<RobotTaskCheck> getRobotTaskCheckListByPage(@Validated @RequestBody RobotTaskCheckQO robotTaskCheckQO) {
        return robotTaskCheckService.getRobotTaskCheckListByPage(robotTaskCheckQO);
    }

    @ApiOperation(value = "分页获取操作日志列表", response = OperateLog.class, notes = "分页获取操作日志列表", httpMethod = "POST")
    @PostMapping("/operate-logs")
    @LoginAuth
    public PageInfo<OperateLog> getOperateLogListByPage(@Validated @RequestBody OperateLogQO operateLogQO) {
        return operateLogService.getOperateLogListByCondition(operateLogQO);
    }

    @ApiOperation(value = "获取机器人任务审核详情", response = RobotTaskCheck.class, notes = "获取机器人任务审核详情", httpMethod = "GET")
    @GetMapping("/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataType = "int", paramType = "path", required = true)
    })
    @LoginAuth
    public RobotTaskCheck getRobotTaskCheckById(@PathVariable("id") Integer id) {
        return robotTaskCheckService.getRobotTaskCheckById(id);
    }

    @ApiOperation(value = "形成树结构", notes = "形成树结构", httpMethod = "GET")
    @GetMapping("/chat-resources-tree")
    public List<TreeNode<ChatResource>> getChatResourceTree() {
        return chatResourceService.getChatResourceTree();
    }

}
