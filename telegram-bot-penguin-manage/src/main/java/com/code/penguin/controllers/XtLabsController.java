package com.code.penguin.controllers;

import com.code.penguin.annotations.LoginAuth;
import com.code.penguin.annotations.ResponseResult;
import com.code.penguin.models.XtLab;
import com.code.penguin.models.qo.XtLabsQO;
import com.code.penguin.services.XtLabService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * xtLabs相关的控制器
 *
 * @author xiaoyaowang
 */
@ResponseResult
@RestController("XtLabsController")
@RequestMapping("/xt-labs")
public class XtLabsController {

    private final XtLabService xtLabService;

    public XtLabsController(XtLabService xtLabService) {
        this.xtLabService = xtLabService;
    }

    @ApiOperation(value = "添加XtLabs", notes = "添加XtLabs", httpMethod = "POST")
    @PostMapping("")
    public void addOrUpdateXtLabs(@RequestBody @Validated XtLab xtLab) {
        xtLabService.insertOrUpdateXtLab(xtLab);
    }

    @ApiOperation(value = "获取XtLabs详情", response = XtLab.class, notes = "获取XtLabs详情", httpMethod = "GET")
    @GetMapping("/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataType = "int", paramType = "path", required = true)
    })
    @LoginAuth
    public XtLab getXtLabById(@PathVariable("id") Integer id) {
        return xtLabService.getXtLabByCondition(id);
    }

    @ApiOperation(value = "分页获取XtLabs申请信息列表", response = XtLab.class, notes = "分页获取XtLabs申请信息列表", httpMethod = "POST")
    @PostMapping("/lab-list")
    @LoginAuth
    public PageInfo<XtLab> getXtLabListByPage(@Validated @RequestBody XtLabsQO xtLabsQO) {
        return xtLabService.getXtLabListByPage(xtLabsQO);
    }

}
