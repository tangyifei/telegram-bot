package com.code.penguin.annotations;

import java.lang.annotation.*;

/**
 * penguin官网校验验证注解
 *
 * @author xiaoyaowang
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PenguinWebSiteAccessAuth {

}
