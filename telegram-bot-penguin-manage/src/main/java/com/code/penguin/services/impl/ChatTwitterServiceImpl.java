package com.code.penguin.services.impl;

import com.code.penguin.consts.CommonConsts;
import com.code.penguin.daos.robot.ChatTwitterMapper;
import com.code.penguin.services.ChatTwitterService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 关注推特记录服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatTwitterServiceImpl implements ChatTwitterService {

    @Resource
    ChatTwitterMapper chatTwitterMapper;

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public void deleteChatTwitterByChatId(String chatId) {
        if (null != chatTwitterMapper.existsChatTwitterByChatId(chatId)) {
            chatTwitterMapper.deleteChatTwitterByChatId(chatId);
        }
    }

}
