package com.code.penguin.services.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.code.penguin.daos.robot.ChatResourceMapper;
import com.code.penguin.models.bo.ConvertTree;
import com.code.penguin.models.bo.TreeNode;
import com.code.penguin.models.po.ChatResource;
import com.code.penguin.services.ChatResourceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

@Service
public class ChatResourceServiceImpl implements ChatResourceService {

    @Resource
    private ChatResourceMapper chatResourceMapper;

    @Override
    public List<TreeNode<ChatResource>> getChatResourceTree() {
        List<ChatResource> list = chatResourceMapper.getChatResourceListByOrgId(1);
        if (CollectionUtil.isNotEmpty(list)) {
            ConvertTree<ChatResource> convertTree = new ConvertTree<>();
            return convertTree.getForest(list);
        }
        return Collections.emptyList();
    }
}
