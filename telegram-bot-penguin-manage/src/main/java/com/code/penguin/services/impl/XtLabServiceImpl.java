package com.code.penguin.services.impl;

import com.code.penguin.daos.robot.XtLabMapper;
import com.code.penguin.models.XtLab;
import com.code.penguin.models.qo.XtLabsQO;
import com.code.penguin.services.XtLabService;
import com.code.penguin.utils.TimeUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * XtLab服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class XtLabServiceImpl implements XtLabService {

    @Resource
    private XtLabMapper xtLabMapper;

    @Override
    public void insertOrUpdateXtLab(XtLab xtLab) {
        //Integer id = xtLab.getId();
        //if (null != id && null != xtLabMapper.selectByPrimaryKey(id)) {
        //    xtLab.setUpdatedAt(new Date());
        //    xtLabMapper.updateByPrimaryKeySelective(xtLab);
        //} else {
        //    synchronized (XtLabServiceImpl.class) {
        //        if (xtLabMapper.getXtLabCount() == 0) {
        //
        //        }
        //    }
        //
        //}
        xtLab.setDeleted(0);
        xtLab.setCreatedAt(new Date());
        xtLab.setUpdatedAt(new Date());
        xtLabMapper.insert(xtLab);
    }

    @Override
    public XtLab getXtLabByCondition(Integer id) {
        return xtLabMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageInfo<XtLab> getXtLabListByPage(XtLabsQO xtLabsQO) {
        Integer pageNum = xtLabsQO.getPageNum();
        Integer pageSize = xtLabsQO.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        return PageInfo.of(xtLabMapper.getXtLabListByPage(xtLabsQO));
    }
}
