package com.code.penguin.services;

import com.code.penguin.models.ChatEmail;
import com.code.penguin.models.qo.ChatEmailQO;
import com.github.pagehelper.PageInfo;

/**
 * 用户邮箱业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatEmailService {

    /**
     * 获取邮箱地址列表
     *
     * @param chatEmailQO 邮箱查询实体类
     * @return 邮箱地址列表
     */
    PageInfo<ChatEmail> getEmailListByCondition(ChatEmailQO chatEmailQO);

    /**
     * 插入邮箱
     *
     * @param email 邮箱地址对象
     * @return 影响的行数
     */
    int insertChatEmail(ChatEmail email);

}
