package com.code.penguin.services;

import com.code.penguin.models.po.PenguinWebsiteNews;
import com.code.penguin.models.qo.PenguinWebsiteNewsQO;
import com.github.pagehelper.PageInfo;

/**
 * 新闻业务接口
 *
 * @author xiaoyaowang
 */
public interface PenguinWebsiteNewsService {

    /**
     * 获取新闻列表
     *
     * @param penguinWebsiteNewsQO 新闻查询实体类
     * @return 新闻列表
     */
    PageInfo<PenguinWebsiteNews> getNewsListByCondition(PenguinWebsiteNewsQO penguinWebsiteNewsQO);

    /**
     * 插入新闻
     *
     * @param penguinWebsiteNews 新闻对象
     * @return 影响的行数
     */
    int insertNews(PenguinWebsiteNews penguinWebsiteNews);

    /**
     * 通过主键获取英文标题
     *
     * @param id 主键
     * @return 英文标题
     */
    String getNewsEnTitleById(Integer id);

    /**
     * 更新新闻
     *
     * @param penguinWebsiteNews 新闻对象
     * @return 影响的行数
     */
    int updateNews(PenguinWebsiteNews penguinWebsiteNews);

}
