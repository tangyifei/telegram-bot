package com.code.penguin.services.impl;

import com.code.penguin.consts.CommonConsts;
import com.code.penguin.daos.penguinwebsite.PenguinWebsiteNewsMapper;
import com.code.penguin.models.po.PenguinWebsiteNews;
import com.code.penguin.models.qo.PenguinWebsiteNewsQO;
import com.code.penguin.services.PenguinWebsiteNewsService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 新闻服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class PenguinWebsiteNewsImpl implements PenguinWebsiteNewsService {

    @Resource
    PenguinWebsiteNewsMapper penguinWebsiteNewsMapper;

    @Override
    public PageInfo<PenguinWebsiteNews> getNewsListByCondition(PenguinWebsiteNewsQO penguinWebsiteNewsQO) {
        Integer pageNum = penguinWebsiteNewsQO.getPageNum();
        Integer pageSize = penguinWebsiteNewsQO.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<PenguinWebsiteNews> penguinWebsiteNewsList;
        if (0 == penguinWebsiteNewsQO.getIsShowDisableNews()) {
            penguinWebsiteNewsList = penguinWebsiteNewsMapper.getEnableNewsListByCondition(penguinWebsiteNewsQO);
        } else {
            penguinWebsiteNewsList = penguinWebsiteNewsMapper.getAllNewsListByCondition(penguinWebsiteNewsQO);
        }
        return PageInfo.of(penguinWebsiteNewsList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public int insertNews(PenguinWebsiteNews penguinWebsiteNews) {
        penguinWebsiteNews.setEnableState(1);
        penguinWebsiteNews.setDeleted(0);
        penguinWebsiteNews.setCreatedAt(new Date());
        penguinWebsiteNews.setUpdatedAt(new Date());
        int count = penguinWebsiteNewsMapper.insert(penguinWebsiteNews);
        count += penguinWebsiteNewsMapper.insertPenguinWebsiteNewsContent(penguinWebsiteNews);
        return count;
    }

    @Override
    public String getNewsEnTitleById(Integer id) {
        PenguinWebsiteNews penguinWebsiteNewsForDb = penguinWebsiteNewsMapper.selectByPrimaryKey(id);
        if (null != penguinWebsiteNewsForDb) {
            return penguinWebsiteNewsForDb.getNewsEnTitle();
        }
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public int updateNews(PenguinWebsiteNews penguinWebsiteNews) {
        Integer id = penguinWebsiteNews.getId();
        PenguinWebsiteNews penguinWebsiteNewsForDb = penguinWebsiteNewsMapper.selectByPrimaryKey(id);
        int count = 0;
        if (null != penguinWebsiteNewsForDb) {
            penguinWebsiteNews.setUpdatedAt(new Date());
            count += penguinWebsiteNewsMapper.updateByPrimaryKeySelective(penguinWebsiteNews);
        }
        Integer whetherExistNewsContent = penguinWebsiteNewsMapper.whetherExistNewsContentByNewsId(id);
        if (null != whetherExistNewsContent) {
            penguinWebsiteNews.setUpdatedAt(new Date());
            count += penguinWebsiteNewsMapper.updatePenguinWebsiteNewsContent(penguinWebsiteNews);
        }
        return count;
    }

}
