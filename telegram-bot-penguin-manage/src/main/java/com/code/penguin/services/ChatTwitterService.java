package com.code.penguin.services;

/**
 * 关注推特记录业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatTwitterService {

    /**
     * 通过聊天唯一标识删除用户关注的推特记录
     *
     * @param chatId 聊天唯一标识
     */
    void deleteChatTwitterByChatId(String chatId);

}
