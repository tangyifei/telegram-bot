package com.code.penguin.services.impl;

import com.code.penguin.daos.robot.OperateLogMapper;
import com.code.penguin.models.po.OperateLog;
import com.code.penguin.models.qo.OperateLogQO;
import com.code.penguin.services.OperateLogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 操作日志服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class OperateLogServiceImpl implements OperateLogService {

    @Resource
    private OperateLogMapper operateLogMapper;

    @Override
    public PageInfo<OperateLog> getOperateLogListByCondition(OperateLogQO operateLogQO) {
        Integer pageNum = operateLogQO.getPageNum();
        Integer pageSize = operateLogQO.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        return PageInfo.of(operateLogMapper.getOperateLogListByCondition(operateLogQO));
    }

    @Override
    public int insertOperateLog(OperateLog operateLog) {
        operateLog.setDeleted(0);
        operateLog.setCreatedAt(new Date());
        operateLog.setUpdatedAt(new Date());
        return operateLogMapper.insert(operateLog);
    }
}
