package com.code.penguin.services;

import com.code.penguin.models.RobotTaskCheck;
import com.code.penguin.models.qo.RobotTaskCheckQO;
import com.github.pagehelper.PageInfo;

/**
 * 机器人任务审核业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotTaskCheckService {

    /**
     * 更新任务的审核状态
     *
     * @param robotTaskCheck 机器人任务审核记录实体类
     * @return 影响的行数
     */
    int updateRobotTaskState(RobotTaskCheck robotTaskCheck);

    /**
     * 分页获取审核的任务列表
     *
     * @param robotTaskCheckQO 机器人任务审核记录查询实体类QO
     * @return 分页过后的审核任务列表
     */
    PageInfo<RobotTaskCheck> getRobotTaskCheckListByPage(RobotTaskCheckQO robotTaskCheckQO);

    /**
     * 通过主键获取单个审核任务
     *
     * @param id 主键
     * @return 单个审核任务
     */
    RobotTaskCheck getRobotTaskCheckById(int id);

}
