package com.code.penguin.services.impl;

import com.code.penguin.daos.robot.ChatLinkMapper;
import com.code.penguin.services.ChatLinkService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户分享链接服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatLinkServiceImpl implements ChatLinkService {

    @Resource
    ChatLinkMapper chatLinkMapper;

    @Override
    public String getChatLinkListByChatIdAndLinkCategory(String chatId, Integer linkCategory) {
        return chatLinkMapper.getChatLinkListByChatIdAndLinkCategory(chatId, linkCategory);
    }

    @Override
    public void deleteChatLinkByChatId(String chatId) {
        if (null != chatLinkMapper.existsChatLinkByChatId(chatId)) {
            chatLinkMapper.deleteChatLinkByChatId(chatId);
        }
    }
}
