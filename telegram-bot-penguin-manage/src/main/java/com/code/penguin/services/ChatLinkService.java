package com.code.penguin.services;

import java.util.List;

/**
 * 用户分享链接业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatLinkService {

    /**
     * 根据用户链接地址和链接种类获取用户的分享链接
     *
     * @param chatId       聊天唯一id
     * @param linkCategory 分享链接种类
     * @return 用户的分享链接
     */
    String getChatLinkListByChatIdAndLinkCategory(String chatId, Integer linkCategory);

    /**
     * 通过聊天唯一标识删除用户关注的链接记录
     *
     * @param chatId 聊天唯一标识
     */
    void deleteChatLinkByChatId(String chatId);

}
