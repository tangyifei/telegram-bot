package com.code.penguin.services.impl;

import com.code.penguin.daos.robot.ChatEmailMapper;
import com.code.penguin.models.ChatEmail;
import com.code.penguin.models.qo.ChatEmailQO;
import com.code.penguin.services.ChatEmailService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 邮箱地址服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatEmailServiceImpl implements ChatEmailService {

    @Resource
    ChatEmailMapper chatEmailMapper;

    @Override
    public PageInfo<ChatEmail> getEmailListByCondition(ChatEmailQO chatEmailQO) {
        Integer pageNum = chatEmailQO.getPageNum();
        Integer pageSize = chatEmailQO.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        return PageInfo.of(chatEmailMapper.getEmailListByCondition(chatEmailQO));
    }

    @Override
    public int insertChatEmail(ChatEmail chatEmail) {
        if (null == chatEmailMapper.whetherExistEmailByEmail(chatEmail.getEmail())) {
            chatEmail.setChatId("0");
            chatEmail.setDeleted(0);
            chatEmail.setCreatedAt(new Date());
            chatEmail.setUpdatedAt(new Date());
            return chatEmailMapper.insert(chatEmail);
        }
        return 0;
    }
}
