package com.code.penguin.services;

import com.code.penguin.models.XtLab;
import com.code.penguin.models.qo.XtLabsQO;
import com.github.pagehelper.PageInfo;

/**
 * XtLab业务接口
 *
 * @author xiaoyaowang
 */
public interface XtLabService {

    void insertOrUpdateXtLab(XtLab xtLab);

    XtLab getXtLabByCondition(Integer id);

    PageInfo<XtLab> getXtLabListByPage(XtLabsQO xtLabsQO);
}
