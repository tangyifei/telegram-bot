package com.code.penguin.services.impl;

import com.code.penguin.consts.CommonConsts;
import com.code.penguin.daos.robot.RobotTaskCheckMapper;
import com.code.penguin.managers.HttpManager;
import com.code.penguin.models.RobotChatFinishTask;
import com.code.penguin.models.RobotTaskCheck;
import com.code.penguin.models.qo.RobotTaskCheckQO;
import com.code.penguin.services.ChatLinkService;
import com.code.penguin.services.ChatTwitterService;
import com.code.penguin.services.RobotTaskCheckService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 机器人任务审核服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class RobotTaskCheckServiceImpl implements RobotTaskCheckService {

    @Resource
    RobotTaskCheckMapper robotTaskCheckMapper;

    @Resource
    ChatLinkService chatLinkService;

    @Resource
    ChatTwitterService chatTwitterService;

    @Resource
    HttpManager httpManager;

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public int updateRobotTaskState(RobotTaskCheck robotTaskCheckQuery) {
        Integer id = robotTaskCheckQuery.getId();
        Integer whetherExistRobotTaskCheck = robotTaskCheckMapper.whetherExistRobotTaskCheckById(id);
        int count = 0;
        if (null != whetherExistRobotTaskCheck) {
            Integer state = robotTaskCheckQuery.getState();
            if (null == state) {
                state = 0;
            }
            String chatId = robotTaskCheckMapper.getChatIdById(id);
            if (state == 0) {
                if (null != robotTaskCheckMapper.existsChatBanByChatId(chatId)) {
                    robotTaskCheckMapper.deleteChatBanByChatId(chatId);
                }
            }
            // 审核失败
            if (state == 1) {
                // 删除第六步加群任务
                Integer sixthTaskId = robotTaskCheckMapper.getTaskIdBySort(6);
                RobotChatFinishTask robotChatFinishSixthTaskDb = robotTaskCheckMapper.getRobotChatFinishTaskByChatIdAndTaskId(chatId, sixthTaskId);
                if (null != robotChatFinishSixthTaskDb) {
                    robotTaskCheckMapper.deleteRobotChatFinishTask(robotChatFinishSixthTaskDb);
                }

                // 删除第五步收集任务
                Integer fifthTaskId = robotTaskCheckMapper.getTaskIdBySort(5);
                RobotChatFinishTask robotChatFinishFifthTaskDb = robotTaskCheckMapper.getRobotChatFinishTaskByChatIdAndTaskId(chatId, fifthTaskId);
                if (null != robotChatFinishFifthTaskDb) {
                    robotTaskCheckMapper.deleteRobotChatFinishTask(robotChatFinishFifthTaskDb);
                }
                Integer chatCollectId = robotTaskCheckMapper.getChatCollectIdByChatId(chatId);
                if (null != chatCollectId) {
                    Integer whetherCollectContent = robotTaskCheckMapper.whetherCollectContentByCollectId(chatCollectId);
                    if (null != whetherCollectContent) {
                        robotTaskCheckMapper.deleteCollectContentByCollectId(chatCollectId);
                    }
                    robotTaskCheckMapper.deleteCollectByCollectId(chatCollectId);
                }

                // 删除第4步任务
                Integer fourthTaskId = robotTaskCheckMapper.getTaskIdBySort(4);
                RobotChatFinishTask robotChatFinishFourthTaskDb = robotTaskCheckMapper.getRobotChatFinishTaskByChatIdAndTaskId(chatId, fourthTaskId);
                if (null != robotChatFinishFourthTaskDb) {
                    robotTaskCheckMapper.deleteRobotChatFinishTask(robotChatFinishFourthTaskDb);
                }
                Integer whetherExistWalletAddress = robotTaskCheckMapper.whetherExistWalletAddressByChatId(chatId);
                if (null != whetherExistWalletAddress) {
                    robotTaskCheckMapper.deleteWalletAddressByChatId(chatId);
                }

                // 删除第3步任务
                Integer thirdTaskId = robotTaskCheckMapper.getTaskIdBySort(3);
                RobotChatFinishTask robotChatFinishThirdTaskDb = robotTaskCheckMapper.getRobotChatFinishTaskByChatIdAndTaskId(chatId, thirdTaskId);
                if (null != robotChatFinishThirdTaskDb) {
                    robotTaskCheckMapper.deleteRobotChatFinishTask(robotChatFinishThirdTaskDb);
                }
                chatTwitterService.deleteChatTwitterByChatId(chatId);
                chatLinkService.deleteChatLinkByChatId(chatId);

                // 删除第2步任务
                Integer secondTaskId = robotTaskCheckMapper.getTaskIdBySort(2);
                RobotChatFinishTask robotChatFinishSecondTaskDb = robotTaskCheckMapper.getRobotChatFinishTaskByChatIdAndTaskId(chatId, secondTaskId);
                if (null != robotChatFinishSecondTaskDb) {
                    robotTaskCheckMapper.deleteRobotChatFinishTask(robotChatFinishSecondTaskDb);
                }
                Integer whetherExistDiscord = robotTaskCheckMapper.whetherExistDiscordByChatId(chatId);
                if (null != whetherExistDiscord) {
                    robotTaskCheckMapper.deleteDiscordByChatId(chatId);
                }

                // 删除第1步任务
                Integer firstTaskId = robotTaskCheckMapper.getTaskIdBySort(1);
                RobotChatFinishTask robotChatFinishFirstTaskDb = robotTaskCheckMapper.getRobotChatFinishTaskByChatIdAndTaskId(chatId, firstTaskId);
                if (null != robotChatFinishFirstTaskDb) {
                    robotTaskCheckMapper.deleteRobotChatFinishTask(robotChatFinishFirstTaskDb);
                }
                Integer whetherExistEmail = robotTaskCheckMapper.whetherExistEmailByChatId(chatId);
                if (null != whetherExistEmail) {
                    robotTaskCheckMapper.deleteEmailByChatId(chatId);
                }

                httpManager.sendMessage(Long.parseLong(chatId), "Your task was returned due to unqualified, Please re-submit the task again.");
            }
            count = robotTaskCheckMapper.updateRobotTaskState(id, state, new Date());
        }
        return count;
    }

    @Override
    public PageInfo<RobotTaskCheck> getRobotTaskCheckListByPage(RobotTaskCheckQO robotTaskCheckQO) {
        Integer sort = robotTaskCheckQO.getSort();
        if (null == sort) {
            robotTaskCheckQO.setSort(2);
        }
        Integer pageNum = robotTaskCheckQO.getPageNum();
        Integer pageSize = robotTaskCheckQO.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<RobotTaskCheck> list = robotTaskCheckMapper.getRobotTaskCheckListByPage(robotTaskCheckQO);
        if (!CollectionUtils.isEmpty(list)) {
            list.forEach(item -> {
                if (null != robotTaskCheckMapper.existsChatBanByChatId(item.getChatId())) {
                    item.setState(3);
                }
            });
        }
        return PageInfo.of(list);
    }

    @Override
    public RobotTaskCheck getRobotTaskCheckById(int id) {
        RobotTaskCheck robotTaskCheck = robotTaskCheckMapper.getRobotTaskCheckById(id);
        if (null != robotTaskCheck) {
            String chatId = robotTaskCheck.getChatId();
            if (null != robotTaskCheckMapper.existsChatBanByChatId(chatId)) {
                robotTaskCheck.setState(3);
            }
            robotTaskCheck.setTwitterShareLink(chatLinkService.getChatLinkListByChatIdAndLinkCategory(chatId, 2));
            // 获取留言信息
            robotTaskCheck.setCollectContent(robotTaskCheckMapper.getChatCollectContentByChatId(chatId));
        }
        return robotTaskCheck;
    }

}
