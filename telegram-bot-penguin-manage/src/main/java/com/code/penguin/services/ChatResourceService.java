package com.code.penguin.services;

import com.code.penguin.models.bo.TreeNode;
import com.code.penguin.models.po.ChatResource;

import java.util.List;

/**
 * 形成树木业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatResourceService {

    List<TreeNode<ChatResource>> getChatResourceTree();

}
