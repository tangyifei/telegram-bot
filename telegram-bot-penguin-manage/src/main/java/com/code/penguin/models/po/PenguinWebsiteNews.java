package com.code.penguin.models.po;

import com.code.penguin.models.BaseRobot;
import com.code.penguin.models.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * penguin官网新闻实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("penguin官网新闻实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PenguinWebsiteNews extends BaseRobot {

    private static final long serialVersionUID = -5411411354664132129L;

    @ApiModelProperty(value = "新闻中文标题 非必填 字符串类型 长度100个字符", example = "神州十三号太空开课")
    @Length(min = 1, max = 100, message = "长度100个字符")
    private String newsCnTitle;

    @ApiModelProperty(value = "新闻英文标题 非必填 字符串类型 长度100个字符", example = "Shenzhou 13 Space Class begins")
    @Length(min = 1, max = 100, message = "长度100个字符")
    private String newsEnTitle;

    @ApiModelProperty(value = "0-停用 1-启用 整型 非必填", example = "1")
    @NotNull(groups = {Update.class})
    private Integer enableState;

    @ApiModelProperty(value = "新闻中文内容 非必填 字符串类型", example = "神州十三号太空开课")
    @Length(min = 1)
    @Transient
    private String newsCnContent;

    @ApiModelProperty(value = "新闻英文内容 非必填 字符串类型", example = "Shenzhou 13 Space Class begins")
    @Length(min = 1)
    @Transient
    private String newsEnContent;

}
