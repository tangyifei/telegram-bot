package com.code.penguin.models;

import java.io.Serializable;

/**
 * model类
 *
 * @author xiaoyaowang
 */
public interface Model extends Serializable {
}
