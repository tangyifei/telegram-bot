package com.code.penguin.models.po;

import com.code.penguin.models.BaseRobot;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 操作日志实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("操作日志实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OperateLog extends BaseRobot {

    private static final long serialVersionUID = -2495855536798003458L;

    @ApiModelProperty(value = "客户端的ip地址", example = "192.168.1.10")
    private String clientIp;

    @ApiModelProperty(value = "账号名称", example = "admin")
    private String accountName;

    @ApiModelProperty(value = "页面名称", example = "TP活动审核")
    private String pageName;

    @ApiModelProperty(value = "telegram账号名称", example = "shenzhou")
    private String telegramName;

    @ApiModelProperty(value = "钱包地址", example = "XWCNMwnptDYwpbFdjBXmZWLfQFEHTVsKdchVV")
    private String walletAddr;

    @ApiModelProperty(value = "操作项（比如审核项）", example = "审核")
    private String operateItem;

    @ApiModelProperty(value = "操作类型（审核成功）即操作详情", example = "审核成功")
    private String operateType;

    @ApiModelProperty(value = "用户的浏览器引擎", example = "用户的浏览器引擎")
    private String userAgent;

}
