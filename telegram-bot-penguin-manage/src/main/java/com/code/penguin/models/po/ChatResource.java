package com.code.penguin.models.po;

import com.code.penguin.annotations.TreeFid;
import com.code.penguin.annotations.TreeId;
import com.code.penguin.models.BaseRobot;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 资源实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("资源实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatResource extends BaseRobot {

    private static final long serialVersionUID = -5974686559793888635L;

    @TreeId
    private Integer id;

    private Integer deleted;

    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "Asia/Shanghai"
    )
    private Date createdAt;

    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "Asia/Shanghai"
    )
    private Date updatedAt;

    @TreeFid
    private Integer parentId;

    private Integer orgId;

    private Integer productId;

    private Integer type;

    private String name;

    private String url;

    private Integer sort;

    private String note;

}
