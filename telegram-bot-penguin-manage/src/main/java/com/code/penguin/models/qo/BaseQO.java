package com.code.penguin.models.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 基本查询父类
 *
 * @author xiaoyaowang
 */
@ApiModel("基本查询父类QO")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseQO implements Serializable {

    private static final long serialVersionUID = 30796789034952272L;

    @ApiModelProperty(value = "当前页码 整型 必填", example = "0")
    @NotNull(message = "当前页码不能为空")
    private Integer pageNum;

    @ApiModelProperty(value = "每页的大小 整型 必填", example = "10")
    @NotNull(message = "每页的大小不能为空")
    private Integer pageSize;

}
