package com.code.penguin.models.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 新闻查询实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("新闻查询实体类")
@Data
@NoArgsConstructor
public class PenguinWebsiteNewsQO extends BaseQO {

    private static final long serialVersionUID = 3057408659838711358L;

    @ApiModelProperty(value = "展示禁用新闻与否 整型 为0不展示 为1展示 必填", example = "0")
    @NotNull(message = "展示禁用新闻与否不为空")
    private Integer isShowDisableNews;

}
