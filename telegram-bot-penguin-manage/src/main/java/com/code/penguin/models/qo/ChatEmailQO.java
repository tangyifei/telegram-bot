package com.code.penguin.models.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 邮箱查询实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("邮箱查询实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatEmailQO extends BaseQO {

    private static final long serialVersionUID = -3397446774874888216L;

    @ApiModelProperty(value = "邮箱地址 字符串类型 非必填", example = "tyf@163.com")
    private String email;

}
