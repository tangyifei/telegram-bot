package com.code.penguin.models.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * XTLabs记录查询实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("XTLabs记录查询实体类QO")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class XtLabsQO extends BaseQO {

    private static final long serialVersionUID = -7496653660734005099L;

    @ApiModelProperty(value = "开始时间 日期时间类型 非必填", example = "2022-02-17")
    private Date startDateTime;

    @ApiModelProperty(value = "结束时间 日期时间类型 非必填", example = "2022-02-17")
    private Date endDateTime;

}
