package com.code.penguin.configs.redis;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * jedis相关的配置
 *
 * @author xiaoyaowang
 */
@Configuration
@ConfigurationProperties(prefix = "spring.redis")
@Getter
@Setter
public class JedisConfig {

    private Integer database;

    private String host;

    private String password;

    private Integer timeout;

    private Integer port;

    @Resource
    private Pool pool;

    @Configuration
    @ConfigurationProperties(prefix = "spring.redis.jedis.pool")
    @Getter
    @Setter
    class Pool {

        private Integer maxIdle;

        private Integer minIdle;

        private Integer maxActive;

        private Integer maxWait;

        private Boolean testOnCreate;

        /**
         * 在获取连接的时候检查有效性，不然redis使用一段时间后会报RedisConnectionFailureException: java.net.SocketException: Broken pipe错误，检查到无效的连接时会清除无效的连接
         */
        private Boolean testOnBorrow;

        /**
         * 当调用return Object方法时，是否进行有效性检查，不然redis使用一段时间后会报RedisConnectionFailureException: java.net.SocketException: Broken pipe错误，检查到无效的连接时会清除无效的连接
         */
        private Boolean testOnReturn;

        /**
         * 在空闲时检查有效性，不然redis使用一段时间后会报RedisConnectionFailureException: java.net.SocketException: Broken pipe错误，检查到无效的连接时会清除无效的连接
         */
        private Boolean testWhileIdle;

    }

}
