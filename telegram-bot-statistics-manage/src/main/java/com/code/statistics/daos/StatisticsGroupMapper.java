package com.code.statistics.daos;

import com.code.statistics.daos.base.CrudMapper;
import com.code.statistics.models.StatisticsGroup;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 群组统计持久层
 *
 * @author xiaoyaowang
 */
public interface StatisticsGroupMapper extends CrudMapper<StatisticsGroup> {

    /**
     * 获取群组列表
     *
     * @return 群组列表
     */
    List<StatisticsGroup> getStatisticsGroupList();

    /**
     * 判断是否存在相关的群组
     *
     * @param id 主键
     * @param groupUrl 群组链接地址
     * @return 是否存在相关的群组
     */
    Integer whetherExistGroup(@Param("id") Integer id, @Param("groupUrl") String groupUrl);

}
