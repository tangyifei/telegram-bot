package com.code.statistics.daos;

import com.code.statistics.daos.base.CrudMapper;
import com.code.statistics.models.CurrentDayGroupStatistics;
import com.code.statistics.models.qo.StatisticsQO;

import java.util.List;

/**
 * 当日群组统计信息持久层
 *
 * @author xiaoyaowang
 */
public interface CurrentDayGroupStatisticsMapper extends CrudMapper<CurrentDayGroupStatistics> {

    /**
     * 根据相关的条件获取当日群组统计信息列表
     *
     * @param statisticsQO 群组统计查询实体类
     * @return 当日群组统计信息列表
     */
    List<CurrentDayGroupStatistics> getCurrentDayGroupStatisticsListByRelativeCondition(StatisticsQO statisticsQO);

    /**
     * 默认获取前一天的数据
     *
     * @return 群组统计信息列表
     */
    List<CurrentDayGroupStatistics> getBeforeDayGroupStatisticsList();

}
