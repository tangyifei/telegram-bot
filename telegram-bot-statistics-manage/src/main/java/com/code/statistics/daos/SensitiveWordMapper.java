package com.code.statistics.daos;

import com.code.statistics.daos.base.CrudMapper;
import com.code.statistics.models.SensitiveWord;
import com.code.statistics.models.StatisticsGroup;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 群组敏感词汇持久层
 *
 * @author xiaoyaowang
 */
public interface SensitiveWordMapper extends CrudMapper<SensitiveWord> {

    /**
     * 获取敏感词汇列表
     *
     * @return 敏感词汇列表
     */
    List<SensitiveWord> getSensitiveWordList();

    /**
     * 判断是否存在相关的敏感词汇
     *
     * @param id 主键
     * @return 是否存在相关的敏感词汇
     */
    Integer whetherExistSensitiveWord(@Param("id") Integer id);

}
