package com.code.statistics.services;

import com.code.statistics.models.SensitiveWord;
import com.code.statistics.models.qo.BaseQO;
import com.github.pagehelper.PageInfo;

/**
 * 敏感词汇业务接口
 *
 * @author xiaoyaowang
 */
public interface SensitiveWordService {

    /**
     * 获取敏感词汇列表
     *
     * @param baseQO 基本查询父类QO
     * @return 敏感词汇列表
     */
    PageInfo<SensitiveWord> getSensitiveWordListByCondition(BaseQO baseQO);

    /**
     * 插入敏感词汇
     *
     * @param sensitiveWord 敏感词汇对象
     * @return 影响的行数
     */
    int insertSensitiveWord(SensitiveWord sensitiveWord);

    /**
     * 编辑敏感词汇
     *
     * @param sensitiveWord 敏感词汇对象
     * @return 影响的行数
     */
    int modifySensitiveWord(SensitiveWord sensitiveWord);

    /**
     * 删除敏感词汇
     *
     * @param id 主键
     * @return 影响的行数
     */
    int deleteSensitiveWordById(Integer id);

}
