package com.code.statistics.services;

import com.code.statistics.models.StatisticsGroup;
import com.code.statistics.models.qo.StatisticsQO;
import com.github.pagehelper.PageInfo;

/**
 * 群组统计业务接口
 *
 * @author xiaoyaowang
 */
public interface StatisticsGroupService {

    /**
     * 获取群组列表
     *
     * @param statisticsQO 统计查询实体类
     * @return 群组列表
     */
    PageInfo<StatisticsGroup> getStatisticsGroupListByCondition(StatisticsQO statisticsQO);

    /**
     * 插入群组
     *
     * @param statisticsGroup 群组对象
     * @return 影响的行数
     */
    int insertStatisticsGroup(StatisticsGroup statisticsGroup);

    /**
     * 编辑群组
     *
     * @param statisticsGroup 群组对象
     * @return 影响的行数
     */
    int modifyStatisticsGroup(StatisticsGroup statisticsGroup);

    /**
     * 删除群组
     *
     * @param id 主键
     * @return 影响的行数
     */
    int deleteStatisticsGroupById(Integer id);

}
