package com.code.statistics.services.impl;

import com.code.statistics.daos.StatisticsGroupMapper;
import com.code.statistics.models.StatisticsGroup;
import com.code.statistics.models.qo.StatisticsQO;
import com.code.statistics.services.StatisticsGroupService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 群组统计服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class StatisticsGroupServiceImpl implements StatisticsGroupService {

    @Resource
    private StatisticsGroupMapper statisticsGroupMapper;

    @Override
    public PageInfo<StatisticsGroup> getStatisticsGroupListByCondition(StatisticsQO statisticsQO) {
        Integer pageNum = statisticsQO.getPageNum();
        Integer pageSize = statisticsQO.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        return PageInfo.of(statisticsGroupMapper.getStatisticsGroupList());
    }

    @Override
    public int insertStatisticsGroup(StatisticsGroup statisticsGroup) {
        if (null == statisticsGroupMapper.whetherExistGroup(null, statisticsGroup.getGroupUrl())) {
            statisticsGroup.setDeleted(0);
            statisticsGroup.setCreatedAt(new Date());
            statisticsGroup.setUpdatedAt(new Date());
            return statisticsGroupMapper.insert(statisticsGroup);
        }
        return 0;
    }

    @Override
    public int modifyStatisticsGroup(StatisticsGroup statisticsGroup) {
        if (null != statisticsGroupMapper.whetherExistGroup(statisticsGroup.getId(), null)) {
            statisticsGroup.setUpdatedAt(new Date());
            return statisticsGroupMapper.updateByPrimaryKeySelective(statisticsGroup);
        }
        return 0;
    }

    @Override
    public int deleteStatisticsGroupById(Integer id) {
        if (null != statisticsGroupMapper.whetherExistGroup(id, null)) {
            return statisticsGroupMapper.deleteByPrimaryKey(id);
        }
        return 0;
    }
}
