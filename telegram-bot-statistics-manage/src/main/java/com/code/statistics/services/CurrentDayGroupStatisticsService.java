package com.code.statistics.services;

import com.code.statistics.models.CurrentDayGroupStatistics;
import com.code.statistics.models.StatisticsGroup;
import com.code.statistics.models.qo.StatisticsQO;
import com.github.pagehelper.PageInfo;

/**
 * 当日群组统计业务接口
 *
 * @author xiaoyaowang
 */
public interface CurrentDayGroupStatisticsService {

    /**
     * 获取当日群组列表
     *
     * @param statisticsQO 统计查询实体类
     * @return 当日群组列表
     */
    PageInfo<CurrentDayGroupStatistics> getCurrentDayGroupStatisticsListByCondition(StatisticsQO statisticsQO);

}
