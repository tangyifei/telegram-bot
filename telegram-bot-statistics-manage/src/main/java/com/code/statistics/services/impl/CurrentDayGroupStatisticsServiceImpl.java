package com.code.statistics.services.impl;

import com.code.statistics.daos.CurrentDayGroupStatisticsMapper;
import com.code.statistics.models.CurrentDayGroupStatistics;
import com.code.statistics.models.qo.StatisticsQO;
import com.code.statistics.services.CurrentDayGroupStatisticsService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 当日群组统计服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class CurrentDayGroupStatisticsServiceImpl implements CurrentDayGroupStatisticsService {

    @Resource
    private CurrentDayGroupStatisticsMapper currentDayGroupStatisticsMapper;

    @Override
    public PageInfo<CurrentDayGroupStatistics> getCurrentDayGroupStatisticsListByCondition(StatisticsQO statisticsQO) {
        Integer pageNum = statisticsQO.getPageNum();
        Integer pageSize = statisticsQO.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        Date startDateTime = statisticsQO.getStartDateTime();
        Date endDateTime = statisticsQO.getEndDateTime();
        if (null == startDateTime && null == endDateTime) {
            // 默认展示前一天的数据
            return PageInfo.of(currentDayGroupStatisticsMapper.getBeforeDayGroupStatisticsList());
        } else {
            if (null != startDateTime && null != endDateTime) {
                if (startDateTime.equals(endDateTime)) {
                    statisticsQO.setStartDateTime(null);
                }
            }
            return PageInfo.of(currentDayGroupStatisticsMapper.getCurrentDayGroupStatisticsListByRelativeCondition(statisticsQO));
        }

    }

}
