package com.code.statistics.services.impl;

import com.code.statistics.daos.SensitiveWordMapper;
import com.code.statistics.models.SensitiveWord;
import com.code.statistics.models.qo.BaseQO;
import com.code.statistics.services.SensitiveWordService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 敏感词汇服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class SensitiveWordServiceImpl implements SensitiveWordService {

    @Resource
    private SensitiveWordMapper sensitiveWordMapper;

    @Override
    public PageInfo<SensitiveWord> getSensitiveWordListByCondition(BaseQO baseQO) {
        Integer pageNum = baseQO.getPageNum();
        Integer pageSize = baseQO.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        return PageInfo.of(sensitiveWordMapper.getSensitiveWordList());
    }

    @Override
    public int insertSensitiveWord(SensitiveWord sensitiveWord) {
        sensitiveWord.setDeleted(0);
        sensitiveWord.setCreatedAt(new Date());
        sensitiveWord.setUpdatedAt(new Date());
        return sensitiveWordMapper.insert(sensitiveWord);
    }

    @Override
    public int modifySensitiveWord(SensitiveWord sensitiveWord) {
        if (null != sensitiveWordMapper.whetherExistSensitiveWord(sensitiveWord.getId())) {
            sensitiveWord.setUpdatedAt(new Date());
            return sensitiveWordMapper.updateByPrimaryKeySelective(sensitiveWord);
        }
        return 0;
    }

    @Override
    public int deleteSensitiveWordById(Integer id) {
        if (null != sensitiveWordMapper.whetherExistSensitiveWord(id)) {
            return sensitiveWordMapper.deleteByPrimaryKey(id);
        }
        return 0;
    }
}
