package com.code.statistics.annotations;

import java.lang.annotation.*;

/**
 * 操作日志注解
 *
 * @author xiaoyaowang
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OperateLogAnno {

    String pageName() default "";

    String operateItem() default "";

}
