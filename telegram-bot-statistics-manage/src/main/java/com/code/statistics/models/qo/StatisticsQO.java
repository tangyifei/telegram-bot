package com.code.statistics.models.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 群组统计查询实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("群组统计查询实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatisticsQO extends BaseQO {

    private static final long serialVersionUID = -8576763795499901136L;

    @ApiModelProperty(value = "开始时间 日期时间类型 非必填", example = "2022-02-17")
    private Date startDateTime;

    @ApiModelProperty(value = "结束时间 日期时间类型 非必填", example = "2022-02-17")
    private Date endDateTime;

}
