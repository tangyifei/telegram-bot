package com.code.statistics.controllers;

import com.code.statistics.annotations.LoginAuth;
import com.code.statistics.annotations.ResponseResult;
import com.code.statistics.models.CurrentDayGroupStatistics;
import com.code.statistics.models.qo.StatisticsQO;
import com.code.statistics.services.CurrentDayGroupStatisticsService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 电报机器人当日群组统计相关的控制器
 *
 * @author xiaoyaowang
 */
@ResponseResult
@RestController("currentDayGroupStatisticsManageController")
@RequestMapping("/current-day-statistics")
public class CurrentDayGroupStatisticsManageController {

    private final CurrentDayGroupStatisticsService currentDayGroupStatisticsService;

    public CurrentDayGroupStatisticsManageController(CurrentDayGroupStatisticsService currentDayGroupStatisticsService) {
        this.currentDayGroupStatisticsService = currentDayGroupStatisticsService;
    }

    @ApiOperation(value = "根据相关条件分页获取当日群组列表", response = CurrentDayGroupStatistics.class, notes = "根据相关条件分页获取当日群组列表", httpMethod = "POST")
    @PostMapping("")
    @LoginAuth
    public PageInfo<CurrentDayGroupStatistics> getCurrentDayGroupStatisticsListByPage(@Validated @RequestBody StatisticsQO statisticsQO) {
        return currentDayGroupStatisticsService.getCurrentDayGroupStatisticsListByCondition(statisticsQO);
    }

}
