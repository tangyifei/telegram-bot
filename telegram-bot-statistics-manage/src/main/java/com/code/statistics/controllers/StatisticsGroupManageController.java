package com.code.statistics.controllers;

import com.code.statistics.annotations.LoginAuth;
import com.code.statistics.annotations.ResponseResult;
import com.code.statistics.models.Save;
import com.code.statistics.models.StatisticsGroup;
import com.code.statistics.models.Update;
import com.code.statistics.models.qo.StatisticsQO;
import com.code.statistics.services.StatisticsGroupService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 电报机器人群组统计相关的控制器
 *
 * @author xiaoyaowang
 */
@ResponseResult
@RestController("statisticsGroupManageController")
@RequestMapping("/group-statistics")
public class StatisticsGroupManageController {

    private final StatisticsGroupService statisticsGroupService;

    public StatisticsGroupManageController(StatisticsGroupService statisticsGroupService) {
        this.statisticsGroupService = statisticsGroupService;
    }

    @ApiOperation(value = "添加统计的群组相关的信息", notes = "添加统计的群组相关的信息", httpMethod = "POST")
    @PostMapping("")
    @LoginAuth
    public int insertStatisticsGroup(@RequestBody @Validated(Save.class) StatisticsGroup statisticsGroup) {
        return statisticsGroupService.insertStatisticsGroup(statisticsGroup);
    }

    @ApiOperation(value = "删除统计的群组相关的信息", notes = "删除统计的群组相关的信息", httpMethod = "DELETE")
    @DeleteMapping("")
    @LoginAuth
    public int deleteStatisticsGroupById(@RequestParam Integer id) {
        return statisticsGroupService.deleteStatisticsGroupById(id);
    }

    @ApiOperation(value = "更新统计的群组相关的信息", notes = "更新统计的群组相关的信息", httpMethod = "PATCH")
    @PatchMapping("")
    @LoginAuth
    public int modifyStatisticsGroup(@RequestBody @Validated(Update.class) StatisticsGroup statisticsGroup) {
        return statisticsGroupService.modifyStatisticsGroup(statisticsGroup);
    }

    @ApiOperation(value = "分页获取群组列表", response = StatisticsGroup.class, notes = "分页获取群组列表", httpMethod = "POST")
    @PostMapping("/page")
    @LoginAuth
    public PageInfo<StatisticsGroup> getStatisticsGroupListByPage(@Validated @RequestBody StatisticsQO statisticsQO) {
        return statisticsGroupService.getStatisticsGroupListByCondition(statisticsQO);
    }

}
