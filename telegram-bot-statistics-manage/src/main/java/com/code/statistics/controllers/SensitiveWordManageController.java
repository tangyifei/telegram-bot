package com.code.statistics.controllers;

import com.code.statistics.annotations.LoginAuth;
import com.code.statistics.annotations.ResponseResult;
import com.code.statistics.models.Save;
import com.code.statistics.models.SensitiveWord;
import com.code.statistics.models.StatisticsGroup;
import com.code.statistics.models.Update;
import com.code.statistics.models.qo.BaseQO;
import com.code.statistics.services.SensitiveWordService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 电报机器人敏感词汇相关的控制器
 *
 * @author xiaoyaowang
 */
@ResponseResult
@RestController("sensitiveWordManageController")
@RequestMapping("/sensitive-words")
public class SensitiveWordManageController {

    private final SensitiveWordService sensitiveWordService;

    public SensitiveWordManageController(SensitiveWordService sensitiveWordService) {
        this.sensitiveWordService = sensitiveWordService;
    }

    @ApiOperation(value = "添加敏感词汇相关的信息", notes = "添加敏感词汇相关的信息", httpMethod = "POST")
    @PostMapping("")
    @LoginAuth
    public int insertSensitiveWord(@RequestBody @Validated(Save.class) SensitiveWord sensitiveWord) {
        return sensitiveWordService.insertSensitiveWord(sensitiveWord);
    }

    @ApiOperation(value = "删除敏感词汇相关的信息", notes = "删除敏感词汇相关的信息", httpMethod = "DELETE")
    @DeleteMapping("")
    @LoginAuth
    public int deleteSensitiveWordById(@RequestParam Integer id) {
        return sensitiveWordService.deleteSensitiveWordById(id);
    }

    @ApiOperation(value = "更新敏感词汇相关的信息", notes = "更新敏感词汇相关的信息", httpMethod = "PATCH")
    @PatchMapping("")
    @LoginAuth
    public int modifySensitiveWord(@RequestBody @Validated(Update.class) SensitiveWord sensitiveWord) {
        return sensitiveWordService.modifySensitiveWord(sensitiveWord);
    }

    @ApiOperation(value = "分页获取敏感词汇列表", response = StatisticsGroup.class, notes = "分页获取敏感词汇列表", httpMethod = "POST")
    @PostMapping("/page")
    @LoginAuth
    public PageInfo<SensitiveWord> getSensitiveWordListByPage(@Validated @RequestBody BaseQO baseQO) {
        return sensitiveWordService.getSensitiveWordListByCondition(baseQO);
    }

}
