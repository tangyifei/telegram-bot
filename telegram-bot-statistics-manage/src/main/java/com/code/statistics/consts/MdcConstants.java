package com.code.statistics.consts;

/**
 * 日志追踪相关的常量
 */
public interface MdcConstants {

    String MDC_KEY = "requestId";
}
