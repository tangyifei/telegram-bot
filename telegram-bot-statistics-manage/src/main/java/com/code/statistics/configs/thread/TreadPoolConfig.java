package com.code.statistics.configs.thread;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import javax.annotation.Resource;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 线程池配置
 * 可以参考：https://mp.weixin.qq.com/s/KvPJMGBzOxGbH98ed9QRpw
 *
 * @author xiaoyaowang
 */
@Configuration
@EnableConfigurationProperties(TreadPoolConfig.TreadPoolProperties.class)
@AutoConfigureAfter(TreadPoolConfig.TreadPoolProperties.class)
@EnableAsync
@EnableScheduling
@Slf4j
public class TreadPoolConfig implements SchedulingConfigurer, AsyncConfigurer {

    @Resource
    private TreadPoolProperties treadPoolProperties;

    /**
     * 定时任务使用的线程池
     *
     * @return 定时任务的线程池
     */
    @Bean(destroyMethod = "shutdown", name = "taskScheduler")
    public ThreadPoolTaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(treadPoolProperties.getCoreSize());
        scheduler.setThreadNamePrefix("schedule-task-");
        scheduler.setAwaitTerminationSeconds(treadPoolProperties.getAwaitTerminationSeconds());
        scheduler.setWaitForTasksToCompleteOnShutdown(treadPoolProperties.getWaitForTasksToCompleteOnShutdown());
        return scheduler;
    }

    /**
     * 异步任务执行线程池
     *
     * @return 异步任务线程池
     */
    @Bean(name = "asyncExecutor")
    public ThreadPoolTaskExecutor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(treadPoolProperties.getCoreSize());
        executor.setQueueCapacity(treadPoolProperties.getBlockQueueSize());
        executor.setKeepAliveSeconds(treadPoolProperties.getActiveTime());
        executor.setMaxPoolSize(treadPoolProperties.getMaxSize());
        executor.setThreadNamePrefix("asyncTaskExecutor-");
        // 用来设置线程池关闭的时候等待所有任务都完成再继续销毁其他的Bean
        executor.setWaitForTasksToCompleteOnShutdown(true);
        // 该方法用来设置线程池中任务的等待时间，如果超过这个时候还没有销毁就强制销毁，以确保应用最后能够被关闭，而不是阻塞住
        executor.setAwaitTerminationSeconds(60);
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 初始化
        executor.initialize();
        return executor;
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        ThreadPoolTaskScheduler taskScheduler = taskScheduler();
        scheduledTaskRegistrar.setTaskScheduler(taskScheduler);
    }

    /**
     * 可以修改配置类让其实现AsyncConfigurer，并重写getAsyncExecutor()方法，指定默认线程池
     * 然后在方法上使用@Async注解就不需要指定线程池的名称了
     *
     * @return 默认线程池
     */
    @Override
    public Executor getAsyncExecutor() {
        return asyncExecutor();
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return (throwable, method, objects) -> log.error("异步任务执行出现异常, message {}, method {}, params {}", throwable, method, objects);
    }

    /**
     * 读取线程池相关的属性的类
     *
     * @author xiaoyaowang
     */
    @ConfigurationProperties(prefix = "thread.pool")
    @Getter
    @Setter
    static class TreadPoolProperties {
        private Integer coreSize;
        private Integer maxSize;
        private Integer activeTime;
        private Integer blockQueueSize;
        private Integer awaitTerminationSeconds;
        private Boolean waitForTasksToCompleteOnShutdown;
    }

}
