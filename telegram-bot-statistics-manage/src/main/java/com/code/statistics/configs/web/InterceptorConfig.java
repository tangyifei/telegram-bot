package com.code.statistics.configs.web;

import com.code.statistics.interceptors.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 拦截器配置
 *
 * @author xiaoyaowang
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    /**
     * 注册接口响应体控制拦截器相关的bean
     *
     * @return 接口响应体控制拦截器对象
     */
    @Bean
    public ResponseResultInterceptor responseResultInterceptor() {
        return new ResponseResultInterceptor();
    }

    /**
     * 注册校验请求头中相关参数完整性和有效性拦截器相关的bean
     *
     * @return 校验请求头中相关参数完整性和有效性拦截器对象
     */
    @Bean
    public HeaderParamsCheckInterceptor headerParamsCheckInterceptor() {
        return new HeaderParamsCheckInterceptor();
    }

    /**
     * 注册校验用户是否登录的拦截器相关的bean
     *
     * @return 拦截用户是否登录的拦截器
     */
    @Bean
    public LoginAuthInterceptor loginAuthInterceptor() {
        return new LoginAuthInterceptor();
    }

    /**
     * 国际化拦截器相关的bean
     *
     * @return 国际化拦截器对象
     */
    @Bean
    public InternationalInterceptor internationalInterceptor() {
        return new InternationalInterceptor();
    }

    /**
     * 在不使用线程池的情况下，当前线程在执行完毕后会被销毁，这时 当前线程 中的 threadLocals 参数 将会被情况，也就清空 了 LOCAL_PAGE 中 当前线程的 page 参数。
     * <p>
     * 但是如果使用了线程池，当前线程执行完毕，并不会被销毁，而是会将当前线程再次存放到池中，标记为空闲状态，以便后续使用。在后续使用这个线程的时候，
     * 由于 线程 的 threadLocals 依旧存在有值，尽管我们在第 1 步时未设置 page 参数，第 3 步 的也能获取到page参数，从而生成 count sql 和 page sql，从而影响我们的正常查询。
     *
     * @return 分页插件拦截器插件
     */
    @Bean
    public PageLocalWebInterceptor pageLocalWebInterceptor() {
        return new PageLocalWebInterceptor();
    }

    /**
     * 注册拦截器，可以调整多个registry.addInterceptor方法的代码顺序来控制拦截器的调用顺序
     *
     * @param registry 拦截器注册器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //响应结果控制拦截
        registry.addInterceptor(responseResultInterceptor());
        //请求头参数校验，不拦截swagger
        registry.addInterceptor(headerParamsCheckInterceptor()).excludePathPatterns("/v2/**", "/swagger-resources/**", "/webjars/**", "/swagger-ui.html/**");
        //登录拦截
        registry.addInterceptor(loginAuthInterceptor());
        //国际化拦截
        registry.addInterceptor(internationalInterceptor());
        registry.addInterceptor(pageLocalWebInterceptor());
    }

    /**
     * 发现如果继承了WebMvcConfigurationSupport，则在yml中配置的相关内容会失效。 需要重新指定静态资源
     *
     * @param registry 注册器
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations(
                "classpath:/static/");
        registry.addResourceHandler("swagger-ui.html").addResourceLocations(
                "classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations(
                "classpath:/META-INF/resources/webjars/");
        WebMvcConfigurer.super.addResourceHandlers(registry);
    }

}
