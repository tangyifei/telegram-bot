package com.code.statistics.interceptors;

import com.code.statistics.consts.MdcConstants;
import org.slf4j.MDC;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

public class RestTemplateTraceIdInterceptor implements ClientHttpRequestInterceptor {
    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes, ClientHttpRequestExecution clientHttpRequestExecution) throws IOException {
        String traceId = MDC.get(MdcConstants.MDC_KEY);
        if (traceId != null) {
            httpRequest.getHeaders().add(MdcConstants.MDC_KEY, traceId);
        }

        return clientHttpRequestExecution.execute(httpRequest, bytes);
    }
}
