package com.code.statistics.schedules;

import com.code.statistics.services.CurrentDayGroupStatisticsService;
import com.code.statistics.services.InOutGroupService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 统计当天入群人数
 *
 * @author xiaoyaowang
 */
@Component
public class SynCurrentDayMembersSchedule {

    @Resource
    private CurrentDayGroupStatisticsService currentDayGroupStatisticsService;

    @Resource
    private InOutGroupService inOutGroupService;

    @Scheduled(cron = "0 0 0 * * ?")
    // @Scheduled(cron = "0 0/1 * * * ?")
    // @Scheduled(cron = "00 58 23 * * ?")
    public void synCurrentDayJoinGroupMembers() {
        currentDayGroupStatisticsService.synCurrentDayJoinGroupMembers();
    }

    @Scheduled(cron = "*/1 * * * * ?")
    public void synInOutGroupInCurrentDay() {
        inOutGroupService.synInOutGroupInCurrentDay();
    }
}
