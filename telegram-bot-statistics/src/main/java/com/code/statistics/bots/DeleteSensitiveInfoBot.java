package com.code.statistics.bots;

import cn.hutool.json.JSONUtil;
import com.code.statistics.services.SensitiveWordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * 删除敏感信息的机器人
 *
 * @author xiaoyaowang
 */
public class DeleteSensitiveInfoBot extends TelegramLongPollingBot {

    private static Logger logger = LoggerFactory.getLogger(DeleteSensitiveInfoBot.class);

    private String robotName;

    private String robotToken;

    private SensitiveWordService sensitiveWordService;

    public void setRobotName(String robotName) {
        this.robotName = robotName;
    }

    public void setRobotToken(String robotToken) {
        this.robotToken = robotToken;
    }

    public void setSensitiveWordService(SensitiveWordService sensitiveWordService) {
        this.sensitiveWordService = sensitiveWordService;
    }

    @Override
    public String getBotUsername() {
        return robotName;
    }

    @Override
    public String getBotToken() {
        return robotToken;
    }

    @Override
    public void onUpdateReceived(Update update) {
        logger.info("delete sensitive bot start work,execute onUpdateReceived method，update:{}", JSONUtil.toJsonStr(update));
        try {
            Message messageText = null;
            if (update.hasEditedMessage()) {
                messageText = update.getEditedMessage();
            }
            if (update.hasMessage()) {
                if (null == messageText) {
                    messageText = update.getMessage();
                }
            }
            if (null == messageText) {
                return;
            }
            Chat chat = messageText.getChat();
            if (null == chat) {
                return;
            }
            // 可以访问https://api.telegram.org/bot5297727617:AAFFmbSfuhMTpTxObd7_6HQr0o660RPlIX0/deleteMessage?chat_id=-1001516644478&message_id=193
            if (sensitiveWordService.whetherContainSensitiveWord(messageText.getText())) {
                DeleteMessage deleteMessage = new DeleteMessage(String.valueOf(chat.getId()), messageText.getMessageId());
                execute(deleteMessage);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("onUpdateReceived error:", e);
        }


    }

    ///**
    // * 发送自动欢迎语，3s后机器人自动删除欢迎语
    // *
    // * @param chatId         聊天群组唯一标识
    // * @param newChatMembers 新加入的成员列表
    // */
    //private void sendAutoWelcome(String chatId, List<User> newChatMembers) {
    //    SendMessage newChatMemberMessage;
    //    String firstName;
    //    String lastName;
    //    String userName;
    //    StringBuilder sb;
    //    String suffix = null;
    //    try {
    //        for (User newChatMember : newChatMembers) {
    //            sb = new StringBuilder(1 << 3);
    //            if (languageType == 1 || languageType == 0) {
    //                sb.append("欢迎");
    //                suffix = "入群";
    //            }
    //            if (languageType == 2) {
    //                sb.append("Welcome ");
    //                suffix = " to the group";
    //            }
    //            if (languageType == 3) {
    //                sb.append("Chào ");
    //                suffix = " mừng đến với nhóm";
    //            }
    //            firstName = newChatMember.getFirstName();
    //            lastName = newChatMember.getLastName();
    //            if (StringUtil.isNotEmpty(firstName) && StringUtil.isNotEmpty(lastName)) {
    //                userName = sb.append(firstName).append(lastName).append(suffix).toString();
    //            } else if (StringUtil.isNotEmpty(firstName) && StringUtil.isEmpty(lastName)) {
    //                userName = sb.append(firstName).append(suffix).toString();
    //            } else if (StringUtil.isEmpty(firstName) && StringUtil.isNotEmpty(lastName)) {
    //                userName = sb.append(lastName).append(suffix).toString();
    //            } else if (StringUtil.isNotEmpty(newChatMember.getUserName())) {
    //                userName = sb.append(newChatMember.getUserName()).append(suffix).toString();
    //            } else {
    //                userName = sb.append(suffix).toString();
    //            }
    //            newChatMemberMessage = new SendMessage(chatId, userName);
    //            Integer messageId = execute(newChatMemberMessage).getMessageId();
    //            // 3s后自动删除发送的欢迎语
    //            Thread.sleep(3000);
    //            DeleteMessage deleteMessage = new DeleteMessage(chatId, messageId);
    //            execute(deleteMessage);
    //        }
    //    } catch (Exception e) {
    //        logger.error("sendAutoWelcome for error：", e);
    //    }
    //
    //}


}
