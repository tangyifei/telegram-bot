package com.code.statistics.bots;

import cn.hutool.json.JSONUtil;
import com.code.statistics.services.CurrentDayGroupStatisticsService;
import com.code.statistics.services.StatisticsGroupService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * 采集机器人
 *
 * @author xiaoyaowang
 */
public class CollectBot extends TelegramLongPollingBot {

    private static final String GROUP_URL_PREFIX = "https://t.me/";

    private static Logger logger = LoggerFactory.getLogger(CollectBot.class);

    private String robotName;

    private String robotToken;

    private StatisticsGroupService statisticsGroupService;

    private CurrentDayGroupStatisticsService currentDayGroupStatisticsService;

    public void setRobotName(String robotName) {
        this.robotName = robotName;
    }

    public void setRobotToken(String robotToken) {
        this.robotToken = robotToken;
    }


    @Override
    public String getBotUsername() {
        return robotName;
    }

    @Override
    public String getBotToken() {
        return robotToken;
    }

    public void setStatisticsGroupService(StatisticsGroupService statisticsGroupService) {
        this.statisticsGroupService = statisticsGroupService;
    }

    public void setCurrentDayGroupStatisticsService(CurrentDayGroupStatisticsService currentDayGroupStatisticsService) {
        this.currentDayGroupStatisticsService = currentDayGroupStatisticsService;
    }

    @Override
    public void onUpdateReceived(Update update) {
        logger.info("collect bot start work,execute onUpdateReceived method，update:{}", JSONUtil.toJsonStr(update));
        try {
            if (update.hasMessage()) {
                Message messageText = update.getMessage();
                if (null == messageText) {
                    return;
                }
                Long chatIdLong = messageText.getChatId();
                if (null == chatIdLong) {
                    return;
                }
                Chat chat = messageText.getChat();
                if (null == chat) {
                    return;
                }
                String userName = chat.getUserName();
                if (StringUtils.isBlank(userName)) {
                    return;
                }
                String groupUrl = GROUP_URL_PREFIX + userName;
                logger.info("groupUrl:{}", groupUrl);
                if (!statisticsGroupService.whetherExistStatisticsGroup(groupUrl)) {
                    return;
                }
                statisticsGroupService.updateGroupChatIdByGroupUrl(groupUrl, chat.getId());
                if (null == messageText.getLeftChatMember()) {
                    int version = currentDayGroupStatisticsService.getVersionByGroupUrl(groupUrl);
                    // 统计留言数
                    currentDayGroupStatisticsService.statisticsGroupMessagesOrGroupNewMembers(groupUrl, version, 0, 1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("onUpdateReceived error:", e);
        }


    }

}
