package com.code.statistics.daos;

import com.code.statistics.daos.base.CrudMapper;
import com.code.statistics.models.SensitiveWord;

import java.util.Set;

/**
 * 敏感词汇持久层
 *
 * @author xiaoyaowang
 */
public interface SensitiveWordMapper extends CrudMapper<SensitiveWord> {

    /**
     * 获取敏感词汇列表
     *
     * @return 群是否存在
     */
    Set<String> getSensitiveWordSets();

}
