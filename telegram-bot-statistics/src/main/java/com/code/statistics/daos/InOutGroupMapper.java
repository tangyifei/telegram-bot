package com.code.statistics.daos;

import com.code.statistics.daos.base.CrudMapper;
import com.code.statistics.models.InOutGroup;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 当日入群与退群的统计持久层
 *
 * @author xiaoyaowang
 */
public interface InOutGroupMapper extends CrudMapper<InOutGroup> {

    /**
     * 查询当日的入群或者退群人数列表
     *
     * @param groupUrl   群的链接地址
     * @param inOutType  入群与退群类型 1-入群 2-退群
     * @param currentDay 当天的日期
     * @return 当日的入群或者退群人数列表
     */
    List<Integer> getMembersByGroupUrlAndInOutTypeInCurrentDay(@Param("groupUrl") String groupUrl, @Param("inOutType") Integer inOutType,
                                                               @Param("currentDay") String currentDay);

    /**
     * 删除当日的当日的入群或者退群人数统计记录
     *
     * @param currentDay 当天的日期
     * @return 影响的行数
     */
    int deleteInOutGroupByCurrentDay(@Param("currentDay") String currentDay);

}
