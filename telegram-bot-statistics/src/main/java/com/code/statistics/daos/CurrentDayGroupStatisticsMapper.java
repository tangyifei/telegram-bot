package com.code.statistics.daos;

import com.code.statistics.daos.base.CrudMapper;
import com.code.statistics.models.CurrentDayGroupStatistics;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 当日群组统计持久层
 *
 * @author xiaoyaowang
 */
public interface CurrentDayGroupStatisticsMapper extends CrudMapper<CurrentDayGroupStatistics> {

    /**
     * 根据群组主键和当日日期判断当日统计记录是否存在
     *
     * @param groupId     群主键
     * @param currentDate 当日日期
     * @return 当日统计记录是否存在
     */
    Integer whetherExistCurrentDayGroupStatistics(@Param("groupId") Integer groupId, @Param("currentDate") String currentDate);

    /**
     * 增加留言数
     *
     * @param id      主键
     * @param version 版本号
     * @return 影响的行数
     */
    int increaseMessages(@Param("id") Integer id, @Param("version") Integer version, @Param("updatedAt") Date updatedAt);

    /**
     * 增加新成员数目
     *
     * @param id         主键
     * @param newMembers 新成员数
     * @param version    版本号
     * @return 影响的行数
     */
    int increaseNewMembers(@Param("id") Integer id, @Param("newMembers") Integer newMembers,
                           @Param("version") Integer version,
                           @Param("updatedAt") Date updatedAt);

    /**
     * 批量更新当日统计信息
     *
     * @param list 列表
     * @return 影响的行数
     */
    int patchUpdateNewMembers(List<CurrentDayGroupStatistics> list);

    /**
     * 获取版本号
     *
     * @param groupId     群主键
     * @param currentDate 当日日期
     * @return 版本号
     */
    Integer getVersionByGroupIdAndCurrentDate(@Param("groupId") Integer groupId, @Param("currentDate") String currentDate);

}
