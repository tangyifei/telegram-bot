package com.code.statistics.daos;

import com.code.statistics.daos.base.CrudMapper;
import com.code.statistics.models.Dict;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 字典持久层
 *
 * @author xiaoyaowang
 */
public interface DictMapper extends CrudMapper<Dict> {

    /**
     * 根据字典key获取字典值
     *
     * @param dictKey 字典值
     * @return 群是否存在
     */
    String getDictValueByDictKey(@Param("dictKey") String dictKey);

    /**
     * 批量更新字典的值
     *
     * @param dictList 字典列表
     * @return 影响的行数
     */
    int patchUpdateDictByDictKeys(List<Dict> dictList);

    /**
     * 判断是否存在相关的字典项
     *
     * @param dictKey 主键
     * @return 是否存在相关的字典项
     */
    Integer whetherExistDictKey(@Param("dictKey") String dictKey);

}
