package com.code.statistics.daos;

import com.code.statistics.daos.base.CrudMapper;
import com.code.statistics.models.StatisticsGroup;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 群组持久层
 *
 * @author xiaoyaowang
 */
public interface StatisticsGroupMapper extends CrudMapper<StatisticsGroup> {

    /**
     * 相关的群是否存在
     *
     * @param groupUrl 群的链接地址
     * @return 群是否存在
     */
    Integer whetherExistStatisticsGroup(@Param("groupUrl") String groupUrl);

    /**
     * 通过群组链接地址获取群组主键
     *
     * @param groupUrl 群组的链接地址
     * @return 群组主键
     */
    Integer getIdByGroupUrl(@Param("groupUrl") String groupUrl);

    /**
     * 根据群组地址获取群组聊天唯一标识
     *
     * @param groupUrl 群组地址
     * @return 群组聊天唯一标识
     */
    String getGroupChatIdByGroupUrl(@Param("groupUrl") String groupUrl);

    /**
     * 获取所有统计相关的群组列表
     *
     * @return 所有统计相关的群组列表
     */
    List<StatisticsGroup> getStatisticsGroupList();

}
