package com.code.statistics.runners;

import com.code.statistics.bots.CollectBot;
import com.code.statistics.bots.DeleteSensitiveInfoBot;
import com.code.statistics.services.CurrentDayGroupStatisticsService;
import com.code.statistics.services.SensitiveWordService;
import com.code.statistics.services.StatisticsGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import javax.annotation.Resource;

/**
 * 注册机器人
 *
 * @author xiaoyaowang
 */
@Component
public class RobotRunner implements ApplicationRunner {

    @Resource
    private StatisticsGroupService statisticsGroupService;

    @Resource
    private SensitiveWordService sensitiveWordService;

    @Resource
    private CurrentDayGroupStatisticsService currentDayGroupStatisticsService;

    @Value("${collect.task.bot.name}")
    private String collectTaskBotName;

    @Value("${delete.task.bot.name}")
    private String deleteTaskBotName;

    @Value("${collect.task.bot.token}")
    private String collectTaskBotToken;

    @Value("${delete.task.bot.token}")
    private String deleteTaskBotToken;

    private static Logger logger = LoggerFactory.getLogger(RobotRunner.class);

    @Override
    public void run(ApplicationArguments args) {
        try {
            TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);

            CollectBot taskRobot = new CollectBot();
            taskRobot.setRobotName(collectTaskBotName);
            taskRobot.setRobotToken(collectTaskBotToken);
            taskRobot.setStatisticsGroupService(statisticsGroupService);
            taskRobot.setCurrentDayGroupStatisticsService(currentDayGroupStatisticsService);
            botsApi.registerBot(taskRobot);
            logger.info("采集任务机器人已注册，名称:{} token:{}", collectTaskBotName, collectTaskBotToken);

            DeleteSensitiveInfoBot deleteSensitiveInfoBot = new DeleteSensitiveInfoBot();
            deleteSensitiveInfoBot.setSensitiveWordService(sensitiveWordService);
            deleteSensitiveInfoBot.setRobotName(deleteTaskBotName);
            deleteSensitiveInfoBot.setRobotToken(deleteTaskBotToken);
            botsApi.registerBot(deleteSensitiveInfoBot);
            logger.info("删除敏感信息的机器人已注册，名称:{} token:{}", deleteTaskBotName, deleteTaskBotToken);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

}
