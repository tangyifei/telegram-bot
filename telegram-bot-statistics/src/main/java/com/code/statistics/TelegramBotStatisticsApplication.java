package com.code.statistics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 机器人群组统计启动类
 *
 * @author xiaoyaowang
 */
@SpringBootApplication
public class TelegramBotStatisticsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TelegramBotStatisticsApplication.class, args);
    }

}
