package com.code.statistics.services.impl;

import com.code.statistics.daos.SensitiveWordMapper;
import com.code.statistics.services.SensitiveWordService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Set;

/**
 * 敏感词汇实现类
 *
 * @author xiaoyaowang
 */
@Service
public class SensitiveWordServiceImpl implements SensitiveWordService {

    @Resource
    SensitiveWordMapper sensitiveWordMapper;

    @Override
    public Boolean whetherContainSensitiveWord(String content) {
        if (StringUtils.isBlank(content)) {
            return false;
        }
        Set<String> sensitiveWordSets = sensitiveWordMapper.getSensitiveWordSets();
        for (String sensitiveWordSet : sensitiveWordSets) {
            if (content.contains(sensitiveWordSet)) {
                return true;
            }
        }
        return false;
    }
}
