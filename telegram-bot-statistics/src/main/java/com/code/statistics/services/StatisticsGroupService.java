package com.code.statistics.services;

import com.code.statistics.models.StatisticsGroup;

import java.util.List;

/**
 * 群组业务接口
 *
 * @author xiaoyaowang
 */
public interface StatisticsGroupService {

    /**
     * 判断群组是否存在
     *
     * @param groupUrl 群组的链接地址
     * @return 群组存在与否 为true表示存在
     */
    Boolean whetherExistStatisticsGroup(String groupUrl);

    /**
     * 通过群组链接地址获取群组主键
     *
     * @param groupUrl 群组的链接地址
     * @return 群组主键
     */
    Integer getIdByGroupUrl(String groupUrl);

    /**
     * 获取所有统计相关的群组列表
     *
     * @return 所有统计相关的群组列表
     */
    List<StatisticsGroup> getStatisticsGroupList();

    /**
     * 更新群组聊天唯一标识
     *
     * @param groupUrl    群组url
     * @param groupChatId 群组聊天唯一标识
     * @return 影响的行数
     */
    int updateGroupChatIdByGroupUrl(String groupUrl, Long groupChatId);

    /**
     * 根据群组地址获取群组聊天唯一标识
     *
     * @param groupUrl 群组地址
     * @return 群组聊天唯一标识
     */
    String getGroupChatIdByGroupUrl(String groupUrl);

}
