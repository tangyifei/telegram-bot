package com.code.statistics.services.impl;

import com.code.statistics.daos.StatisticsGroupMapper;
import com.code.statistics.models.StatisticsGroup;
import com.code.statistics.services.StatisticsGroupService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 群组实现类
 *
 * @author xiaoyaowang
 */
@Service
public class StatisticsGroupServiceImpl implements StatisticsGroupService {

    @Resource
    StatisticsGroupMapper statisticsGroupMapper;

    @Override
    public Boolean whetherExistStatisticsGroup(String groupUrl) {
        return null != statisticsGroupMapper.whetherExistStatisticsGroup(groupUrl);
    }

    @Override
    public Integer getIdByGroupUrl(String groupUrl) {
        return statisticsGroupMapper.getIdByGroupUrl(groupUrl);
    }

    @Override
    public List<StatisticsGroup> getStatisticsGroupList() {
        return statisticsGroupMapper.getStatisticsGroupList();
    }

    @Override
    public int updateGroupChatIdByGroupUrl(String groupUrl, Long groupChatId) {
        if (null != statisticsGroupMapper.whetherExistStatisticsGroup(groupUrl)) {
            StatisticsGroup updateStatisticsGroup = new StatisticsGroup();
            updateStatisticsGroup.setId(statisticsGroupMapper.getIdByGroupUrl(groupUrl));
            updateStatisticsGroup.setGroupChatId(groupChatId);
            updateStatisticsGroup.setUpdatedAt(new Date());
            statisticsGroupMapper.updateByPrimaryKeySelective(updateStatisticsGroup);
        }
        return 0;
    }

    @Override
    public String getGroupChatIdByGroupUrl(String groupUrl) {
        return statisticsGroupMapper.getGroupChatIdByGroupUrl(groupUrl);
    }
}
