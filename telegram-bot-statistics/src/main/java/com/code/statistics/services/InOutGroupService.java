package com.code.statistics.services;

import com.code.statistics.models.Dict;
import com.code.statistics.models.InOutGroup;

import java.util.List;

/**
 * 当日入群与退群的统计业务接口
 *
 * @author xiaoyaowang
 */
public interface InOutGroupService {

    /**
     * 批量插入当日入群与退群的统计列表和批量修改字典项
     *
     * @param dictList       字典项列表
     * @param inOutGroupList 批量插入当日入群与退群的统计列表
     * @return 影响的行数
     */
    int insertInOutGroupListAndPatchUpdateDictList(List<Dict> dictList, List<InOutGroup> inOutGroupList);

    /**
     * 同步当天的入群或者退群记录
     */
    void synInOutGroupInCurrentDay();

    /**
     * 根据当日日期删除当日入群与退群的人数统计
     *
     * @param currentDay 当日日期
     * @return 影响的行数
     */
    int deleteInOutGroupByCurrentDay(String currentDay);

    /**
     * 查询当日的入群或者退群人数列表
     *
     * @param groupUrl   群的链接地址
     * @param inOutType  入群与退群类型 1-入群 2-退群
     * @param currentDay 当天的日期
     * @return 当日的入群或者退群人数列表
     */
    List<Integer> getMembersByGroupUrlAndInOutTypeInCurrentDay(String groupUrl, Integer inOutType,
                                                               String currentDay);

}
