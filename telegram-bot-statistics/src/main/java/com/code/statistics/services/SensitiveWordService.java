package com.code.statistics.services;

/**
 * 敏感词汇业务接口
 *
 * @author xiaoyaowang
 */
public interface SensitiveWordService {

    /**
     * 判断是否包含敏感词汇
     *
     * @param content 发送的内容
     * @return 是否包含敏感词汇
     */
    Boolean whetherContainSensitiveWord(String content);

}
