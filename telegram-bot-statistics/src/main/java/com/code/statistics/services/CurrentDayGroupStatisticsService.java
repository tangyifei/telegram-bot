package com.code.statistics.services;

import com.code.statistics.models.CurrentDayGroupStatistics;

import java.util.List;

/**
 * 当日群组统计业务接口
 *
 * @author xiaoyaowang
 */
public interface CurrentDayGroupStatisticsService {

    /**
     * 统计留言数或者统计新成员数目
     *
     * @param version  乐观锁版本号
     * @param groupUrl 群组链接地址
     * @param type     1-统计留言数 2-统计新成员数
     * @return 影响的行数
     */
    int statisticsGroupMessagesOrGroupNewMembers(String groupUrl, int version, int newMembers, int type);

    /**
     * 获取版本号
     *
     * @param groupUrl 群组链接地址
     * @return 版本号
     */
    int getVersionByGroupUrl(String groupUrl);

    /**
     * 同步当日的新人入群数
     */
    void synCurrentDayJoinGroupMembers();

    /**
     * 批量新增或者批量修改当日统计
     *
     * @param currentDayGroupStatisticList 当日新增成员列表
     * @return 影响的行数
     */
    int insertCurrentDayGroupStatisticListOrPatchUpdateCurrentDayGroupStatisticList(List<CurrentDayGroupStatistics> currentDayGroupStatisticList);

}
