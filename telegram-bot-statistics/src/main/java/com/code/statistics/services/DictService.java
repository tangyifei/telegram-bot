package com.code.statistics.services;

import com.code.statistics.models.Dict;

import java.util.List;

/**
 * 字典业务接口
 *
 * @author xiaoyaowang
 */
public interface DictService {

    /**
     * 根据字典key获取字典值
     *
     * @param dictKey 字典值
     * @return 群是否存在
     */
    String getDictValueByDictKey(String dictKey);

    /**
     * 批量更新字典的值
     *
     * @param dictList 字典列表
     * @return 影响的行数
     */
    int patchUpdateDictByDictKeys(List<Dict> dictList);

    /**
     * 判断是否存在相关的字典项
     *
     * @param dictKey 主键
     * @return 是否存在相关的字典项
     */
    Boolean whetherExistDictKey(String dictKey);

}
