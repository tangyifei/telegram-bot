/*
 Navicat Premium Data Transfer

 Source Server         : 空投机器人线上环境
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : rm-j6ca3jeeni2t070ln.mysql.rds.aliyuncs.com:3306
 Source Schema         : telegram_bot_statistics

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 03/03/2022 17:39:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for current_day_group_statistics
-- ----------------------------
DROP TABLE IF EXISTS `current_day_group_statistics`;
CREATE TABLE `current_day_group_statistics`  (
  `id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `group_id` int(1) UNSIGNED NOT NULL COMMENT '关联的群组主键',
  `current_day_messages` int(1) UNSIGNED NOT NULL COMMENT '当日留言数',
  `current_day_new_members` int(1) UNSIGNED NOT NULL COMMENT '当日新增的成员数',
  `current_day_date` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '当日日期',
  `version` int(1) UNSIGNED NOT NULL COMMENT '版本号',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '删除状态 0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL COMMENT '创建日期时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新日期时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dict
-- ----------------------------
DROP TABLE IF EXISTS `dict`;
CREATE TABLE `dict`  (
  `id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dict_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典key',
  `dict_value` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典值',
  `remark` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '备注',
  `deleted` tinyint(1) NOT NULL COMMENT '删除状态 0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建日期时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新日期时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sensitive_word
-- ----------------------------
DROP TABLE IF EXISTS `sensitive_word`;
CREATE TABLE `sensitive_word`  (
  `id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sensitive_word` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '敏感词汇',
  `is_enable` tinyint(1) NOT NULL COMMENT '0-禁用 1-启用',
  `deleted` tinyint(1) NOT NULL COMMENT '删除状态 0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建日期时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新日期时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for statistics_group
-- ----------------------------
DROP TABLE IF EXISTS `statistics_group`;
CREATE TABLE `statistics_group`  (
  `id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `group_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '群组名称',
  `group_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '群组链接地址',
  `is_enable` tinyint(1) UNSIGNED NOT NULL COMMENT '是否启用 0-未启用 1-启用',
  `deleted` tinyint(1) NOT NULL COMMENT '删除状态 0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期时间',
  `updated_at` timestamp(0) NOT NULL COMMENT '更新日期时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_group_url`(`group_url`) USING BTREE COMMENT '群组地址唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
