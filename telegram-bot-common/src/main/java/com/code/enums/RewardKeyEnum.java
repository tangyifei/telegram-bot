package com.code.enums;

/**
 * 奖励key的枚举
 *
 * @author xiaoyaowang
 */

public enum RewardKeyEnum {

    /**
     * 邀请朋友获取的奖励
     */
    INVITE_FRIENDS_FINISH_TASK_REWARD,

    /**
     * 手动完成第四步任务与否
     */
    MANUAL_FINISH_FOURTH_TASK,

    /**
     * 发送的截图最低限制
     */
    PHOTO_DOWN_LIMIT,

    /**
     * 完成任务获得的奖励
     */
    FINISH_TASK_REWARD,

    /**
     * 提币是否关闭
     */
    WITHDRAW_OFF,

    /**
     * 每个任务的奖励
     */
    EVERY_TASK_REWARD,

    /**
     * 邀请好友的获得的奖励上限
     */
    INVITE_FRIEND_UP_LIMIT,

    /**
     * 审核不合格主动向用户发送的信息
     */
    SEND_PHOTO_TASK,

    /**
     * 用户被封禁
     */
    BAN,

    /**
     * 暂停机器人的任务
     */
    STOP_ROBOT,

    /**
     * 暂停机器人执行任务时发送的消息
     */
    STOP_ROBOT_MESSAGE,

    /**
     * 加入BZ群的相关消息
     */
    JOIN_BZ_GROUP_MESSAGE,

    /**
     * 扫描入群信息
     */
    SCAN_JOIN_GROUP,

    /**
     * 代理人领取奖励功能关闭
     */
    AGENT_RECEIVE_INCOME_OFF,

}
