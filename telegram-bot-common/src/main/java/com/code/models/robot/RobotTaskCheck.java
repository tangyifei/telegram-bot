package com.code.models.robot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Transient;
import java.util.List;

/**
 * 机器人任务审核记录实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("机器人任务审核记录实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RobotTaskCheck extends BaseRobot {

    private static final long serialVersionUID = 3522992676325318444L;

    @ApiModelProperty(value = "任务审核主键", example = "1")
    @Transient
    private Integer taskCheckId;

    @ApiModelProperty(value = "聊天标识", example = "1")
    private String chatId;

    @ApiModelProperty(value = "用户名", example = "limingcheng")
    private String userName;

    @ApiModelProperty(value = "名", example = "mingcheng")
    private String firstName;

    @ApiModelProperty(value = "姓", example = "li")
    private String lastName;

    @ApiModelProperty(value = "图片地址", example = "http://")
    @Transient
    private String picUrl;

    @ApiModelProperty(value = "图片地址", example = "http://")
    @Transient
    private List<RobotCheckPic> robotCheckPicList;

    @ApiModelProperty(value = "钱包地址", example = "XWC1131313414141443")
    private String walletAddr;

    @ApiModelProperty(value = "bz账号", example = "15062230055")
    @Transient
    private String bzAccount;

    @ApiModelProperty(value = "发送内容", example = "")
    @Transient
    private String sendText;

    @ApiModelProperty(value = "推特账户名", example = "")
    @Transient
    private String twitterAccountName;

    @ApiModelProperty(value = "失败消息", example = "")
    @Transient
    private String failureInfo;

    @ApiModelProperty(value = "TWITTER分享链接", example = "")
    @Transient
    private String twitterShareLink;

    @ApiModelProperty(value = "群消息链接", example = "")
    @Transient
    private List<String> groupShareLink;

    @ApiModelProperty(value = "审核状态 0-未审核 1-审核失败 2-审核成功 3-已封禁", example = "0")
    private Integer state;

    @ApiModelProperty(value = "代理人状态 0-不是代理人 1-是代理人", example = "0")
    private Integer agentState;

    @ApiModelProperty(value = "BZ任务是否完成 0-未完成 1完成 2全部", example = "1")
    @Transient
    private Integer bzTaskFinishState;

}
