package com.code.models.robot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Transient;

/**
 * 用户发送消息的统计
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("用户发送消息的统计")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatMessageNums extends BaseRobot {

    private static final long serialVersionUID = -7863121443761307055L;

    @ApiModelProperty(value = "聊天唯一标识", example = "1")
    private String chatId;

    @ApiModelProperty(value = "推特账号", example = "1")
    @Transient
    private String telegramAccountName;

    @ApiModelProperty(value = "用户名", example = "1")
    private String userName;

    @ApiModelProperty(value = "姓", example = "1")
    private String firstName;

    @ApiModelProperty(value = "名", example = "1")
    private String lastName;

    @ApiModelProperty(value = "当日日期", example = "2022-04-25")
    private String currentDay;

    @ApiModelProperty(value = "用户发送的消息总数", example = "1")
    private Integer messageNums;

    @ApiModelProperty(value = "乐观锁版本号", example = "0")
    private Integer version;

}
