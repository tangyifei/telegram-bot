package com.code.models.robot;

/**
 * 用户相关链接实体类
 *
 * @author xiaoyaowang
 */
public class ChatLink extends BaseRobot {

    private static final long serialVersionUID = 801099662509493372L;

    private String chatId;

    private String shareLink;

    private Integer linkCategory;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getShareLink() {
        return shareLink;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    public Integer getLinkCategory() {
        return linkCategory;
    }

    public void setLinkCategory(Integer linkCategory) {
        this.linkCategory = linkCategory;
    }

    @Override
    public String toString() {
        return "ChatLink{" +
                "chatId='" + chatId + '\'' +
                ", shareLink='" + shareLink + '\'' +
                ", linkCategory=" + linkCategory +
                "} " + super.toString();
    }
}
