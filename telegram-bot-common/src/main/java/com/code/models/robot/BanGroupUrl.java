package com.code.models.robot;

import io.swagger.annotations.ApiModelProperty;

/**
 * 禁止完成群分享任务的群地址列表
 *
 * @author xiaoyaowang
 */
public class BanGroupUrl extends BaseRobot {

    private static final long serialVersionUID = 801099662509493372L;

    @ApiModelProperty(value = "群地址 新增和编辑的时候群地址不能为空", example = "http://.me")
    private String groupUrl;

    public String getGroupUrl() {
        return groupUrl;
    }

    public void setGroupUrl(String groupUrl) {
        this.groupUrl = groupUrl;
    }

    @Override
    public String toString() {
        return "BanGroupUrl{" +
                "groupUrl='" + groupUrl + '\'' +
                "} " + super.toString();
    }
}
