package com.code.models.robot;

import javax.persistence.Transient;

/**
 * 用户关注推特失败实体类
 *
 * @author xiaoyaowang
 */
public class FocusOnTwitterFailure extends BaseRobot {

    private static final long serialVersionUID = 8296948237567649542L;

    private String chatId;

    private String twitterAccountName;

    private String walletAddr;

    @Transient
    private String bzAccount;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getTwitterAccountName() {
        return twitterAccountName;
    }

    public void setTwitterAccountName(String twitterAccountName) {
        this.twitterAccountName = twitterAccountName;
    }

    public String getWalletAddr() {
        return walletAddr;
    }

    public void setWalletAddr(String walletAddr) {
        this.walletAddr = walletAddr;
    }

    public String getBzAccount() {
        return bzAccount;
    }

    public void setBzAccount(String bzAccount) {
        this.bzAccount = bzAccount;
    }

    @Override
    public String toString() {
        return "FocusOnTwitterFailure{" +
                "chatId='" + chatId + '\'' +
                ", twitterAccountName='" + twitterAccountName + '\'' +
                ", walletAddr='" + walletAddr + '\'' +
                ", bzAccount='" + bzAccount + '\'' +
                "} " + super.toString();
    }
}
