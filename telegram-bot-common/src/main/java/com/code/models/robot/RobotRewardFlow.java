package com.code.models.robot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Transient;

/**
 * 机器人奖励流水实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("机器人奖励流水实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RobotRewardFlow extends BaseRobot {

    @ApiModelProperty(value = "聊天的唯一标识", example = "1")
    @Transient
    private String chatId;

    @ApiModelProperty(value = "流水号", example = "1")
    private String serialNo;

    @ApiModelProperty(value = "钱包地址", example = "XWCNfmGDXeVGKTk3VtcCxpb8dzt7c6HDJj5Dc")
    private String walletAddress;

    @ApiModelProperty(value = "奖励金额", example = "100")
    private String amount;

    @ApiModelProperty(value = "查不查询流水", example = "100")
    @Transient
    private Integer rewardFlowQuery;

}
