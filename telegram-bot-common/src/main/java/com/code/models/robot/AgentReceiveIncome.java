package com.code.models.robot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 代理人领取收益记录实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("代理人领取收益记录实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AgentReceiveIncome extends BaseRobot {

    private static final long serialVersionUID = 3164229258835809019L;

    @ApiModelProperty(value = "聊天标识", example = "1")
    private String chatId;

    @ApiModelProperty(value = "关联的用户审核记录主键", example = "1")
    private Integer robotTaskCheckId;

    @ApiModelProperty(value = "领取的收益", example = "0")
    private Integer receiveIncome;

}
