package com.code.models.robot;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * 机器人Model公用的字段
 *
 * @author xiaoyaowang
 */
@ApiModel("任务审核公用VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseRobot implements Serializable {

    private static final long serialVersionUID = -7644650616803284047L;

    @ApiModelProperty(value = "主键 添加时必填 更新或者删除时必填", example = "1")
    @Id
    private Integer id;

    @ApiModelProperty(value = "删除状态 非必填", example = "1")
    private Integer deleted;

    @ApiModelProperty(value = "创建时间 非必填", example = "1")
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "Asia/Shanghai"
    )
    private Date createdAt;

    @ApiModelProperty(value = "更新时间  非必填", example = "1")
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "Asia/Shanghai"
    )
    private Date updatedAt;

}
