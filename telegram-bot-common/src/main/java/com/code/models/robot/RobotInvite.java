package com.code.models.robot;

/**
 * 邀请实体类
 *
 * @author xiaoyaowang
 */
public class RobotInvite extends BaseRobot {

    private static final long serialVersionUID = 6149225588576576529L;

    private String chatId;

    private String inviteChatId;

    private Integer inviterRewardReset;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getInviteChatId() {
        return inviteChatId;
    }

    public void setInviteChatId(String inviteChatId) {
        this.inviteChatId = inviteChatId;
    }

    public Integer getInviterRewardReset() {
        return inviterRewardReset;
    }

    public void setInviterRewardReset(Integer inviterRewardReset) {
        this.inviterRewardReset = inviterRewardReset;
    }
}
