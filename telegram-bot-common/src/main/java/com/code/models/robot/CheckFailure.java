package com.code.models.robot;

/**
 * 审核失败记录实体类
 *
 * @author xiaoyaowang
 */
public class CheckFailure extends BaseRobot {

    private static final long serialVersionUID = -316944909316641719L;

    private String chatId;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    @Override
    public String toString() {
        return "ChatBan{" +
                "chatId='" + chatId + '\'' +
                "} " + super.toString();
    }
}
