package com.code.models.robot;

import javax.persistence.Transient;

/**
 * 用户已完成的任务实体类
 *
 * @author xiaoyaowang
 */
public class RobotChatFinishTask extends BaseRobot {

    private static final long serialVersionUID = -5267828055301972686L;

    private String chatId;

    private Integer taskId;

    @Transient
    private String coinAddr;

    @Transient
    private String bzAccount;

    @Transient
    private String twitterAccountName;

    @Transient
    private Integer sendPhoto;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getCoinAddr() {
        return coinAddr;
    }

    public void setCoinAddr(String coinAddr) {
        this.coinAddr = coinAddr;
    }

    public String getBzAccount() {
        return bzAccount;
    }

    public void setBzAccount(String bzAccount) {
        this.bzAccount = bzAccount;
    }

    public void setTwitterAccountName(String twitterAccountName) {
        this.twitterAccountName = twitterAccountName;
    }

    public String getTwitterAccountName() {
        return twitterAccountName;
    }

    public Integer getSendPhoto() {
        return sendPhoto;
    }

    public void setSendPhoto(Integer sendPhoto) {
        this.sendPhoto = sendPhoto;
    }

    @Override
    public String toString() {
        return "RobotChatFinishTask{" +
                "chatId='" + chatId + '\'' +
                ", taskId=" + taskId +
                ", coinAddr='" + coinAddr + '\'' +
                ", bzAccount='" + bzAccount + '\'' +
                ", twitterAccountName='" + twitterAccountName + '\'' +
                ", sendPhoto=" + sendPhoto +
                "} " + super.toString();
    }
}
