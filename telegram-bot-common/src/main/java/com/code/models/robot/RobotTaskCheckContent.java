package com.code.models.robot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 机器人任务审核内容记录实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("机器人任务审核内容记录实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RobotTaskCheckContent extends BaseRobot {

    @ApiModelProperty(value = "任务审核主键", example = "1")
    private Integer taskCheckId;

    @ApiModelProperty(value = "图片地址", example = "http://")
    private String picUrl;

    @ApiModelProperty(value = "发送内容", example = "")
    private String sendText;

}
