package com.code.models.robot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 用户审核图片实体类
 *
 * @author xiaoyaowang
 */
@ApiModel("用户审核图片实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RobotCheckPic implements Serializable {

    private static final long serialVersionUID = -5787665577020373793L;

    @ApiModelProperty(value = "创建时间", example = "2021-11-01")
    private String createAt;

    @ApiModelProperty(value = "图片地址列表", example = "http://")
    private List<String> picUrlList;

}
