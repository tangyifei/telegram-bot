package com.code.consts;

/**
 * 共享的常量
 *
 * @author xiaoyaowang
 */
public interface CommonConsts {

    String TRANSACTION_MANAGER = "testTransactionManager";

    /**
     * BZ奖励
     */
    String BZ_REWARD = "50";
}
