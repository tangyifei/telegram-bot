package com.code.bz.controllers;

import com.code.bz.annotations.LoginAuth;
import com.code.bz.annotations.OperateLogAnno;
import com.code.bz.annotations.ResponseResult;
import com.code.bz.models.qo.OperateLogQO;
import com.code.bz.models.qo.RobotTaskCheckQO;
import com.code.bz.services.OperateLogService;
import com.code.bz.services.RobotSystemService;
import com.code.bz.services.RobotTaskCheckService;
import com.code.bz.models.po.OperateLog;
import com.code.models.robot.RobotTaskCheck;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 电报机器人相关的控制器
 *
 * @author xiaoyaowang
 */
@ResponseResult
@RestController("telegramBzController")
@RequestMapping("/task-checks")
public class TelegramBzManageController {

    @Resource
    private RobotTaskCheckService robotTaskCheckService;

    @Resource
    private RobotSystemService robotSystemService;

    @Resource
    private OperateLogService operateLogService;

    @ApiOperation(value = "更新机器人任务的审核状态", response = RobotTaskCheck.class, notes = "更新机器人任务的审核状态", httpMethod = "PATCH")
    @PatchMapping("")
    @LoginAuth
    @OperateLogAnno(operateItem = "审核", pageName = "BZ活动审核")
    public int updateRobotTaskState(@RequestBody RobotTaskCheck robotTaskCheck) {
        return robotTaskCheckService.updateRobotTaskState(robotTaskCheck);
    }

    @ApiOperation(value = "手动关闭机器人执行任务", notes = "手动关闭机器人执行任务", httpMethod = "PATCH")
    @PatchMapping("/{systemValue}/whether-stop-robot")
    @LoginAuth
    @ApiImplicitParams({
            @ApiImplicitParam(name = "systemValue", value = "字典value，字符串类型，1表示关闭，0表示开启", dataType = "string", paramType = "path", required = true)
    })
    public void stopRobotExecuteTask(@PathVariable String systemValue) {
        robotSystemService.updateRobotSystem(null, systemValue);
    }

    @ApiOperation(value = "分页获取机器人任务审核列表", response = RobotTaskCheck.class, notes = "分页获取机器人任务审核列表", httpMethod = "POST")
    @PostMapping("")
    @LoginAuth
    public PageInfo<RobotTaskCheck> getRobotTaskCheckListByPage(@Validated @RequestBody RobotTaskCheckQO robotTaskCheckQO) {
        return robotTaskCheckService.getRobotTaskCheckListByPage(robotTaskCheckQO);
    }

    @ApiOperation(value = "分页获取操作日志列表", response = OperateLog.class, notes = "分页获取操作日志列表", httpMethod = "POST")
    @PostMapping("/operate-logs")
    @LoginAuth
    public PageInfo<OperateLog> getOperateLogListByPage(@Validated @RequestBody OperateLogQO operateLogQO) {
        return operateLogService.getOperateLogListByCondition(operateLogQO);
    }

    @ApiOperation(value = "获取机器人任务审核详情", response = RobotTaskCheck.class, notes = "获取机器人任务审核详情", httpMethod = "GET")
    @GetMapping("/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataType = "int", paramType = "path", required = true)
    })
    @LoginAuth
    public RobotTaskCheck getRobotTaskCheckById(@PathVariable("id") Integer id) {
        return robotTaskCheckService.getRobotTaskCheckById(id);
    }

    @ApiOperation(value = "获取机器人状态", notes = "获取机器人状态", httpMethod = "GET")
    @GetMapping("/robot-states")
    @LoginAuth
    public Integer getRobotState() {
        return robotTaskCheckService.getRobotState();
    }

}
