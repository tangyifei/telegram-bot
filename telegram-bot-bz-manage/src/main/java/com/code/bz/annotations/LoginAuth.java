package com.code.bz.annotations;

import java.lang.annotation.*;

/**
 * 已登录权限验证注解
 *
 * @author xiaoyaowang
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LoginAuth {

}
