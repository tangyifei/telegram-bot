package com.code.bz.configs.swagger;

import com.code.bz.consts.HeaderConstants;
import com.code.bz.enums.EnvironmentEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * swagger相关的配置
 *
 * @author xiaoyaowang
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Autowired
    private Environment env;

    @Bean
    public Docket productApiDocket() {
        List<Parameter> pars = setHeaderParameters();
        return new Docket(DocumentationType.SWAGGER_12)
                .enable(!EnvironmentEnum.isProdEnv(env))
                .groupName("telegram-bz-manage")
                .apiInfo(new ApiInfoBuilder().title("telegram-bz-manage").description("机器人后台模块API").build())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.code.bz.controllers"))
                .paths(PathSelectors.any())
                .build().globalOperationParameters(pars);
    }

    private List<Parameter> setHeaderParameters() {
        List<Parameter> pars = new ArrayList<>(1 << 4);
        //X-Token
        ParameterBuilder xTokenPar = new ParameterBuilder();
        xTokenPar.name(HeaderConstants.X_TOKEN).description("token, 登录接口不需要传，如果是浏览器访问，登录后直接放入cookie中，直接从cookie中获取")
                .modelRef(new ModelRef("string")).parameterType("header")
                //header中的X-Token参数必填，登陆接口不需要
                .required(false).defaultValue("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXNzd29yZCI6ImUxMGFkYzM5NDliYTU5YWJiZTU2ZTA1N2YyMGY4ODNlIiwidXNlclN0YXR1cyI6IjAiLCJpZCI6IjEiLCJ1c2VyTmFtZSI6ImFkbWluIiwiZXhwIjoxNjI3MjYzNTk3LCJpYXQiOjE2MjcxNzcxOTd9.LNTkPjLmLcoTGLvfb1d1N8iXNxc8PEZxdzQI5JONMXc").build();
        pars.add(xTokenPar.build());
        //账号名称
        ParameterBuilder accountNamePar = new ParameterBuilder();
        accountNamePar.name(HeaderConstants.ACCOUNT_NAME).description("登录账号，BZ活动审核的时候必填")
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(false).defaultValue("admin").build();
        pars.add(accountNamePar.build());
        //调用来源
        ParameterBuilder callSourcePar = new ParameterBuilder();
        callSourcePar.name(HeaderConstants.CALL_SOURCE).description("调用来源：WEB、PC、WECHAT、IOS、ANDROID")
                .modelRef(new ModelRef("string")).parameterType("header")
                //header中的Call-Source参数必填，传空也可以
                .required(true).defaultValue("WEB").build();
        pars.add(callSourcePar.build());
        // nonce
        ParameterBuilder noncePar = new ParameterBuilder();
        noncePar.name(HeaderConstants.NONCE).description("请求唯一标识-实现防重放攻击")
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(false).defaultValue("uZghr1Ev").build();
        pars.add(noncePar.build());
        // 时间戳
        ParameterBuilder timestampPar = new ParameterBuilder();
        timestampPar.name(HeaderConstants.TIMESTAMP).description("时间戳（毫秒级别）客户端的时间与服务器的时间要保持一致-实现防重放攻击")
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(false).defaultValue("1611814156560").build();
        pars.add(timestampPar.build());

        ParameterBuilder apiLanguagePar = new ParameterBuilder();
        apiLanguagePar.name(HeaderConstants.LANGUAGE).description("语言：中文传cn，英文传en，韩文传ko，日文传ja，中文繁体传hk")
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(false).defaultValue("cn").build();
        //根据每个方法名也知道当前方法在设置什么参数
        pars.add(apiLanguagePar.build());

        ParameterBuilder signPar = new ParameterBuilder();
        signPar.name(HeaderConstants.SIGN).description("签名，一般外部系统接口调用需要")
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(false).build();
        //根据每个方法名也知道当前方法在设置什么参数
        pars.add(signPar.build());

        return pars;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("南京编码科技有限公司相关业务API")
                .description("接口写得好，联调通的早")
                .version("1.0.0")
                .build();
    }
}
