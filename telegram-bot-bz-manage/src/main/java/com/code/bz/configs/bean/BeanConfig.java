package com.code.bz.configs.bean;

import com.code.bz.aspects.RestControllerAspect;
import com.code.bz.handlers.GlobalExceptionHandler;
import com.code.bz.handlers.ResponseResultHandler;
import com.code.bz.utils.RequestContextUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * bean相关的配置类
 *
 * @author xiaoyaowang
 */
@Configuration
@ConditionalOnClass(value = {RedisTemplate.class})
public class BeanConfig {

    /**
     * 注册全局异常处理器到spring容器中
     *
     * @return 全局异常处理器相关的bean
     */
    @Bean
    public GlobalExceptionHandler globalExceptionHandler() {
        return new GlobalExceptionHandler();
    }

    /**
     * 注册记录日志的控制器切面到spring容器中
     *
     * @return 记录日志的控制器切面相关的bean
     */
    @Bean
    public RestControllerAspect restControllerAspect() {
        return new RestControllerAspect();
    }

    /**
     * 注册接口响应体处理器到spring容器中
     *
     * @return 接口响应体处理器相关的bean
     */
    @Bean
    public ResponseResultHandler responseResultHandler() {
        return new ResponseResultHandler();
    }

    /**
     * 初始化请求上下文对象
     *
     * @return 请求上下文对象
     */
    @Bean
    @Lazy(value = false)
    public RequestContextUtil springContextHolder() {
        return new RequestContextUtil();
    }

}
