package com.code.bz.results;

import com.code.bz.enums.BusinessExceptionEnum;
import com.code.bz.enums.ResultCode;
import com.code.bz.exceptions.BusinessException;
import com.code.bz.utils.RequestContextUtil;
import com.code.bz.utils.StringUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;

import java.util.Date;

/**
 * 默认全局错误返回结果
 * 备注：
 * 该返回信息是spring boot的默认异常时返回结果，目前也是我们服务的默认返回结果
 *
 * @author xiaoyaowang
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DefaultErrorResult implements Result {

    private static final long serialVersionUID = 1899083570489722793L;

    /**
     * HTTP响应状态码 {@link HttpStatus}
     */
    private Integer status;

    /**
     * HTTP响应状态码的英文提示
     */
    private String error;

    /**
     * 异常堆栈的精简信息
     */
    private String message;

    /**
     * 我们系统内部自定义的返回值编码，{@link ResultCode} 它是对错误更加详细的编码
     * <p>
     * 备注：spring boot默认返回异常时，该字段为null
     */
    private Integer code;

    /**
     * 调用接口路径
     */
    private String path;

    /**
     * 异常的名字
     */
    private String exception;

    /**
     * 异常的错误传递的数据
     */
    private Object errors;

    /**
     * 时间戳
     */
    private Date timestamp;

    /**
     * 出现异常时全局异常返回结果的封装
     *
     * @param resultCode api统一结果状态码
     * @param e          异常
     * @param httpStatus http状态码
     * @param errors     异常的错误传递的数据
     * @return 全局异常返回结果
     */
    public static DefaultErrorResult failure(ResultCode resultCode, Throwable e, HttpStatus httpStatus, Object errors) {
        DefaultErrorResult result = DefaultErrorResult.failure(resultCode, e, httpStatus);
        result.setErrors(errors);
        return result;
    }

    /**
     * 出现异常时全局异常返回结果的封装
     *
     * @param resultCode api统一结果状态码
     * @param e          异常
     * @param httpStatus http状态码
     * @return 全局异常返回结果
     */
    public static DefaultErrorResult failure(ResultCode resultCode, Throwable e, HttpStatus httpStatus) {
        DefaultErrorResult result = new DefaultErrorResult();
        result.setCode(resultCode.code());
        //results.setMessage(resultCode.message());
        result.setMessage(RequestContextUtil.getApplicationContext().getMessage(String.valueOf(resultCode.code()), null, LocaleContextHolder.getLocale()));
        result.setStatus(httpStatus.value());
        result.setError(httpStatus.getReasonPhrase());
        result.setException(e.getClass().getName());
        String path = RequestContextUtil.getRequest().getRequestURI();
        result.setPath(path);
        result.setTimestamp(new Date());
        return result;
    }

    /**
     * 出现自定义的业务异常时返回的全局异常结果
     *
     * @param e 业务异常
     * @return 全局异常返回结果
     */
    public static DefaultErrorResult failure(BusinessException e) {
        BusinessExceptionEnum ee = BusinessExceptionEnum.getByEClass(e.getClass());
        if (null != ee) {
            return DefaultErrorResult.failure(ee.getResultCode(), e, ee.getHttpStatus(), e.getData());
        }

        DefaultErrorResult defaultErrorResult = DefaultErrorResult.failure(null == e.getResultCode() ? ResultCode.SUCCESS : e.getResultCode(), e, HttpStatus.OK, e.getData());
        if (StringUtil.isNotEmpty(e.getMessage())) {
            defaultErrorResult.setMessage(e.getMessage());
        }
        return defaultErrorResult;
    }

}
