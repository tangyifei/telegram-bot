package com.code.bz.models.qo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 操作日志查询实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("操作日志查询实体类")
@Data
@NoArgsConstructor
public class OperateLogQO extends BaseQO {

    private static final long serialVersionUID = -9028567140273691019L;

}
