package com.code.bz.models.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 机器人任务审核记录查询实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("机器人任务审核记录查询实体类QO")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RobotTaskCheckQO extends BaseQO {

    private static final long serialVersionUID = 3522992676325318444L;

    @ApiModelProperty(value = "用户名-账户名（telegram账号） 字符串类型 非必填", example = "limingcheng")
    private String userName;

    @ApiModelProperty(value = "推特账号 字符串类型 非必填", example = "limingcheng")
    private String twitterAccountName;

    @ApiModelProperty(value = "bz账号 字符串类型 非必填", example = "15062230055")
    private String bzAccount;

    @ApiModelProperty(value = "审核状态 整型 0-未审核（待审核） 1-审核失败 2-审核成功 非必填", example = "0")
    private Integer state;

    @ApiModelProperty(value = "1-升序 2降序 非必填 默认按照时间降序", example = "2")
    private Integer sort;

}
