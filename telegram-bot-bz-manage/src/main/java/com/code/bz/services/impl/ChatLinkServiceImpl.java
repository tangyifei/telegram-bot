package com.code.bz.services.impl;

import com.code.bz.daos.robot.ChatLinkMapper;
import com.code.bz.services.ChatLinkService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户分享链接服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatLinkServiceImpl implements ChatLinkService {

    @Resource
    ChatLinkMapper chatLinkMapper;

    @Override
    public List<String> getChatLinkListByChatIdAndLinkCategory(String chatId, Integer linkCategory) {
        return chatLinkMapper.getChatLinkListByChatIdAndLinkCategory(chatId, linkCategory);
    }

    @Override
    public void deleteChatLinkByChatId(String chatId) {
        if (null != chatLinkMapper.existsChatLinkByChatId(chatId)) {
            chatLinkMapper.deleteChatLinkByChatId(chatId);
        }
    }
}
