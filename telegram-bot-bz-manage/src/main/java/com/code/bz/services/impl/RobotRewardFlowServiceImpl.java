package com.code.bz.services.impl;

import com.code.bz.daos.robot.RobotRewardFlowMapper;
import com.code.bz.services.RobotRewardFlowService;
import com.code.models.robot.RobotRewardFlow;
import com.code.utils.SnowFlakeIdUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 机器人奖励服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class RobotRewardFlowServiceImpl implements RobotRewardFlowService {

    @Resource
    RobotRewardFlowMapper robotRewardFlowMapper;

    @Override
    public int insertRobotRewardFlow(RobotRewardFlow robotRewardFlow) {
        String walletAddress = robotRewardFlow.getWalletAddress();
        String amount = robotRewardFlow.getAmount();
        Integer exitsRobotRewardFlow = robotRewardFlowMapper.existRobotRewardFlowByWalletAddressAndAmount(walletAddress, amount);
        if (null == exitsRobotRewardFlow) {
            robotRewardFlow.setSerialNo("RD" + SnowFlakeIdUtil.defaultId());
            robotRewardFlow.setDeleted(0);
            robotRewardFlow.setCreatedAt(new Date());
            robotRewardFlow.setUpdatedAt(new Date());
            return robotRewardFlowMapper.insert(robotRewardFlow);
        }
        return 0;
    }

    @Override
    public Boolean existRobotRewardFlowByWalletAddressAndAmount(String walletAddress, String amount) {
        return null != robotRewardFlowMapper.existRobotRewardFlowByWalletAddressAndAmount(walletAddress, amount);
    }

}
