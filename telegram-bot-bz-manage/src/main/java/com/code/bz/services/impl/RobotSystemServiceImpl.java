package com.code.bz.services.impl;

import com.code.bz.daos.system.RobotSystemMapper;
import com.code.bz.services.RobotSystemService;
import com.code.enums.RewardKeyEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 系统设置服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class RobotSystemServiceImpl implements RobotSystemService {

    @Resource
    RobotSystemMapper robotSystemMapper;


    @Override
    public String getSystemValueBySystemKey(String systemKey) {
        return robotSystemMapper.getSystemValueBySystemKey(systemKey);
    }

    @Override
    public void updateRobotSystem(String systemKey, String systemValue) {
        if (StringUtils.isBlank(systemKey)) {
            systemKey = RewardKeyEnum.STOP_ROBOT.name();
        }
        if (StringUtils.isNotBlank(robotSystemMapper.getSystemValueBySystemKey(systemKey))) {
            robotSystemMapper.updateSystemValueBySystemKey(systemKey, systemValue, new Date());
        }
    }
}
