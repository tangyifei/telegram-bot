package com.code.bz.daos.robot;

import com.code.bz.daos.base.CrudMapper;
import com.code.models.robot.ChatTwitter;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 关注推特记录持久层
 *
 * @author xiaoyaowang
 */
public interface ChatTwitterMapper extends CrudMapper<ChatTwitter> {

    Integer existsChatTwitterByChatId(@Param("chatId") String chatId);

    Integer existsFocusOnTwitterFailureByChatId(@Param("chatId") String chatId);

    void deleteChatTwitterByChatId(@Param("chatId") String chatId);

    void deleteFocusOnTwitterFailureByChatId(@Param("chatId") String chatId);

}
