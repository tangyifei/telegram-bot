package com.code.bz.daos.base;

/**
 * 基础增删改查功能mapper
 *
 * @author xiaoyaowang
 */
public interface CrudMapper<T> extends
        InsertMapper<T>,
        DeleteMapper<T>,
        UpdateMapper<T>,
        SelectMapper<T> {
}
