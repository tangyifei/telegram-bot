package com.code.bz.daos.system;

import com.code.bz.daos.base.CrudMapper;
import com.code.models.robot.RobotSystem;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * 机器人系统设置持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface RobotSystemMapper extends CrudMapper<RobotSystem> {

    /**
     * 通过系统key获取系统值
     *
     * @param systemKey 系统key
     * @return 获取的系统值
     */
    String getSystemValueBySystemKey(@Param("systemKey") String systemKey);

    /**
     * 通过字典key更新字典value
     *
     * @param systemKey   字典key
     * @param systemValue 字典value
     * @param updatedAt   更新时间
     * @return 影响的行数
     */
    int updateSystemValueBySystemKey(@Param("systemKey") String systemKey, @Param("systemValue") String systemValue, @Param("updatedAt") Date updatedAt);

}
