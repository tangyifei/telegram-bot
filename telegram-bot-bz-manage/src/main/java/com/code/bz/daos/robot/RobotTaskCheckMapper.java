package com.code.bz.daos.robot;

import com.code.bz.daos.base.CrudMapper;
import com.code.bz.models.qo.RobotTaskCheckQO;
import com.code.models.robot.RobotChatFinishTask;
import com.code.models.robot.RobotTaskCheck;
import com.code.models.robot.RobotTaskCheckContent;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 任务审核持久层
 *
 * @author xiaoyaowang
 */
public interface RobotTaskCheckMapper extends CrudMapper<RobotTaskCheck> {

    /**
     * 根据相关的条件获取机器人任务审核记录列表
     *
     * @param robotTaskCheckQO 机器人任务审核记录查询实体类QO
     * @return 机器人任务审核记录列表
     */
    List<RobotTaskCheck> getRobotTaskCheckListByPage(RobotTaskCheckQO robotTaskCheckQO);

    /**
     * 根据主键获取任务审核详情记录
     *
     * @param id 主键
     * @return 任务审核详情记录
     */
    RobotTaskCheck getRobotTaskCheckById(@Param("id") int id);

    /**
     * 通过任务审核id获取图片列表
     *
     * @param id 任务审核主键
     * @return 图片列表
     */
    List<RobotTaskCheckContent> getRobotTaskCheckContentListByTaskCheckId(@Param("id") int id);

    /**
     * 更新任务的审核状态
     *
     * @param id        主键
     * @param state     审核状态 0-未审核 1-审核失败 2-审核成功
     * @param updatedAt 更新时间
     * @return 影响的行数
     */
    int updateRobotTaskState(@Param("id") int id, @Param("state") int state, @Param("updatedAt") Date updatedAt);

    /**
     * 根据排序获取任务主键
     *
     * @param sort 排序
     * @return 任务主键
     */
    Integer getTaskIdBySort(@Param("sort") Integer sort);

    /**
     * 通过taskId和chatId获取完成的任务记录
     *
     * @param chatId 聊天唯一标识
     * @param taskId 任务主键
     * @return 完成的任务记录
     */
    RobotChatFinishTask getRobotChatFinishTaskByChatIdAndTaskId(@Param("chatId") String chatId, @Param("taskId") Integer taskId);

    /**
     * 删除用户完成任务记录
     *
     * @param robotChatFinishTaskDb 用户已完成的任务实体类
     */
    void deleteRobotChatFinishTask(RobotChatFinishTask robotChatFinishTaskDb);

    /**
     * 批量修改图片审核记录
     *
     * @param list 机器人任务审核内容记录列表
     */
    void deleteRobotTaskCheckContentList(List<RobotTaskCheckContent> list);

    /**
     * 判断用户是否被封禁
     *
     * @param chatId 聊天唯一id
     * @return 判断用户是否被封禁
     */
    Integer existsChatBanByChatId(@Param("chatId") String chatId);

    /**
     * 删除封禁的用户
     *
     * @param chatId 聊天唯一id
     */
    void deleteChatBanByChatId(@Param("chatId") String chatId);

    /**
     * 根据主键获取任务审核详情记录
     *
     * @param id 主键
     * @return 任务审核详情记录
     */
    RobotTaskCheck selectRobotTaskCheckById(@Param("id") int id);

    /**
     * 删除审核记录
     *
     * @param id 主键
     */
    void deleteRobotTaskCheckById(@Param("id") int id);

}
