package com.code.bz.daos.robot;

import com.code.bz.daos.base.CrudMapper;
import com.code.models.robot.RobotRewardFlow;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 机器人奖励流水持久层
 *
 * @author xiaoyaowang
 */
public interface RobotRewardFlowMapper extends CrudMapper<RobotRewardFlow> {

    /**
     * 判断某一个钱包地址是否给予了奖励
     *
     * @param walletAddress 钱包地址
     * @param amount        奖励金额
     * @return 记录数
     */
    Integer existRobotRewardFlowByWalletAddressAndAmount(@Param("walletAddress") String walletAddress, @Param("amount") String amount);

}
