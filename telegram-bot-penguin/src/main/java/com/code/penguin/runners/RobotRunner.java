package com.code.penguin.runners;

import com.code.penguin.bots.ListenBot;
import com.code.penguin.bots.TaskRobot;
import com.code.penguin.managers.HttpManager;
import com.code.penguin.services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import javax.annotation.Resource;

/**
 * 注册机器人
 *
 * @author xiaoyaowang
 */
@Component
public class RobotRunner implements ApplicationRunner {

    @Value("${reply.button.text}")
    private String replyButtonText;

    @Value("${reply.done.text}")
    private String replyDoneText;

    @Value("${error.text}")
    private String errorText;

    @Value("${menu.first-button-text}")
    private String menuFirstButtonText;

    @Value("${menu.second-button-text}")
    private String menuSecondButtonText;

    @Value("${menu.third-button-text}")
    private String menuThirdButtonText;

    @Value("${menu.fourth-button-text}")
    private String menuFourthButtonText;

    @Resource
    private RobotChatService robotChatService;

    @Resource
    private RobotTaskCheckService robotTaskCheckService;

    @Resource
    private RobotTaskService robotTaskService;

    @Resource
    private ChatMemberGroupService chatMemberGroupService;

    @Resource
    private VerifiedAddrService verifiedAddrService;

    @Resource
    private ChatLinkService chatLinkService;

    @Resource
    private ChatBanService chatBanService;

    @Resource
    private RobotSystemService robotSystemService;

    @Resource
    private HttpManager httpManager;

    @Resource
    private ChatTwitterService chatTwitterService;

    @Resource
    private ChatEmailService chatEmailService;

    @Resource
    private ChatDiscordService chatDiscordService;

    @Resource
    private ChatCollectService chatCollectService;

    @Value("${listen.join.group.bot}")
    private String listenJoinGroupBot;

    @Value("${listen.join.group.token}")
    private String listenJoinGroupToken;

    @Value("${execute.task.bot.name}")
    private String taskBotName;

    @Value("${execute.task.bot.token}")
    private String taskBotToken;

    @Value("${listen.join.group.url-prefix}")
    private String listenJoinGroupUrlPrefix;

    @Value("${listen.join.group.name}")
    private String listenJoinGroupName;

    @Value("${wallet.download.url}")
    private String walletDownLoadUrl;

    private static Logger logger = LoggerFactory.getLogger(RobotRunner.class);

    @Override
    public void run(ApplicationArguments args) {
        // 电报机器人配置
        try {
            TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);

            TaskRobot taskRobot = new TaskRobot();
            taskRobot.setRobotName(taskBotName);
            taskRobot.setRobotToken(taskBotToken);
            taskRobot.setListenJoinGroupUrlPrefix(listenJoinGroupUrlPrefix);
            taskRobot.setListenJoinGroupName(listenJoinGroupName);
            taskRobot.setChatMemberGroupService(chatMemberGroupService);
            taskRobot.setRobotChatService(robotChatService);
            taskRobot.setHttpManager(httpManager);
            taskRobot.setVerifiedAddrService(verifiedAddrService);
            taskRobot.setChatTwitterService(chatTwitterService);
            taskRobot.setRobotTaskService(robotTaskService);
            taskRobot.setRobotTaskCheckService(robotTaskCheckService);
            taskRobot.setRobotSystemService(robotSystemService);
            taskRobot.setChatBanService(chatBanService);
            taskRobot.setReplyButtonText(replyButtonText);
            taskRobot.setErrorText(errorText);
            taskRobot.setMenuFirstButtonText(menuFirstButtonText);
            taskRobot.setMenuSecondButtonText(menuSecondButtonText);
            taskRobot.setMenuThirdButtonText(menuThirdButtonText);
            taskRobot.setMenuFourthButtonText(menuFourthButtonText);
            taskRobot.setReplyDoneText(replyDoneText);
            taskRobot.setChatLinkService(chatLinkService);
            taskRobot.setChatEmailService(chatEmailService);
            taskRobot.setChatDiscordService(chatDiscordService);
            taskRobot.setChatCollectService(chatCollectService);
            taskRobot.setWalletDownLoadUrl(walletDownLoadUrl);
            botsApi.registerBot(taskRobot);
            logger.info("任务机器人已注册，名称:{} token:{}", taskBotName, taskBotToken);

            ListenBot listenBot = new ListenBot();
            listenBot.setRobotName(listenJoinGroupBot);
            listenBot.setRobotToken(listenJoinGroupToken);
            listenBot.setListenJoinGroupUrlPrefix(listenJoinGroupUrlPrefix);
            listenBot.setListenJoinGroupName(listenJoinGroupName);
            listenBot.setChatMemberGroupService(chatMemberGroupService);
            listenBot.setRobotChatService(robotChatService);
            listenBot.setRobotSystemService(robotSystemService);
            botsApi.registerBot(listenBot);
            logger.info("监听加群机器人已注册，名称:{} token:{}", listenJoinGroupBot, listenJoinGroupToken);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

}
