package com.code.penguin.models;

import java.io.Serializable;

/**
 * 已空投记录
 *
 * @author xiaoyaowang
 */
public class ClientResponse implements Serializable {

    private static final long serialVersionUID = 8540623981033809467L;

    private Integer retCode;

    private String retMsg;

    private Integer version;

    private Object data;

    public Integer getRetCode() {
        return retCode;
    }

    public void setRetCode(Integer retCode) {
        this.retCode = retCode;
    }

    public String getRetMsg() {
        return retMsg;
    }

    public void setRetMsg(String retMsg) {
        this.retMsg = retMsg;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ClientResponse{" +
                "retCode=" + retCode +
                ", retMsg='" + retMsg + '\'' +
                ", version=" + version +
                ", data=" + data +
                '}';
    }
}
