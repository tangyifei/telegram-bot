package com.code.penguin.models;

/**
 * 用户收集内容记录
 *
 * @author xiaoyaowang
 */
public class ChatCollectContent extends BaseRobot {

    private static final long serialVersionUID = 6093009722365100166L;

    private Integer chatCollectId;

    private String collectContent;

    public Integer getChatCollectId() {
        return chatCollectId;
    }

    public void setChatCollectId(Integer chatCollectId) {
        this.chatCollectId = chatCollectId;
    }

    public String getCollectContent() {
        return collectContent;
    }

    public void setCollectContent(String collectContent) {
        this.collectContent = collectContent;
    }

    @Override
    public String toString() {
        return "ChatCollectContent{" +
                "chatCollectId=" + chatCollectId +
                ", collectContent='" + collectContent + '\'' +
                "} " + super.toString();
    }
}
