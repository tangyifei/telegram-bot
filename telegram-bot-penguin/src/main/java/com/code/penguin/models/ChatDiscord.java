package com.code.penguin.models;

/**
 * Discord记录
 *
 * @author xiaoyaowang
 */
public class ChatDiscord extends BaseRobot {

    private static final long serialVersionUID = -1256152113592896612L;

    private String chatId;

    private String discordAddress;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getDiscordAddress() {
        return discordAddress;
    }

    public void setDiscordAddress(String discordAddress) {
        this.discordAddress = discordAddress;
    }

    @Override
    public String toString() {
        return "ChatDiscord{" +
                "chatId='" + chatId + '\'' +
                ", discordAddress='" + discordAddress + '\'' +
                "} " + super.toString();
    }
}
