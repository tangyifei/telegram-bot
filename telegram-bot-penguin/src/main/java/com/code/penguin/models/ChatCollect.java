package com.code.penguin.models;

/**
 * 用户收集记录
 *
 * @author xiaoyaowang
 */
public class ChatCollect extends BaseRobot {

    private static final long serialVersionUID = -6602216773422494939L;

    private String chatId;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    @Override
    public String toString() {
        return "ChatCollect{" +
                "chatId='" + chatId + '\'' +
                "} " + super.toString();
    }
}
