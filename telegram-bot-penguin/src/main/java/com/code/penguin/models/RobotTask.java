package com.code.penguin.models;

/**
 * 机器人任务记录实体类
 *
 * @author xiaoyaowang
 */
public class RobotTask extends BaseRobot {

    private static final long serialVersionUID = -8543473777345355842L;

    private String taskTitle;

    private String taskDescription;

    private Integer taskSort;

    public String getTaskTitle() {
        return taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public Integer getTaskSort() {
        return taskSort;
    }

    public void setTaskSort(Integer taskSort) {
        this.taskSort = taskSort;
    }

    @Override
    public String toString() {
        return "RobotTask{" +
                "taskTitle='" + taskTitle + '\'' +
                ", taskDescription='" + taskDescription + '\'' +
                ", taskSort=" + taskSort +
                "} " + super.toString();
    }
}
