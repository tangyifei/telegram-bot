package com.code.penguin.models;

/**
 * 已校验地址实体类
 *
 * @author xiaoyaowang
 */
public class VerifiedAddr extends BaseRobot {

    private static final long serialVersionUID = 6449232222669538946L;

    private String chatId;

    private String coinAddr;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getCoinAddr() {
        return coinAddr;
    }

    public void setCoinAddr(String coinAddr) {
        this.coinAddr = coinAddr;
    }

    @Override
    public String toString() {
        return "VerifiedAddr{" +
                "chatId='" + chatId + '\'' +
                ", coinAddr='" + coinAddr + '\'' +
                "} " + super.toString();
    }
}
