package com.code.penguin.models;

/**
 * 与相关机器人聊天的用户记录实体类
 *
 * @author xiaoyaowang
 */
public class RobotChat extends BaseRobot {

    private static final long serialVersionUID = 8643125232070874983L;

    private String chatId;

    private String robotName;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getRobotName() {
        return robotName;
    }

    public void setRobotName(String robotName) {
        this.robotName = robotName;
    }

    @Override
    public String toString() {
        return "RobotChat{" +
                "chatId='" + chatId + '\'' +
                ", robotName='" + robotName + '\'' +
                "} " + super.toString();
    }
}
