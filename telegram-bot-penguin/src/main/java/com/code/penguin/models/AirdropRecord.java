package com.code.penguin.models;

/**
 * 已空投记录
 *
 * @author xiaoyaowang
 */
public class AirdropRecord extends BaseRobot {

    private static final long serialVersionUID = 3545728156576288236L;

    private String chatId;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    @Override
    public String toString() {
        return "AirdropRecord{" +
                "chatId='" + chatId + '\'' +
                "} " + super.toString();
    }
}
