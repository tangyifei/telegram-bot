package com.code.penguin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 机器人启动类
 *
 * @author xiaoyaowang
 */
@SpringBootApplication
public class TelegramBotPenguinApplication {

    public static void main(String[] args) {
        SpringApplication.run(TelegramBotPenguinApplication.class, args);
    }

}
