package com.code.penguin.services;

/**
 * 推特账户业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatTwitterService {

    /**
     * 添加与机器人聊天的推特账户
     *
     * @param chatId             聊天id
     * @param twitterAccountName 推特账户名
     * @return 影响的行数
     */
    int insertChatTwitter(String chatId, String twitterAccountName);

    /**
     * 判断该推特账户是否被使用
     *
     * @param chatId             聊天唯一标识
     * @param twitterAccountName 推特账户
     * @param text               推特链接地址
     * @return 推特账户是否被使用
     */
    boolean whetherExistChatTwitterAccountNameByTwitterAccountName(String chatId, String twitterAccountName, String text);

}
