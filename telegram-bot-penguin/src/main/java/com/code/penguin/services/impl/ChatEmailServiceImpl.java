package com.code.penguin.services.impl;

import com.code.penguin.daos.task.ChatEmailMapper;
import com.code.penguin.models.ChatEmail;
import com.code.penguin.services.ChatEmailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 邮箱实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatEmailServiceImpl implements ChatEmailService {

    @Resource
    ChatEmailMapper chatEmailMapper;

    @Override
    public boolean whetherExistEmailByChatIdAndEmail(String chatId, String email) {
        return null != chatEmailMapper.whetherExistEmailByChatIdAndEmail(chatId, email);
    }

    @Override
    public String getEmailByChatId(String chatId) {
        return chatEmailMapper.getEmailByChatId(chatId);
    }

    @Override
    public int insertChatEmail(String chatId, String email) {
        if (null == chatEmailMapper.whetherExistEmailByChatIdAndEmail(chatId, email)) {
            ChatEmail chatEmail = new ChatEmail();
            chatEmail.setChatId(chatId);
            chatEmail.setEmail(email);
            chatEmail.setDeleted(0);
            chatEmail.setCreatedAt(new Date());
            chatEmail.setUpdatedAt(new Date());
            return chatEmailMapper.insert(chatEmail);
        }
        return 0;
    }
}
