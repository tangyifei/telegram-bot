package com.code.penguin.services.impl;

import com.code.penguin.consts.CommonConsts;
import com.code.penguin.daos.task.RobotTaskCheckMapper;
import com.code.penguin.models.RobotTaskCheck;
import com.code.penguin.services.RobotTaskCheckService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 机器人任务审核服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class RobotTaskCheckServiceImpl implements RobotTaskCheckService {

    @Resource
    RobotTaskCheckMapper robotTaskCheckMapper;

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public int insertRobotTaskCheck(RobotTaskCheck robotTaskCheck) {
        RobotTaskCheck robotTaskCheckForDb = robotTaskCheckMapper.getRobotTaskCheckByCondition(robotTaskCheck);
        int count = 0;
        if (null == robotTaskCheckForDb) {
            robotTaskCheck.setDeleted(0);
            robotTaskCheck.setCreatedAt(new Date());
            robotTaskCheck.setUpdatedAt(new Date());
            Integer state = robotTaskCheck.getState();
            if (state != 3) {
                robotTaskCheck.setState(0);
            }
            count = robotTaskCheckMapper.insert(robotTaskCheck);
        } else {
            robotTaskCheck.setId(robotTaskCheckForDb.getId());
            robotTaskCheck.setUpdatedAt(new Date());
            robotTaskCheckMapper.updateByPrimaryKeySelective(robotTaskCheck);
        }
        return count;
    }

    @Override
    public boolean whetherBanedByChatId(String chatId) {
        return null != robotTaskCheckMapper.getBanTaskCheckByChatId(chatId);
    }

}
