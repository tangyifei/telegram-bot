package com.code.penguin.services;

import com.code.penguin.models.ChatLink;

/**
 * 用户分享链接业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatLinkService {

    /**
     * 根据用户链接地址和链接种类获取用户的分享链接
     *
     * @param chatId       聊天唯一id
     * @param linkCategory 分享链接种类
     * @return 用户的分享链接
     */
    String getChatLinkListByChatIdAndLinkCategory(String chatId, Integer linkCategory);

    /**
     * 添加与机器人聊天的分享链接
     *
     * @param chatLink 聊天id
     * @return 影响的行数
     */
    int insertChatLink(ChatLink chatLink);

    /**
     * 根据用户链接地址判断是否存在用户的分享链接
     *
     * @param chatId    聊天唯一id
     * @param shareLink 分享链接
     * @return 是否存在用户的分享链接
     */
    boolean whetherExistChatLinkByChatIdAndShareLink(String chatId, String shareLink);

}
