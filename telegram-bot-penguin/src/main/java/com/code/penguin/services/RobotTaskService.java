package com.code.penguin.services;

import com.code.penguin.models.AirdropRecord;
import com.code.penguin.models.RobotChatFinishTask;
import com.code.penguin.models.RobotTask;

/**
 * 机器人任务业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotTaskService {

    /**
     * 获取用户完成的任务的最大序号
     *
     * @param chatId 聊天id
     * @return 用户完成的任务的最大序号
     */
    int getFinishedTaskMaxSort(String chatId);

    /**
     * 获取总的任务数
     *
     * @return 总的任务数
     */
    int getTotalTasks();

    /**
     * 获取完成的任务数
     *
     * @return 完成的任务数
     */
    int getTotalFinishTasks(String chatId);

    /**
     * 根据序号获取任务
     *
     * @param taskSort 任务序号
     * @return 用户完成的任务的最大序号
     */
    RobotTask getRobotTaskByTaskSort(Integer taskSort);

    /**
     * 添加用户已完成的任务记录
     *
     * @param robotChatFinishTask 用户已完成的任务实体类
     * @return 影响的行数
     */
    int insertRobotChatFinishTask(RobotChatFinishTask robotChatFinishTask);

    /**
     * 判断用户是否参加过空投
     *
     * @param airdropRecord 已空投记录
     * @return 用户是否参加过空投
     */
    boolean existsAirdropRecordByCondition(AirdropRecord airdropRecord);

}
