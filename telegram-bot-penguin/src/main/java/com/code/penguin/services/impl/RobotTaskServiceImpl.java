package com.code.penguin.services.impl;

import com.code.penguin.consts.CommonConsts;
import com.code.penguin.daos.task.RobotChatFinishTaskMapper;
import com.code.penguin.daos.task.RobotTaskMapper;
import com.code.penguin.models.*;
import com.code.penguin.services.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 机器人任务服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class RobotTaskServiceImpl implements RobotTaskService {

    @Resource
    RobotTaskMapper robotTaskMapper;

    @Resource
    RobotChatFinishTaskMapper robotChatFinishTaskMapper;

    @Resource
    VerifiedAddrService verifiedAddrService;

    @Resource
    ChatTwitterService chatTwitterService;

    @Resource
    RobotTaskCheckService robotTaskCheckService;

    @Resource
    ChatMemberGroupService chatMemberGroupService;

    @Resource
    AirdropRecordService airdropRecordService;

    @Resource
    ChatEmailService chatEmailService;

    @Resource
    ChatDiscordService chatDiscordService;

    @Resource
    ChatCollectService chatCollectService;

    @Override
    public int getFinishedTaskMaxSort(String chatId) {
        Integer finishedTaskMaxSort = robotTaskMapper.getFinishedTaskMaxSort(chatId);
        if (null != finishedTaskMaxSort) {
            return finishedTaskMaxSort;
        }
        return 0;
    }

    @Override
    public int getTotalTasks() {
        return robotTaskMapper.getTotalTasks();
    }

    @Override
    public int getTotalFinishTasks(String chatId) {
        return robotTaskMapper.getTotalFinishTasks(chatId);
    }

    @Override
    public RobotTask getRobotTaskByTaskSort(Integer taskSort) {
        return robotTaskMapper.getRobotTaskByTaskSort(taskSort);
    }

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public int insertRobotChatFinishTask(RobotChatFinishTask robotChatFinishTask) {
        String chatId = robotChatFinishTask.getChatId();
        Integer taskId = robotChatFinishTask.getTaskId();
        int count = 0;
        if (null != taskId) {
            Integer existCount = robotTaskMapper.existsRobotChatFinishTaskByChatIdAndTaskId(taskId, chatId);
            if (null == existCount) {
                robotChatFinishTask.setDeleted(0);
                robotChatFinishTask.setCreatedAt(new Date());
                robotChatFinishTask.setUpdatedAt(new Date());
                count = robotChatFinishTaskMapper.insert(robotChatFinishTask);
            }
        }
        String coinAddr = robotChatFinishTask.getCoinAddr();
        if (StringUtils.isNotBlank(coinAddr)) {
            verifiedAddrService.insertVerifiedAddr(chatId, coinAddr);
        }
        String twitterAccountName = robotChatFinishTask.getTwitterAccountName();
        if (StringUtils.isNotBlank(twitterAccountName)) {
            chatTwitterService.insertChatTwitter(chatId, twitterAccountName);
        }
        Integer insertRobotTaskCheckFlag = robotChatFinishTask.getInsertRobotTaskCheckFlag();
        if (null != insertRobotTaskCheckFlag) {
            addRobotCheckTask(chatId);
        }
        String email = robotChatFinishTask.getEmail();
        if (StringUtils.isNotBlank(email)) {
            chatEmailService.insertChatEmail(chatId, email);
        }
        String discord = robotChatFinishTask.getDiscord();
        if (StringUtils.isNotBlank(discord)) {
            chatDiscordService.insertDiscord(chatId, discord);
        }
        String collectContent = robotChatFinishTask.getCollectContent();
        if (StringUtils.isNotBlank(collectContent)) {
            chatCollectService.insertCollect(chatId, collectContent);
        }

        return count;
    }

    @Override
    public boolean existsAirdropRecordByCondition(AirdropRecord airdropRecord) {
        return airdropRecordService.existsAirdropRecordByCondition(airdropRecord);
    }

    private void addRobotCheckTask(String chatId) {
        RobotTaskCheck robotTaskCheck = new RobotTaskCheck();
        robotTaskCheck.setChatId(chatId);
        ChatMemberGroup chatMemberGroup = chatMemberGroupService.getChatMemberGroupByChatId(chatId);
        if (null != chatMemberGroup) {
            robotTaskCheck.setUserName(chatMemberGroup.getUserName());
            robotTaskCheck.setFirstName(chatMemberGroup.getFirstName());
            robotTaskCheck.setLastName(chatMemberGroup.getLastName());
        }
        robotTaskCheck.setState(0);
        robotTaskCheckService.insertRobotTaskCheck(robotTaskCheck);
    }
}
