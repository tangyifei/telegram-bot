package com.code.penguin.services;

/**
 * 用户收集业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatCollectService {

    /**
     * 该用户是否已完成收集
     *
     * @param chatId 聊天唯一id
     * @param text   收集内容
     * @return 用户是否已完成收集
     */
    boolean whetherFinishCollectByChatId(String chatId, String text);

    /**
     * 增加收集
     *
     * @param chatId         聊天唯一id
     * @param collectContent 收集内容
     * @return 影响的行数
     */
    int insertCollect(String chatId, String collectContent);

}
