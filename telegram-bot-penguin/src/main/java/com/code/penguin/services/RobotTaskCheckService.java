package com.code.penguin.services;

import com.code.penguin.models.RobotTaskCheck;

/**
 * 机器人任务审核业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotTaskCheckService {

    /**
     * 添加用户已完成的任务审核记录
     *
     * @param robotTaskCheck 用户已完成的任务审核列表
     * @return 影响的行数
     */
    int insertRobotTaskCheck(RobotTaskCheck robotTaskCheck);

    /**
     * 用户是否被封禁
     *
     * @param chatId 聊天id
     * @return 用户是否被封禁
     */
    boolean whetherBanedByChatId(String chatId);

}
