package com.code.penguin.services.impl;

import com.code.penguin.daos.task.ChatDiscordMapper;
import com.code.penguin.models.ChatDiscord;
import com.code.penguin.services.ChatDiscordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Discord实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatDiscordServiceImpl implements ChatDiscordService {

    @Resource
    ChatDiscordMapper chatDiscordMapper;

    @Override
    public boolean whetherExistDiscordByChatIdAndDiscordAddress(String chatId, String discordAddress) {
        return null != chatDiscordMapper.whetherExistDiscordByChatIdAndDiscord(chatId, discordAddress);
    }

    @Override
    public String getDiscordByChatId(String chatId) {
        return chatDiscordMapper.getDiscordByChatId(chatId);
    }

    @Override
    public int insertDiscord(String chatId, String discordAddress) {
        if (null == chatDiscordMapper.whetherExistDiscordByChatIdAndDiscord(chatId, discordAddress)) {
            ChatDiscord chatDiscord = new ChatDiscord();
            chatDiscord.setChatId(chatId);
            chatDiscord.setDiscordAddress(discordAddress);
            chatDiscord.setDeleted(0);
            chatDiscord.setCreatedAt(new Date());
            chatDiscord.setUpdatedAt(new Date());
            chatDiscordMapper.insert(chatDiscord);
        }
        return 0;
    }
}
