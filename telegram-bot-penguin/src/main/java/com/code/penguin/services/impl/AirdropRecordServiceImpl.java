package com.code.penguin.services.impl;

import com.code.penguin.daos.task.AirdropRecordMapper;
import com.code.penguin.models.AirdropRecord;
import com.code.penguin.services.AirdropRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 已空投实现类
 *
 * @author xiaoyaowang
 */
@Service
public class AirdropRecordServiceImpl implements AirdropRecordService {

    @Resource
    AirdropRecordMapper airdropRecordMapper;

    @Override
    public boolean existsAirdropRecordByCondition(AirdropRecord airdropRecord) {
        return null != airdropRecordMapper.existsAirdropRecordByCondition(airdropRecord.getChatId());
    }

}
