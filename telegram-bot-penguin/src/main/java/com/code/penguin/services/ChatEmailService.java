package com.code.penguin.services;

/**
 * 用户邮箱业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatEmailService {

    /**
     * 该邮箱地址是否存在
     *
     * @param chatId 聊天唯一id
     * @param email  邮箱
     * @return 该邮箱地址是否存在
     */
    boolean whetherExistEmailByChatIdAndEmail(String chatId, String email);

    /**
     * 根据聊天唯一标识获取邮箱地址
     *
     * @param chatId 聊天唯一标识
     * @return 邮箱地址
     */
    String getEmailByChatId(String chatId);

    /**
     * 插入邮箱
     *
     * @param chatId 聊天唯一标识
     * @param email  邮箱
     * @return 影响的行数
     */
    int insertChatEmail(String chatId, String email);

}
