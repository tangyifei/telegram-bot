package com.code.penguin.services;

import com.code.penguin.models.AirdropRecord;

/**
 * 已空投业务接口
 *
 * @author xiaoyaowang
 */
public interface AirdropRecordService {

    /**
     * 判断用户是否参加过空投
     *
     * @param airdropRecord 已空投记录
     * @return 用户是否参加过空投
     */
    boolean existsAirdropRecordByCondition(AirdropRecord airdropRecord);

}
