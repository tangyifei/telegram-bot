package com.code.penguin.services;

/**
 * 用户discord业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatDiscordService {

    /**
     * 该discord地址是否存在
     *
     * @param chatId         聊天唯一id
     * @param discordAddress discord地址
     * @return 该discord地址是否存在
     */
    boolean whetherExistDiscordByChatIdAndDiscordAddress(String chatId, String discordAddress);

    /**
     * 根据聊天唯一标识获取discord地址
     *
     * @param chatId 聊天唯一标识
     * @return discord地址
     */
    String getDiscordByChatId(String chatId);

    /**
     * 插入discord
     *
     * @param chatId         聊天唯一标识
     * @param discordAddress discord地址
     * @return 影响的行数
     */
    int insertDiscord(String chatId, String discordAddress);

}
