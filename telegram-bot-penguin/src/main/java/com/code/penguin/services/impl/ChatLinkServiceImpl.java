package com.code.penguin.services.impl;

import com.code.penguin.daos.task.ChatLinkMapper;
import com.code.penguin.models.ChatLink;
import com.code.penguin.services.ChatLinkService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 分享链接实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatLinkServiceImpl implements ChatLinkService {

    @Resource
    ChatLinkMapper chatLinkMapper;

    @Override
    public String getChatLinkListByChatIdAndLinkCategory(String chatId, Integer linkCategory) {
        return chatLinkMapper.getChatLinkListByChatIdAndLinkCategory(chatId, linkCategory);
    }

    @Override
    public int insertChatLink(ChatLink chatLink) {
        ChatLink chatLinkInDbResult = chatLinkMapper.selectOne(chatLink);
        if (null == chatLinkInDbResult) {
            chatLink.setDeleted(0);
            chatLink.setCreatedAt(new Date());
            chatLink.setUpdatedAt(new Date());
            return chatLinkMapper.insert(chatLink);
        }
        return 0;
    }

    @Override
    public boolean whetherExistChatLinkByChatIdAndShareLink(String chatId, String shareLink) {
        return null != chatLinkMapper.existsChatLinkByChatIdAndShareLink(chatId, shareLink);
    }

}
