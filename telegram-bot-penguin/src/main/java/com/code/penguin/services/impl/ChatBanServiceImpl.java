package com.code.penguin.services.impl;

import com.code.penguin.daos.task.ChatBanMapper;
import com.code.penguin.services.ChatBanService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 封禁用户实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatBanServiceImpl implements ChatBanService {

    @Resource
    ChatBanMapper chatBanMapper;

    @Override
    public boolean whetherExistChatBanByChatId(String chatId) {
        return null != chatBanMapper.existsChatBanByChatId(chatId);
    }

}
