package com.code.penguin.services.impl;

import com.code.penguin.daos.task.ChatTwitterMapper;
import com.code.penguin.models.ChatLink;
import com.code.penguin.models.ChatTwitter;
import com.code.penguin.services.ChatLinkService;
import com.code.penguin.services.ChatTwitterService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 推特账户服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatTwitterServiceImpl implements ChatTwitterService {

    @Resource
    ChatTwitterMapper chatTwitterMapper;

    @Resource
    ChatLinkService chatLinkService;

    @Override
    public int insertChatTwitter(String chatId, String twitterAccountName) {
        ChatTwitter chatTwitter = new ChatTwitter();
        chatTwitter.setChatId(chatId);
        chatTwitter.setTwitterAccountName(twitterAccountName);
        chatTwitter.setUpdatedAt(new Date());
        Integer existsChatTwitter = chatTwitterMapper.existsChatTwitterByChatIdAndTwitterAccountName(chatId, null);
        if (null == existsChatTwitter) {
            chatTwitter.setDeleted(0);
            chatTwitter.setCreatedAt(new Date());
            return chatTwitterMapper.insert(chatTwitter);
        } else {
            chatTwitter.setId(chatTwitterMapper.getChatTwitterIdByChatIdAndTwitterAccountName(chatId, null));
            return chatTwitterMapper.updateByPrimaryKeySelective(chatTwitter);
        }
    }

    @Override
    public boolean whetherExistChatTwitterAccountNameByTwitterAccountName(String chatId, String twitterAccountName, String text) {
        boolean whetherExistChatLink = chatLinkService.whetherExistChatLinkByChatIdAndShareLink(chatId, text);
        if (!whetherExistChatLink) {
            ChatLink chatLink = new ChatLink();
            chatLink.setChatId(chatId);
            chatLink.setLinkCategory(2);
            chatLink.setShareLink(text);
            chatLinkService.insertChatLink(chatLink);
        }
        return null != chatTwitterMapper.getChatTwitterAccountNameCountByTwitterAccountName(twitterAccountName);
    }

}
