package com.code.penguin.services.impl;

import com.code.penguin.consts.CommonConsts;
import com.code.penguin.daos.task.ChatCollectContentMapper;
import com.code.penguin.daos.task.ChatCollectMapper;
import com.code.penguin.models.ChatCollect;
import com.code.penguin.models.ChatCollectContent;
import com.code.penguin.services.ChatCollectService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 用户收集实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatCollectServiceImpl implements ChatCollectService {

    @Resource
    ChatCollectMapper chatCollectMapper;

    @Resource
    ChatCollectContentMapper chatCollectContentMapper;

    @Override
    public boolean whetherFinishCollectByChatId(String chatId, String text) {
        return null != chatCollectMapper.whetherFinishCollectByChatId(chatId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public int insertCollect(String chatId, String collectContent) {
        Integer whetherFinishCollect = chatCollectMapper.whetherFinishCollectByChatId(chatId);
        int count = 0;
        if (null == whetherFinishCollect) {
            ChatCollect chatCollect = new ChatCollect();
            chatCollect.setChatId(chatId);
            chatCollect.setDeleted(0);
            chatCollect.setCreatedAt(new Date());
            chatCollect.setUpdatedAt(new Date());
            count += chatCollectMapper.insert(chatCollect);
            ChatCollectContent chatCollectContent = new ChatCollectContent();
            chatCollectContent.setChatCollectId(chatCollectMapper.getIdByChatId(chatId));
            chatCollectContent.setCollectContent(collectContent);
            chatCollectContent.setDeleted(0);
            chatCollectContent.setCreatedAt(new Date());
            chatCollectContent.setUpdatedAt(new Date());
            count += chatCollectContentMapper.insert(chatCollectContent);
        }
        return count;
    }
}
