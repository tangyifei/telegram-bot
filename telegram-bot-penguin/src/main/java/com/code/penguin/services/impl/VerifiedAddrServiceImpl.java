package com.code.penguin.services.impl;

import com.code.penguin.daos.task.VerifiedAddrMapper;
import com.code.penguin.models.VerifiedAddr;
import com.code.penguin.services.VerifiedAddrService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 校验成功的地址任务服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class VerifiedAddrServiceImpl implements VerifiedAddrService {

    @Resource
    VerifiedAddrMapper verifiedAddrMapper;

    @Override
    public boolean judgeCoinAddrVerifySuccess(String coinAddr) {
        return null != verifiedAddrMapper.getVerifySuccessCoinAddrByCoinAddr(coinAddr);
    }

    @Override
    public String getCoinAddrByChatId(String chatId) {
        return verifiedAddrMapper.getCoinAddrByChatId(chatId);
    }

    @Override
    public int insertVerifiedAddr(String chatId, String coinAddr) {
        Integer existCount = verifiedAddrMapper.getVerifySuccessCoinAddrByCoinAddr(coinAddr);
        if (null == existCount) {
            Integer chatExistCoinAddrCount = verifiedAddrMapper.getExistCoinAddrCountByChatId(chatId);
            if (null == chatExistCoinAddrCount) {
                VerifiedAddr verifiedAddr = new VerifiedAddr();
                verifiedAddr.setChatId(chatId);
                verifiedAddr.setCoinAddr(coinAddr);
                verifiedAddr.setDeleted(0);
                verifiedAddr.setCreatedAt(new Date());
                verifiedAddr.setUpdatedAt(new Date());
                return verifiedAddrMapper.insert(verifiedAddr);
            }
        }
        return 0;
    }

}
