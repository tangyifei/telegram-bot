package com.code.penguin.services;

/**
 * 用户封禁业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatBanService {

    /**
     * 判断用户是否被封禁
     *
     * @param chatId 聊天唯一id
     * @return 判断用户是否被封禁
     */
    boolean whetherExistChatBanByChatId(String chatId);

}
