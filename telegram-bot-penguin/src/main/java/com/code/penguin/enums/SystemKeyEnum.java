package com.code.penguin.enums;

/**
 * 奖励key的枚举
 *
 * @author xiaoyaowang
 */

public enum SystemKeyEnum {

    /**
     * 暂停机器人的key
     */
    STOP_ROBOT,

    /**
     * 用户封禁对应的key
     */
    BAN,

    /**
     * Penguin详情信息
     */
    PENGUIN_DETAIL_MESSAGE,

    /**
     * 暂停机器人的key对应的消息
     */
    STOP_ROBOT_MESSAGE;

}
