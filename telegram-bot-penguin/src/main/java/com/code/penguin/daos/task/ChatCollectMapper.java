package com.code.penguin.daos.task;

import com.code.penguin.daos.CrudMapper;
import com.code.penguin.models.ChatCollect;
import org.apache.ibatis.annotations.Param;

/**
 * 用户收集持久层
 *
 * @author xiaoyaowang
 */
public interface ChatCollectMapper extends CrudMapper<ChatCollect> {

    /**
     * 该用户是否已完成收集
     *
     * @param chatId 聊天唯一id
     * @return 用户是否已完成收集
     */
    Integer whetherFinishCollectByChatId(@Param("chatId") String chatId);

    /**
     * 通过聊天唯一标识获取收集记录的主键
     *
     * @param chatId 聊天唯一标识
     * @return 收集记录的主键
     */
    Integer getIdByChatId(@Param("chatId") String chatId);

}
