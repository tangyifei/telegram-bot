package com.code.penguin.daos.task;

import com.code.penguin.daos.CrudMapper;
import com.code.penguin.models.VerifiedAddr;
import org.apache.ibatis.annotations.Param;

/**
 * 已校验地址持久层
 *
 * @author xiaoyaowang
 */
public interface VerifiedAddrMapper extends CrudMapper<VerifiedAddr> {

    /**
     * 根据地址获取已校验成功记录
     *
     * @param coinAddr 钱包地址
     * @return 已校验成功记录
     */
    Integer getVerifySuccessCoinAddrByCoinAddr(@Param("coinAddr") String coinAddr);

    /**
     * 通过chatId获取用户已存在的地址数
     *
     * @param chatId 聊天id
     * @return 用户已存在的地址数
     */
    Integer getExistCoinAddrCountByChatId(@Param("chatId") String chatId);

    /**
     * 通过chatId获取校验的钱包地址
     *
     * @param chatId 聊天id
     * @return 钱包地址
     */
    String getCoinAddrByChatId(@Param("chatId") String chatId);

}
