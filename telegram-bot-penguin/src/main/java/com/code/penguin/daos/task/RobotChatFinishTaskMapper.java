package com.code.penguin.daos.task;

import com.code.penguin.daos.CrudMapper;
import com.code.penguin.models.RobotChatFinishTask;
import org.apache.ibatis.annotations.Param;

/**
 * 用户已完成的任务持久层
 *
 * @author xiaoyaowang
 */
public interface RobotChatFinishTaskMapper extends CrudMapper<RobotChatFinishTask> {

    /**
     * 根据聊天id获取完成的任务数
     *
     * @param chatId 聊天id
     * @return 完成的任务数
     */
    int getFinishTasksByChatId(@Param("chatId") String chatId);

}
