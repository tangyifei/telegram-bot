package com.code.penguin.daos.task;

import com.code.penguin.daos.CrudMapper;
import com.code.penguin.models.ChatLink;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户分享链接持久层
 *
 * @author xiaoyaowang
 */
public interface ChatLinkMapper extends CrudMapper<ChatLink> {

    /**
     * 根据用户链接地址判断是否存在用户的分享链接
     *
     * @param chatId    聊天唯一id
     * @param shareLink 分享链接
     * @return 是否存在用户的分享链接
     */
    Integer existsChatLinkByChatIdAndShareLink(@Param("chatId") String chatId, @Param("shareLink") String shareLink);

    /**
     * 根据用户链接地址和链接种类获取用户的分享链接
     *
     * @param chatId       聊天唯一id
     * @param linkCategory 分享链接种类
     * @return 用户的分享链接
     */
    String getChatLinkListByChatIdAndLinkCategory(@Param("chatId") String chatId, @Param("linkCategory") Integer linkCategory);

}
