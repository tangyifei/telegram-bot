package com.code.penguin.daos.system;

import com.code.penguin.daos.CrudMapper;
import com.code.penguin.models.RobotSystem;
import org.apache.ibatis.annotations.Param;

/**
 * 机器人系统设置持久层
 *
 * @author xiaoyaowang
 */
public interface RobotSystemMapper extends CrudMapper<RobotSystem> {

    /**
     * 通过系统key获取系统值
     *
     * @param systemKey 系统key
     * @return 获取的系统值
     */
    String getSystemValueBySystemKey(@Param("systemKey") String systemKey);

}
