package com.code.penguin.daos.task;

import com.code.penguin.daos.CrudMapper;
import com.code.penguin.models.ChatBan;
import org.apache.ibatis.annotations.Param;

/**
 * 封禁用户持久层
 *
 * @author xiaoyaowang
 */
public interface ChatBanMapper extends CrudMapper<ChatBan> {

    /**
     * 判断用户是否被封禁
     *
     * @param chatId 聊天唯一id
     * @return 判断用户是否被封禁
     */
    Integer existsChatBanByChatId(@Param("chatId") String chatId);

}
