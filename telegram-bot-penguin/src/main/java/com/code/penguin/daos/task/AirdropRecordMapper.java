package com.code.penguin.daos.task;

import com.code.penguin.daos.CrudMapper;
import com.code.penguin.models.AirdropRecord;
import org.apache.ibatis.annotations.Param;

/**
 * 已空投记录持久层
 *
 * @author xiaoyaowang
 */
public interface AirdropRecordMapper extends CrudMapper<AirdropRecord> {

    /**
     * 根据相关条件判断用户是否已参加过空投
     *
     * @param chatId 聊天唯一标识
     * @return 存在记录数
     */
    Integer existsAirdropRecordByCondition(@Param("chatId") String chatId);

}
