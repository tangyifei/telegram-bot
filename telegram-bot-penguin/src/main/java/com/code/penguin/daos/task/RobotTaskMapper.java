package com.code.penguin.daos.task;

import com.code.penguin.daos.CrudMapper;
import com.code.penguin.models.RobotChatFinishTask;
import com.code.penguin.models.RobotTask;
import org.apache.ibatis.annotations.Param;

/**
 * 任务持久层
 *
 * @author xiaoyaowang
 */
public interface RobotTaskMapper extends CrudMapper<RobotTask> {

    /**
     * 获取用户完成的任务的最大序号
     *
     * @param chatId 聊天id
     * @return 用户完成的任务的最大序号
     */
    Integer getFinishedTaskMaxSort(@Param("chatId") String chatId);

    /**
     * 获取总的任务数
     *
     * @return 总的任务数
     */
    int getTotalTasks();

    /**
     * 获取完成的任务数
     *
     * @param chatId 聊天id
     * @return 完成的任务数
     */
    int getTotalFinishTasks(@Param("chatId") String chatId);

    /**
     * 根据序号获取任务
     *
     * @param taskSort 任务序号
     * @return 用户完成的任务的最大序号
     */
    RobotTask getRobotTaskByTaskSort(@Param("taskSort") Integer taskSort);

    /**
     * 查询是否存在已完成的任务
     *
     * @param taskId 任务主键
     * @param chatId 聊天id
     * @return 是否存在已完成的任务
     */
    Integer existsRobotChatFinishTaskByChatIdAndTaskId(@Param("taskId") Integer taskId, @Param("chatId") String chatId);

    /**
     * 获取用户已完成的任务
     *
     * @param taskId 任务主键
     * @param chatId 聊天id
     * @return 用户已完成的任务
     */
    RobotChatFinishTask getRobotChatFinishTaskByChatIdAndTaskId(@Param("taskId") Integer taskId, @Param("chatId") String chatId);

}
