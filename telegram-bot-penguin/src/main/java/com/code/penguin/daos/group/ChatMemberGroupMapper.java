package com.code.penguin.daos.group;

import com.code.penguin.daos.CrudMapper;
import com.code.penguin.models.ChatMemberGroup;
import org.apache.ibatis.annotations.Param;

/**
 * 新人加入的群组持久层
 *
 * @author xiaoyaowang
 */
public interface ChatMemberGroupMapper extends CrudMapper<ChatMemberGroup> {

    /**
     * 获取用户加入的群组记录
     *
     * @param chatMemberGroup 新人加入的群组记录实体类
     * @return 用户加入的群组记录
     */
    ChatMemberGroup getChatMemberGroup(ChatMemberGroup chatMemberGroup);

    /**
     * 通过chatId获取用户的telegram名称
     *
     * @param chatId 聊天id
     * @return 新人加入的群组记录实体类
     */
    ChatMemberGroup getChatMemberGroupByChatId(@Param("chatId") String chatId);

}
