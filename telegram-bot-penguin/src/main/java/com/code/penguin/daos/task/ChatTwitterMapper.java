package com.code.penguin.daos.task;

import com.code.penguin.daos.CrudMapper;
import com.code.penguin.models.ChatTwitter;
import org.apache.ibatis.annotations.Param;

/**
 * 用户推特账户持久层
 *
 * @author xiaoyaowang
 */
public interface ChatTwitterMapper extends CrudMapper<ChatTwitter> {

    /**
     * 根据聊天id和账户名称是否存在用户的推特账户
     *
     * @param chatId             聊天id
     * @param twitterAccountName 账户名称
     * @return 是否存在用户的推特账户
     */
    Integer existsChatTwitterByChatIdAndTwitterAccountName(@Param("chatId") String chatId, @Param("twitterAccountName") String twitterAccountName);

    /**
     * 通过聊天id和推特账户名称获取推特账户
     *
     * @param chatId             聊天id
     * @param twitterAccountName 账户名称
     * @return 推特账户记录
     */
    int getChatTwitterIdByChatIdAndTwitterAccountName(@Param("chatId") String chatId, @Param("twitterAccountName") String twitterAccountName);

    /**
     * 通过聊天id获取推特的账户名
     *
     * @param chatId 聊天id
     * @return 推特的账户名
     */
    String getTwitterAccountNameByChatId(@Param("chatId") String chatId);

    /**
     * 通过chatId获取用户推特账号
     *
     * @param chatId 聊天id
     * @return 用户推特账号
     */
    Integer getChatTwitterByChatId(@Param("chatId") String chatId);

    /**
     * 通过推特账户名获取推特账户数
     *
     * @param twitterAccountName 推特账户名
     * @return 推特账户数
     */
    Integer getChatTwitterAccountNameCountByTwitterAccountName(@Param("twitterAccountName") String twitterAccountName);

}
