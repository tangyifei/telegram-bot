package com.code.penguin.daos.task;

import com.code.penguin.daos.CrudMapper;
import com.code.penguin.models.ChatDiscord;
import org.apache.ibatis.annotations.Param;

/**
 * Discord持久层
 *
 * @author xiaoyaowang
 */
public interface ChatDiscordMapper extends CrudMapper<ChatDiscord> {

    /**
     * 该discord地址是否存在
     *
     * @param chatId         聊天唯一id
     * @param discordAddress discord地址
     * @return 该discord地址是否存在
     */
    Integer whetherExistDiscordByChatIdAndDiscord(@Param("chatId") String chatId, @Param("discordAddress") String discordAddress);

    /**
     * 根据聊天唯一标识获取discord地址
     *
     * @param chatId 聊天唯一标识
     * @return discord地址
     */
    String getDiscordByChatId(@Param("chatId") String chatId);

}
