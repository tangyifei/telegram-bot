package com.code.penguin.daos.task;

import com.code.penguin.daos.CrudMapper;
import com.code.penguin.models.ChatCollectContent;

/**
 * 用户收集内容持久层
 *
 * @author xiaoyaowang
 */
public interface ChatCollectContentMapper extends CrudMapper<ChatCollectContent> {

}
