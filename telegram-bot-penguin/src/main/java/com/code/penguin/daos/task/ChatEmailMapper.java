package com.code.penguin.daos.task;

import com.code.penguin.daos.CrudMapper;
import com.code.penguin.models.ChatEmail;
import org.apache.ibatis.annotations.Param;

/**
 * 邮箱持久层
 *
 * @author xiaoyaowang
 */
public interface ChatEmailMapper extends CrudMapper<ChatEmail> {

    /**
     * 该邮箱地址是否存在
     *
     * @param chatId 聊天唯一id
     * @param email  邮箱
     * @return 该邮箱地址是否存在
     */
    Integer whetherExistEmailByChatIdAndEmail(@Param("chatId") String chatId, @Param("email") String email);

    /**
     * 根据聊天唯一标识获取邮箱地址
     *
     * @param chatId 聊天唯一标识
     * @return 邮箱地址
     */
    String getEmailByChatId(@Param("chatId") String chatId);

}
