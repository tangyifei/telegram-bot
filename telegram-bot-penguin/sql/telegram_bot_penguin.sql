/*
 Navicat Premium Data Transfer

 Source Server         : 电报机器人测试库
 Source Server Type    : MySQL
 Source Server Version : 50651
 Source Host           : 154.222.22.76:3306
 Source Schema         : telegram_bot_penguin

 Target Server Type    : MySQL
 Target Server Version : 50651
 File Encoding         : 65001

 Date: 06/12/2021 15:17:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for airdrop_record
-- ----------------------------
DROP TABLE IF EXISTS `airdrop_record`;
CREATE TABLE `airdrop_record`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '聊天唯一标识',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_chat_id_deleted`(`chat_id`, `deleted`) USING BTREE COMMENT '聊天唯一标识和删除状态唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '已空投记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for chat_ban
-- ----------------------------
DROP TABLE IF EXISTS `chat_ban`;
CREATE TABLE `chat_ban`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '聊天唯一id',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_chat_id_deleted`(`chat_id`, `deleted`) USING BTREE COMMENT '聊天唯一标识和删除状态的唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for chat_collect
-- ----------------------------
DROP TABLE IF EXISTS `chat_collect`;
CREATE TABLE `chat_collect`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '聊天唯一标识',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_chat_id`(`chat_id`) USING BTREE COMMENT '聊天唯一标识唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for chat_collect_content
-- ----------------------------
DROP TABLE IF EXISTS `chat_collect_content`;
CREATE TABLE `chat_collect_content`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_collect_id` int(10) UNSIGNED NOT NULL COMMENT '用户收集记录主键',
  `collect_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '收集内容',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_chat_collet_id`(`chat_collect_id`) USING BTREE COMMENT '收集外键唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for chat_discord
-- ----------------------------
DROP TABLE IF EXISTS `chat_discord`;
CREATE TABLE `chat_discord`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '聊天唯一标识',
  `discord_address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'discord 地址',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for chat_email
-- ----------------------------
DROP TABLE IF EXISTS `chat_email`;
CREATE TABLE `chat_email`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '聊天唯一标识',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_email`(`email`) USING BTREE COMMENT '邮箱唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for chat_link
-- ----------------------------
DROP TABLE IF EXISTS `chat_link`;
CREATE TABLE `chat_link`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '聊天唯一id',
  `share_link` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分享连接',
  `link_category` tinyint(1) UNSIGNED NOT NULL COMMENT '1-群分享链接 2-推特链接',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_chat_id_share_link_deleted`(`chat_id`, `share_link`, `deleted`) USING BTREE COMMENT '聊天id和链接、删除状态联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 215 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '聊天链接' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for chat_member_group
-- ----------------------------
DROP TABLE IF EXISTS `chat_member_group`;
CREATE TABLE `chat_member_group`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '聊天唯一标识',
  `group_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '相关的群的url',
  `group_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '群组的名字',
  `first_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '新人的first_name',
  `last_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '新人的last_name',
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `is_bot` tinyint(1) UNSIGNED NOT NULL COMMENT '是否是bot（0-不是 1-是）',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_chat_id_group_url_deleted`(`chat_id`, `group_url`, `deleted`) USING BTREE COMMENT '聊天id、群组链接和删除状态联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 8093 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '加群记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for chat_twitter
-- ----------------------------
DROP TABLE IF EXISTS `chat_twitter`;
CREATE TABLE `chat_twitter`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '聊天id',
  `twitter_account_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '推特账号',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '删除状态 0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_account_name`(`twitter_account_name`) USING BTREE COMMENT '推特账户名唯一索引',
  UNIQUE INDEX `uk_chat_id`(`chat_id`) USING BTREE COMMENT '聊天唯一标识唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 756 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '关注推特记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_chat
-- ----------------------------
DROP TABLE IF EXISTS `robot_chat`;
CREATE TABLE `robot_chat`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '聊天唯一标识',
  `robot_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机器人用户名',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 266 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '机器人聊天' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_chat_finish_task
-- ----------------------------
DROP TABLE IF EXISTS `robot_chat_finish_task`;
CREATE TABLE `robot_chat_finish_task`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '聊天id',
  `task_id` int(10) UNSIGNED NOT NULL COMMENT '任务主键',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '删除状态 0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_chat_id_task_id_deleted`(`chat_id`, `task_id`, `deleted`) USING BTREE COMMENT '聊天id和任务id删除状态的联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 1151 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '完成任务记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_system
-- ----------------------------
DROP TABLE IF EXISTS `robot_system`;
CREATE TABLE `robot_system`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `system_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '系统key',
  `system_value` varchar(1500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统值',
  `remark` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统字典' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_task
-- ----------------------------
DROP TABLE IF EXISTS `robot_task`;
CREATE TABLE `robot_task`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `task_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务标题',
  `task_description` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务描述',
  `task_sort` tinyint(2) UNSIGNED NOT NULL COMMENT '任务序号',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '机器人具体任务' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for robot_task_check
-- ----------------------------
DROP TABLE IF EXISTS `robot_task_check`;
CREATE TABLE `robot_task_check`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '聊天id',
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `first_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名',
  `last_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓',
  `state` tinyint(1) UNSIGNED NOT NULL COMMENT '审核状态 0-未审核（待审核） 1-审核失败 2-审核成功 3-已封禁',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '0-未删除 1-已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_chat_id_deleted`(`chat_id`, `deleted`) USING BTREE COMMENT '聊天id和删除状态联合唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 258 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '审核记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for verified_addr
-- ----------------------------
DROP TABLE IF EXISTS `verified_addr`;
CREATE TABLE `verified_addr`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '聊天id',
  `coin_addr` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '钱包地址',
  `deleted` tinyint(1) UNSIGNED NOT NULL COMMENT '0-未删除 1已删除',
  `created_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_chat_id_deleted`(`chat_id`, `deleted`) USING BTREE COMMENT '聊天id和删除状态联合唯一索引',
  UNIQUE INDEX `uk_coin_address`(`coin_addr`) USING BTREE COMMENT '钱包地址唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 970 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '地址校验' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
