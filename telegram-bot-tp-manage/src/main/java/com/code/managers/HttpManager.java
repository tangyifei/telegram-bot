package com.code.managers;

import com.alibaba.fastjson.JSON;
import com.code.models.checktoken.TokenSwapManageResponse;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

/**
 * http调用的相关管理类
 *
 * @author xiaoyaowang
 */
@Component
@Slf4j
public class HttpManager {

    @Value("${tokenSwap.manageUrlCheckToken}")
    private String tokenSwapManageCheckTokenUrl;

    @Value("${wallet.url}")
    private String walletUrl;

    @Value("${task.token}")
    private String taskToken;

    @Value("${telegram.url}")
    private String telegramUrl;

    @Autowired
    private RestTemplate restTemplate;

    private static final String ACCEPT = "Accept";

    /**
     * 验证token
     *
     * @param token 登录token
     * @return 验证与否
     */
    public Boolean verifyToken(String token) {
        //设置请求参数
        Map<String, Object> postData = Maps.newHashMapWithExpectedSize(1 << 4);
        postData.put("token", token);
        String requestBody = JSON.toJSONString(postData);
        log.info("请求体数据：【{}】", requestBody);
        HttpEntity<String> formEntity = new HttpEntity<>(requestBody, getJsonHeader());
        HttpMethod method = HttpMethod.POST;
        ResponseEntity<TokenSwapManageResponse> response = restTemplate.exchange(tokenSwapManageCheckTokenUrl, method, formEntity, TokenSwapManageResponse.class);
        log.info("验证token结果:{}", JSON.toJSONString(response));
        TokenSwapManageResponse body = response.getBody();
        if (null != body) {
            String rtnCode = body.getCode();
            if ("0".equals(rtnCode)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

//    /**
//     * 验证用户是否有权限
//     *
//     * @param token 登录token
//     * @return 验证与否
//     */
//    public Boolean verifyAuthorityByToken(String token) {
//        //设置请求参数
//        Map<String, Object> postData = Maps.newHashMapWithExpectedSize(1 << 4);
//        postData.put("token", token);
//        String requestBody = JSON.toJSONString(postData);
//        log.info("请求体数据：【{}】", requestBody);
//        HttpEntity<String> formEntity = new HttpEntity<>(requestBody, getJsonHeader());
//        HttpMethod method = HttpMethod.POST;
//        ResponseEntity<TokenSwapManageResponse> response = restTemplate.exchange(tokenSwapManageCheckTokenUrl, method, formEntity, TokenSwapManageResponse.class);
//        log.info("验证用户是否有权限的结果:{}", JSON.toJSONString(response));
//        TokenSwapManageResponse body = response.getBody();
//        if (null != body) {
//            String rtnCode = body.getCode();
//            if ("0".equals(rtnCode)) {
//                // TODO 待定
//                return true;
//            } else {
//                return false;
//            }
//        } else {
//            return false;
//        }
//    }

    /**
     * 发送POST请求任务
     *
     * @param reqMethod 请求方法
     * @param params    请求参数
     * @return 结果
     */
    public String sendRpcReq(String reqMethod, List<Object> params) {
        //设置请求参数
        Map<String, Object> requestJson = Maps.newHashMapWithExpectedSize(1 << 4);
        requestJson.put("jsonrpc", "2.0");
        requestJson.put("id", 1);
        requestJson.put("method", reqMethod);
        requestJson.put("params", params);
        String requestBody = JSON.toJSONString(requestJson);
        log.info("请求体数据：【{}】", requestBody);
        HttpEntity<String> formEntity = new HttpEntity<>(requestBody, getJsonHeader());
        HttpMethod method = HttpMethod.POST;
        ResponseEntity<String> response = restTemplate.exchange(walletUrl, method, formEntity, String.class);
        String body = response.getBody();
        log.info("返回的响应数据：{}", body);
        return body;
    }

    /**
     * 发送重新发送截图任务的消息
     *
     * @param chatId 群组唯一标识
     * @param text   发送的内容
     */
    @Async
    public void sendReSendPhoto(Long chatId, String text) {
        log.info("发送的内容：{}", text);
        String uri = telegramUrl + "/bot" + taskToken + "/sendMessage?chat_id=" + chatId + "&text=" + text + "&parse_mode=HTML";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        String strBody = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class).getBody();
        log.info("发送的消息的响应:{}", strBody);
    }

    /**
     * 构造http请求的头部信息
     *
     * @return http头部信息
     */
    private HttpHeaders getJsonHeader() {
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        headers.add(ACCEPT, MediaType.APPLICATION_JSON.toString());
        return headers;
    }

}
