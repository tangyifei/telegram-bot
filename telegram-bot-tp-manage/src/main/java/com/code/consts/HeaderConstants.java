package com.code.consts;

/**
 * Header的key罗列
 *
 * @author xiaoyaowang
 */
public interface HeaderConstants {

    /**
     * 用户的登录token
     */
    String X_TOKEN = "token";

    /**
     * 语言
     */
    String LANGUAGE = "App-Language";

    /**
     * 签名
     */
    String SIGN = "sign";

    /**
     * 账号名
     */
    String ACCOUNT_NAME = "AccountName";

    /**
     * nonce
     */
    String NONCE = "nonce";

    /**
     * 时间戳
     */
    String TIMESTAMP = "timestamp";

    /**
     * 调用来源
     */
    String CALL_SOURCE = "Call-Source";

}
