package com.code.schedules;

import com.code.services.RobotTaskCheckService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 通知以前已经审核的用户加入Bz群
 *
 * @author xiaoyaowang
 */
@Component
public class SynNoticeCheckedUserJoinBzGroupSchedule {

    @Resource
    private RobotTaskCheckService robotTaskCheckService;

    /**
     * 每间隔60分钟通知以前已经审核的用户加入Bz群
     */
//    @Scheduled(cron = "0 0/60 * * * ?")
    @Scheduled(cron = "0 0 */10 * * ?")
    @Async
    public void synNoticeCheckedUserJoinBzGroup() {
        robotTaskCheckService.synNoticeCheckedUserJoinBzGroup();
    }
}
