package com.code.services;

import com.code.models.qo.AgentRankQO;
import com.code.models.qo.InviterQO;
import com.code.models.qo.RobotTaskCheckQO;
import com.code.models.robot.RobotTaskCheck;
import com.code.models.vo.*;
import com.github.pagehelper.PageInfo;

/**
 * 机器人任务审核业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotTaskCheckService {

    /**
     * 更新任务的审核状态
     *
     * @param robotTaskCheck 机器人任务审核记录实体类
     * @return 影响的行数
     */
    RobotTaskCheck updateRobotTaskState(RobotTaskCheck robotTaskCheck);

    /**
     * 分页获取审核的任务列表
     *
     * @param robotTaskCheckQO 机器人任务审核记录查询实体类QO
     * @return 分页过后的审核任务列表
     */
    PageInfo<RobotTaskCheck> getRobotTaskCheckListByPage(RobotTaskCheckQO robotTaskCheckQO);

    /**
     * 通过主键获取单个审核任务
     *
     * @param id 主键
     * @return 单个审核任务
     */
    RobotTaskCheck getRobotTaskCheckById(int id);

    /**
     * 通过聊天唯一标识获取审核记录
     *
     * @param chatId 聊天唯一标识
     * @return 审核记录
     */
    RobotTaskCheck getRobotTaskCheckByChatId(String chatId);

    /**
     * 获取奖金池的余额
     *
     * @return 奖金池的余额
     */
    RobotIndexVO getBalance();

    /**
     * 推送消息
     *
     * @param robotSendMessageVO 机器人推送消息视图对象
     */
    void pushMessage(RobotSendMessageVO robotSendMessageVO);

    /**
     * 通知以前已经审核的用户加入Bz群
     */
    void synNoticeCheckedUserJoinBzGroup();

    /**
     * 自动完成审核
     */
    void autoFinishCheck();

    /**
     * 根据聊天唯一标识获取推广链接
     *
     * @param chatId 聊天唯一标识
     * @return 推广链接
     */
    String getSponsoredLinks(String chatId);

    /**
     * 设置或者取消代理人
     *
     * @param agentVO 代理人视图对象
     * @return 影响的行数
     */
    int setOrCancelAgent(AgentVO agentVO);

    /**
     * 分页获取代理人排名列表
     *
     * @param agentRankQO 代理人排名查询对象
     * @return 代理人排名列表分页对象
     */
    PageInfo<AgentRankVO> getAgentRankListByPage(AgentRankQO agentRankQO);

    /**
     * 获取邀请成员列表
     *
     * @param inviterQO 邀请人员查询实体类
     * @return 邀请成员列表分页对象
     */
    PageInfo<InviterVO> getInviterListByPage(InviterQO inviterQO);

}
