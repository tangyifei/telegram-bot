package com.code.services;

import com.code.models.po.OperateLog;
import com.code.models.qo.OperateLogQO;
import com.github.pagehelper.PageInfo;

/**
 * 操作日志业务接口
 *
 * @author xiaoyaowang
 */
public interface OperateLogService {

    /**
     * 获取操作日志列表
     *
     * @param operateLogQO 操作日志查询实体类
     * @return 操作日志列表
     */
    PageInfo<OperateLog> getOperateLogListByCondition(OperateLogQO operateLogQO);

    /**
     * 插入操作日志
     *
     * @param operateLog 操作日志对象
     * @return 影响的行数
     */
    int insertOperateLog(OperateLog operateLog);

}
