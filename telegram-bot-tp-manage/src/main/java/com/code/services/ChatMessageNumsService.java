package com.code.services;

import com.code.models.robot.ChatMessageNums;
import com.github.pagehelper.PageInfo;

/**
 * 统计用户发送的消息业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatMessageNumsService {

    /**
     * 根据相关条件获取用户发送的消息数列表
     *
     * @param telegramAccountName 电报账号
     * @param page                当前页
     * @param pageSize            每页的大小
     * @return 用户发送的消息数列表
     */
    PageInfo<ChatMessageNums> getChatMessageNumsListByCondition(String telegramAccountName, String currentDay, Integer page, Integer pageSize);

    /**
     * 获取总的群发消息数
     *
     * @return 总的群发消息数
     */
    int getTotalMessageNums();

    /**
     * 清空历史群发消息记录
     */
    void clearMessageNums();

    /**
     * 增加用户在群里发送的历史消息数
     *
     * @param chatMessageNums 用户发送消息的统计
     * @return 影响的行数
     */
    int incrementChatMessageNumsHistory(ChatMessageNums chatMessageNums);

}
