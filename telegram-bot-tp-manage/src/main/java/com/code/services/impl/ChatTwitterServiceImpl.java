package com.code.services.impl;

import com.code.consts.CommonConsts;
import com.code.daos.robot.ChatTwitterMapper;
import com.code.daos.robot.FocusOnTwitterFailureMapper;
import com.code.daos.robot.RobotChatFinishTaskMapper;
import com.code.daos.robot.RobotTaskCheckMapper;
import com.code.enums.ResultCode;
import com.code.enums.RewardKeyEnum;
import com.code.exceptions.BusinessException;
import com.code.models.qo.FocusOnTwitterFailureQO;
import com.code.models.robot.ChatTwitter;
import com.code.models.robot.FocusOnTwitterFailure;
import com.code.models.robot.RobotChatFinishTask;
import com.code.models.robot.RobotTaskCheck;
import com.code.services.ChatTwitterService;
import com.code.services.RobotSystemService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 关注推特记录服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatTwitterServiceImpl implements ChatTwitterService {

    @Resource
    RobotTaskCheckMapper robotTaskCheckMapper;

    @Resource
    RobotChatFinishTaskMapper robotChatFinishTaskMapper;

    @Resource
    ChatTwitterMapper chatTwitterMapper;

    @Resource
    FocusOnTwitterFailureMapper focusOnTwitterFailureMapper;

    @Resource
    RobotSystemService robotSystemService;

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public void finishFocusOnTwitter(String walletAddr) {
        String value = robotSystemService.getSystemValueBySystemKey(RewardKeyEnum.MANUAL_FINISH_FOURTH_TASK.name());
        if ("1".equals(value)) {
            throw new BusinessException(ResultCode.MANUAL_FINISH_FOURTH_TASK_OFF_ERROR);
        }
        int finishTaskNums = robotTaskCheckMapper.getFinishTaskNumsByInviterWalletAddress(walletAddr);
        if (finishTaskNums >= 3) {
            // 第一步：插入关注推特的完成任务和推特关注记录
            String chatId = robotChatFinishTaskMapper.getChatIdByWalletAddr(walletAddr);
            Integer taskId = robotChatFinishTaskMapper.getTaskIdBySort(4);
            Integer existCount = robotChatFinishTaskMapper.existsRobotChatFinishTaskByChatIdAndTaskId(taskId, chatId);
            if (null == existCount) {
                RobotChatFinishTask robotChatFinishTask = new RobotChatFinishTask();
                robotChatFinishTask.setTaskId(taskId);
                robotChatFinishTask.setChatId(chatId);
                robotChatFinishTask.setDeleted(0);
                robotChatFinishTask.setCreatedAt(new Date());
                robotChatFinishTask.setUpdatedAt(new Date());
                robotChatFinishTaskMapper.insert(robotChatFinishTask);
            }
            String twitterAccountName = robotChatFinishTaskMapper.getTwitterAccountNameByWalletAddr(chatId, walletAddr);
            if (!StringUtils.isBlank(twitterAccountName)) {
                Integer existsChatTwitter = robotChatFinishTaskMapper.existsChatTwitterByChatIdAndTwitterAccountName(chatId, twitterAccountName);
                ChatTwitter chatTwitter = new ChatTwitter();
                chatTwitter.setChatId(chatId);
                chatTwitter.setTwitterAccountName(twitterAccountName);
                chatTwitter.setUpdatedAt(new Date());
                if (null == existsChatTwitter) {
                    chatTwitter.setDeleted(0);
                    chatTwitter.setCreatedAt(new Date());
                    chatTwitterMapper.insert(chatTwitter);
                } else {
                    chatTwitter.setId(robotChatFinishTaskMapper.getChatTwitterIdByChatIdAndTwitterAccountName(chatId, twitterAccountName));
                    chatTwitterMapper.updateByPrimaryKeySelective(chatTwitter);
                }
                // 第二步：插入审核记录
                int totalTaskNums = robotTaskCheckMapper.getTaskNums();
                finishTaskNums = robotTaskCheckMapper.getFinishTaskNumsByInviterWalletAddress(walletAddr);
                if (finishTaskNums == totalTaskNums) {
                    RobotTaskCheck robotTaskCheckQuery = new RobotTaskCheck();
                    robotTaskCheckQuery.setChatId(chatId);
                    robotTaskCheckQuery.setWalletAddr(walletAddr);
                    robotTaskCheckQuery.setDeleted(0);
                    RobotTaskCheck robotTaskCheckForDb = robotTaskCheckMapper.selectOne(robotTaskCheckQuery);
                    if (null == robotTaskCheckForDb) {
                        RobotTaskCheck robotTaskCheck = new RobotTaskCheck();
                        robotTaskCheck.setDeleted(0);
                        robotTaskCheck.setCreatedAt(new Date());
                        robotTaskCheck.setUpdatedAt(new Date());
                        robotTaskCheck.setState(0);
                        robotTaskCheck.setChatId(chatId);
                        robotTaskCheck.setUserName(robotChatFinishTaskMapper.getUserNameByChatId(chatId));
                        robotTaskCheck.setFirstName(robotChatFinishTaskMapper.getFirstNameByChatId(chatId));
                        robotTaskCheck.setLastName(robotChatFinishTaskMapper.getLastNameByChatId(chatId));
                        robotTaskCheck.setWalletAddr(walletAddr);
                        robotTaskCheckMapper.insert(robotTaskCheck);
                    }

                }
            }
            // 第三步：删除关注推特失败的记录
            FocusOnTwitterFailure focusOnTwitterFailureQuery = new FocusOnTwitterFailure();
            focusOnTwitterFailureQuery.setChatId(chatId);
            focusOnTwitterFailureQuery.setDeleted(0);
            FocusOnTwitterFailure whetherExistFocusOnTwitterFailure = focusOnTwitterFailureMapper.selectOne(focusOnTwitterFailureQuery);
            if (null != whetherExistFocusOnTwitterFailure) {
                whetherExistFocusOnTwitterFailure.setUpdatedAt(new Date());
                whetherExistFocusOnTwitterFailure.setDeleted(1);
                focusOnTwitterFailureMapper.updateByPrimaryKey(whetherExistFocusOnTwitterFailure);
            }


        }
    }

    @Override
    public PageInfo<FocusOnTwitterFailure> getFocusOnTwitterFailureListByPage(FocusOnTwitterFailureQO focusOnTwitterFailureQO) {
        Integer pageNum = focusOnTwitterFailureQO.getPageNum();
        Integer pageSize = focusOnTwitterFailureQO.getPageSize();
        if (null == pageNum) {
            pageNum = 0;
        }
        if (null == pageSize) {
            pageSize = 100;
        }
        PageHelper.startPage(pageNum, pageSize);
        List<FocusOnTwitterFailure> list = chatTwitterMapper.getFocusOnTwitterFailureListByPage(focusOnTwitterFailureQO);
        return PageInfo.of(list);
    }

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public void deleteChatTwitterByChatId(String chatId) {
        if (null != chatTwitterMapper.existsChatTwitterByChatId(chatId)) {
            chatTwitterMapper.deleteChatTwitterByChatId(chatId);
        }
        if (null != chatTwitterMapper.existsFocusOnTwitterFailureByChatId(chatId)) {
            chatTwitterMapper.deleteFocusOnTwitterFailureByChatId(chatId);
        }
    }

}
