package com.code.services.impl;

import com.code.daos.robot.RobotRewardFlowMapper;
import com.code.models.qo.RobotRewardFlowQO;
import com.code.models.robot.RobotRewardFlow;
import com.code.services.RobotRewardFlowService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * 机器人奖励服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class RobotRewardFlowServiceImpl implements RobotRewardFlowService {

    @Value("${wallet.transfer.precision}")
    private Integer precision;

    @Resource
    RobotRewardFlowMapper robotRewardFlowMapper;

    @Override
    public int insertRobotRewardFlow(RobotRewardFlow robotRewardFlow) {
        if (null != robotRewardFlow.getRewardFlowQuery()) {
            String walletAddress = robotRewardFlow.getWalletAddress();
            String amount = robotRewardFlow.getAmount();
            Integer exitsRobotRewardFlow = robotRewardFlowMapper.existRobotRewardFlowByWalletAddressAndAmount(walletAddress, amount);
            if (null == exitsRobotRewardFlow) {
                robotRewardFlow.setDeleted(0);
                robotRewardFlow.setCreatedAt(new Date());
                robotRewardFlow.setUpdatedAt(new Date());
                return robotRewardFlowMapper.insert(robotRewardFlow);
            }
        } else {
            robotRewardFlow.setDeleted(0);
            robotRewardFlow.setCreatedAt(new Date());
            robotRewardFlow.setUpdatedAt(new Date());
            return robotRewardFlowMapper.insert(robotRewardFlow);
        }
        return 0;
    }

    @Override
    public Boolean existRobotRewardFlowByWalletAddressAndAmount(String walletAddress, String amount) {
        return null != robotRewardFlowMapper.existRobotRewardFlowByWalletAddressAndAmount(walletAddress, amount);
    }

    @Override
    public Map<String, Object> getRobotRewardFlowListByPage(RobotRewardFlowQO robotRewardFlowQO) {
        Integer pageNum = robotRewardFlowQO.getPageNum();
        Integer pageSize = robotRewardFlowQO.getPageSize();
        if (null == pageNum) {
            pageNum = 0;
        }
        if (null == pageSize) {
            pageSize = 100;
        }
        PageHelper.startPage(pageNum, pageSize);
        List<RobotRewardFlow> list = robotRewardFlowMapper.getRobotRewardFlowList(robotRewardFlowQO);
        PageInfo<RobotRewardFlow> pageInfo = PageInfo.of(list);
        // 获取总的发放奖励
        List<String> rewardList = robotRewardFlowMapper.getRewardList();
        BigDecimal totalRewardBd = BigDecimal.ZERO;
        if (!CollectionUtils.isEmpty(rewardList)) {
            Optional<BigDecimal> rewardOptional = rewardList.stream().map(BigDecimal::new).reduce(BigDecimal::add);
            if (rewardOptional.isPresent()) {
                totalRewardBd = rewardOptional.get();
            }
        }
        List<String> airdropRewardList = robotRewardFlowMapper.getAirdropRewardList();
        BigDecimal totalAirdropRewardBd = BigDecimal.ZERO;
        if (!CollectionUtils.isEmpty(airdropRewardList)) {
            Optional<BigDecimal> airdropRewardOptional = airdropRewardList.stream().map(BigDecimal::new).reduce(BigDecimal::add);
            if (airdropRewardOptional.isPresent()) {
                totalAirdropRewardBd = airdropRewardOptional.get();
            }
        }
        String totalReward = totalRewardBd.add(totalAirdropRewardBd).setScale(precision, BigDecimal.ROUND_HALF_UP).stripTrailingZeros().toPlainString();
        Map<String, Object> resultMap = new HashMap<>(1 << 4);
        resultMap.put("size", pageInfo.getSize());
        resultMap.put("pageSize", pageInfo.getPageSize());
        resultMap.put("pageNum", pageInfo.getPageNum());
        resultMap.put("pages", pageInfo.getPages());
        resultMap.put("total", pageInfo.getTotal());
        resultMap.put("list", pageInfo.getList());
        resultMap.put("totalReward", totalReward);
        return resultMap;
    }

    @Override
    public boolean whetherJoinBzGroup(String walletAddress) {
        return null != robotRewardFlowMapper.whetherJoinBzGroup(walletAddress);
    }

}
