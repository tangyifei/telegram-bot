package com.code.services.impl;

import com.code.daos.robot.ChatMessageNumsHistoryMapper;
import com.code.daos.robot.ChatMessageNumsMapper;
import com.code.models.po.ChatMessageNumsHistory;
import com.code.models.robot.ChatMessageNums;
import com.code.services.ChatMessageNumsService;
import com.code.task.NoteChatMassageNumsHistoryTask;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

/**
 * 禁止完成群分享任务的群地址服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class ChatMessageNumsServiceImpl implements ChatMessageNumsService {

    @Resource
    ChatMessageNumsMapper chatMessageNumsMapper;

    @Resource
    @Lazy
    NoteChatMassageNumsHistoryTask noteChatMassageNumsHistoryTask;

    @Resource
    ChatMessageNumsHistoryMapper chatMessageNumsHistoryMapper;

    @Override
    public PageInfo<ChatMessageNums> getChatMessageNumsListByCondition(String telegramAccountName, String currentDay, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        List<ChatMessageNums> list;
        if (StringUtils.isBlank(currentDay)) {
            currentDay = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDate.now());
        }
        if (StringUtils.isNotBlank(telegramAccountName)) {
            list = chatMessageNumsMapper.getChatMessageNumsListByCondition(telegramAccountName, currentDay);
        } else {
            list = chatMessageNumsMapper.getChatMessageNumsList(currentDay);
        }
        if (!CollectionUtils.isEmpty(list)) {
            list.forEach(item -> {
                if (StringUtils.isBlank(item.getTelegramAccountName())) {
                    String userName = null;
                    String firstName = item.getFirstName();
                    String lastName = item.getLastName();
                    if (StringUtils.isNotBlank(firstName)) {
                        userName = firstName;
                        if (StringUtils.isNotBlank(lastName)) {
                            userName = firstName + lastName;
                        }
                    } else {
                        if (StringUtils.isNotBlank(lastName)) {
                            userName = lastName;
                        }
                    }
                    if (StringUtils.isBlank(userName)) {
                        ChatMessageNums chatMessageNums = chatMessageNumsMapper.getChatMemberGroupByChatId(item.getChatId());
                        if (null != chatMessageNums) {
                            firstName = chatMessageNums.getFirstName();
                            lastName = chatMessageNums.getLastName();
                            if (StringUtils.isNotBlank(firstName)) {
                                userName = firstName;
                                if (StringUtils.isNotBlank(lastName)) {
                                    userName = firstName + lastName;
                                }
                            } else {
                                if (StringUtils.isNotBlank(lastName)) {
                                    userName = lastName;
                                }
                            }
                        }

                    }
                    item.setTelegramAccountName(userName);
                }
            });

        }
        return PageInfo.of(list);
    }

    @Override
    public int getTotalMessageNums() {
        Integer totalMessageNums = chatMessageNumsMapper.getTotalMessageNums();
        if (null == totalMessageNums) {
            return 0;
        }
        return totalMessageNums;
    }

    @Override
    public void clearMessageNums() {
        List<ChatMessageNums> chatMessageNumsList = chatMessageNumsMapper.getAllChatMessageNumsList();
        if (!CollectionUtils.isEmpty(chatMessageNumsList)) {
            noteChatMassageNumsHistoryTask.noteChatMassageNumsHistory(chatMessageNumsList);
        }
        chatMessageNumsMapper.clearMessageNums();
    }

    @Override
    public int incrementChatMessageNumsHistory(ChatMessageNums chatMessageNums) {
        String chatId = chatMessageNums.getChatId();
        Integer messageNumsHistory = chatMessageNums.getMessageNums();
        ChatMessageNumsHistory chatMessageNumHistoryDb = chatMessageNumsHistoryMapper.getChatMessageNumsHistoryByChatId(chatId);
        if (null == chatMessageNumHistoryDb) {
            ChatMessageNumsHistory chatMessageNumHistory = new ChatMessageNumsHistory();
            chatMessageNumHistory.setChatId(chatId);
            chatMessageNumHistory.setMessageNumsHistory(messageNumsHistory);
            chatMessageNumHistory.setFirstName(chatMessageNums.getFirstName());
            chatMessageNumHistory.setLastName(chatMessageNums.getLastName());
            chatMessageNumHistory.setUserName(chatMessageNums.getTelegramAccountName());
            chatMessageNumHistory.setDeleted(0);
            chatMessageNumHistory.setVersion(0);
            chatMessageNumHistory.setCreatedAt(new Date());
            chatMessageNumHistory.setUpdatedAt(new Date());
            return chatMessageNumsHistoryMapper.insert(chatMessageNumHistory);
        } else {
            return chatMessageNumsHistoryMapper.incrementChatMessageNumsHistory(chatMessageNumHistoryDb.getId(), messageNumsHistory, chatMessageNumHistoryDb.getVersion(), new Date());
        }
    }

}
