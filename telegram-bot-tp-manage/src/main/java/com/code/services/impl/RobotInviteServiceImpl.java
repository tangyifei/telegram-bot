package com.code.services.impl;

import com.code.consts.CommonConsts;
import com.code.daos.robot.RobotInviteMapper;
import com.code.services.RobotInviteService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 邀请服务实现类
 *
 * @author xiaoyaowang
 */
@Service
public class RobotInviteServiceImpl implements RobotInviteService {

    @Resource
    RobotInviteMapper robotInviteMapper;

    @Override
    public Set<String> getInviteWalletAddressByWalletAddress(String walletAddress) {
        List<String> inviteChatIdList = robotInviteMapper.getInviteChatIdByWalletAddress(walletAddress);
        if (!CollectionUtils.isEmpty(inviteChatIdList)) {
            return robotInviteMapper.getInviteWalletAddressByInviteChatIdList(inviteChatIdList);
        }
        return new HashSet<>();
    }

    @Override
    public Set<String> getInvitedWalletAddressListByWalletAddress(String walletAddress) {
        return robotInviteMapper.getInvitedWalletAddressListByWalletAddress(walletAddress);
    }

    @Override
    public List<Integer> getRobotInviteIdListByRobotCheckId(Integer id) {
        return robotInviteMapper.getRobotInviteIdListByRobotCheckId(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class, transactionManager = CommonConsts.TRANSACTION_MANAGER)
    public int patchCancelRobotInviteRewardReset(List<Integer> inviteIdList) {
        return robotInviteMapper.patchCancelRobotInviteRewardReset(inviteIdList);
    }

}
