package com.code.services;

import com.code.models.qo.FocusOnTwitterFailureQO;
import com.code.models.robot.FocusOnTwitterFailure;
import com.github.pagehelper.PageInfo;

/**
 * 关注推特记录业务接口
 *
 * @author xiaoyaowang
 */
public interface ChatTwitterService {

    /**
     * 完成关注推特
     *
     * @param walletAddr 钱包地址
     */
    void finishFocusOnTwitter(String walletAddr);

    /**
     * 分页获取关注推特失败的用户列表
     *
     * @param focusOnTwitterFailureQO 关注推特失败记录记录查询实体类QO
     * @return 分页过后的关注推特失败的用户列表
     */
    PageInfo<FocusOnTwitterFailure> getFocusOnTwitterFailureListByPage(FocusOnTwitterFailureQO focusOnTwitterFailureQO);

    /**
     * 通过聊天唯一标识删除用户关注的推特记录
     *
     * @param chatId 聊天唯一标识
     */
    void deleteChatTwitterByChatId(String chatId);

}
