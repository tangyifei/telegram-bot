package com.code.services;


import com.code.models.user.LoginTokenBO;

/**
 * 用户登录TOKEN服务
 *
 * @author xiaoyaowang
 * @since 2019-5-24 10:43:30
 */
public interface LoginTokenService {

    /**
     * Add login token.
     *
     * @param loginToken the login token
     * @return the login token
     */
    LoginTokenBO add(LoginTokenBO loginToken);

    /**
     * Delete by id.
     *
     * @param id the id
     */
    void deleteById(String id);

    /**
     * Get by id login token.
     *
     * @param id the id
     * @return the login token
     */

    LoginTokenBO getById(String id);

    /**
     * Ttl long.
     *
     * @param id the id
     * @return the long
     */
    long ttl(String id);

}
