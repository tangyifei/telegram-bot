package com.code.services;

import com.code.models.qo.RobotRewardFlowQO;
import com.code.models.robot.RobotRewardFlow;

import java.util.Map;

/**
 * 机器人奖励流水业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotRewardFlowService {

    /**
     * 插入机器人奖励流水
     *
     * @param robotRewardFlow 机器人奖励流水实体类
     * @return 影响的行数
     */
    int insertRobotRewardFlow(RobotRewardFlow robotRewardFlow);

    /**
     * 判断某一个钱包地址是否给予了奖励
     *
     * @param walletAddress 钱包地址
     * @param amount        奖励金额
     * @return 存在与否
     */
    Boolean existRobotRewardFlowByWalletAddressAndAmount(String walletAddress, String amount);

    /**
     * 分页获取奖励流水户列表
     *
     * @param robotRewardFlowQO 奖励流水查询实体类
     * @return 分页过后的奖励流水户列表
     */
    Map<String, Object> getRobotRewardFlowListByPage(RobotRewardFlowQO robotRewardFlowQO);

    /**
     * 判断用户是否加入BZ群组
     *
     * @param walletAddress 钱包地址
     * @return 用户是否加入BZ群组
     */
    boolean whetherJoinBzGroup(String walletAddress);


}
