package com.code.services;

import java.util.List;
import java.util.Set;

/**
 * 邀请业务接口
 *
 * @author xiaoyaowang
 */
public interface RobotInviteService {

    /**
     * 根据被邀请人的钱包地址查询邀请人的钱包地址
     *
     * @param walletAddress 被邀请人的钱包地址
     * @return 邀请人的钱包地址
     */
    Set<String> getInviteWalletAddressByWalletAddress(String walletAddress);

    /**
     * 获取被邀请人的钱包地址列表
     *
     * @param walletAddress 邀请人的钱包地址
     * @return 被邀请人的钱包地址列表
     */
    Set<String> getInvitedWalletAddressListByWalletAddress(String walletAddress);

    /**
     * 通过审核记录主键获取当前审核记录关联的用户邀请主键列表
     *
     * @param id 审核记录主键
     * @return 用户邀请主键列表
     */
    List<Integer> getRobotInviteIdListByRobotCheckId(Integer id);

    /**
     * 批量重置邀请人奖励
     *
     * @param inviteIdList 邀请主键列表
     * @return 重置邀请人奖励
     */
    int patchCancelRobotInviteRewardReset(List<Integer> inviteIdList);

}
