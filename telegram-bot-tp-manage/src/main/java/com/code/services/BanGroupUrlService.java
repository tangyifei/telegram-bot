package com.code.services;

import com.code.models.robot.BanGroupUrl;
import com.github.pagehelper.PageInfo;

/**
 * 禁止完成群分享任务的群地址业务接口
 *
 * @author xiaoyaowang
 */
public interface BanGroupUrlService {

    /**
     * 根据群地址获取禁止完成群分享任务的群地址列表
     *
     * @param groupUrl 群地址
     * @param page     当前页
     * @param pageSize 每页的大小
     * @return 禁止完成群分享任务的群地址列表
     */
    PageInfo<BanGroupUrl> getBanGroupUrlListByCondition(String groupUrl, Integer page, Integer pageSize);

    /**
     * 删除禁止完成群分享任务的群地址
     *
     * @param id 主键
     */
    void deleteBanGroupUrlById(Integer id);

    /**
     * 添加禁止完成群分享任务的群地址
     *
     * @param banGroupUrl 禁止完成群分享任务的群地址对象
     */
    int addBanGroupUrl(BanGroupUrl banGroupUrl);

    /**
     * 修改禁止完成群分享任务的群地址
     *
     * @param banGroupUrl 禁止完成群分享任务的群地址对象
     */
    int updateBanGroupUrl(BanGroupUrl banGroupUrl);

}
