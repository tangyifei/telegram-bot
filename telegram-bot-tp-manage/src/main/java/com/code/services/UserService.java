package com.code.services;

import com.code.models.login.LoginCredentialVO;
import com.code.models.user.LoginTokenBO;

/**
 * 用户业务接口
 *
 * @author xiaoyaowang
 */
public interface UserService {

    /**
     * 登陆
     *
     * @param loginCredentialVO 登陆凭证视图对象
     * @return 登陆的token信息
     * @throws Exception 异常
     */
    LoginTokenBO login(LoginCredentialVO loginCredentialVO) throws Exception;

    /**
     * 注销
     */
    void logout();

}
