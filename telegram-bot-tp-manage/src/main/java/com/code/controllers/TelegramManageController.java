package com.code.controllers;

import com.code.annotations.LoginAuth;
import com.code.annotations.OperateLogAnno;
import com.code.annotations.ResponseResult;
import com.code.models.po.OperateLog;
import com.code.models.qo.*;
import com.code.models.robot.*;
import com.code.models.vo.*;
import com.code.services.*;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 电报机器人相关的控制器
 *
 * @author xiaoyaowang
 */
@ResponseResult
@RestController("telegramController")
@RequestMapping("/task-checks")
public class TelegramManageController {

    @Resource
    private RobotTaskCheckService robotTaskCheckService;

    @Resource
    private ChatTwitterService chatTwitterService;

    @Resource
    private RobotRewardFlowService robotRewardFlowService;

    @Resource
    private RobotSystemService robotSystemService;

    @Resource
    private BanGroupUrlService banGroupUrlService;

    @Resource
    private ChatMessageNumsService chatMessageNumsService;
    @Resource
    private OperateLogService operateLogService;

    @ApiOperation(value = "更新机器人任务的审核状态", response = RobotTaskCheck.class, notes = "更新机器人任务的审核状态", httpMethod = "PATCH")
    @PatchMapping("")
    @LoginAuth
    @OperateLogAnno(operateItem = "审核", pageName = "TP活动审核")
    public RobotTaskCheck updateRobotTaskState(@RequestBody RobotTaskCheck robotTaskCheck) {
        return robotTaskCheckService.updateRobotTaskState(robotTaskCheck);
    }

    @ApiOperation(value = "手动批量更新机器人任务的审核状态为成功状态", notes = "手动批量更新机器人任务的审核状态为成功状态", httpMethod = "PATCH")
    @PatchMapping("/manual-check-success")
    @LoginAuth
    @OperateLogAnno(operateItem = "一键审核", pageName = "TP活动审核")
    public void manualCheckSuccess() {
        robotTaskCheckService.autoFinishCheck();
    }

    @ApiOperation(value = "手动关闭机器人执行任务", notes = "手动关闭机器人执行任务", httpMethod = "PATCH")
    @PatchMapping("/{systemValue}/whether-stop-robot")
    @LoginAuth
    @ApiImplicitParams({
            @ApiImplicitParam(name = "systemValue", value = "字典value，字符串类型，1表示关闭，0表示开启", dataType = "string", paramType = "path", required = true)
    })
    @OperateLogAnno(operateItem = "机器人", pageName = "TP活动审核")
    public void stopRobotExecuteTask(@PathVariable String systemValue) {
        robotSystemService.updateRobotSystem(null, systemValue);
    }

    @ApiOperation(value = "分页获取机器人任务审核列表", response = RobotTaskCheck.class, notes = "分页获取机器人任务审核列表", httpMethod = "POST")
    @PostMapping("")
    @LoginAuth
    public PageInfo<RobotTaskCheck> getRobotTaskCheckListByPage(@Validated @RequestBody RobotTaskCheckQO robotTaskCheckQO) {
        return robotTaskCheckService.getRobotTaskCheckListByPage(robotTaskCheckQO);
    }

    @ApiOperation(value = "推送消息", notes = "推送消息", httpMethod = "POST")
    @PostMapping("/push-message")
    @LoginAuth
    @OperateLogAnno(operateItem = "推送", pageName = "TP活动审核")
    public void pushMessage(@Validated @RequestBody RobotSendMessageVO robotSendMessageVO) {
        robotTaskCheckService.pushMessage(robotSendMessageVO);
    }

    @ApiOperation(value = "获取机器人任务审核详情", response = RobotTaskCheck.class, notes = "获取机器人任务审核详情", httpMethod = "GET")
    @GetMapping("/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataType = "int", paramType = "path", required = true)
    })
    @LoginAuth
    public RobotTaskCheck getRobotTaskCheckById(@PathVariable("id") Integer id) {
        return robotTaskCheckService.getRobotTaskCheckById(id);
    }

    @ApiOperation(value = "获取奖金池的余额", notes = "获取奖金池的余额", response = RobotIndexVO.class, httpMethod = "GET")
    @GetMapping("/balances")
    @LoginAuth
    public RobotIndexVO getBalance() {
        return robotTaskCheckService.getBalance();
    }

    @ApiOperation(value = "手动完成某一个用户关注推特任务", notes = "手动完成某一个用户关注推特任务", httpMethod = "POST")
    @PostMapping("/twitter-tasks")
    @LoginAuth
    @Deprecated
    public void finishFocusOnTwitter(@RequestBody FocusOnTwitterFailure focusOnTwitterFailure) {
        chatTwitterService.finishFocusOnTwitter(focusOnTwitterFailure.getWalletAddr());
    }

    @ApiOperation(value = "分页获取关注推特失败的用户列表", response = FocusOnTwitterFailure.class, notes = "分页获取关注推特失败的用户列表", httpMethod = "POST")
    @PostMapping("/focus-on-twitter-failures")
    @LoginAuth
    @Deprecated
    public PageInfo<FocusOnTwitterFailure> getFocusOnTwitterFailureListByPage(@RequestBody FocusOnTwitterFailureQO focusOnTwitterFailureQO) {
        return chatTwitterService.getFocusOnTwitterFailureListByPage(focusOnTwitterFailureQO);
    }

    @ApiOperation(value = "分页获取用户奖励流水列表", response = RobotRewardFlow.class, notes = "分页获取用户奖励流水列表", httpMethod = "POST")
    @PostMapping("/robot-reward-flows")
    @LoginAuth
    public Map<String, Object> getRobotRewardFlowListByPage(@RequestBody RobotRewardFlowQO robotRewardFlowQO) {
        return robotRewardFlowService.getRobotRewardFlowListByPage(robotRewardFlowQO);
    }

    @ApiOperation(value = "分页获取禁止完成群分享任务的群地址列表", response = BanGroupUrl.class, notes = "分页获取禁止完成群分享任务的群地址列表", httpMethod = "POST")
    @PostMapping("/ban-group-urls")
    @LoginAuth
    public PageInfo<BanGroupUrl> getBanGroupUrlListByCondition(@RequestBody BanGroupUrlQO banGroupUrlQO) {
        return banGroupUrlService.getBanGroupUrlListByCondition(banGroupUrlQO.getGroupUrl(), banGroupUrlQO.getPageNum(), banGroupUrlQO.getPageSize());
    }

    @ApiOperation(value = "新增禁止完成群分享任务的群地址", notes = "新增禁止完成群分享任务的群地址", httpMethod = "POST")
    @PostMapping("/group-urls")
    @LoginAuth
    public int addBanGroupUrl(@RequestBody BanGroupUrl banGroupUrl) {
        return banGroupUrlService.addBanGroupUrl(banGroupUrl);
    }

    @ApiOperation(value = "删除禁止完成群分享任务的群地址", notes = "删除禁止完成群分享任务的群地址", httpMethod = "DELETE")
    @DeleteMapping("/group-urls/{id}")
    @LoginAuth
    public void deleteBanGroupUrlById(@PathVariable Integer id) {
        banGroupUrlService.deleteBanGroupUrlById(id);
    }

    @ApiOperation(value = "部分更新禁止完成群分享任务的群地址", notes = "部分更新禁止完成群分享任务的群地址", httpMethod = "PATCH")
    @PatchMapping("/group-urls")
    @LoginAuth
    public int updateBanGroupUrl(@RequestBody BanGroupUrl banGroupUrl) {
        return banGroupUrlService.updateBanGroupUrl(banGroupUrl);
    }

    @ApiOperation(value = "分页获取统计用户群消息数列表", response = ChatMessageNums.class, notes = "分页获取统计用户群消息数列表", httpMethod = "POST")
    @PostMapping("/chat-messages")
    @LoginAuth
    public PageInfo<ChatMessageNums> getChatMessageNumsListByCondition(@RequestBody ChatMessageNumsQO chatMessageNumsQO) {
        return chatMessageNumsService.getChatMessageNumsListByCondition(chatMessageNumsQO.getTelegramAccountName(), chatMessageNumsQO.getCurrentDay(), chatMessageNumsQO.getPageNum(), chatMessageNumsQO.getPageSize());
    }

    @ApiOperation(value = "获取总的用户群发消息数", notes = "获取总的用户群发消息数", httpMethod = "GET")
    @GetMapping("/chat-messages-total")
    @LoginAuth
    public int getTotalMessageNums() {
        return chatMessageNumsService.getTotalMessageNums();
    }

    @ApiOperation(value = "清空用户群发消息数", notes = "清空用户群发消息数", httpMethod = "DELETE")
    @DeleteMapping("/chat-messages")
    @LoginAuth
    public void clearMessageNums() {
        chatMessageNumsService.clearMessageNums();
    }

    @ApiOperation(value = "查看推广连接", notes = "查看推广连接", httpMethod = "GET")
    @GetMapping("/{chatId}/sponsored-links")
    @LoginAuth
    @ApiImplicitParams({
            @ApiImplicitParam(name = "chatId", value = "聊天唯一标识", dataType = "string", paramType = "path", required = true)
    })
    public String getSponsoredLinks(@PathVariable("chatId") String chatId) {
        return robotTaskCheckService.getSponsoredLinks(chatId);
    }

    @ApiOperation(value = "设置或者取消代理人", notes = "设置或者取消代理人", httpMethod = "PATCH")
    @PatchMapping("/set-or-cancel-agent")
    @LoginAuth
    @OperateLogAnno(operateItem = "代理人", pageName = "TP活动审核")
    public int setOrCancelAgent(@Validated @RequestBody AgentVO agentVO) {
        return robotTaskCheckService.setOrCancelAgent(agentVO);
    }

    @ApiOperation(value = "分页获取代理人排名列表", response = AgentRankVO.class, notes = "分页获取代理人排名列表", httpMethod = "POST")
    @PostMapping("/get-agent-rank-list")
    @LoginAuth
    public PageInfo<AgentRankVO> getAgentRankListByPage(@RequestBody AgentRankQO agentRankQO) {
        return robotTaskCheckService.getAgentRankListByPage(agentRankQO);
    }

    @ApiOperation(value = "获取邀请成员列表", response = InviterVO.class, notes = "获取邀请成员列表", httpMethod = "POST")
    @PostMapping("/get-inviter-list")
    @LoginAuth
    public PageInfo<InviterVO> getInviterListByPage(@RequestBody InviterQO inviterQO) {
        return robotTaskCheckService.getInviterListByPage(inviterQO);
    }

    @ApiOperation(value = "分页获取操作日志列表", response = OperateLog.class, notes = "分页获取操作日志列表", httpMethod = "POST")
    @PostMapping("/operate-logs")
    @LoginAuth
    public PageInfo<OperateLog> getOperateLogListByPage(@Validated @RequestBody OperateLogQO operateLogQO) {
        return operateLogService.getOperateLogListByCondition(operateLogQO);
    }

}
