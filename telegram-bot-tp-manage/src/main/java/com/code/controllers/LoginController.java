package com.code.controllers;

import com.code.annotations.LoginAuth;
import com.code.annotations.ResponseResult;
import com.code.models.login.LoginCredentialVO;
import com.code.models.user.LoginTokenBO;
import com.code.services.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 登录控制器
 *
 * @author xiaoyaowang
 */
@ResponseResult
@RestController("loginController")
public class LoginController {

    @Resource
    private UserService userService;

    @ApiOperation(value = "用户登录", response = LoginTokenBO.class, notes = "用户登录", httpMethod = "POST")
    @PostMapping("/taskLogin")
    public LoginTokenBO doLogin(@Validated @RequestBody LoginCredentialVO loginCredentialVO) throws Exception {
        return userService.login(loginCredentialVO);
    }

    @ApiOperation(value = "退出登录", notes = "退出登录", httpMethod = "POST")
    @PostMapping("/logout")
    @LoginAuth
    public void logout() {
        userService.logout();
    }
}
