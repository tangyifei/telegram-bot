package com.code.utils;

import org.apache.commons.lang3.StringUtils;

import java.net.URLEncoder;
import java.util.*;

/**
 * 对参数按key进行字典升序排列
 *
 * @author xiaoyaowang
 */
public class SortUtil {

    /**
     * @param param   参数
     * @param encode  编码
     * @param isLower 是否小写
     * @return
     */
    public static String formatUrlParam(Map<String, Object> param, String encode, boolean isLower) {
        String params = "";
        Map<String, Object> map = param;

        try {
            List<Map.Entry<String, Object>> itmes = new ArrayList<>(map.entrySet());

            //对所有传入的参数按照字段名从小到大排序
            //Collections.sort(items); 默认正序
            //可通过实现Comparator接口的compare方法来完成自定义排序
            Collections.sort(itmes, Comparator.comparing(Map.Entry::getKey));

            //构造URL 键值对的形式
            StringBuffer sb = new StringBuffer();
            for (Map.Entry<String, Object> item : itmes) {
                if (StringUtils.isNotBlank(item.getKey())) {
                    String key = item.getKey();
                    String val = String.valueOf(item.getValue());
                    val = URLEncoder.encode(val, encode);
                    if (isLower) {
                        sb.append(key.toLowerCase() + "=" + val);
                    } else {
                        sb.append(key + "=" + val);
                    }
                    sb.append("&");
                }
            }

            params = sb.toString();
            if (!params.isEmpty()) {
                params = params.substring(0, params.length() - 1);
            }
        } catch (Exception e) {
            return "";
        }
        return params;
    }

//    public static void main(String[] args) {
//        Map<String, Object> map = new HashMap<>();
//        map.put("price", 200);
//        map.put("title", "测试标题");
//        map.put("content", "测试内容");
//        map.put("order_no", "1807160850122023");
//        String url = formatUrlParam(map, "utf-8", true);
//        System.out.println(url);
//    }
}
