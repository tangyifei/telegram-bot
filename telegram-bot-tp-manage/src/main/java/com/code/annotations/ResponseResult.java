package com.code.annotations;


import com.code.results.PlatformResult;
import com.code.results.Result;

import java.lang.annotation.*;

/**
 * 接口返回结果增强，会通过拦截器拦截后放入标记，在WebResponseBodyHandler进行结果处理
 *
 * @author xiaoyaowang
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResponseResult {

    Class<? extends Result> value() default PlatformResult.class;

}
