package com.code.models.po;

import com.code.models.robot.BaseRobot;

/**
 * 用户群发历史消息数实体类
 *
 * @author xiaoyaowang
 */
public class ChatMessageNumsHistory extends BaseRobot {

    private static final long serialVersionUID = 3535812342962211524L;

    private String chatId;

    private Integer messageNumsHistory;

    private String userName;

    private String firstName;

    private String lastName;

    private Integer version;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public Integer getMessageNumsHistory() {
        return messageNumsHistory;
    }

    public void setMessageNumsHistory(Integer messageNumsHistory) {
        this.messageNumsHistory = messageNumsHistory;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "ChatMessageNumsHistory{" +
                "chatId='" + chatId + '\'' +
                ", messageNumsHistory=" + messageNumsHistory +
                ", userName='" + userName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", version=" + version +
                "} " + super.toString();
    }
}
