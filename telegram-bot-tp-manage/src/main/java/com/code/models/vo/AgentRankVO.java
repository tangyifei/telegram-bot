package com.code.models.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 代理人排名视图对象
 *
 * @author xiaoyaowang
 */
@ApiModel("代理人排名视图对象")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AgentRankVO implements Serializable {

    private static final long serialVersionUID = 8881011830119977413L;

    @ApiModelProperty(value = "代理人钱包地址 字符串型", example = "XWCERETETETETEYEYEYEYYEY")
    private String walletAddr;

    @ApiModelProperty(value = "未领取的TP数 整型", example = "0")
    private Integer unReceivedTpAmount;

    @ApiModelProperty(value = "累计领取的TP数 整型", example = "0")
    private Integer receivedTpAmount;

    @ApiModelProperty(value = "邀请的人数 整型", example = "0")
    private Integer invitePersonCount;

    @ApiModelProperty(value = "邀请成员对应的聊天唯一标识 字符串型", example = "0")
    private String chatId;

}
