package com.code.models.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 机器人首页视图对象
 *
 * @author xiaoyaowang
 */
@ApiModel("机器人首页视图对象")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RobotIndexVO implements Serializable {

    private static final long serialVersionUID = 7327616111596816481L;

    @ApiModelProperty(value = "TP奖金池余额", example = "0")
    private String rewardBalance;

    @ApiModelProperty(value = "XWC余额", example = "0")
    private String xwcBalance;

    @ApiModelProperty(value = "机器人的状态 1-关闭 0-开启", example = "0")
    private Integer robotStatus;
}
