package com.code.models.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 机器人推送消息视图对象
 *
 * @author xiaoyaowang
 */
@ApiModel("机器人推送消息视图对象")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RobotSendMessageVO implements Serializable {

    private static final long serialVersionUID = 7327616111596816481L;

    @ApiModelProperty(value = "聊天唯一id 必填", example = "0")
    @NotBlank(message = "chatId is empty")
    private String chatId;

    @ApiModelProperty(value = "推送内容 必填", example = "0")
    @NotBlank(message = "sendMessage is empty")
    private String sendMessage;

}
