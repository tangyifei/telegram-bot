package com.code.models.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 邀请成员视图对象
 *
 * @author xiaoyaowang
 */
@ApiModel("邀请成员视图对象")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InviterVO implements Serializable {

    private static final long serialVersionUID = -5640068617754906528L;

    @ApiModelProperty(value = "钱包地址 字符串型", example = "XWCERETETETETEYEYEYEYYEY")
    private String walletAddr;

    @ApiModelProperty(value = "姓 字符串型", example = "张")
    @JsonIgnore
    private String firstName;

    @ApiModelProperty(value = "名 字符串型", example = "三")
    @JsonIgnore
    private String lastName;

    @ApiModelProperty(value = "用户名 字符串型", example = "张三")
    @JsonIgnore
    private String userName;

    @ApiModelProperty(value = "telegram账号名称 字符串型", example = "0")
    private String telegramAccountName;

}
