package com.code.models.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 代理人视图对象
 *
 * @author xiaoyaowang
 */
@ApiModel("代理人视图对象")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AgentVO implements Serializable {

    private static final long serialVersionUID = -6275470970312334847L;

    @ApiModelProperty(value = "主键 整型 设置或者取消代理人必填", example = "10")
    @NotNull(message = "主键不能为空")
    private Integer id;

    @ApiModelProperty(value = "代理人状态 整型 0-不是代理人 1-是代理人 必填", example = "0")
    @NotNull(message = "代理人状态不能为空")
    private Integer agentState;

}
