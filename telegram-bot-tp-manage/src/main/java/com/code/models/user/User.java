package com.code.models.user;

import com.code.models.po.BasePO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Id;

/**
 * 用户PO及持久层对象
 *
 * @author xiaoyaowang
 * @since 2019-5-24 09:42:13
 */
@EqualsAndHashCode(callSuper = false)
@ApiModel("用户PO及持久层对象")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class User extends BasePO<String> {

    private static final long serialVersionUID = -7491215402569546437L;

    @ApiModelProperty(value = "用户主键 添加时非必填 修改、删除时必填", example = "1")
    @Id
    @Length(min = 1, max = 64)
    private String id;

    @ApiModelProperty(value = "手机号", example = "150****0055")
    @NotBlank(message = "用户手机号不能为空")
    private String phone;

    @ApiModelProperty(value = "账号状态", example = "1")
    private String userStatus;

    @ApiModelProperty(value = "是否为超级管理员 0不是超级管理员  1超级管理员", example = "1")
    private Integer isAdmin;

    /**
     * 用户状态枚举
     */
    public enum UserStatusEnum {
        /**
         * 启用
         */
        ENABLED,

        /**
         * 禁用
         */
        DISABLED;

    }

}
