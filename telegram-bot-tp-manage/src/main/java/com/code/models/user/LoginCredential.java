package com.code.models.user;

import com.code.models.po.BasePO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

/**
 * 登录凭证持久层对象
 *
 * @author xiaoyaowang
 * @since 2019-5-24 09:37:56
 */
@ApiModel("登录凭证PO")
@EqualsAndHashCode(callSuper = false)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginCredential extends BasePO<Long> {

    private static final long serialVersionUID = 5550420394013305835L;

    @ApiModelProperty(value = "用户凭证主键 添加时非必填 修改、删除时必填", example = "1")
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    @ApiModelProperty(value = "用户账号", example = "1")
    @NotBlank(message = "用户账号不能为空")
    @Length(min = 1, max = 128)
    private String account;

    @ApiModelProperty(value = "用户密码（MD5加密传输）", example = "35ecd85942c861d34da6bcb972f9594125608b94a995f253c7eae228aa7b687c")
    @NotBlank(message = "用户密码（MD5加密传输）不能为空")
    private String pwd;

    @ApiModelProperty(value = "密码加密随机盐", example = "1")
    @Length(max = 64)
    private String randomSalt;

    @ApiModelProperty(value = "用户主键", example = "1")
    private String userId;

}
