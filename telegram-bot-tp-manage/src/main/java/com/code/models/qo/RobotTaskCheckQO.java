package com.code.models.qo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 机器人任务审核记录查询实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("机器人任务审核记录查询实体类QO")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RobotTaskCheckQO extends BaseQO {

    private static final long serialVersionUID = 3522992676325318444L;

    @ApiModelProperty(value = "用户名-账户名（telegram账号） 字符串类型 非必填", example = "limingcheng")
    private String userName;

    @ApiModelProperty(value = "推特账号 字符串类型 非必填", example = "limingcheng")
    private String twitterAccountName;

    @ApiModelProperty(value = "钱包地址 字符串类型 非必填", example = "XWCNfmGDXeVGKTk3VtcCxpb8dzt7c6HDJj5qw")
    private String walletAddr;

    @ApiModelProperty(value = "开始时间 日期时间类型 非必填", example = "2021-07-07 09:00:00")
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "Asia/Shanghai"
    )
    private Date startDateTime;

    @ApiModelProperty(value = "结束时间 日期时间类型 非必填", example = "2021-07-07 19:00:00")
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "Asia/Shanghai"
    )
    private Date endDateTime;

    @ApiModelProperty(value = "审核状态 整型 0-未审核（待审核） 1-审核失败 2-审核成功 非必填", example = "0")
    private Integer state;

    @ApiModelProperty(value = "代理人状态 0-不是代理人 1-是代理人 非必填", example = "0")
    private Integer agentState;

    @ApiModelProperty(value = "1-升序 2降序 非必填 默认按照时间降序", example = "2")
    private Integer sort;

    @ApiModelProperty(value = "BZ任务是否完成 0-未完成 1完成 2全部", example = "1")
    private Integer bzTaskFinishState;

}
