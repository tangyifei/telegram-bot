package com.code.models.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 统计用户发送的消息数查询对象
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("统计用户发送的消息数查询对象")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatMessageNumsQO extends BaseQO {

    private static final long serialVersionUID = -7893118234338113538L;

    @ApiModelProperty(value = "电报账号 字符串类型 非必填", example = "tyf")
    private String telegramAccountName;

    @ApiModelProperty(value = "当日日期 字符串类型 非必填", example = "2022-04-25")
    private String currentDay;

}
