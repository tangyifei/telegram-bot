package com.code.models.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 关注推特失败记录记录查询实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("关注推特失败记录记录查询实体类QO")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FocusOnTwitterFailureQO extends BaseQO {

    private static final long serialVersionUID = 3900675768056177551L;

    @ApiModelProperty(value = "推特账号 字符串类型 非必填", example = "limingcheng")
    private String twitterAccountName;

    @ApiModelProperty(value = "钱包地址 字符串类型 非必填", example = "XWCNfmGDXeVGKTk3VtcCxpb8dzt7c6HDJj5qw")
    private String walletAddr;

}
