package com.code.models.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 邀请人员查询实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("邀请人员查询实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InviterQO extends BaseQO {

    private static final long serialVersionUID = -3935196096072424436L;

    @ApiModelProperty(value = "聊天唯一标识 必填", example = "11313141")
    private String chatId;

}
