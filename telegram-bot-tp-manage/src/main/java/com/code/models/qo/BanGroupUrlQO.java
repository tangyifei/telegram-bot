package com.code.models.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 禁止完成群分享任务的群地址实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("禁止完成群分享任务的群地址实体类QO")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BanGroupUrlQO extends BaseQO {

    private static final long serialVersionUID = 2886406269452828826L;

    @ApiModelProperty(value = "禁止完成群分享任务的群地址 字符串类型 非必填", example = "http://me")
    private String groupUrl;

}
