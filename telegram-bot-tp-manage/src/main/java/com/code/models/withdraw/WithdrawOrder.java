package com.code.models.withdraw;

import io.swagger.annotations.ApiModel;
import lombok.*;

import java.io.Serializable;

/**
 * 提币订单PO及持久层对象
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = false)
@ApiModel("提币订单PO及持久层对象")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WithdrawOrder implements Serializable {

    private static final long serialVersionUID = -5116457094521844143L;

    private Integer orderId;

    private String coinType;

    private String toAddr;

    private String amount;

}
