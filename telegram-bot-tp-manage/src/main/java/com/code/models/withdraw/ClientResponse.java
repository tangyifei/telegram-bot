package com.code.models.withdraw;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 调用数字管理平台返回的响应数据格式
 *
 * @author xiaoyaowang
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClientResponse {
    private int id;
    private String jsonrpc;
    private String result;
    private Object error;
}
