package com.code.models.checktoken;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 调用TokenSwap后台响应数据格式
 *
 * @author xiaoyaowang
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TokenSwapManageResponse {
    private String code;
    private String msg;
    private Object data = new Object();
}
