package com.code.models.login;

import com.code.models.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 登录凭证VO
 *
 * @author xiaoyaowang
 * @since 2019-5-24 15:20:30
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginCredentialVO implements Model {

    private static final long serialVersionUID = 5550420394013305835L;

    @ApiModelProperty(value = "账号（一般为手机号）必填", example = "15062230055")
    @NotBlank(message = "登陆账号不能为空")
    private String account;

    @ApiModelProperty(value = "密码(MD5加密等) 必填", example = "6bd5b28db72f5098dca92ed2477371b0")
    @NotBlank(message = "密码不能为空")
    private String pwd;

}
