package com.code.daos.user;

import com.code.daos.base.CrudMapper;
import com.code.models.user.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 用户持久层映射接口
 *
 * @author xiaoyaowang
 * @since 2019-5-24 10:23:26
 */
@Repository
public interface UserMapper extends CrudMapper<User> {

    String getIdByPhone(@Param("phone") String phone);

    User getUserByPhone(@Param("phone") String phone);
}
