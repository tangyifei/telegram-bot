package com.code.daos.user;

import com.code.daos.base.CrudMapper;
import com.code.models.user.LoginCredential;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 登录凭证持久层映射接口
 *
 * @author xiaoyaowang
 * @since 2019-5-24 10:20:01
 */
@Repository
public interface LoginCredentialMapper extends CrudMapper<LoginCredential> {

    LoginCredential getLoginCredentialByPhone(@Param("phone") String phone);

}
