package com.code.daos.robot;

import com.code.daos.base.CrudMapper;
import com.code.models.robot.FocusOnTwitterFailure;
import org.springframework.stereotype.Repository;

/**
 * 关注推特失败记录持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface FocusOnTwitterFailureMapper extends CrudMapper<FocusOnTwitterFailure> {

}
