package com.code.daos.robot;

import com.code.daos.base.CrudMapper;
import com.code.models.robot.RobotChatFinishTask;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 完成任务记录持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface RobotChatFinishTaskMapper extends CrudMapper<RobotChatFinishTask> {

    String getChatIdByWalletAddr(@Param("walletAddress") String walletAddress);

    String getTwitterAccountNameByWalletAddr(@Param("chatId") String chatId, @Param("walletAddress") String walletAddress);

    Integer getTaskIdBySort(@Param("sort") int sort);

    String getUserNameByChatId(@Param("chatId") String chatId);

    String getFirstNameByChatId(@Param("chatId") String chatId);

    String getLastNameByChatId(@Param("chatId") String chatId);

    Integer existsRobotChatFinishTaskByChatIdAndTaskId(@Param("taskId") Integer taskId, @Param("chatId") String chatId);

    Integer existsChatTwitterByChatIdAndTwitterAccountName(@Param("chatId") String chatId, @Param("twitterAccountName") String twitterAccountName);

    int getChatTwitterIdByChatIdAndTwitterAccountName(@Param("chatId") String chatId, @Param("twitterAccountName") String twitterAccountName);

}
