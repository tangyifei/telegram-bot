package com.code.daos.robot;

import com.code.daos.base.CrudMapper;
import com.code.models.po.ChatMessageNumsHistory;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * 统计用户发送的历史消息数持久层
 *
 * @author xiaoyaowang
 */
public interface ChatMessageNumsHistoryMapper extends CrudMapper<ChatMessageNumsHistory> {

    /**
     * 根据聊天唯一标识获取用户发送的历史消息数列表
     *
     * @param chatId 聊天唯一标识
     * @return 用户发送的历史消息数列表
     */
    ChatMessageNumsHistory getChatMessageNumsHistoryByChatId(@Param("chatId") String chatId);

    /**
     * 增加用户在群里发送的历史消息数
     *
     * @param id                 主键
     * @param messageNumsHistory 群发历史消息数
     * @param version            版本号
     * @param updateAt           更新时间
     * @return 影响的行数
     */
    int incrementChatMessageNumsHistory(@Param("id") Integer id,
                                        @Param("messageNumsHistory") Integer messageNumsHistory,
                                        @Param("version") Integer version,
                                        @Param("updateAt") Date updateAt);

}
