package com.code.daos.robot;

import com.code.daos.base.CrudMapper;
import com.code.models.robot.ChatMessageNums;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 统计用户发送的消息数持久层
 *
 * @author xiaoyaowang
 */
public interface ChatMessageNumsMapper extends CrudMapper<ChatMessageNums> {

    /**
     * 根据相关条件获取用户发送的消息数列表
     *
     * @param telegramAccountName 电报账号
     * @return 用户发送的消息数列表
     */
    List<ChatMessageNums> getChatMessageNumsListByCondition(@Param("telegramAccountName") String telegramAccountName, @Param("currentDay") String currentDay);

    /**
     * 获取聊天人的信息
     *
     * @param chatId 聊天唯一标识
     * @return 聊天人的信息
     */
    ChatMessageNums getChatMemberGroupByChatId(@Param("chatId") String chatId);

    /**
     * 直接获取用户发送的消息数列表
     *
     * @return 用户发送的消息数列表
     */
    List<ChatMessageNums> getChatMessageNumsList(@Param("currentDay") String currentDay);

    /**
     * 获取总的群发消息数
     *
     * @return 总的群发消息数
     */
    Integer getTotalMessageNums();

    /**
     * 清空历史群发消息记录
     */
    void clearMessageNums();

    /**
     * 直接获取总的用户发送的消息数列表
     *
     * @return 用户发送的消息数列表
     */
    List<ChatMessageNums> getAllChatMessageNumsList();

}
