package com.code.daos.robot;

import com.code.daos.base.CrudMapper;
import com.code.models.qo.FocusOnTwitterFailureQO;
import com.code.models.robot.ChatTwitter;
import com.code.models.robot.FocusOnTwitterFailure;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 关注推特记录持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface ChatTwitterMapper extends CrudMapper<ChatTwitter> {

    List<FocusOnTwitterFailure> getFocusOnTwitterFailureListByPage(FocusOnTwitterFailureQO focusOnTwitterFailureQO);

    Integer existsChatTwitterByChatId(@Param("chatId") String chatId);

    Integer existsFocusOnTwitterFailureByChatId(@Param("chatId") String chatId);

    void deleteChatTwitterByChatId(@Param("chatId") String chatId);

    void deleteFocusOnTwitterFailureByChatId(@Param("chatId") String chatId);

}
