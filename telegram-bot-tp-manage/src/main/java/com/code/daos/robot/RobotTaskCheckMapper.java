package com.code.daos.robot;

import com.code.daos.base.CrudMapper;
import com.code.models.qo.AgentRankQO;
import com.code.models.qo.RobotTaskCheckQO;
import com.code.models.robot.RobotChatFinishTask;
import com.code.models.robot.RobotTaskCheck;
import com.code.models.robot.RobotTaskCheckContent;
import com.code.models.vo.AgentRankVO;
import com.code.models.vo.InviterVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 任务审核持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface RobotTaskCheckMapper extends CrudMapper<RobotTaskCheck> {

    /**
     * 判断用户的任务审核记录是否存在
     *
     * @param robotTaskCheck 机器人任务审核记录实体类
     * @return 存在的记录数
     */
    Integer getRobotTaskCheckCount(RobotTaskCheck robotTaskCheck);

    /**
     * 根据相关的条件获取机器人任务审核记录列表
     *
     * @param robotTaskCheckQO 机器人任务审核记录查询实体类QO
     * @return 机器人任务审核记录列表
     */
    List<RobotTaskCheck> getRobotTaskCheckListByPage(RobotTaskCheckQO robotTaskCheckQO);

    /**
     * 根据相关的条件获取加入BZ群的任务审核记录列表
     *
     * @param robotTaskCheckQO 机器人任务审核记录查询实体类QO
     * @return 加入BZ群的任务审核记录列表
     */
    List<RobotTaskCheck> getRobotTaskCheckListWithBzByPage(RobotTaskCheckQO robotTaskCheckQO);

    /**
     * 根据主键获取任务审核详情记录
     *
     * @param id 主键
     * @return 任务审核详情记录
     */
    RobotTaskCheck getRobotTaskCheckById(@Param("id") int id);

    /**
     * 通过任务审核id获取图片列表
     *
     * @param id 任务审核主键
     * @return 图片列表
     */
    List<RobotTaskCheckContent> getRobotTaskCheckContentListByTaskCheckId(@Param("id") int id);

    /**
     * 根据主键获取钱包地址
     *
     * @param id 主键
     * @return 钱包地址
     */
    String getWalletAddressByTaskCheckId(@Param("id") int id);

    /**
     * 更新任务的审核状态
     *
     * @param id        主键
     * @param state     审核状态 0-未审核 1-审核失败 2-审核成功
     * @param updatedAt 更新时间
     * @return 影响的行数
     */
    int updateRobotTaskState(@Param("id") int id, @Param("state") int state, @Param("updatedAt") Date updatedAt);

    /**
     * 获取当前任务数
     *
     * @return 当前任务的最大序号
     */
    int getTaskNums();

    /**
     * 根据主键获取代理人状态
     *
     * @param id 主键
     * @return 代理人状态
     */
    Integer getAgentStateById(@Param("id") int id);

    /**
     * 根据钱包地址获取代理人状态
     *
     * @param walletAddr 钱包地址
     * @return 代理人状态
     */
    Integer getAgentStateByWalletAddress(@Param("walletAddr") String walletAddr);

    /**
     * 通过邀请人的钱包地址获取邀请人的完成任务数
     *
     * @param inviterWalletAddress 邀请人的钱包地址
     * @return 邀请人的完成任务数
     */
    int getFinishTaskNumsByInviterWalletAddress(@Param("inviterWalletAddress") String inviterWalletAddress);

    /**
     * 根据邀请人的钱包地址获取最近一条审核记录的审核状态
     *
     * @param inviterWalletAddress 邀请人的钱包地址
     * @return 最近一条审核记录的审核状态
     */
    Integer getRecentStateByInviterWalletAddress(@Param("inviterWalletAddress") String inviterWalletAddress);

    /**
     * 通过聊天id获取推特账户名称
     *
     * @param chatId 聊天id
     * @return 推特账户名称
     */
    String getTwitterAccountNameByChatId(@Param("chatId") String chatId);

    /**
     * 根据排序获取任务主键
     *
     * @param sort 排序
     * @return 任务主键
     */
    Integer getTaskIdBySort(@Param("sort") Integer sort);

    /**
     * 通过taskId和chatId获取完成的任务记录
     *
     * @param chatId 聊天唯一标识
     * @param taskId 任务主键
     * @return 完成的任务记录
     */
    RobotChatFinishTask getRobotChatFinishTaskByChatIdAndTaskId(@Param("chatId") String chatId, @Param("taskId") Integer taskId);

    /**
     * 删除用户完成任务记录
     *
     * @param robotChatFinishTaskDb 用户已完成的任务实体类
     */
    void deleteRobotChatFinishTask(RobotChatFinishTask robotChatFinishTaskDb);

    /**
     * 批量修改图片审核记录
     *
     * @param list 机器人任务审核内容记录列表
     */
    void deleteRobotTaskCheckContentList(List<RobotTaskCheckContent> list);

    /**
     * 判断用户是否被封禁
     *
     * @param chatId 聊天唯一id
     * @return 判断用户是否被封禁
     */
    Integer existsChatBanByChatId(@Param("chatId") String chatId);

    /**
     * 删除封禁的用户
     *
     * @param chatId 聊天唯一id
     */
    void deleteChatBanByChatId(@Param("chatId") String chatId);

    /**
     * 得到审核成功的没有加入BZ群的用户审核记录chatId列表
     *
     * @param serialNoPrefix 序列号前缀
     * @return 审核成功的没有加入BZ群的用户审核记录列表
     */
    Set<String> getChatIdListWithNoJoinBzGroup(@Param("serialNoPrefix") String serialNoPrefix);

    /**
     * 根据聊天唯一标识获取钱包地址
     *
     * @param chatId 聊天唯一标识
     * @return 钱包地址
     */
    String getWalletAddressByChatId(@Param("chatId") String chatId);

    /**
     * 判断用户审核记录是否存在
     *
     * @param id 主键
     * @return 存在与否
     */
    Integer existsRobotTaskCheckById(@Param("id") Integer id);

    /**
     * 分页获取代理人排名视图对象列表
     *
     * @param agentRankQO 代理人排名查询实体类
     * @return 代理人排名视图对象列表
     */
    List<AgentRankVO> getAgentRankListByPage(AgentRankQO agentRankQO);

    /**
     * 获取邀请成员视图对象列表
     *
     * @param chatId 聊天唯一标识
     * @return 邀请成员视图对象列表
     */
    List<InviterVO> getInviterList(@Param("chatId") String chatId);

    /**
     * 批量删除用户领取奖励的记录
     *
     * @param id 审核主键
     * @return 影响的行数
     */
    int deleteAgentReceiveIncomeByRobotCheckId(@Param("id") Integer id);

    /**
     * 获取未审核列表
     *
     * @return 未审核列表
     */
    List<RobotTaskCheck> getUnCheckList();

    /**
     * 获取用户完成的任务数
     *
     * @param chatId 聊天唯一标识
     * @return 用户完成的任务数
     */
    int getFinishTaskNumsByChatId(@Param("chatId") String chatId);

    /**
     * 根据聊天唯一标识获取累加的TP领取数
     *
     * @param chatId 聊天唯一标识
     * @return 累加的TP领取数
     */
    Integer getTotalReceivedTpAmountByChatId(@Param("chatId") String chatId);

    /**
     * 通过聊天唯一标识获取审核记录
     *
     * @param chatId 聊天唯一标识
     * @return 审核记录
     */
    RobotTaskCheck getRobotTaskCheckByChatId(@Param("chatId") String chatId);

}
