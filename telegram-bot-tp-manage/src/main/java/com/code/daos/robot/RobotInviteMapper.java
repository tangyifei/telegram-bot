package com.code.daos.robot;

import com.code.daos.base.CrudMapper;
import com.code.models.robot.RobotInvite;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * 邀请持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface RobotInviteMapper extends CrudMapper<RobotInvite> {

    /**
     * 根据邀请人的聊天id查询邀请人的钱包地址
     *
     * @param inviteChatIdList 邀请人的聊天id列表
     * @return 邀请人的钱包地址
     */
    Set<String> getInviteWalletAddressByInviteChatIdList(List<String> inviteChatIdList);

    /**
     * 通过被邀请人的钱包地址获取邀请人的聊天id
     *
     * @param walletAddress 被邀请人的钱包地址
     * @return 邀请人的聊天id
     */
    List<String> getInviteChatIdByWalletAddress(@Param("walletAddress") String walletAddress);

    /**
     * 获取被邀请人的钱包地址列表
     *
     * @param walletAddress 邀请人的钱包地址
     * @return 被邀请人的钱包地址列表
     */
    Set<String> getInvitedWalletAddressListByWalletAddress(@Param("walletAddress") String walletAddress);

    /**
     * 通过审核记录主键获取当前审核记录关联的用户邀请主键列表
     *
     * @param id 审核记录主键
     * @return 用户邀请主键列表
     */
    List<Integer> getRobotInviteIdListByRobotCheckId(@Param("id") Integer id);

    /**
     * 批量重置邀请人奖励
     *
     * @param inviteIdList 邀请人主键列表
     * @return 影响的行数
     */
    int patchCancelRobotInviteRewardReset(List<Integer> inviteIdList);

}
