package com.code.daos.robot;

import com.code.daos.base.CrudMapper;
import com.code.models.robot.CheckFailure;
import org.springframework.stereotype.Repository;

/**
 * 审核失败持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface CheckFailureMapper extends CrudMapper<CheckFailure> {

}
