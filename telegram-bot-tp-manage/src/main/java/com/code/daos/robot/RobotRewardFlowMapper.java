package com.code.daos.robot;

import com.code.daos.base.CrudMapper;
import com.code.models.qo.RobotRewardFlowQO;
import com.code.models.robot.RobotRewardFlow;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * 机器人奖励流水持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface RobotRewardFlowMapper extends CrudMapper<RobotRewardFlow> {

    /**
     * 判断某一个钱包地址是否给予了奖励
     *
     * @param walletAddress 钱包地址
     * @param amount        奖励金额
     * @return 记录数
     */
    Integer existRobotRewardFlowByWalletAddressAndAmount(@Param("walletAddress") String walletAddress, @Param("amount") String amount);

    /**
     * 获取奖励流水户列表
     *
     * @param robotRewardFlowQO 奖励流水查询实体类
     * @return 奖励流水户列表
     */
    List<RobotRewardFlow> getRobotRewardFlowList(RobotRewardFlowQO robotRewardFlowQO);

    /**
     * 获取发放奖励列表
     *
     * @return 发放奖励列表
     */
    List<String> getRewardList();

    /**
     * 获取已空投发放奖励列表
     *
     * @return 发放奖励列表
     */
    List<String> getAirdropRewardList();

    /**
     * 判断用户是否加入BZ群组
     *
     * @param walletAddress 钱包地址
     * @return 用户是否加入BZ群组
     */
    Integer whetherJoinBzGroup(@Param("walletAddress") String walletAddress);

}
