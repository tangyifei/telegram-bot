package com.code.daos.robot;

import com.code.daos.base.CrudMapper;
import com.code.models.robot.ChatLink;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户分享链接持久层
 *
 * @author xiaoyaowang
 */
@Repository
public interface ChatLinkMapper extends CrudMapper<ChatLink> {

    /**
     * 根据用户链接地址和链接种类获取用户的分享链接
     *
     * @param chatId       聊天唯一id
     * @param linkCategory 分享链接种类
     * @return 用户的分享链接
     */
    List<String> getChatLinkListByChatIdAndLinkCategory(@Param("chatId") String chatId, @Param("linkCategory") Integer linkCategory);

    /**
     * 通过聊天唯一标识删除用户关注的链接记录
     *
     * @param chatId 聊天唯一标识
     */
    void deleteChatLinkByChatId(String chatId);

    Integer existsChatLinkByChatId(@Param("chatId") String chatId);

}
