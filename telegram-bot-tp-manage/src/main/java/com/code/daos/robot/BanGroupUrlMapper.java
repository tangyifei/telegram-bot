package com.code.daos.robot;

import com.code.daos.base.CrudMapper;
import com.code.models.robot.BanGroupUrl;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 禁止完成群分享任务的群地址持久层
 *
 * @author xiaoyaowang
 */
public interface BanGroupUrlMapper extends CrudMapper<BanGroupUrl> {

    /**
     * 根据群地址获取禁止完成群分享任务的群地址列表
     *
     * @param groupUrl 群地址
     * @return 禁止完成群分享任务的群地址列表
     */
    List<BanGroupUrl> getBanGroupUrlListByCondition(@Param("groupUrl") String groupUrl);

    /**
     * 判断禁止完成群分享任务的群地址是否存在
     *
     * @param groupUrl 群组地址
     * @return 禁止完成群分享任务的群地址存在的结果
     */
    Integer banGroupUrlPresent(@Param("groupUrl") String groupUrl);

}
