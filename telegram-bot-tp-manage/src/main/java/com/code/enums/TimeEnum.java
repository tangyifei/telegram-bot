package com.code.enums;

/**
 * 常用时间枚举类
 *
 * @author xiaoyaowang
 */
public enum TimeEnum {

    /**
     * 一分钟
     */
    ONE_MINUTES(60),

    /**
     * 五分钟
     */
    FIVE_MINUTES(5 * 60),

    /**
     * 三十分钟
     */
    THIRTY_MINUTES(30 * 60),

    /**
     * 一小时
     */
    ONE_HOUR(60 * 60),

    /**
     * 一天
     */
    ONE_DAY(60 * 60 * 24),

    /**
     * 一周
     */
    ONE_WEEK(60 * 60 * 24 * 7),

    /**
     * 一个月
     */
    ONE_MONTH(60 * 60 * 24 * 30),

    /**
     * 一年
     */
    ONE_YEAR(60 * 60 * 24 * 30 * 12),

    /**
     * 二十年
     */
    TWENTY_YEAR(60 * 60 * 24 * 30 * 12 * 20);

    private final Integer sec;

    TimeEnum(Integer sec) {
        this.sec = sec;
    }

    public Integer sec() {
        return this.sec;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
