package com.code.enums;

/**
 * 语言枚举类
 *
 * @author xiaoyaowang
 */
public enum LanguageEnum {

    /**
     * 中文
     */
    cn,

    /**
     * 英文
     */
    en,

    /**
     * 韩文
     */
    ko,

    /**
     * 日文
     **/
    ja,

    /**
     * 俄文
     **/
    ru,

    /**
     * 中文繁体
     */
    hk;

    /**
     * 校验枚举的有效性
     *
     * @param name 待校验的枚举值
     * @return 校验是否成功
     */
    public static boolean isValid(String name) {
        for (LanguageEnum language : LanguageEnum.values()) {
            if (language.name().equals(name)) {
                return true;
            }
        }
        return false;
    }
}
