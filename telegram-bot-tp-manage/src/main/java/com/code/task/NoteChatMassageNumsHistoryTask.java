package com.code.task;

import com.code.models.robot.ChatMessageNums;
import com.code.services.ChatMessageNumsService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 统计用户群发历史消息数的任务
 */
@Component
public class NoteChatMassageNumsHistoryTask {

    @Resource
    private ChatMessageNumsService chatMessageNumsService;

    @Async
    public void noteChatMassageNumsHistory(List<ChatMessageNums> chatMessageNumsList) {
        chatMessageNumsList.forEach(chatMessageNums -> {
            chatMessageNumsService.incrementChatMessageNumsHistory(chatMessageNums);
        });
    }

}
