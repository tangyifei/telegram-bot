package com.code.helpers;

import com.code.utils.RequestContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * 国际化辅助类
 *
 * @author xiaoyaowang
 */
@Slf4j
public class I18nHelper {

    /**
     * 获取消息
     *
     * @param code key
     * @return value
     */
    public static String getMessage(String code) {
        try {
            return RequestContextUtil.getApplicationContext().getMessage(code, null, LocaleContextHolder.getLocale());
        } catch (NoSuchMessageException e) {
            log.error("获取国际化资源失败，异常信息： {}，code：{}，cause by {}", e.getMessage(), code, e);
        }
        return null;
    }
}
