package com.code.configs.db;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 读取druid数据源属性
 *
 * @author xiaoyaowang
 */
@ConfigurationProperties(prefix = "druid")
@Getter
@Setter
public class DruidProperties {
    private String url;
    private String username;
    private String password;
    private String driverClass;

    private int maxActive;
    private int minIdle;
    private int initialSize;
    private boolean testOnBorrow;
}
