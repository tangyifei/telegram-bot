package com.code.configs.bean;

import com.code.configs.redis.RedisConfig;
import com.code.models.user.LoginTokenBO;
import com.code.services.LoginTokenService;
import com.code.services.impl.LoginTokenCacheServiceImpl;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;

import javax.annotation.Resource;

/**
 * token相关的配置类
 *
 * @author xiaoyaowang
 * @since 2019-5-24 13:55:53
 */
@Configuration
@ConditionalOnClass(value = {LoginTokenCacheServiceImpl.class, RedisTemplate.class})
@AutoConfigureAfter({RedisConfig.class})
public class TokenBeanConfig {

    @Resource
    private RedisConnectionFactory redisConnectionFactory;

    /**
     * 注册配置登录token缓存相关的bean
     * 主要执行登录token相关的缓存操作
     *
     * @param loginTokenRedisTemplate 登录token redis缓存模板
     * @return 用户登录TOKEN服务
     */
    @Bean
    @ConditionalOnMissingBean(name = "loginTokenService")
    public LoginTokenService loginTokenService(RedisTemplate<String, LoginTokenBO> loginTokenRedisTemplate) {
        return new LoginTokenCacheServiceImpl(loginTokenRedisTemplate, "background:login_tokens:");
    }

    /**
     * 注册登录token相关的redis模板相关的bean
     *
     * @return 登录token相关的redis模板
     */
    @Bean
    public RedisTemplate<String, LoginTokenBO> loginTokenRedisTemplate() {
        RedisTemplate<String, LoginTokenBO> tokenRedisTemplate = new RedisTemplate<>();
        tokenRedisTemplate.setKeySerializer(new GenericToStringSerializer<>(String.class));
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        Jackson2JsonRedisSerializer<LoginTokenBO> serializer = new Jackson2JsonRedisSerializer<>(LoginTokenBO.class);
        serializer.setObjectMapper(mapper);
        tokenRedisTemplate.setValueSerializer(serializer);
        tokenRedisTemplate.setConnectionFactory(redisConnectionFactory);
        return tokenRedisTemplate;
    }
}
