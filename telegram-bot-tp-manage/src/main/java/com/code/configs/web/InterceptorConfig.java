package com.code.configs.web;

import com.code.interceptors.HeaderParamsCheckInterceptor;
import com.code.interceptors.InternationalInterceptor;
import com.code.interceptors.LoginAuthInterceptor;
import com.code.interceptors.ResponseResultInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 拦截器配置
 *
 * @author xiaoyaowang
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    /**
     * 注册接口响应体控制拦截器相关的bean
     *
     * @return 接口响应体控制拦截器对象
     */
    @Bean
    public ResponseResultInterceptor responseResultInterceptor() {
        return new ResponseResultInterceptor();
    }

    /**
     * 注册校验请求头中相关参数完整性和有效性拦截器相关的bean
     *
     * @return 校验请求头中相关参数完整性和有效性拦截器对象
     */
    @Bean
    public HeaderParamsCheckInterceptor headerParamsCheckInterceptor() {
        return new HeaderParamsCheckInterceptor();
    }

    /**
     * 注册校验用户是否登录的拦截器相关的bean
     *
     * @return 拦截用户是否登录的拦截器
     */
    @Bean
    public LoginAuthInterceptor loginAuthInterceptor() {
        return new LoginAuthInterceptor();
    }

    /**
     * 国际化拦截器相关的bean
     *
     * @return 国际化拦截器对象
     */
    @Bean
    public InternationalInterceptor internationalInterceptor() {
        return new InternationalInterceptor();
    }

    /**
     * 注册拦截器，可以调整多个registry.addInterceptor方法的代码顺序来控制拦截器的调用顺序
     *
     * @param registry 拦截器注册器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //响应结果控制拦截
        registry.addInterceptor(responseResultInterceptor());
        //请求头参数校验，不拦截swagger
        registry.addInterceptor(headerParamsCheckInterceptor()).excludePathPatterns("/v2/**", "/swagger-resources/**", "/webjars/**", "/swagger-ui.html/**");
        //登录拦截
        registry.addInterceptor(loginAuthInterceptor());
        //国际化拦截
        registry.addInterceptor(internationalInterceptor());
    }

    /**
     * 发现如果继承了WebMvcConfigurationSupport，则在yml中配置的相关内容会失效。 需要重新指定静态资源
     *
     * @param registry 注册器
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations(
                "classpath:/static/");
        registry.addResourceHandler("swagger-ui.html").addResourceLocations(
                "classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations(
                "classpath:/META-INF/resources/webjars/");
        WebMvcConfigurer.super.addResourceHandlers(registry);
    }

}
