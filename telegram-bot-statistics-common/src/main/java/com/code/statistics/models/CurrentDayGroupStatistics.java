package com.code.statistics.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * 群组的当日的统计信息实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("群组的当日的统计信息实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrentDayGroupStatistics extends BaseRobot {

    private static final long serialVersionUID = 5978061216556054024L;

    @ApiModelProperty(value = "关联的群组主键", example = "1")
    @NotNull(groups = {Save.class}, message = "groupId is null!")
    private Integer groupId;

    @ApiModelProperty(value = "乐观锁版本号", example = "0")
    private Integer version;

    @ApiModelProperty(value = "群组名称", example = "1")
    @Transient
    private String groupName;

    @ApiModelProperty(value = "群组链接地址", example = "1")
    @Transient
    private String groupUrl;

    @ApiModelProperty(value = "是否添加 1-添加", example = "1")
    @Transient
    private Integer isIncrease;

    @ApiModelProperty(value = "当日留言数", example = "1")
    @NotNull(groups = {Save.class}, message = "currentDayMessages is null!")
    private Integer currentDayMessages;

    @ApiModelProperty(value = "当日新增的成员数", example = "1")
    @NotNull(groups = {Save.class}, message = "currentDayNewMembers is null!")
    private Integer currentDayNewMembers;

    @ApiModelProperty(value = "当日成员数量(零点)", example = "1")
    @NotNull(groups = {Save.class}, message = "currentDayMembers is null!")
    private Integer currentDayMembers;

    @ApiModelProperty(value = "当日日期", example = "2021-12-07")
    @NotNull(groups = {Save.class}, message = "currentDate is null!")
    private String currentDayDate;

}
