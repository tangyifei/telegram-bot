package com.code.statistics.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 敏感词汇实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("敏感词汇实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SensitiveWord extends BaseRobot {

    private static final long serialVersionUID = -5673466117569284225L;

    @ApiModelProperty(value = "敏感词汇", example = "1")
    @NotBlank(groups = {Save.class}, message = "sensitiveWord is null!")
    private String sensitiveWord;

    @ApiModelProperty(value = "0-禁用 1-启用", example = "1")
    @NotNull(groups = {Save.class}, message = "isEnable is null!")
    private Integer isEnable;

}
