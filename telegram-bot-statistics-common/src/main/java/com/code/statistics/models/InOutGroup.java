package com.code.statistics.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 当日入群与退群的统计实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("当日入群与退群的统计实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InOutGroup extends BaseRobot {

    private static final long serialVersionUID = -7783587329313062882L;

    @ApiModelProperty(value = "群组链接地址", example = "1")
    @NotBlank(groups = {Save.class}, message = "groupUrl is null!")
    private String groupUrl;

    @ApiModelProperty(value = "退群或者是入群人数", example = "1")
    @NotNull(groups = {Save.class}, message = "members is null!")
    private Integer members;

    @ApiModelProperty(value = "入群与退群类型 1-入群 2-退群", example = "1")
    @NotNull(groups = {Save.class}, message = "inOutType is null!")
    private Integer inOutType;

    @ApiModelProperty(value = "当天的日期", example = "1")
    @NotBlank(groups = {Save.class}, message = "currentDay is null!")
    private String currentDay;

}
