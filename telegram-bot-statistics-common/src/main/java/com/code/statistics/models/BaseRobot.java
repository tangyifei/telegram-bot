package com.code.statistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 群组统计公用字段
 *
 * @author xiaoyaowang
 */
@ApiModel("群组统计公用字段")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseRobot implements Serializable {

    private static final long serialVersionUID = 926166612390764954L;

    @ApiModelProperty(value = "主键 添加时非必填 更新或者删除时必填", example = "1")
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    @NotNull(groups = {Update.class})
    private Integer id;

    @ApiModelProperty(value = "删除状态 0-未删除 1-已删除 非必填", example = "1")
    private Integer deleted;

    @ApiModelProperty(value = "创建时间 非必填", example = "2021-12-07 14:50:54")
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "Asia/Shanghai"
    )
    private Date createdAt;

    @ApiModelProperty(value = "更新时间  非必填", example = "2021-12-07 14:50:54")
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "Asia/Shanghai"
    )
    private Date updatedAt;

}
