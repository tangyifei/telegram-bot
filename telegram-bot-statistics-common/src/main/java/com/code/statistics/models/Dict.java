package com.code.statistics.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 字典实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("字典实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Dict extends BaseRobot {

    private static final long serialVersionUID = -5673466117569284225L;

    @ApiModelProperty(value = "字典key", example = "1")
    private String dictKey;

    @ApiModelProperty(value = "字典值", example = "1")
    private String dictValue;

    @ApiModelProperty(value = "备注", example = "1")
    private String remark;

}
