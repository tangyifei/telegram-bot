package com.code.statistics.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 统计相关的群组实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("统计相关的群组实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatisticsGroup extends BaseRobot {

    private static final long serialVersionUID = 3125167044798160223L;

    @ApiModelProperty(value = "群组名称", example = "1")
    @NotBlank(groups = {Save.class}, message = "groupName is null!")
    private String groupName;

    @ApiModelProperty(value = "群组链接地址", example = "1")
    @NotBlank(groups = {Save.class}, message = "groupUrl is null!")
    private String groupUrl;

    @ApiModelProperty(value = "群组聊天唯一标识", example = "1")
    @NotNull(groups = {Update.class}, message = "groupChatId is null!")
    private Long groupChatId;

    @ApiModelProperty(value = "是否启用 0-未启用 1-启用", example = "1")
    @NotNull(groups = {Save.class}, message = "isEnable is null!")
    private Integer isEnable;

}
