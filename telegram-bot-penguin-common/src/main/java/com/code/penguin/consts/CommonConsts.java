package com.code.penguin.consts;

/**
 * 共享的常量
 *
 * @author xiaoyaowang
 */
public interface CommonConsts {

    String TRANSACTION_MANAGER = "testTransactionManager";
}
