package com.code.penguin.utils;

import org.apache.commons.lang.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 验证工具类
 *
 * @author xiaoyaowang
 */
public class ValidatorUtil {

    private ValidatorUtil() {
    }

    /**
     * xwc地址正则表达式
     */
    private static final String XWC_FORMAT = "^[a-z0-9A-Z]+$";

    /**
     * xwc地址匹配模式
     */
    private static final Pattern XWC_PATTERN = Pattern.compile(XWC_FORMAT);

    /**
     * 邮箱格式正则表达式
     */
    private static final String EMAIL_FORMAT = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

    /**
     * 邮箱匹配模式
     */
    private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_FORMAT);

    /**
     * 推特链接地址正则表达式
     */
    private static final String TWITTER_LINK_FORMAT = "https://twitter.com/[\\w]+/status/\\d+";

    /**
     * 推特链接匹配模式
     */
    private static final Pattern TWITTER_LINK_PATTERN = Pattern.compile(TWITTER_LINK_FORMAT);

    /**
     * 判断是否为XWC地址
     *
     * @param walletAddress xwc地址
     * @return xwc地址验证与否
     */
    public static boolean isXwc(String walletAddress) {
        if (StringUtils.isEmpty(walletAddress)
                || !walletAddress.startsWith("XWC")
                || walletAddress.length() != 37) {
            return false;
        }
        Matcher m = XWC_PATTERN.matcher(walletAddress);
        return m.matches();
    }

    /**
     * 判断是否为邮箱
     *
     * @param email 邮箱
     * @return 邮箱验证与否
     */
    public static boolean isEmail(String email) {
        if (StringUtils.isEmpty(email)) {
            return false;
        }
        Matcher m = EMAIL_PATTERN.matcher(email);
        return m.matches();
    }

    /**
     * 判断是否为推特链接地址
     *
     * @param twitterLinkUrl 推特链接地址
     * @return 是否为推特链接地址
     */
    public static boolean isTwitterLinkUrl(String twitterLinkUrl) {
        if (StringUtils.isBlank(twitterLinkUrl)) {
            return false;
        }
        Matcher m = TWITTER_LINK_PATTERN.matcher(twitterLinkUrl);
        return m.matches();
    }

    //public static void main(String[] args) {
    //    System.out.println(isEmail("hshij@163.com"));
    //System.out.println(isTwitterLinkUrl("https://twitter.com/Lumini081/status/1422965935787364352?s=20"));
    //String text = "https://twitter.com/hshj77431327/status/1422817004105334789";
    //System.out.println(text.substring(text.indexOf("twitter.com/") + 12, text.indexOf("/status")));
//}
}
