package com.code.penguin.models;

/**
 * 用户推特账户实体类
 *
 * @author xiaoyaowang
 */
public class ChatTwitter extends BaseRobot {

    private static final long serialVersionUID = 2704989963867526852L;

    private String chatId;

    private String twitterAccountName;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getTwitterAccountName() {
        return twitterAccountName;
    }

    public void setTwitterAccountName(String twitterAccountName) {
        this.twitterAccountName = twitterAccountName;
    }

    @Override
    public String toString() {
        return "ChatTwitter{" +
                "chatId='" + chatId + '\'' +
                ", twitterAccountName='" + twitterAccountName + '\'' +
                "} " + super.toString();
    }
}
