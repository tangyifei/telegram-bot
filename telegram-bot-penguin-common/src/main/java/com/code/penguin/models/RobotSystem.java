package com.code.penguin.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 系统key-value实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("系统key-value实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RobotSystem extends BaseRobot {

    private static final long serialVersionUID = 4867489592815835899L;

    @ApiModelProperty(value = "系统key", example = "1")
    private String systemKey;

    @ApiModelProperty(value = "系统值", example = "1")
    private String systemValue;

    @ApiModelProperty(value = "备注", example = "1")
    private String remark;

}
