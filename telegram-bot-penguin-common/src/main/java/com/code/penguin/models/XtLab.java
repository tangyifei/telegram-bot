package com.code.penguin.models;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * xtLab实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("xtLab实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class XtLab extends BaseRobot {

    private static final long serialVersionUID = 4527176331903729103L;

    @NotBlank(message = "xtLabsName can not null")
    @Length(max = 100, message = "xtLabsName is between 0 and 100")
    private String xtLabsName;

    @NotBlank(message = "xtLabsTelegram can not null")
    @Length(max = 100, message = "xtLabsTelegram is between 0 and 100")
    private String xtLabsTelegram;

    @NotBlank(message = "xtLabsEmail can not null")
    @Length(max = 100, message = "xtLabsEmail is between 0 and 100")
    private String xtLabsEmail;

    private String xtLabsProjectUrl;

    private String xtLabsDesc;

    private String xtLabsDevelopmentStage;

    private String xtLabsValuation;

    private String xtLabsReferral;

}
