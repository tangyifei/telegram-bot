package com.code.penguin.models;

import javax.persistence.Transient;

/**
 * 用户已完成的任务实体类
 *
 * @author xiaoyaowang
 */
public class RobotChatFinishTask extends BaseRobot {

    private static final long serialVersionUID = -5267828055301972686L;

    private String chatId;

    private Integer taskId;

    @Transient
    private String coinAddr;

    @Transient
    private String email;

    @Transient
    private String discord;

    @Transient
    private String collectContent;

    @Transient
    private String twitterAccountName;

    @Transient
    private Integer insertRobotTaskCheckFlag;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getCoinAddr() {
        return coinAddr;
    }

    public void setCoinAddr(String coinAddr) {
        this.coinAddr = coinAddr;
    }

    public void setTwitterAccountName(String twitterAccountName) {
        this.twitterAccountName = twitterAccountName;
    }

    public String getTwitterAccountName() {
        return twitterAccountName;
    }

    public Integer getInsertRobotTaskCheckFlag() {
        return insertRobotTaskCheckFlag;
    }

    public void setInsertRobotTaskCheckFlag(Integer insertRobotTaskCheckFlag) {
        this.insertRobotTaskCheckFlag = insertRobotTaskCheckFlag;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDiscord() {
        return discord;
    }

    public void setDiscord(String discord) {
        this.discord = discord;
    }

    public String getCollectContent() {
        return collectContent;
    }

    public void setCollectContent(String collectContent) {
        this.collectContent = collectContent;
    }

    @Override
    public String toString() {
        return "RobotChatFinishTask{" +
                "chatId='" + chatId + '\'' +
                ", taskId=" + taskId +
                ", coinAddr='" + coinAddr + '\'' +
                ", email='" + email + '\'' +
                ", discord='" + discord + '\'' +
                ", collectContent='" + collectContent + '\'' +
                ", twitterAccountName='" + twitterAccountName + '\'' +
                ", insertRobotTaskCheckFlag=" + insertRobotTaskCheckFlag +
                "} " + super.toString();
    }
}
