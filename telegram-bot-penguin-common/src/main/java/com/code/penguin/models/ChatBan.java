package com.code.penguin.models;

/**
 * 封禁用户实体类
 *
 * @author xiaoyaowang
 */
public class ChatBan extends BaseRobot {

    private static final long serialVersionUID = 5651024005683503625L;

    private String chatId;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    @Override
    public String toString() {
        return "ChatBan{" +
                "chatId='" + chatId + '\'' +
                "} " + super.toString();
    }

}
