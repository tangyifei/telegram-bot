package com.code.penguin.models;

import com.code.penguin.validators.IsEmail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * 邮箱记录
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("邮箱记录")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatEmail extends BaseRobot {

    private static final long serialVersionUID = -3923130684855069587L;

    @ApiModelProperty(value = "聊天唯一标识 非必填 默认值为0", example = "0")
    private String chatId;

    @ApiModelProperty(value = "邮箱地址 必填", example = "123@163.com")
    @NotBlank(message = "邮箱地址不能为空")
    @IsEmail
    private String email;

}
