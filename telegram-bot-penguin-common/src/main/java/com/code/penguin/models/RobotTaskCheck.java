package com.code.penguin.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * 机器人任务审核记录实体类
 *
 * @author xiaoyaowang
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("机器人任务审核记录实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RobotTaskCheck extends BaseRobot {

    private static final long serialVersionUID = 3522992676325318444L;

    @ApiModelProperty(value = "聊天标识", example = "1")
    private String chatId;

    @ApiModelProperty(value = "telegram账户", example = "limingcheng")
    private String userName;

    @ApiModelProperty(value = "名", example = "mingcheng")
    private String firstName;

    @ApiModelProperty(value = "姓", example = "li")
    private String lastName;

    @ApiModelProperty(value = "钱包地址", example = "XWCWEWRTTTGGFFEVEGRBRBRBRH131VEE")
    @Transient
    private String coinAddr;

    @ApiModelProperty(value = "邮箱", example = "lmc@163.com")
    @Transient
    private String email;

    @ApiModelProperty(value = "discord账号", example = "lmc#1234")
    @Transient
    private String discord;

    @ApiModelProperty(value = "收集信息-留言信息", example = "BTC")
    @Transient
    private String collectContent;

    @ApiModelProperty(value = "推特账户名", example = "")
    @Transient
    private String twitterAccountName;

    @ApiModelProperty(value = "TWITTER分享链接", example = "")
    @Transient
    private String twitterShareLink;

    @ApiModelProperty(value = "审核状态 0-未审核 1-审核失败 2-审核成功 3-已封禁", example = "0")
    @NotNull(groups = {Update.class})
    private Integer state;

}
