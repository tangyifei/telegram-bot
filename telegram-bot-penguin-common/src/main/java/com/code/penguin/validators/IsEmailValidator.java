package com.code.penguin.validators;

import com.code.penguin.utils.ValidatorUtil;
import org.apache.commons.lang.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 验证邮箱格式
 *
 * @author xiaoyaowang
 */
public class IsEmailValidator implements ConstraintValidator<IsEmail, String> {

    private boolean required = false;

    @Override
    public void initialize(IsEmail constraintAnnotation) {
        required = constraintAnnotation.required();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (required) {
            return ValidatorUtil.isEmail(value);
        } else {
            if (StringUtils.isEmpty(value)) {
                return true;
            } else {
                return ValidatorUtil.isEmail(value);
            }
        }
    }

}
